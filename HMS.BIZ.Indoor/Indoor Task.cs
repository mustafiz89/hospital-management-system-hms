﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DynamicSoft.DAL;
using HMS.Model.Indoor;
using System.Data.SqlClient;
using System.Data;
using HMS_Helper.Model.Indoor;
using HMS.Model.Inddor;
namespace HMS.BIZ.Indoor
{
   public class Indoor_Task
    {
       public static int saveREGISTRATION_ADMISSION(REGISTRATION_ADMISSION objREGISTRATION_ADMISSION)
       {
           int count = 0;
           var con = ConnectionClass.GetConnection();
           var objSqlCommand = new SqlCommand();

           objSqlCommand.CommandText = "FSP_PATIENT_REGISTRATION_I";
           objSqlCommand.CommandType = CommandType.StoredProcedure;
           objSqlCommand.Connection = con;
           objSqlCommand.Parameters.Add(new SqlParameter("@PATIENT_REGISTRATION_NO", SqlDbType.Int));
           objSqlCommand.Parameters["@PATIENT_REGISTRATION_NO"].Value = objREGISTRATION_ADMISSION.PATIENT_REGISTRATION_NO;

           objSqlCommand.Parameters.Add(new SqlParameter("@ADMISSION_NO", SqlDbType.VarChar, 50));
           objSqlCommand.Parameters["@ADMISSION_NO"].Value = objREGISTRATION_ADMISSION.ADMISSION_NO;

           objSqlCommand.Parameters.Add(new SqlParameter("@START_DATE", SqlDbType.DateTime));
           objSqlCommand.Parameters["@START_DATE"].Value = objREGISTRATION_ADMISSION.START_DATE;

           objSqlCommand.Parameters.Add(new SqlParameter("@SALUTION_ID", SqlDbType.UniqueIdentifier));
           objSqlCommand.Parameters["@SALUTION_ID"].Value = objREGISTRATION_ADMISSION.SALUTION_ID;

           objSqlCommand.Parameters.Add(new SqlParameter("@PATIENT_NAME", SqlDbType.VarChar, 50));
           objSqlCommand.Parameters["@PATIENT_NAME"].Value = objREGISTRATION_ADMISSION.PATIENT_NAME;

           objSqlCommand.Parameters.Add(new SqlParameter("@PATIENT_DOB", SqlDbType.VarChar, 50));
           objSqlCommand.Parameters["@PATIENT_DOB"].Value = objREGISTRATION_ADMISSION.PATIENT_DOB;

           objSqlCommand.Parameters.Add(new SqlParameter("@PATIENT_AGE", SqlDbType.Int));
           objSqlCommand.Parameters["@PATIENT_AGE"].Value = objREGISTRATION_ADMISSION.PATIENT_AGE;

           objSqlCommand.Parameters.Add(new SqlParameter("@GENDER_ID", SqlDbType.UniqueIdentifier));
           objSqlCommand.Parameters["@GENDER_ID"].Value = objREGISTRATION_ADMISSION.GENDER_ID;

           objSqlCommand.Parameters.Add(new SqlParameter("@MARITAL_STATUS_ID", SqlDbType.UniqueIdentifier));
           objSqlCommand.Parameters["@MARITAL_STATUS_ID"].Value = objREGISTRATION_ADMISSION.MARITAL_STATUS_ID;

           objSqlCommand.Parameters.Add(new SqlParameter("@BLOOD_GROUP_ID", SqlDbType.UniqueIdentifier));
           objSqlCommand.Parameters["@BLOOD_GROUP_ID"].Value = objREGISTRATION_ADMISSION.BLOOD_GROUP_ID;

           objSqlCommand.Parameters.Add(new SqlParameter("@PATIENT_PHONE", SqlDbType.VarChar, 50));
           objSqlCommand.Parameters["@PATIENT_PHONE"].Value = objREGISTRATION_ADMISSION.PATIENT_PHONE;

           objSqlCommand.Parameters.Add(new SqlParameter("@PATIENT_PRESENT_ADDRESS", SqlDbType.VarChar, 50));
           objSqlCommand.Parameters["@PATIENT_PRESENT_ADDRESS"].Value = objREGISTRATION_ADMISSION.PATIENT_PRESENT_ADDRESS;

           objSqlCommand.Parameters.Add(new SqlParameter("@RELIGION_ID", SqlDbType.UniqueIdentifier));
           objSqlCommand.Parameters["@RELIGION_ID"].Value = objREGISTRATION_ADMISSION.RELIGION_ID;

           objSqlCommand.Parameters.Add(new SqlParameter("@CITY_ID", SqlDbType.UniqueIdentifier));
           objSqlCommand.Parameters["@CITY_ID"].Value = objREGISTRATION_ADMISSION.CITY_ID;

           objSqlCommand.Parameters.Add(new SqlParameter("@NATIONALITY_ID", SqlDbType.UniqueIdentifier));
           objSqlCommand.Parameters["@NATIONALITY_ID"].Value = objREGISTRATION_ADMISSION.NATIONALITY_ID;

           objSqlCommand.Parameters.Add(new SqlParameter("@OCCUPATION_ID", SqlDbType.UniqueIdentifier));
           objSqlCommand.Parameters["@OCCUPATION_ID"].Value = objREGISTRATION_ADMISSION.OCCUPATION_ID;

           objSqlCommand.Parameters.Add(new SqlParameter("@PATIENT_FATHER_NAME", SqlDbType.VarChar, 50));
           objSqlCommand.Parameters["@PATIENT_FATHER_NAME"].Value = objREGISTRATION_ADMISSION.PATIENT_FATHER_NAME;

           objSqlCommand.Parameters.Add(new SqlParameter("@PATIENT_MOTHER_NAME", SqlDbType.VarChar, 50));
           objSqlCommand.Parameters["@PATIENT_MOTHER_NAME"].Value = objREGISTRATION_ADMISSION.PATIENT_MOTHER_NAME;

           objSqlCommand.Parameters.Add(new SqlParameter("@RELATION_GURDIAN", SqlDbType.VarChar, 50));
           objSqlCommand.Parameters["@RELATION_GURDIAN"].Value = objREGISTRATION_ADMISSION.RELATION_GURDIAN;

           objSqlCommand.Parameters.Add(new SqlParameter("@GURDIAN_NAME", SqlDbType.VarChar, 50));
           objSqlCommand.Parameters["@GURDIAN_NAME"].Value = objREGISTRATION_ADMISSION.GURDIAN_NAME;

           objSqlCommand.Parameters.Add(new SqlParameter("@GURDIAN_PHONE", SqlDbType.VarChar, 50));
           objSqlCommand.Parameters["@GURDIAN_PHONE"].Value = objREGISTRATION_ADMISSION.GURDIAN_PHONE;

           objSqlCommand.Parameters.Add(new SqlParameter("@ADMITTED_UNDER_ID", SqlDbType.UniqueIdentifier));
           objSqlCommand.Parameters["@ADMITTED_UNDER_ID"].Value = objREGISTRATION_ADMISSION.ADMITTED_UNDER_ID;

           objSqlCommand.Parameters.Add(new SqlParameter("@ADMITTED_DEPARTMENT_ID", SqlDbType.UniqueIdentifier));
           objSqlCommand.Parameters["@ADMITTED_DEPARTMENT_ID"].Value = objREGISTRATION_ADMISSION.ADMITTED_DEPARTMENT_ID;

           objSqlCommand.Parameters.Add(new SqlParameter("@PATIENT_DISEASE_ID", SqlDbType.UniqueIdentifier));
           objSqlCommand.Parameters["@PATIENT_DISEASE_ID"].Value = objREGISTRATION_ADMISSION.PATIENT_DISEASE_ID;

           objSqlCommand.Parameters.Add(new SqlParameter("@REFFERENCE_ID", SqlDbType.UniqueIdentifier));
           objSqlCommand.Parameters["@REFFERENCE_ID"].Value = objREGISTRATION_ADMISSION.REFFERENCE_ID;

           objSqlCommand.Parameters.Add(new SqlParameter("@REFFERENCE_ADDRESS", SqlDbType.NVarChar,50));
           objSqlCommand.Parameters["@REFFERENCE_ADDRESS"].Value = objREGISTRATION_ADMISSION.REFERENCE_ADDRESS;

           objSqlCommand.Parameters.Add(new SqlParameter("@COLLECTION_TYPE", SqlDbType.Char,10));
           objSqlCommand.Parameters["@COLLECTION_TYPE"].Value = objREGISTRATION_ADMISSION.COLLECTION_TYPE;

           objSqlCommand.Parameters.Add(new SqlParameter("@COLLECTED_AMOUNT", SqlDbType.Decimal));
           objSqlCommand.Parameters["@COLLECTED_AMOUNT"].Value = objREGISTRATION_ADMISSION.COLLECTED_AMOUNT;

           objSqlCommand.Parameters.Add(new SqlParameter("@ADDMISSION_CHARGE", SqlDbType.Decimal));
           objSqlCommand.Parameters["@ADDMISSION_CHARGE"].Value = objREGISTRATION_ADMISSION.ADDMISSION_CHARGE;           

           objSqlCommand.Parameters.Add(new SqlParameter("@BED_ID", SqlDbType.UniqueIdentifier));
           objSqlCommand.Parameters["@BED_ID"].Value = objREGISTRATION_ADMISSION.BED_ID;

           
           con.Open();
           try
           {
               count = objSqlCommand.ExecuteNonQuery();
           }
           catch//(Exception ex) 
           { }
           finally
           {
               con.Close();
           }
           return count;
       }

       public static int SaveBabyBorninformation(BABY_BORN_INFORMATION objBabyBornInformation)
       {
           int count = 0;
           var con = ConnectionClass.GetConnection();
           var objSqlCommand = new SqlCommand();

           objSqlCommand.CommandText = "FSP_BABY_BORN_INFORMATION_I";
           objSqlCommand.CommandType = CommandType.StoredProcedure;
           objSqlCommand.Connection = con;

           objSqlCommand.Parameters.Add(new SqlParameter("@PATIENT_ADDMISSION_NO", SqlDbType.NVarChar,30));
           objSqlCommand.Parameters["@PATIENT_ADDMISSION_NO"].Value = objBabyBornInformation.PATIENT_ADDMISSION_NO;

           objSqlCommand.Parameters.Add(new SqlParameter("@PATIENT_NAME", SqlDbType.NVarChar, 30));
           objSqlCommand.Parameters["@PATIENT_NAME"].Value = objBabyBornInformation.PATIENT_NAME;

           objSqlCommand.Parameters.Add(new SqlParameter("@BABY_NAME", SqlDbType.NVarChar, 30));
           objSqlCommand.Parameters["@BABY_NAME"].Value = objBabyBornInformation.BABY_NAME;

           objSqlCommand.Parameters.Add(new SqlParameter("@BABY_FATHER_NAME", SqlDbType.NVarChar, 30));
           objSqlCommand.Parameters["@BABY_FATHER_NAME"].Value = objBabyBornInformation.BABY_FATHER_NAME;

           objSqlCommand.Parameters.Add(new SqlParameter("@BABY_MOTHER_NAME", SqlDbType.NVarChar, 30));
           objSqlCommand.Parameters["@BABY_MOTHER_NAME"].Value = objBabyBornInformation.BABY_MOTHER_NAME;

           objSqlCommand.Parameters.Add(new SqlParameter("@PRESENT_ADDRESS", SqlDbType.NVarChar, 30));
           objSqlCommand.Parameters["@PRESENT_ADDRESS"].Value = objBabyBornInformation.PRESENT_ADDRESS;

           objSqlCommand.Parameters.Add(new SqlParameter("@PERMANENT_ADDRESS", SqlDbType.NVarChar, 30));
           objSqlCommand.Parameters["@PERMANENT_ADDRESS"].Value = objBabyBornInformation.PERMANENT_ADDRESS;

           objSqlCommand.Parameters.Add(new SqlParameter("@BORN_DATE", SqlDbType.DateTime));
           objSqlCommand.Parameters["@BORN_DATE"].Value = objBabyBornInformation.BORN_DATE;

           objSqlCommand.Parameters.Add(new SqlParameter("@NATIONALITY_ID", SqlDbType.UniqueIdentifier));
           objSqlCommand.Parameters["@NATIONALITY_ID"].Value = objBabyBornInformation.NATIONALITY_ID;

           objSqlCommand.Parameters.Add(new SqlParameter("@GENDER_ID", SqlDbType.UniqueIdentifier));
           objSqlCommand.Parameters["@GENDER_ID"].Value = objBabyBornInformation.GENDER_ID;

           objSqlCommand.Parameters.Add(new SqlParameter("@BLOOD_GROUP_ID", SqlDbType.UniqueIdentifier));
           objSqlCommand.Parameters["@BLOOD_GROUP_ID"].Value = objBabyBornInformation.BLOOD_GROUP_ID;

           objSqlCommand.Parameters.Add(new SqlParameter("@RELIGION_ID", SqlDbType.UniqueIdentifier));
           objSqlCommand.Parameters["@RELIGION_ID"].Value = objBabyBornInformation.BLOOD_GROUP_ID;

           con.Open();
           try
           {
               count = objSqlCommand.ExecuteNonQuery();
           }
           catch//(Exception ex) 
           { }
           finally
           {
               con.Close();
           }
           return count;
       }

       public static int SaveIndoorOTService(INDOOR_OT_SERVICE objIndoorOTService)
       {
           int count = 0;
           var con = ConnectionClass.GetConnection();
           var objSqlCommand = new SqlCommand();

           objSqlCommand.CommandText = "FSP_INDOOR_OT_SERVICE_I";
           objSqlCommand.CommandType = CommandType.StoredProcedure;
           objSqlCommand.Connection = con;

           objSqlCommand.Parameters.Add(new SqlParameter("@INVOICE_NO", SqlDbType.NVarChar, 30));
           objSqlCommand.Parameters["@INVOICE_NO"].Value = objIndoorOTService.INVOICE_NO;

           objSqlCommand.Parameters.Add(new SqlParameter("@PATIENT_REGISTRATION_NO", SqlDbType.NVarChar, 30));
           objSqlCommand.Parameters["@PATIENT_REGISTRATION_NO"].Value = objIndoorOTService.PATIENT_RESISTRATION_NO;

           objSqlCommand.Parameters.Add(new SqlParameter("@OT_COMMENTS_ID", SqlDbType.UniqueIdentifier));
           objSqlCommand.Parameters["@OT_COMMENTS_ID"].Value = objIndoorOTService.OT_COMMENTS_ID;

           objSqlCommand.Parameters.Add(new SqlParameter("@COLLECTION_MODE", SqlDbType.NVarChar, 30));
           objSqlCommand.Parameters["@COLLECTION_MODE"].Value = objIndoorOTService.COLLECTION_MODE;

           objSqlCommand.Parameters.Add(new SqlParameter("@TOTAL_AMOUNT", SqlDbType.Decimal));
           objSqlCommand.Parameters["@TOTAL_AMOUNT"].Value = objIndoorOTService.TOTAL_AMOUNT;

           objSqlCommand.Parameters.Add(new SqlParameter("@DISCOUNT", SqlDbType.Decimal));
           objSqlCommand.Parameters["@DISCOUNT"].Value = objIndoorOTService.DISCOUNT;

           objSqlCommand.Parameters.Add(new SqlParameter("@COLLECTED_AMOUNT", SqlDbType.Decimal));
           objSqlCommand.Parameters["@COLLECTED_AMOUNT"].Value = objIndoorOTService.COLLECTED_AMOUNT;

           objSqlCommand.Parameters.Add(new SqlParameter("@BANK_ID", SqlDbType.UniqueIdentifier));
           objSqlCommand.Parameters["@BANK_ID"].Value = objIndoorOTService.COLLECTED_AMOUNT;

           objSqlCommand.Parameters.Add(new SqlParameter("@CHANGE_AMOUNT", SqlDbType.Decimal));
           objSqlCommand.Parameters["@CHANGE_AMOUNT"].Value = objIndoorOTService.CHANGE_AMOUNT;

           objSqlCommand.Parameters.Add(new SqlParameter("@DUE_AMOUNT", SqlDbType.Decimal));
           objSqlCommand.Parameters["@DUE_AMOUNT"].Value = objIndoorOTService.CHANGE_AMOUNT;

           objSqlCommand.Parameters.Add(new SqlParameter("@VAT", SqlDbType.Decimal));
           objSqlCommand.Parameters["@VAT"].Value = objIndoorOTService.VAT;

           objSqlCommand.Parameters.Add(new SqlParameter("@OT_ID", SqlDbType.UniqueIdentifier));
           objSqlCommand.Parameters["@OT_ID"].Value = objIndoorOTService.OT_ID;
         

           con.Open();
           try
           {
               count = objSqlCommand.ExecuteNonQuery();
           }
           catch//(Exception ex) 
           { }
           finally
           {
               con.Close();
           }
           return count;
       }
    }
}
