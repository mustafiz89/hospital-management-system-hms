﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IA.Model.Setup.Accounts
{
    public class ACCOUNT_GROUP
    {
        private string _ACCOUNT_GROUP_CODE;

        public string ACCOUNT_GROUP_CODE
        {
            get { return _ACCOUNT_GROUP_CODE; }
            set { _ACCOUNT_GROUP_CODE = value; }
        }
        private string _ACCOUNT_GROUP_NAME;

        public string ACCOUNT_GROUP_NAME
        {
            get { return _ACCOUNT_GROUP_NAME; }
            set { _ACCOUNT_GROUP_NAME = value; }
        }
        private string _ACCOUNT_GROUP_TYPE;

        public string ACCOUNT_GROUP_TYPE
        {
            get { return _ACCOUNT_GROUP_TYPE; }
            set { _ACCOUNT_GROUP_TYPE = value; }
        }
        private string _COMPANY_ID;

        public string COMPANY_ID
        {
            get { return _COMPANY_ID; }
            set { _COMPANY_ID = value; }
        }
    }
}
