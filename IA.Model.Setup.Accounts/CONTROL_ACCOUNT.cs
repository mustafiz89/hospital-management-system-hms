﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IA.Model.Setup.Accounts
{
    public class CONTROL_ACCOUNT
    {
        private string _CONTROL_CODE;

        public string CONTROL_CODE
        {
            get { return _CONTROL_CODE; }
            set { _CONTROL_CODE = value; }
        }
        private string _CONTROL_NAME;

        public string CONTROL_NAME
        {
            get { return _CONTROL_NAME; }
            set { _CONTROL_NAME = value; }
        }
        private string _ACCOUNT_GROUP_CODE;

        public string ACCOUNT_GROUP_CODE
        {
            get { return _ACCOUNT_GROUP_CODE; }
            set { _ACCOUNT_GROUP_CODE = value; }
        }
        private string _COMPANY_ID;

        public string COMPANY_ID
        {
            get { return _COMPANY_ID; }
            set { _COMPANY_ID = value; }
        }
        private string _NAME_VALUE_LIST;

        public string NAME_VALUE_LIST
        {
            get { return _NAME_VALUE_LIST; }
            set { _NAME_VALUE_LIST = value; }
        }
        public string ACCOUNT_GROUP_NAME { get; set; }
        public string ACCOUNT_GROUP_TYPE { get; set; }


        public string SUBSIDIARY_CODE { get; set; }

        public string CONTROL_CODE_NEW { get; set; }
    }
}
