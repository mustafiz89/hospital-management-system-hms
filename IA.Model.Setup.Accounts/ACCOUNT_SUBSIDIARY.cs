﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Accounts
{
    public class ACCOUNT_SUBSIDIARY
    {
        private string _SUBSIDIARY_CODE;

        public string SUBSIDIARY_CODE
        {
            get { return _SUBSIDIARY_CODE; }
            set { _SUBSIDIARY_CODE = value; }
        }
        private string _ACCOUNT_GROUP_CODE;

        public string ACCOUNT_GROUP_CODE
        {
            get { return _ACCOUNT_GROUP_CODE; }
            set { _ACCOUNT_GROUP_CODE = value; }
        }
        private string _CONTROL_CODE;

        public string CONTROL_CODE
        {
            get { return _CONTROL_CODE; }
            set { _CONTROL_CODE = value; }
        }
        private string _SUBSIDIARY_NAME;

        public string SUBSIDIARY_NAME
        {
            get { return _SUBSIDIARY_NAME; }
            set { _SUBSIDIARY_NAME = value; }
        }
        private string _COMPANY_ID;

        public string COMPANY_ID
        {
            get { return _COMPANY_ID; }
            set { _COMPANY_ID = value; }
        }

         private char _NAME_VALUE_LIST;

         public char NAME_VALUE_LIST
        {
            get { return _NAME_VALUE_LIST; }
            set { _NAME_VALUE_LIST = value; }
        }
         private string _CONTROL_NAME;

         public string CONTROL_NAME
         {
             get { return _CONTROL_NAME; }
             set { _CONTROL_NAME = value; }
         }

    }
}
