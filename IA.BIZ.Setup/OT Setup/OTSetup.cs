﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DynamicSoft.DAL;
using IA.Model.Setup.Other.OT_Setup;
using System.Data.SqlClient;
using System.Data;

namespace IA.BIZ.Setup.OT_Setup
{
    public class OTSetup
    {
        public static int saveOT_CATEGORY(OT_CATEGORY objOT_CATEGORY)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_OT_CATEGORY_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OT_CATEGORY_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@OT_CATEGORY_NAME"].Value = objOT_CATEGORY.OT_CATEGORY_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objOT_CATEGORY.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdateOT_Category(OT_CATEGORY objOT_CATEGORY)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_OT_CATEGORY_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OT_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@OT_CATEGORY_ID"].Value = objOT_CATEGORY.OT_CATEGORY_ID;
                cmd.Parameters.Add(new SqlParameter("@OT_CATEGORY_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@OT_CATEGORY_NAME"].Value = objOT_CATEGORY.OT_CATEGORY_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objOT_CATEGORY.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteOT_Category(OT_CATEGORY objOT_CATEGORY)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_OT_CATEGORY_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OT_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@OT_CATEGORY_ID"].Value = objOT_CATEGORY.OT_CATEGORY_ID;
               
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int saveOTCharge(OT_CHARGE objOTCharge)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_OT_CHARGE_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OT_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@OT_ID"].Value = objOTCharge.OT_ID;
                cmd.Parameters.Add(new SqlParameter("@OT_AMOUNT", SqlDbType.Decimal));
                cmd.Parameters["@OT_AMOUNT"].Value = objOTCharge.OT_AMOUNT;
                cmd.Parameters.Add(new SqlParameter("@OI_DISCOUNT", SqlDbType.Decimal));
                cmd.Parameters["@OI_DISCOUNT"].Value = objOTCharge.OT_DISCOUNT;
                conn.Open();

                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdateOTCharge(OT_CHARGE objOTCharge)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_OT_CHARGE_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OT_CHARGE_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@OT_CHARGE_ID"].Value = objOTCharge.OT_CHARGE_ID;
                cmd.Parameters.Add(new SqlParameter("@OT_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@OT_ID"].Value = objOTCharge.OT_ID;
                cmd.Parameters.Add(new SqlParameter("@OT_AMOUNT", SqlDbType.Decimal));
                cmd.Parameters["@OT_AMOUNT"].Value = objOTCharge.OT_AMOUNT;
                cmd.Parameters.Add(new SqlParameter("@OI_DISCOUNT", SqlDbType.Decimal));
                cmd.Parameters["@OI_DISCOUNT"].Value = objOTCharge.OT_DISCOUNT;
                conn.Open();

                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteOTCharge(OT_CHARGE objOTCharge)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_OT_CHARGE_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OT_CHARGE_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@OT_CHARGE_ID"].Value = objOTCharge.OT_CHARGE_ID;
             
                conn.Open();

                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int saveOTComments(OT_COMMENTS objOTComments)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_OT_COMMENTS_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OT_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@OT_ID"].Value = objOTComments.OT_ID;
                cmd.Parameters.Add(new SqlParameter("@OT_COMMENT", SqlDbType.NVarChar,50));
                cmd.Parameters["@OT_COMMENT"].Value = objOTComments.OT_COMMENT;
                conn.Open();

                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdateOTComments(OT_COMMENTS objOTComments)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_OT_COMMENTS_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OT_COMMENTS_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@OT_COMMENTS_ID"].Value = objOTComments.OT_COMMENTS_ID;
                cmd.Parameters.Add(new SqlParameter("@OT_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@OT_ID"].Value = objOTComments.OT_ID;
                cmd.Parameters.Add(new SqlParameter("@OT_COMMENT", SqlDbType.NVarChar, 50));
                cmd.Parameters["@OT_COMMENT"].Value = objOTComments.OT_COMMENT;
                conn.Open();

                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteOTComments(OT_COMMENTS objOTComments)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_OT_COMMENTS_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OT_COMMENTS_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@OT_COMMENTS_ID"].Value = objOTComments.OT_COMMENTS_ID;
               
                conn.Open();

                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int saveOTName(OT objName)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_OT_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OT_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@OT_CATEGORY_ID"].Value = objName.OT_CATEGORY_ID;
                cmd.Parameters.Add(new SqlParameter("@OT_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@OT_NAME"].Value = objName.OT_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objName.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdateOTName(OT objName)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_OT_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OT_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@OT_ID"].Value = objName.OT_ID;
                cmd.Parameters.Add(new SqlParameter("@OT_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@OT_CATEGORY_ID"].Value = objName.OT_CATEGORY_ID;
                cmd.Parameters.Add(new SqlParameter("@OT_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@OT_NAME"].Value = objName.OT_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objName.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteOTName(OT objName)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_OT_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OT_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@OT_ID"].Value = objName.OT_ID;
               
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

    }
}
