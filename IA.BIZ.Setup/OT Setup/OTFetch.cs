﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IA.Model.Setup.Other.OT_Setup;
using System.Data.SqlClient;
using DynamicSoft.DAL;
using System.Data;

namespace IA.BIZ.Setup.OT_Setup
{
    public class OTFetch
    {
        public static List<OT_CATEGORY> getALLOTCategory()
        {
            List<OT_CATEGORY> objOTCategory = new List<OT_CATEGORY>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_OT_CATEGORY_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        OT_CATEGORY obj = new OT_CATEGORY();
                        obj.OT_CATEGORY_ID =new Guid (dr["OT_CATEGORY_ID"].ToString());
                        obj.OT_CATEGORY_NAME =dr["OT_CATEGORY_NAME"].ToString();
                        obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                        objOTCategory.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objOTCategory;
        }


        public static List<OT_COMMENTS> getALLOTComments()
        {
            List<OT_COMMENTS> objOTComments = new List<OT_COMMENTS>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_OT_COMMENTS_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        OT_COMMENTS obj = new OT_COMMENTS();
                        obj.OT_COMMENTS_ID =new Guid (dr["OT_COMMENTS_ID"].ToString());
                        obj.OT_ID =new Guid (dr["OT_ID"].ToString());
                        obj.OT_COMMENT = dr["OT_COMMENT"].ToString();
                        obj.OT_NAME = dr["OT_NAME"].ToString();
                        objOTComments.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objOTComments;
        }

        public static List<OT_CHARGE> getALLOTCharge()
        {
            List<OT_CHARGE> objOTCharge = new List<OT_CHARGE>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_OT_CHARGE_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        OT_CHARGE obj = new OT_CHARGE();
                        obj.OT_CHARGE_ID =new Guid (dr["OT_CHARGE_ID"].ToString());
                        obj.OT_ID =new Guid (dr["OT_ID"].ToString());
                        obj.OT_AMOUNT =decimal.Parse(dr["OT_AMOUNT"].ToString());
                        obj.OT_DISCOUNT =decimal.Parse(dr["OI_DISCOUNT"].ToString());
                        obj.OT_NAME = dr["OT_NAME"].ToString();
                        objOTCharge.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objOTCharge;
        }

        public static List<OT> getALLOT()
        {
            List<OT> objOTName = new List<OT>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_OT_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        OT obj = new OT();
                        obj.OT_ID =new Guid (dr["OT_ID"].ToString());
                        obj.OT_CATEGORY_ID =new Guid (dr["OT_CATEGORY_ID"].ToString());
                        obj.OT_NAME = dr["OT_NAME"].ToString();
                        obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                        obj.OT_CATEGORY_NAME = dr["OT_CATEGORY_NAME"].ToString();
                        objOTName.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objOTName;
        }

        public static List<OT> getALLOT(string OT_CATEGORY_ID)
        {
            List<OT> objOTName = new List<OT>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_OT_GK";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@OT_CATEGORY_ID", SqlDbType.UniqueIdentifier));
            objSqlCommand.Parameters["@OT_CATEGORY_ID"].Value =new Guid (OT_CATEGORY_ID);
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        OT obj = new OT();
                        obj.OT_ID = new Guid(dr["OT_ID"].ToString());
                        obj.OT_CATEGORY_ID = new Guid(dr["OT_CATEGORY_ID"].ToString());
                        obj.OT_NAME = dr["OT_NAME"].ToString();
                        obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                        objOTName.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objOTName;
        }
    }
}
