﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IA.Model.Setup.Other.Doctor;
using System.Data.SqlClient;
using DynamicSoft.DAL;
using System.Data;

namespace IA.BIZ.Setup.Doctor
{
   public class DoctorFetch
    {
       public static List<DOCTOR_ADVICE_COMMENTS> getALLAdviceComments()
       {
           List<DOCTOR_ADVICE_COMMENTS> objDoctorAdviceComments = new List<DOCTOR_ADVICE_COMMENTS>();
           SqlConnection conn = ConnectionClass.GetConnection();
           SqlCommand objSqlCommand = new SqlCommand();
           objSqlCommand.CommandText = "FSP_DOCTOR_ADVICE_COMMENTS_GA";
           objSqlCommand.CommandType = CommandType.StoredProcedure;
           objSqlCommand.Connection = conn;
           conn.Open();
           try
           {
               using (objSqlCommand)
               {
                   SqlDataReader dr = objSqlCommand.ExecuteReader();
                   while (dr.Read())
                   {
                       DOCTOR_ADVICE_COMMENTS obj = new DOCTOR_ADVICE_COMMENTS();
                       obj.DOCTOR_ADVICE_COMMENTS_ID =new Guid (dr["DOCTOR_ADVICE_COMMENTS_ID"].ToString());
                       obj.DOCTOR_ADVICE_COMMENTS_TEXT = dr["DOCTOR_ADVICE_COMMENTS_TEXT"].ToString();
                       objDoctorAdviceComments.Add(obj);
                   }
               }
           }
           catch { }
           finally
           {
               conn.Close();
           }
           return objDoctorAdviceComments;
       }

       public static List<DOCTOR_SPECIALIZATION> getALLSpecialization()
       {
           List<DOCTOR_SPECIALIZATION> objDoctorSpecialization = new List<DOCTOR_SPECIALIZATION>();
           SqlConnection conn = ConnectionClass.GetConnection();
           SqlCommand objSqlCommand = new SqlCommand();
           objSqlCommand.CommandText = "FSP_DOCTOR_SPECIALIZATION_GA";
           objSqlCommand.CommandType = CommandType.StoredProcedure;
           objSqlCommand.Connection = conn;
           conn.Open();
           try
           {
               using (objSqlCommand)
               {
                   SqlDataReader dr = objSqlCommand.ExecuteReader();
                   while (dr.Read())
                   {
                       DOCTOR_SPECIALIZATION obj = new DOCTOR_SPECIALIZATION();
                       obj.DOCTOR_SPECIALIZATION_ID =new Guid (dr["DOCTOR_SPECIALIZATION_ID"].ToString());
                       obj.DOCTOR_SPECIALIZATION_NAME = dr["DOCTOR_SPECIALIZATION_NAME"].ToString();
                       obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                       objDoctorSpecialization.Add(obj);
                   }
               }
           }
           catch { }
           finally
           {
               conn.Close();
           }
           return objDoctorSpecialization;
       }

       public static List<DOCTOR_TYPE_OF_VISIT> getALLTypeOfVisit()
       {
           List<DOCTOR_TYPE_OF_VISIT> objDoctorTypeOfVisit = new List<DOCTOR_TYPE_OF_VISIT>();
           SqlConnection conn = ConnectionClass.GetConnection();
           SqlCommand objSqlCommand = new SqlCommand();
           objSqlCommand.CommandText = "FSP_DOCTOR_TYPE_OF_VISIT_GA";
           objSqlCommand.CommandType = CommandType.StoredProcedure;
           objSqlCommand.Connection = conn;
           conn.Open();
           try
           {
               using (objSqlCommand)
               {
                   SqlDataReader dr = objSqlCommand.ExecuteReader();
                   while (dr.Read())
                   {
                       DOCTOR_TYPE_OF_VISIT obj = new DOCTOR_TYPE_OF_VISIT();
                       obj.DOCTOR_TYPE_OF_VISIT_ID =new Guid (dr["DOCTOR_TYPE_OF_VISIT_ID"].ToString());
                       obj.DOCTOR_TYPE_OF_VISIT_NAME = dr["DOCTOR_TYPE_OF_VISIT_NAME"].ToString();
                       obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                       objDoctorTypeOfVisit.Add(obj);
                   }
               }
           }
           catch { }
           finally
           {
               conn.Close();
           }
           return objDoctorTypeOfVisit;
       }

       public static List<DOCTOR_TYPE> getALLDoctorType()
       {
           List<DOCTOR_TYPE> objDoctorType = new List<DOCTOR_TYPE>();
           SqlConnection conn = ConnectionClass.GetConnection();
           SqlCommand objSqlCommand = new SqlCommand();
           objSqlCommand.CommandText = "FSP_DOCTOR_TYPE_GA";
           objSqlCommand.CommandType = CommandType.StoredProcedure;
           objSqlCommand.Connection = conn;
           conn.Open();
           try
           {
               using (objSqlCommand)
               {
                   SqlDataReader dr = objSqlCommand.ExecuteReader();
                   while (dr.Read())
                   {
                       DOCTOR_TYPE obj = new DOCTOR_TYPE();
                       obj.DOCTOR_TYPE_ID =new Guid (dr["DOCTOR_TYPE_ID"].ToString());
                       obj.DOCTOR_TYPE_NAME = dr["DOCTOR_TYPE_NAME"].ToString();
                       obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                       objDoctorType.Add(obj);
                   }
               }
           }
           catch { }
           finally
           {
               conn.Close();
           }
           return objDoctorType;
       }

       public static List<DOCTOR_CHARGE> getALLDoctorCharge()
       {
           List<DOCTOR_CHARGE> objDoctorCharge = new List<DOCTOR_CHARGE>();
           SqlConnection conn = ConnectionClass.GetConnection();
           SqlCommand objSqlCommand = new SqlCommand();
           objSqlCommand.CommandText = "FSP_DOCTOR_CHARGE_GA";
           objSqlCommand.CommandType = CommandType.StoredProcedure;
           objSqlCommand.Connection = conn;
           conn.Open();
           try
           {
               using (objSqlCommand)
               {
                   SqlDataReader dr = objSqlCommand.ExecuteReader();
                   while (dr.Read())
                   {
                       DOCTOR_CHARGE obj = new DOCTOR_CHARGE();
                       obj.DOCTOR_CHARGE_ID =new Guid( dr["DOCTOR_CHARGE_ID"].ToString());
                       obj.DOCTOR_ID =new Guid( dr["DOCTOR_ID"].ToString());
                       obj.DOCTOR_NEW_VISIT_CHARGE = decimal.Parse(dr["DOCTOR_NEW_VISIT_CHARGE"].ToString());
                       obj.DOCTOR_FOLLOW_UP_CHARGE = decimal.Parse(dr["DOCTOR_FOLLOW_UP_CHARGE"].ToString());
                       obj.DOCTOR_CALL_ON_CHARGE = decimal.Parse(dr["DOCTOR_CALL_ON_CHARGE"].ToString());
                       obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                       obj.DOCTOR_NAME = dr["DOCTOR_NAME"].ToString();
                       objDoctorCharge.Add(obj);
                   }
               }
           }
           catch { }
           finally
           {
               conn.Close();
           }
           return objDoctorCharge;
       }

       public static List<INDOOR_DOCTOR> getALLDoctor()
       {
           List<INDOOR_DOCTOR> objIndoorDoctor = new List<INDOOR_DOCTOR>();
           SqlConnection conn = ConnectionClass.GetConnection();
           SqlCommand objSqlCommand = new SqlCommand();
           objSqlCommand.CommandText = "FSP_DOCTOR_GA";
           objSqlCommand.CommandType = CommandType.StoredProcedure;
           objSqlCommand.Connection = conn;
           conn.Open();
           try
           {
               using (objSqlCommand)
               {
                   SqlDataReader dr = objSqlCommand.ExecuteReader();
                   while (dr.Read())
                   {
                       INDOOR_DOCTOR obj = new INDOOR_DOCTOR();
                       obj.DOCTOR_ID =new Guid( dr["DOCTOR_ID"].ToString());
                       obj.DOCTOR_SPECIALIZATION_ID =new Guid( dr["DOCTOR_SPECIALIZATION_ID"].ToString());
                       obj.DOCTOR_TYPE_ID =new Guid( dr["DOCTOR_TYPE_ID"].ToString());
                       obj.DOCTOR_NAME = dr["DOCTOR_NAME"].ToString();
                       obj.DOCTOR_CONTACT_PERSON = dr["DOCTOR_CONTACT_PERSON"].ToString();
                       obj.DOCTOR_PERSONAL_ADDRESS = dr["DOCTOR_PERSONAL_ADDRESS"].ToString();
                       obj.DOCTOR_CHAMBER_ADDRESS = dr["DOCTOR_CHAMBER_ADDRESS"].ToString();
                       obj.DOCTOR_PERSONAL_CONTACT = dr["DOCTOR_PERSONAL_CONTACT"].ToString();
                       obj.DOCTOR_PERSONAL_CONTACT = dr["DOCTOR_PERSONAL_CONTACT"].ToString();
                       obj.DOCTOR_PS_CONTACT = dr["DOCTOR_PS_CONTACT"].ToString();
                       obj.DOCTOR_PATHOLOGY_COMMISSION = decimal.Parse(dr["DOCTOR_PATHOLOGY_COMMISSION"].ToString());
                       obj.DOCTOR_PATIENT_COMMISSION = decimal.Parse(dr["DOCTOR_PATIENT_COMMISSION"].ToString());
                       obj.INDOOR_OUTDOOR_FLAG = dr["INDOOR_OUTDOOR_FLAG"].ToString();
                       obj.DOCTOR_SPECIALIZATION_NAME = dr["DOCTOR_SPECIALIZATION_NAME"].ToString();
                       obj.DOCTOR_TYPE_NAME = dr["DOCTOR_TYPE_NAME"].ToString();
                       objIndoorDoctor.Add(obj);
                   }
               }
           }
           catch { }
           finally
           {
               conn.Close();
           }
           return objIndoorDoctor;
       }

       public static List<INDOOR_DOCTOR> getALLIndoorDoctor()
       {
           List<INDOOR_DOCTOR> objIndoorDoctor = new List<INDOOR_DOCTOR>();
           SqlConnection conn = ConnectionClass.GetConnection();
           SqlCommand objSqlCommand = new SqlCommand();
           objSqlCommand.CommandText = "FSP_INDOOR_DOCTOR_GK";
           objSqlCommand.CommandType = CommandType.StoredProcedure;
           objSqlCommand.Connection = conn;
           conn.Open();
           try
           {
               using (objSqlCommand)
               {
                   SqlDataReader dr = objSqlCommand.ExecuteReader();
                   while (dr.Read())
                   {
                       INDOOR_DOCTOR obj = new INDOOR_DOCTOR();
                       obj.DOCTOR_ID = new Guid(dr["DOCTOR_ID"].ToString());
                       obj.DOCTOR_SPECIALIZATION_ID = new Guid(dr["DOCTOR_SPECIALIZATION_ID"].ToString());
                       obj.DOCTOR_TYPE_ID = new Guid(dr["DOCTOR_TYPE_ID"].ToString());
                       obj.DOCTOR_NAME = dr["DOCTOR_NAME"].ToString();
                       obj.DOCTOR_CONTACT_PERSON = dr["DOCTOR_CONTACT_PERSON"].ToString();
                       obj.DOCTOR_PERSONAL_ADDRESS = dr["DOCTOR_PERSONAL_ADDRESS"].ToString();
                       obj.DOCTOR_CHAMBER_ADDRESS = dr["DOCTOR_CHAMBER_ADDRESS"].ToString();
                       obj.DOCTOR_PERSONAL_CONTACT = dr["DOCTOR_PERSONAL_CONTACT"].ToString();
                       obj.DOCTOR_PERSONAL_CONTACT = dr["DOCTOR_PERSONAL_CONTACT"].ToString();
                       obj.DOCTOR_PS_CONTACT = dr["DOCTOR_PS_CONTACT"].ToString();
                       obj.DOCTOR_PATHOLOGY_COMMISSION = decimal.Parse(dr["DOCTOR_PATHOLOGY_COMMISSION"].ToString());
                       obj.DOCTOR_PATIENT_COMMISSION = decimal.Parse(dr["DOCTOR_PATIENT_COMMISSION"].ToString());
                       obj.INDOOR_OUTDOOR_FLAG = dr["INDOOR_OUTDOOR_FLAG"].ToString();
                       objIndoorDoctor.Add(obj);
                   }
               }
           }
           catch { }
           finally
           {
               conn.Close();
           }
           return objIndoorDoctor;
       }

       public static List<INDOOR_DOCTOR> getALLOutdoorDoctor()
       {
           List<INDOOR_DOCTOR> objIndoorDoctor = new List<INDOOR_DOCTOR>();
           SqlConnection conn = ConnectionClass.GetConnection();
           SqlCommand objSqlCommand = new SqlCommand();
           objSqlCommand.CommandText = "FSP_OUTDOOR_DOCTOR_GK";
           objSqlCommand.CommandType = CommandType.StoredProcedure;
           objSqlCommand.Connection = conn;
           conn.Open();
           try
           {
               using (objSqlCommand)
               {
                   SqlDataReader dr = objSqlCommand.ExecuteReader();
                   while (dr.Read())
                   {
                       INDOOR_DOCTOR obj = new INDOOR_DOCTOR();
                       obj.DOCTOR_ID = new Guid(dr["DOCTOR_ID"].ToString());
                       obj.DOCTOR_SPECIALIZATION_ID = new Guid(dr["DOCTOR_SPECIALIZATION_ID"].ToString());
                       obj.DOCTOR_TYPE_ID = new Guid(dr["DOCTOR_TYPE_ID"].ToString());
                       obj.DOCTOR_NAME = dr["DOCTOR_NAME"].ToString();
                       obj.DOCTOR_CONTACT_PERSON = dr["DOCTOR_CONTACT_PERSON"].ToString();
                       obj.DOCTOR_PERSONAL_ADDRESS = dr["DOCTOR_PERSONAL_ADDRESS"].ToString();
                       obj.DOCTOR_CHAMBER_ADDRESS = dr["DOCTOR_CHAMBER_ADDRESS"].ToString();
                       obj.DOCTOR_PERSONAL_CONTACT = dr["DOCTOR_PERSONAL_CONTACT"].ToString();
                       obj.DOCTOR_PERSONAL_CONTACT = dr["DOCTOR_PERSONAL_CONTACT"].ToString();
                       obj.DOCTOR_PS_CONTACT = dr["DOCTOR_PS_CONTACT"].ToString();
                       obj.DOCTOR_PATHOLOGY_COMMISSION = decimal.Parse(dr["DOCTOR_PATHOLOGY_COMMISSION"].ToString());
                       obj.DOCTOR_PATIENT_COMMISSION = decimal.Parse(dr["DOCTOR_PATIENT_COMMISSION"].ToString());
                       obj.INDOOR_OUTDOOR_FLAG = dr["INDOOR_OUTDOOR_FLAG"].ToString();
                       objIndoorDoctor.Add(obj);
                   }
               }
           }
           catch { }
           finally
           {
               conn.Close();
           }
           return objIndoorDoctor;
       }

       public static List<MEDICINE_DOSE> getALLMedicineDose()
       {
           List<MEDICINE_DOSE> objMedicineDose = new List<MEDICINE_DOSE>();
           SqlConnection conn = ConnectionClass.GetConnection();
           SqlCommand objSqlCommand = new SqlCommand();
           objSqlCommand.CommandText = "FSP_MEDICINE_DOSE_GA";
           objSqlCommand.CommandType = CommandType.StoredProcedure;
           objSqlCommand.Connection = conn;
           conn.Open();
           try
           {
               using (objSqlCommand)
               {
                   SqlDataReader dr = objSqlCommand.ExecuteReader();
                   while (dr.Read())
                   {
                       MEDICINE_DOSE obj = new MEDICINE_DOSE();
                       obj.MEDICINE_DOSE_ID =new Guid (dr["MEDICINE_DOSE_ID"].ToString());
                       obj.MEDICINE_DOSE_NAME = dr["MEDICINE_DOSE_NAME"].ToString();
                       obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                       objMedicineDose.Add(obj);
                   }
               }
           }
           catch { }
           finally
           {
               conn.Close();
           }
           return objMedicineDose;
       }

       //public static List<OUTDOOR_DOCTOR> getALLOutDoorDoctor()
       //{
       //    List<OUTDOOR_DOCTOR> objOutdoorDoctor = new List<OUTDOOR_DOCTOR>();
       //    SqlConnection conn = ConnectionClass.GetConnection();
       //    SqlCommand objSqlCommand = new SqlCommand();
       //    objSqlCommand.CommandText = "FSP_OUTDOOR_DOCTOR_GA";
       //    objSqlCommand.CommandType = CommandType.StoredProcedure;
       //    objSqlCommand.Connection = conn;
       //    conn.Open();
       //    try
       //    {
       //        using (objSqlCommand)
       //        {
       //            SqlDataReader dr = objSqlCommand.ExecuteReader();
       //            while (dr.Read())
       //            {
       //                OUTDOOR_DOCTOR obj = new OUTDOOR_DOCTOR();
       //                obj.DOCTOR_ID =new Guid(dr["DOCTOR_ID"].ToString());
       //                obj.DOCTOR_SPECIALIZATION_ID =new Guid (dr["DOCTOR_SPECIALIZATION_ID"].ToString());
       //                obj.DOCTOR_TYPE_ID =new Guid (dr["DOCTOR_TYPE_ID"].ToString());
       //                obj.DOCTOR_NAME = dr["DOCTOR_NAME"].ToString();
       //                obj.DOCTOR_CONTACT_PERSON = dr["DOCTOR_CONTACT_PERSON"].ToString();
       //                obj.DOCTOR_PERSONAL_ADDRESS = dr["DOCTOR_PERSONAL_ADDRESS"].ToString();
       //                obj.DOCTOR_CHAMBER_ADDRESS = dr["DOCTOR_CHAMBER_ADDRESS"].ToString();
       //                obj.DOCTOR_PERSONAL_CONTACT = dr["DOCTOR_PERSONAL_CONTACT"].ToString();
       //                obj.DOCTOR_PERSONAL_CONTACT = dr["DOCTOR_PERSONAL_CONTACT"].ToString();
       //                obj.DOCTOR_PS_CONTACT = dr["DOCTOR_PS_CONTACT"].ToString();
       //                obj.DOCTOR_PATHOLOGY_COMMISSION = decimal.Parse(dr["DOCTOR_PATHOLOGY_COMMISSION"].ToString());
       //                obj.DOCTOR_PATIENT_COMMISSION = decimal.Parse(dr["DOCTOR_PATIENT_COMMISSION"].ToString());
       //                objOutdoorDoctor.Add(obj);
       //            }
       //        }
       //    }
       //    catch { }
       //    finally
       //    {
       //        conn.Close();
       //    }
       //    return objOutdoorDoctor;
       //}

    }
}
