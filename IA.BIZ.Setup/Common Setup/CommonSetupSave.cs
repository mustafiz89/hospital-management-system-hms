﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DynamicSoft.DAL;
using IA.Model.Setup.Other.Common_Setup;
using System.Data.SqlClient;
using System.Data;

namespace IA.BIZ.Setup.Common_Setup
{
    public class CommonSetupSave
    {
        public static int saveBLOOD_GROUP(BLOOD_GROUP objBLOOD_GROUP)
        {
            using (SqlCommand objSqlCommand = new SqlCommand())
            {
                int i = 0;
                SqlConnection conn = ConnectionClass.GetConnection();
                objSqlCommand.Connection = conn;
                objSqlCommand.CommandText = "FSP_BLOOD_GROUP_I";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add(new SqlParameter("@BLOOD_GROUP_NAME",SqlDbType.NVarChar,30 ));
                objSqlCommand.Parameters["@BLOOD_GROUP_NAME"].Value = objBLOOD_GROUP.BLOOD_GROUP_NAME;
                objSqlCommand.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                objSqlCommand.Parameters["@STATUS_FLAG"].Value = objBLOOD_GROUP.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = objSqlCommand.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int UpdateBloodGroup(BLOOD_GROUP objBLOOD_GROUP)
        {
            using (SqlCommand objSqlCommand = new SqlCommand())
            {
                int i = 0;
                SqlConnection conn = ConnectionClass.GetConnection();
                objSqlCommand.Connection = conn;
                objSqlCommand.CommandText = "FSP_BLOOD_GROUP_U";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add(new SqlParameter("@BLOOD_GROUP_ID", SqlDbType.UniqueIdentifier));
                objSqlCommand.Parameters["@BLOOD_GROUP_ID"].Value = objBLOOD_GROUP.BLOOD_GROUP_ID;
                objSqlCommand.Parameters.Add(new SqlParameter("@BLOOD_GROUP_NAME", SqlDbType.NVarChar, 30));
                objSqlCommand.Parameters["@BLOOD_GROUP_NAME"].Value = objBLOOD_GROUP.BLOOD_GROUP_NAME;
                objSqlCommand.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                objSqlCommand.Parameters["@STATUS_FLAG"].Value = objBLOOD_GROUP.STATUS_FLAG;

                 
                conn.Open();
                try
                {
                    i = objSqlCommand.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteBloodGroup(BLOOD_GROUP objBLOOD_GROUP)
        {
            using (SqlCommand objSqlCommand = new SqlCommand())
            {
                int i = 0;
                SqlConnection conn = ConnectionClass.GetConnection();
                objSqlCommand.Connection = conn;
                objSqlCommand.CommandText = "FSP_BLOOD_GROUP_D";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add(new SqlParameter("@BLOOD_GROUP_ID", SqlDbType.UniqueIdentifier));
                objSqlCommand.Parameters["@BLOOD_GROUP_ID"].Value = objBLOOD_GROUP.BLOOD_GROUP_ID;

                conn.Open();
                try
                {
                    i = objSqlCommand.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }


        public static int saveGender(GENDER objGender)
        {
            using (SqlCommand objSqlCommand = new SqlCommand())
            {
                int i = 0;
                SqlConnection conn = ConnectionClass.GetConnection();
                objSqlCommand.Connection = conn;
                objSqlCommand.CommandText = "FSP_GENDER_I";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add(new SqlParameter("@GENDER_NAME", SqlDbType.NVarChar, 30));
                objSqlCommand.Parameters["@GENDER_NAME"].Value = objGender.GENDER_NAME;
                objSqlCommand.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                objSqlCommand.Parameters["@STATUS_FLAG"].Value = objGender.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = objSqlCommand.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int UpdateGender(GENDER objGender)
        {
            using (SqlCommand objSqlCommand = new SqlCommand())
            {
                int i = 0;
                SqlConnection conn = ConnectionClass.GetConnection();
                objSqlCommand.Connection = conn;
                objSqlCommand.CommandText = "FSP_GENDER_U";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add(new SqlParameter("@GENDER_ID", SqlDbType.UniqueIdentifier));
                objSqlCommand.Parameters["@GENDER_ID"].Value = objGender.GENDER_ID;
                objSqlCommand.Parameters.Add(new SqlParameter("@GENDER_NAME", SqlDbType.NVarChar, 30));
                objSqlCommand.Parameters["@GENDER_NAME"].Value = objGender.GENDER_NAME;
                objSqlCommand.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                objSqlCommand.Parameters["@STATUS_FLAG"].Value = objGender.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = objSqlCommand.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteGender(GENDER objGender)
        {
            using (SqlCommand objSqlCommand = new SqlCommand())
            {
                int i = 0;
                SqlConnection conn = ConnectionClass.GetConnection();
                objSqlCommand.Connection = conn;
                objSqlCommand.CommandText = "FSP_GENDER_D";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add(new SqlParameter("@GENDER_ID", SqlDbType.UniqueIdentifier));
                objSqlCommand.Parameters["@GENDER_ID"].Value = objGender.GENDER_ID;

                conn.Open();
                try
                {
                    i = objSqlCommand.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int saveMARITAL_STATUS(MARITAL_STATUS objMARITAL_STATUS)
        {
            int i = 0;
            using(SqlCommand cmd=new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_MARITAL_STATUS_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@MARITAL_STATUS_NAME",SqlDbType.NVarChar,30));
                cmd.Parameters["@MARITAL_STATUS_NAME"].Value = objMARITAL_STATUS.MARITAL_STATUS_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objMARITAL_STATUS.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally { 
                    conn.Close(); 
                }
            }
            return i;
        }

        public static int UpdateMaritalStatus(MARITAL_STATUS objMARITAL_STATUS)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_MARITAL_STATUS_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@MARITAL_STATUS_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@MARITAL_STATUS_ID"].Value = objMARITAL_STATUS.MARITAL_STATUS_ID;
                cmd.Parameters.Add(new SqlParameter("@MARITAL_STATUS_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@MARITAL_STATUS_NAME"].Value = objMARITAL_STATUS.MARITAL_STATUS_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objMARITAL_STATUS.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
            }
            return i;
        }

        public static int DeleteMaritalStatus(MARITAL_STATUS objMARITAL_STATUS)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_MARITAL_STATUS_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@MARITAL_STATUS_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@MARITAL_STATUS_ID"].Value = objMARITAL_STATUS.MARITAL_STATUS_ID;
              
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
            }
            return i;
        }

        public static int saveNationality(NATIONALITY objNATIONALITY)
        {
            using(SqlCommand cmd=new SqlCommand())
            {
                int i = 0;
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_NATIONALITY_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@NATIONALITY_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@NATIONALITY_NAME"].Value = objNATIONALITY.NATIONALITY_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar,30));
                cmd.Parameters["@STATUS_FLAG"].Value = objNATIONALITY.STATUS_FLAG;
                conn.Open();
                try {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally { conn.Close(); 
                }
                return i;
            }
            
        }

        public static int UpdateNationality(NATIONALITY objNATIONALITY)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                int i = 0;
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_NATIONALITY_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@NATIONALITY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@NATIONALITY_ID"].Value = objNATIONALITY.NATIONALITY_ID;
                cmd.Parameters.Add(new SqlParameter("@NATIONALITY_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@NATIONALITY_NAME"].Value = objNATIONALITY.NATIONALITY_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objNATIONALITY.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteNationality(NATIONALITY objNATIONALITY)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                int i = 0;
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_NATIONALITY_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@NATIONALITY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@NATIONALITY_ID"].Value = objNATIONALITY.NATIONALITY_ID;
                
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int saveOccupation(OCCUPATION objOccupation)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_OCCUPATION_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OCCUPATION_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@OCCUPATION_NAME"].Value = objOccupation.OCCUPATION_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objOccupation.STATUS_FLAG;
                conn.Open();
                try
                {
                    i=cmd.ExecuteNonQuery();
                }
                catch{}
                finally
                {
                    conn.Close();
                }
                return i;


            }

        }

        public static int UpdateOccupation(OCCUPATION objOccupation)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_OCCUPATION_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OCCUPATION_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@OCCUPATION_ID"].Value = objOccupation.OCCUPATION_ID;
                cmd.Parameters.Add(new SqlParameter("@OCCUPATION_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@OCCUPATION_NAME"].Value = objOccupation.OCCUPATION_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objOccupation.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteOccupation(OCCUPATION objOccupation)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_OCCUPATION_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OCCUPATION_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@OCCUPATION_ID"].Value = objOccupation.OCCUPATION_ID;
               
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int saveReligion(RELIGION objReligion)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_RELIGION_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@RELIGION_NAME",SqlDbType.NVarChar,30));
                cmd.Parameters["@RELIGION_NAME"].Value = objReligion.RELIGION_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objReligion.STATUS_FLAG;
                conn.Open();
                try
                {
                    i=cmd.ExecuteNonQuery();
                }
                catch{}
                finally
                {
                    conn.Close();
                }
                return i;
            }
                
        }

        public static int UpdateReligion(RELIGION objReligion)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_RELIGION_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@RELIGION_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@RELIGION_ID"].Value = objReligion.RELIGION_ID;
                cmd.Parameters.Add(new SqlParameter("@RELIGION_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@RELIGION_NAME"].Value = objReligion.RELIGION_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objReligion.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteReligion(RELIGION objReligion)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_RELIGION_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@RELIGION_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@RELIGION_ID"].Value = objReligion.RELIGION_ID;
              
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int saveSalution(SALUTION objSalution)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_SALUTION_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@SALUTION_NAME",SqlDbType.NVarChar,30));
                cmd.Parameters["@SALUTION_NAME"].Value = objSalution.SALUTION_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objSalution.STATUS_FLAG;
                conn.Open();
                try
                {
                    i=cmd.ExecuteNonQuery();
                }
                catch{}
                    finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int UpadateSalution(SALUTION objSalution)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_SALUTION_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@SALUTION_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@SALUTION_ID"].Value = objSalution.SALUTION_ID;
                cmd.Parameters.Add(new SqlParameter("@SALUTION_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@SALUTION_NAME"].Value = objSalution.SALUTION_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objSalution.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteSalution(SALUTION objSalution)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_SALUTION_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@SALUTION_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@SALUTION_ID"].Value = objSalution.SALUTION_ID;

                conn.Open();
              
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }




        //public static int saveMARITAL_STATUS(NATIONALITY objNATIONALITY)
        //{
        //    throw new NotImplementedException();
        //}

        //public static int saveOccupation()
        //{
        //    throw new NotImplementedException();
        //}

        //public static int saveGENDER(GENDER objGENDER)
        //{
        //    throw new NotImplementedException();
        //}
    }
}