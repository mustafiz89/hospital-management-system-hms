﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DynamicSoft.DAL;
using IA.Model.Setup.Other.Settings;
using System.Data.SqlClient;
using System.Data;

namespace IA.BIZ.Setup.Settings
{
    public class SettingsSetup
    {
        public static int SaveCity(City objCity)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_CITY_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CITY_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@CITY_NAME"].Value = objCity.CITY_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objCity.STATUS_FLAG;
                conn.Open();

                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int UpdateCity(City objCity)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_CITY_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CITY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@CITY_ID"].Value = objCity.CITY_ID;
                cmd.Parameters.Add(new SqlParameter("@CITY_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@CITY_NAME"].Value = objCity.CITY_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objCity.STATUS_FLAG;
                conn.Open();

                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteCity(City objCity)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_CITY_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CITY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@CITY_ID"].Value = objCity.CITY_ID;
          
                conn.Open();

                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int SaveCountry(COUNTRY objCountry)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_COUNTRY_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@COUNTRY_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@COUNTRY_NAME"].Value = objCountry.COUNTRY_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objCountry.STATUS_FLAG;
                conn.Open();

                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int UpdateCountry(COUNTRY objCountry)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_COUNTRY_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@COUNTRY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@COUNTRY_ID"].Value = objCountry.COUNTRY_ID;
                cmd.Parameters.Add(new SqlParameter("@COUNTRY_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@COUNTRY_NAME"].Value = objCountry.COUNTRY_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objCountry.STATUS_FLAG;
                conn.Open();

                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteCountry(COUNTRY objCountry)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_COUNTRY_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@COUNTRY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@COUNTRY_ID"].Value = objCountry.COUNTRY_ID;
               
                conn.Open();

                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }
    }
}
