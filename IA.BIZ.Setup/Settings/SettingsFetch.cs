﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IA.Model.Setup.Other.Settings;
using System.Data.SqlClient;
using DynamicSoft.DAL;
using System.Data;

namespace IA.BIZ.Setup.Settings
{
   public class SettingsFetch
    {
       public static List<City> getALLCity()
       {
           List<City> objCity = new List<City>();
           SqlConnection conn = ConnectionClass.GetConnection();
           SqlCommand objSqlCommand = new SqlCommand();
           objSqlCommand.CommandText = "FSP_CITY_GA";
           objSqlCommand.CommandType = CommandType.StoredProcedure;
           objSqlCommand.Connection = conn;
           conn.Open();
           try
           {
               using (objSqlCommand)
               {
                   SqlDataReader dr = objSqlCommand.ExecuteReader();
                   while (dr.Read())
                   {
                       City obj = new City();
                       obj.CITY_ID = new Guid(dr["CITY_ID"].ToString());
                       obj.CITY_NAME = dr["CITY_NAME"].ToString();
                       obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                       objCity.Add(obj);
                   }
               }
           }
           catch { }
           finally
           {
               conn.Close();
           }
           return objCity;
       }

       public static List<COUNTRY> getALLCountry()
       {
           List<COUNTRY> objCountry = new List<COUNTRY>();
           SqlConnection conn = ConnectionClass.GetConnection();
           SqlCommand objSqlCommand = new SqlCommand();
           objSqlCommand.CommandText = "FSP_COUNTRY_GA";
           objSqlCommand.CommandType = CommandType.StoredProcedure;
           objSqlCommand.Connection = conn;
           conn.Open();
           try
           {
               using (objSqlCommand)
               {
                   SqlDataReader dr = objSqlCommand.ExecuteReader();
                   while (dr.Read())
                   {
                       COUNTRY obj = new COUNTRY();
                       obj.COUNTRY_ID = new Guid(dr["COUNTRY_ID"].ToString());
                       obj.COUNTRY_NAME = dr["COUNTRY_NAME"].ToString();
                       obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                       objCountry.Add(obj);
                   }
               }
           }
           catch { }
           finally
           {
               conn.Close();
           }
           return objCountry;
       }
    }
}
