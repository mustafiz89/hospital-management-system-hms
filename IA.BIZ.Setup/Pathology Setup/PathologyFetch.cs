﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IA.Model.Setup.Other.Pathology_Setup;
using System.Data.SqlClient;
using DynamicSoft.DAL;
using System.Data;

namespace IA.BIZ.Setup.Pathology_Setup
{
    public class PathologyFetch
    {
       
        public static List<PATHOLOGY_TEST_ITEM_CATEGORY> getALLPathologyCategory()
        {
            List<PATHOLOGY_TEST_ITEM_CATEGORY> objPathologyCategory = new List<PATHOLOGY_TEST_ITEM_CATEGORY>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_PATHOLOGY_TEST_ITEM_CATEGORY_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        PATHOLOGY_TEST_ITEM_CATEGORY obj = new PATHOLOGY_TEST_ITEM_CATEGORY();
                        obj.PATHOLOGY_TEST_ITEM_CATEGORY_ID =new Guid (dr["PATHOLOGY_TEST_ITEM_CATEGORY_ID"].ToString());
                        obj.PATHOLOGY_TEST_ITEM_CATEGORY_NAME = dr["PATHOLOGY_TEST_ITEM_CATEGORY_NAME"].ToString();
                        obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                        objPathologyCategory.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objPathologyCategory;
        }

        

        public static List<PATHOLOGY_TEST_ITEM_SUB_CATEGORY> getALLPathologySubCategory()
        {
            List<PATHOLOGY_TEST_ITEM_SUB_CATEGORY> objPathologySubCategory = new List<PATHOLOGY_TEST_ITEM_SUB_CATEGORY>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_PATHOLOGY_TEST_ITEM_SUB_CATEGORY_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        PATHOLOGY_TEST_ITEM_SUB_CATEGORY obj = new PATHOLOGY_TEST_ITEM_SUB_CATEGORY();
                        obj.PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID =new Guid (dr["PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID"].ToString());
                        obj.PATHOLOGY_TEST_ITEM_CATEGORY_ID =new Guid (dr["PATHOLOGY_TEST_ITEM_CATEGORY_ID"].ToString());
                        obj.PATHOLOGY_TEST_ITEM_SUB_CATEGORY_NAME = dr["PATHOLOGY_TEST_ITEM_SUB_CATEGORY_NAME"].ToString();
                        obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                        obj.PATHOLOGY_TEST_ITEM_CATEGORY_NAME = dr["PATHOLOGY_TEST_ITEM_CATEGORY_NAME"].ToString();
                        objPathologySubCategory.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objPathologySubCategory;
        }


        public static List<PATHOLOGY_TEST_ITEM_SUB_CATEGORY> getALLPathologySubCategory(string PATHOLOGY_TEST_ITEM_CATEGORY_ID)
        {
            List<PATHOLOGY_TEST_ITEM_SUB_CATEGORY> objPathologySubCategory = new List<PATHOLOGY_TEST_ITEM_SUB_CATEGORY>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_PATHOLOGY_TEST_ITEM_SUB_CATEGORY_GK";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_CATEGORY_ID", SqlDbType.UniqueIdentifier));
            objSqlCommand.Parameters["@PATHOLOGY_TEST_ITEM_CATEGORY_ID"].Value =new Guid (PATHOLOGY_TEST_ITEM_CATEGORY_ID);
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        PATHOLOGY_TEST_ITEM_SUB_CATEGORY obj = new PATHOLOGY_TEST_ITEM_SUB_CATEGORY();
                        obj.PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID = new Guid(dr["PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID"].ToString());
                        obj.PATHOLOGY_TEST_ITEM_CATEGORY_ID = new Guid(dr["PATHOLOGY_TEST_ITEM_CATEGORY_ID"].ToString());
                        obj.PATHOLOGY_TEST_ITEM_SUB_CATEGORY_NAME = dr["PATHOLOGY_TEST_ITEM_SUB_CATEGORY_NAME"].ToString();
                        obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                        objPathologySubCategory.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objPathologySubCategory;
        }
       
        public static List<PATHOLOGY_SPECIMAN> getALLSpecimen()
        {
            List<PATHOLOGY_SPECIMAN> objPathologySpecimen = new List<PATHOLOGY_SPECIMAN>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_PATHOLOGY_SPECIMAN_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        PATHOLOGY_SPECIMAN obj = new PATHOLOGY_SPECIMAN();
                        obj.PATHOLOGY_SPECIMAN_ID =new Guid (dr["PATHOLOGY_SPECIMAN_ID"].ToString());
                        obj.PATHOLOGY_SPECIMAN_NAME = dr["PATHOLOGY_SPECIMAN_NAME"].ToString();
                        obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                        objPathologySpecimen.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objPathologySpecimen;
        }

        public static List<PATHOLOGY_TEST_COMMENTS> getALLTestComments()
        {
            List<PATHOLOGY_TEST_COMMENTS> objPathologyTestComments = new List<PATHOLOGY_TEST_COMMENTS>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_PATHOLOGY_TEST_COMMENTS_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        PATHOLOGY_TEST_COMMENTS obj = new PATHOLOGY_TEST_COMMENTS();
                        obj.PATHOLOGY_TEST_COMMENTS_ID =new Guid (dr["PATHOLOGY_TEST_COMMENTS_ID"].ToString());
                        obj.PATHOLOGY_TEST_COMMENTS_COMMENTS = dr["PATHOLOGY_TEST_COMMENTS_COMMENTS"].ToString();
                        objPathologyTestComments.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objPathologyTestComments;
        }

        public static List<PATHOLOGY_REFERENCE_CATEGORY> getALLReferenceCategory()
        {
            List<PATHOLOGY_REFERENCE_CATEGORY> objReferenceCategory = new List<PATHOLOGY_REFERENCE_CATEGORY>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_PATHOLOGY_REFERENCE_CATEGORY_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        PATHOLOGY_REFERENCE_CATEGORY obj = new PATHOLOGY_REFERENCE_CATEGORY();
                        obj.PATHOLOGY_REFERENCE_CATEGORY_ID =new Guid (dr["PATHOLOGY_REFERENCE_CATEGORY_ID"].ToString());
                        obj.PATHOLOGY_REFERENCE_CATEGORY_NAME = dr["PATHOLOGY_REFERENCE_CATEGORY_NAME"].ToString();
                        obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                        objReferenceCategory.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objReferenceCategory;
        }

        public static List<PATHOLOGY_REFERENCE_PERSON> getALLReferencePerson()
        {
            List<PATHOLOGY_REFERENCE_PERSON> objReferencePerson = new List<PATHOLOGY_REFERENCE_PERSON>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_PATHOLOGY_REFERENCE_PERSON_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        PATHOLOGY_REFERENCE_PERSON obj = new PATHOLOGY_REFERENCE_PERSON();
                        obj.PATHOLOGY_REFERENCE_PERSON_ID =new Guid (dr["PATHOLOGY_REFERENCE_PERSON_ID"].ToString());
                        obj.PATHOLOGY_REFERENCE_CATEGORY_ID = new Guid(dr["PATHOLOGY_REFERENCE_CATEGORY_ID"].ToString());
                        obj.PATHOLOGY_REFERENCE_PERSON_NAME = dr["PATHOLOGY_REFERENCE_PERSON_NAME"].ToString();
                        obj.PATHOLOGY_REFERENCE_PERSON_ADDRESS = dr["PATHOLOGY_REFERENCE_PERSON_ADDRESS"].ToString();
                        obj.PATHOLOGY_REFERENCE_PERSON_CONTACT = dr["PATHOLOGY_REFERENCE_PERSON_CONTACT"].ToString();
                        obj.PATHOLOGY_TESTWISE_COMMISSION =decimal.Parse(dr["PATHOLOGY_TESTWISE_COMMISSION"].ToString());
                        obj.PATHOLOGY_REFERENCE_CATEGORY_NAME = dr["PATHOLOGY_REFERENCE_CATEGORY_NAME"].ToString();
                        objReferencePerson.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objReferencePerson;
        }

        public static List<PATHOLOGY_TEST_ITEM> getALLPathologyTestItem()
        {
            List<PATHOLOGY_TEST_ITEM> objPathologyTestItem = new List<PATHOLOGY_TEST_ITEM>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_PATHOLOGY_TEST_ITEM_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        PATHOLOGY_TEST_ITEM obj = new PATHOLOGY_TEST_ITEM();
                        obj.PATHOLOGY_TEST_ITEM_ID =new Guid (dr["PATHOLOGY_TEST_ITEM_ID"].ToString());
                        obj.PATHOLOGY_TEST_ITEM_CATEGORY_ID =new Guid (dr["PATHOLOGY_TEST_ITEM_CATEGORY_ID"].ToString());
                        obj.PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID =new Guid (dr["PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID"].ToString());
                        obj.PATHOLOGY_TEST_ITEM_SPECIMEN_ID =new Guid (dr["PATHOLOGY_SPECIMEN_ID"].ToString());
                        obj.PATHOLOGY_TEST_ITEM_NAME = dr["PATHOLOGY_TEST_ITEM_NAME"].ToString();
                        obj.PATHOLOGY_TEST_ITEM_PRICE_AMOUNT = decimal.Parse(dr["PATHOLOGY_TEST_ITEM_PRICE_AMOUNT"].ToString());
                        obj.PATHOLOGY_TEST_ITEM_PRICE_DISCOUNT = decimal.Parse(dr["PATHOLOGY_TEST_ITEM_PRICE_DISCOUNT"].ToString());
                        obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                        obj.PATHOLOGY_TEST_ITEM_CATEGORY_NAME = dr["PATHOLOGY_TEST_ITEM_CATEGORY_NAME"].ToString();
                        obj.PATHOLOGY_TEST_ITEM_SUB_CATEGORY_NAME = dr["PATHOLOGY_TEST_ITEM_SUB_CATEGORY_NAME"].ToString();
                        obj.PATHOLOGY_TEST_ITEM_SPECIMEN_NAME = dr["PATHOLOGY_SPECIMAN_NAME"].ToString();
                        objPathologyTestItem.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objPathologyTestItem;
        }
    }
}
