﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DynamicSoft.DAL;
using IA.Model.Setup.Other.Pathology_Setup;
using System.Data.SqlClient;
using System.Data;

namespace IA.BIZ.Setup.Pathology_Setup
{
   public class PathologySetup
    {
        
        public static int savePathologyReferenceCategory(PATHOLOGY_REFERENCE_CATEGORY objPathologyReferenceCategory)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATHOLOGY_REFERENCE_CATEGORY_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_REFERENCE_CATEGORY_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@PATHOLOGY_REFERENCE_CATEGORY_NAME"].Value = objPathologyReferenceCategory.PATHOLOGY_REFERENCE_CATEGORY_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objPathologyReferenceCategory.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdatePathologyReferenceCategory(PATHOLOGY_REFERENCE_CATEGORY objPathologyReferenceCategory)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATHOLOGY_REFERENCE_CATEGORY_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_REFERENCE_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_REFERENCE_CATEGORY_ID"].Value = objPathologyReferenceCategory.PATHOLOGY_REFERENCE_CATEGORY_ID;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_REFERENCE_CATEGORY_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@PATHOLOGY_REFERENCE_CATEGORY_NAME"].Value = objPathologyReferenceCategory.PATHOLOGY_REFERENCE_CATEGORY_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objPathologyReferenceCategory.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeletePathologyReferenceCategory(PATHOLOGY_REFERENCE_CATEGORY objPathologyReferenceCategory)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATHOLOGY_REFERENCE_CATEGORY_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_REFERENCE_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_REFERENCE_CATEGORY_ID"].Value = objPathologyReferenceCategory.PATHOLOGY_REFERENCE_CATEGORY_ID;
                
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int savePathologyReferencePerson(PATHOLOGY_REFERENCE_PERSON objPathologyReferencePerson)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATHOLOGY_REFERENCE_PERSON_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_REFERENCE_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_REFERENCE_CATEGORY_ID"].Value = objPathologyReferencePerson.PATHOLOGY_REFERENCE_CATEGORY_ID;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_REFERENCE_PERSON_NAME", SqlDbType.NVarChar,30));
                cmd.Parameters["@PATHOLOGY_REFERENCE_PERSON_NAME"].Value = objPathologyReferencePerson.PATHOLOGY_REFERENCE_PERSON_NAME;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_REFERENCE_PERSON_ADDRESS", SqlDbType.NVarChar,30));
                cmd.Parameters["@PATHOLOGY_REFERENCE_PERSON_ADDRESS"].Value = objPathologyReferencePerson.PATHOLOGY_REFERENCE_PERSON_ADDRESS;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_REFERENCE_PERSON_CONTACT", SqlDbType.NVarChar,30));
                cmd.Parameters["@PATHOLOGY_REFERENCE_PERSON_CONTACT"].Value = objPathologyReferencePerson.PATHOLOGY_REFERENCE_PERSON_CONTACT;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TESTWISE_COMMISSION", SqlDbType.Decimal));
                cmd.Parameters["@PATHOLOGY_TESTWISE_COMMISSION"].Value = objPathologyReferencePerson.PATHOLOGY_TESTWISE_COMMISSION;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdatePathologyReferencePerson(PATHOLOGY_REFERENCE_PERSON objPathologyReferencePerson)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATHOLOGY_REFERENCE_PERSON_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_REFERENCE_PERSON_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_REFERENCE_PERSON_ID"].Value = objPathologyReferencePerson.PATHOLOGY_REFERENCE_PERSON_ID;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_REFERENCE_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_REFERENCE_CATEGORY_ID"].Value = objPathologyReferencePerson.PATHOLOGY_REFERENCE_CATEGORY_ID;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_REFERENCE_PERSON_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@PATHOLOGY_REFERENCE_PERSON_NAME"].Value = objPathologyReferencePerson.PATHOLOGY_REFERENCE_PERSON_NAME;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_REFERENCE_PERSON_ADDRESS", SqlDbType.NVarChar, 30));
                cmd.Parameters["@PATHOLOGY_REFERENCE_PERSON_ADDRESS"].Value = objPathologyReferencePerson.PATHOLOGY_REFERENCE_PERSON_ADDRESS;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_REFERENCE_PERSON_CONTACT", SqlDbType.NVarChar, 30));
                cmd.Parameters["@PATHOLOGY_REFERENCE_PERSON_CONTACT"].Value = objPathologyReferencePerson.PATHOLOGY_REFERENCE_PERSON_CONTACT;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TESTWISE_COMMISSION", SqlDbType.Decimal));
                cmd.Parameters["@PATHOLOGY_TESTWISE_COMMISSION"].Value = objPathologyReferencePerson.PATHOLOGY_TESTWISE_COMMISSION;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeletePathologyReferencePerson(PATHOLOGY_REFERENCE_PERSON objPathologyReferencePerson)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATHOLOGY_REFERENCE_PERSON_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_REFERENCE_PERSON_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_REFERENCE_PERSON_ID"].Value = objPathologyReferencePerson.PATHOLOGY_REFERENCE_PERSON_ID;
                
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int savePathologySpecimen(PATHOLOGY_SPECIMAN objPathologySpecimen)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATHOLOGY_SPECIMAN_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_SPECIMAN_NAME", SqlDbType.NVarChar,30));
                cmd.Parameters["@PATHOLOGY_SPECIMAN_NAME"].Value = objPathologySpecimen.PATHOLOGY_SPECIMAN_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objPathologySpecimen.STATUS_FLAG;
                conn.Open();

                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdatePathologySpecimen(PATHOLOGY_SPECIMAN objPathologySpecimen)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATHOLOGY_SPECIMAN_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_SPECIMAN_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_SPECIMAN_ID"].Value = objPathologySpecimen.PATHOLOGY_SPECIMAN_ID;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_SPECIMAN_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@PATHOLOGY_SPECIMAN_NAME"].Value = objPathologySpecimen.PATHOLOGY_SPECIMAN_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objPathologySpecimen.STATUS_FLAG;
                conn.Open();

                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeletePathologySpecimen(PATHOLOGY_SPECIMAN objPathologySpecimen)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATHOLOGY_SPECIMAN_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_SPECIMAN_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_SPECIMAN_ID"].Value = objPathologySpecimen.PATHOLOGY_SPECIMAN_ID;
                ;
                conn.Open();

                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int savePathologyTestComments(PATHOLOGY_TEST_COMMENTS objPathologyTestComments)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATHOLOGY_TEST_COMMENTS_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_COMMENTS_COMMENTS", SqlDbType.NVarChar,50));
                cmd.Parameters["@PATHOLOGY_TEST_COMMENTS_COMMENTS"].Value = objPathologyTestComments.PATHOLOGY_TEST_COMMENTS_COMMENTS;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int UpdatePathologyTestComments(PATHOLOGY_TEST_COMMENTS objPathologyTestComments)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATHOLOGY_TEST_COMMENTS_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_COMMENTS_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_TEST_COMMENTS_ID"].Value = objPathologyTestComments.PATHOLOGY_TEST_COMMENTS_ID;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_COMMENTS_COMMENTS", SqlDbType.NVarChar, 50));
                cmd.Parameters["@PATHOLOGY_TEST_COMMENTS_COMMENTS"].Value = objPathologyTestComments.PATHOLOGY_TEST_COMMENTS_COMMENTS;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeletePathologyTestComments(PATHOLOGY_TEST_COMMENTS objPathologyTestComments)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATHOLOGY_TEST_COMMENTS_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_COMMENTS_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_TEST_COMMENTS_ID"].Value = objPathologyTestComments.PATHOLOGY_TEST_COMMENTS_ID;
               
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int savePathologyTestItemCategory(PATHOLOGY_TEST_ITEM_CATEGORY objPathologyTestItemCategory)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATHOLOGY_TEST_ITEM_CATEGORY_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_CATEGORY_NAME", SqlDbType.NVarChar,30));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_CATEGORY_NAME"].Value = objPathologyTestItemCategory.PATHOLOGY_TEST_ITEM_CATEGORY_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objPathologyTestItemCategory.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }

        }

        public static int UpdatePathologyTestItemCategory(PATHOLOGY_TEST_ITEM_CATEGORY objPathologyTestItemCategory)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATHOLOGY_TEST_ITEM_CATEGORY_U";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_CATEGORY_ID"].Value = objPathologyTestItemCategory.PATHOLOGY_TEST_ITEM_CATEGORY_ID;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_CATEGORY_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_CATEGORY_NAME"].Value = objPathologyTestItemCategory.PATHOLOGY_TEST_ITEM_CATEGORY_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objPathologyTestItemCategory.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeletePathologyTestItemCategory(PATHOLOGY_TEST_ITEM_CATEGORY objPathologyTestItemCategory)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATHOLOGY_TEST_ITEM_CATEGORY_D";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_CATEGORY_ID"].Value = objPathologyTestItemCategory.PATHOLOGY_TEST_ITEM_CATEGORY_ID;
             
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }


        public static int savePathologyTestItemSubCategory(PATHOLOGY_TEST_ITEM_SUB_CATEGORY objPathologyTestItemSubCategory)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATHOLOGY_TEST_ITEM_SUB_CATEGORY_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_CATEGORY_ID"].Value = objPathologyTestItemSubCategory.PATHOLOGY_TEST_ITEM_CATEGORY_ID;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_SUB_CATEGORY_NAME", SqlDbType.NVarChar,30));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_SUB_CATEGORY_NAME"].Value = objPathologyTestItemSubCategory.PATHOLOGY_TEST_ITEM_SUB_CATEGORY_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objPathologyTestItemSubCategory.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }

        }

        public static int UpdatePathologyTestItemSubCategory(PATHOLOGY_TEST_ITEM_SUB_CATEGORY objPathologyTestItemSubCategory)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATHOLOGY_TEST_ITEM_SUB_CATEGORY_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID"].Value = objPathologyTestItemSubCategory.PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_CATEGORY_ID"].Value = objPathologyTestItemSubCategory.PATHOLOGY_TEST_ITEM_CATEGORY_ID;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_SUB_CATEGORY_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_SUB_CATEGORY_NAME"].Value = objPathologyTestItemSubCategory.PATHOLOGY_TEST_ITEM_SUB_CATEGORY_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objPathologyTestItemSubCategory.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeletePathologyTestItemSubCategory(PATHOLOGY_TEST_ITEM_SUB_CATEGORY objPathologyTestItemSubCategory)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATHOLOGY_TEST_ITEM_SUB_CATEGORY_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID"].Value = objPathologyTestItemSubCategory.PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID;
            
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int savePathologyTestItem(PATHOLOGY_TEST_ITEM objPathologyTestItem)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATHOLOGY_TEST_ITEM_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_CATEGORY_ID"].Value = objPathologyTestItem.PATHOLOGY_TEST_ITEM_CATEGORY_ID;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID"].Value = objPathologyTestItem.PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_SPECIMEN_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_SPECIMEN_ID"].Value = objPathologyTestItem.PATHOLOGY_TEST_ITEM_SPECIMEN_ID;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_NAME", SqlDbType.NVarChar,30));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_NAME"].Value = objPathologyTestItem.PATHOLOGY_TEST_ITEM_NAME;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_PRICE_AMOUNT", SqlDbType.Decimal));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_PRICE_AMOUNT"].Value = objPathologyTestItem.PATHOLOGY_TEST_ITEM_PRICE_AMOUNT;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_PRICE_DISCOUNT", SqlDbType.Decimal));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_PRICE_DISCOUNT"].Value = objPathologyTestItem.PATHOLOGY_TEST_ITEM_PRICE_DISCOUNT;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar,30));
                cmd.Parameters["@STATUS_FLAG"].Value = objPathologyTestItem.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdatePathologyTestItem(PATHOLOGY_TEST_ITEM objPathologyTestItem)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATHOLOGY_TEST_ITEM_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_ID"].Value = objPathologyTestItem.PATHOLOGY_TEST_ITEM_ID;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_CATEGORY_ID"].Value = objPathologyTestItem.PATHOLOGY_TEST_ITEM_CATEGORY_ID;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID"].Value = objPathologyTestItem.PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_SPECIMEN_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_SPECIMEN_ID"].Value = objPathologyTestItem.PATHOLOGY_TEST_ITEM_SPECIMEN_ID;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_NAME"].Value = objPathologyTestItem.PATHOLOGY_TEST_ITEM_NAME;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_PRICE_AMOUNT", SqlDbType.Decimal));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_PRICE_AMOUNT"].Value = objPathologyTestItem.PATHOLOGY_TEST_ITEM_PRICE_AMOUNT;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_PRICE_DISCOUNT", SqlDbType.Decimal));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_PRICE_DISCOUNT"].Value = objPathologyTestItem.PATHOLOGY_TEST_ITEM_PRICE_DISCOUNT;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objPathologyTestItem.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeletePathologyTestItem(PATHOLOGY_TEST_ITEM objPathologyTestItem)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATHOLOGY_TEST_ITEM_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATHOLOGY_TEST_ITEM_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATHOLOGY_TEST_ITEM_ID"].Value = objPathologyTestItem.PATHOLOGY_TEST_ITEM_ID;
               
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }
    }
}
