﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DynamicSoft.DAL;
using IA.Model.Setup.Other.Outdoor_Setup;
using System.Data.SqlClient;
using System.Data;

namespace IA.BIZ.Setup.Outdoor_Setup
{
    public class OutdoorSetup
    {
        public static int saveDoctorCharge(OUTDORR_DOCTOR_CHARGE objDoctorCharge)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_OUTDOOR_DOCTOR_CHARGE_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@DOCTOR_ID"].Value = objDoctorCharge.DOCTOR_ID;
                cmd.Parameters.Add(new SqlParameter("@OUTDOR_DOCTOR_NEW_VISIT_CHARGE", SqlDbType.Decimal));
                cmd.Parameters["@OUTDOR_DOCTOR_NEW_VISIT_CHARGE"].Value = objDoctorCharge.OUTDORR_DOCTOR_NEW_VISIT_CHARGE;
                cmd.Parameters.Add(new SqlParameter("@OUTDOR_DOCTOR_FOLLOW_UP_CHARGE", SqlDbType.Decimal));
                cmd.Parameters["@OUTDOR_DOCTOR_FOLLOW_UP_CHARGE"].Value = objDoctorCharge.OUTDORR_DOCTOR_FOLLOW_UP_CHARGE;
                cmd.Parameters.Add(new SqlParameter("@OUTDOR_DOCTOR_CALL_ON_CHARGE", SqlDbType.Decimal));
                cmd.Parameters["@OUTDOR_DOCTOR_CALL_ON_CHARGE"].Value = objDoctorCharge.OUTDORR_DOCTOR_CALL_ON_CHARGE;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar,30));
                cmd.Parameters["@STATUS_FLAG"].Value = objDoctorCharge.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdateDoctorCharge(OUTDORR_DOCTOR_CHARGE objDoctorCharge)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_OUTDOOR_DOCTOR_CHARGE_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OUTDOR_DOCTOR_CHARGE_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@OUTDOR_DOCTOR_CHARGE_ID"].Value = objDoctorCharge.OUTDORR_DOCTOR_CHARGE_ID;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@DOCTOR_ID"].Value = objDoctorCharge.DOCTOR_ID;
                cmd.Parameters.Add(new SqlParameter("@OUTDOR_DOCTOR_NEW_VISIT_CHARGE", SqlDbType.Decimal));
                cmd.Parameters["@OUTDOR_DOCTOR_NEW_VISIT_CHARGE"].Value = objDoctorCharge.OUTDORR_DOCTOR_NEW_VISIT_CHARGE;
                cmd.Parameters.Add(new SqlParameter("@OUTDOR_DOCTOR_FOLLOW_UP_CHARGE", SqlDbType.Decimal));
                cmd.Parameters["@OUTDOR_DOCTOR_FOLLOW_UP_CHARGE"].Value = objDoctorCharge.OUTDORR_DOCTOR_FOLLOW_UP_CHARGE;
                cmd.Parameters.Add(new SqlParameter("@OUTDOR_DOCTOR_CALL_ON_CHARGE", SqlDbType.Decimal));
                cmd.Parameters["@OUTDOR_DOCTOR_CALL_ON_CHARGE"].Value = objDoctorCharge.OUTDORR_DOCTOR_CALL_ON_CHARGE;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objDoctorCharge.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteDoctorCharge(OUTDORR_DOCTOR_CHARGE objDoctorCharge)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_OUTDOOR_DOCTOR_CHARGE_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OUTDOR_DOCTOR_CHARGE_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@OUTDOR_DOCTOR_CHARGE_ID"].Value = objDoctorCharge.OUTDORR_DOCTOR_CHARGE_ID;
                
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

    }
}
