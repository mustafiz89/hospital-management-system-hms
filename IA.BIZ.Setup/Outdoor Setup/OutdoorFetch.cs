﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IA.Model.Setup.Other.Outdoor_Setup;
using System.Data.SqlClient;
using DynamicSoft.DAL;
using System.Data;


namespace IA.BIZ.Setup.Outdoor_Setup
{
    public class OutdoorFetch
    {
        public static List<OUTDORR_DOCTOR_CHARGE> getALLDoctorCharge()
        {
            List<OUTDORR_DOCTOR_CHARGE> objDoctorCharge = new List<OUTDORR_DOCTOR_CHARGE>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_OUTDOOR_DOCTOR_CHARGE_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        OUTDORR_DOCTOR_CHARGE obj = new OUTDORR_DOCTOR_CHARGE();
                        obj.OUTDORR_DOCTOR_CHARGE_ID = new Guid(dr["OUTDOR_DOCTOR_CHARGE_ID"].ToString());
                        obj.DOCTOR_ID = new Guid(dr["DOCTOR_ID"].ToString());
                        obj.OUTDORR_DOCTOR_NEW_VISIT_CHARGE = decimal.Parse(dr["OUTDOR_DOCTOR_NEW_VISIT_CHARGE"].ToString());
                        obj.OUTDORR_DOCTOR_FOLLOW_UP_CHARGE = decimal.Parse(dr["OUTDOR_DOCTOR_FOLLOW_UP_CHARGE"].ToString());
                        obj.OUTDORR_DOCTOR_CALL_ON_CHARGE = decimal.Parse(dr["OUTDOR_DOCTOR_CALL_ON_CHARGE"].ToString());
                        obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                        obj.DOCTOR_NAME = dr["DOCTOR_NAME"].ToString();
                        objDoctorCharge.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objDoctorCharge;
        }
    }
}
