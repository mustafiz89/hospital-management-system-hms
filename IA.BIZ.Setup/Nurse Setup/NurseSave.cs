﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DynamicSoft.DAL;
using IA.Model.Setup.Other.Nurse_Setup;
using System.Data.SqlClient;
using System.Data;

namespace IA.BIZ.Setup.Nurse_Setup
{
    public class NurseSave
    {
        public static int saveNurse(NURSE objNurse)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_NURSE_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@NURSE_NAME", SqlDbType.NVarChar,30));
                cmd.Parameters["@NURSE_NAME"].Value = objNurse.NURSE_NAME;
                cmd.Parameters.Add(new SqlParameter("@FLOOR_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@FLOOR_ID"].Value = objNurse.FLOOR_ID;
                cmd.Parameters.Add(new SqlParameter("@NURSE_ADDRESS", SqlDbType.NVarChar,30));
                cmd.Parameters["@NURSE_ADDRESS"].Value = objNurse.NURSE_ADDRESS;
                cmd.Parameters.Add(new SqlParameter("@NURSE_CONTACT", SqlDbType.NVarChar, 30));
                cmd.Parameters["@NURSE_CONTACT"].Value = objNurse.NURSE_CONTACT;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdateNurse(NURSE objNurse)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_NURSE_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@NURSE_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@NURSE_ID"].Value = objNurse.NURSE_ID;
                cmd.Parameters.Add(new SqlParameter("@NURSE_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@NURSE_NAME"].Value = objNurse.NURSE_NAME;
                cmd.Parameters.Add(new SqlParameter("@FLOOR_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@FLOOR_ID"].Value = objNurse.FLOOR_ID;
                cmd.Parameters.Add(new SqlParameter("@NURSE_ADDRESS", SqlDbType.NVarChar, 30));
                cmd.Parameters["@NURSE_ADDRESS"].Value = objNurse.NURSE_ADDRESS;
                cmd.Parameters.Add(new SqlParameter("@NURSE_CONTACT", SqlDbType.NVarChar, 30));
                cmd.Parameters["@NURSE_CONTACT"].Value = objNurse.NURSE_CONTACT;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteNurse(NURSE objNurse)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_NURSE_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@NURSE_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@NURSE_ID"].Value = objNurse.NURSE_ID;
                
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        //public static int saveNurse(global::IA.Model.Setup.Other.Nurse_Setup.NURSE objNurse)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
