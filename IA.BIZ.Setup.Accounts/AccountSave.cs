﻿using System;
using System.Collections.Generic;
using System.Text;
using IA.Model.Setup.Accounts;
using System.Data;
using System.Data.SqlClient;
using IA.Provider;

using IA.Model.Setup.Other;
//using IA.BIZ.Setup.Other;


namespace IA.BIZ.Setup.Accounts
{
    public class AccountSave
    {
        public static int DeleteBANK(ACCOUNTS objACCOUNTS)
        {
            int i = 0;
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_BANK_D";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@ACCOUNTS_CODE", SqlDbType.VarChar, 50));
            objSqlCommand.Parameters["@ACCOUNTS_CODE"].Value = objACCOUNTS.ACCOUNTS_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = objACCOUNTS.COMPANY_ID;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();

            }
            catch { }
            finally
            {
                conn.Close();
            }
            return i;
        }
        public static int DeleteACCOUNTS(ACCOUNTS objACCOUNTS)
        {
            int i = 0;
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_ACCOUNTS_D";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@ACCOUNTS_CODE", SqlDbType.VarChar, 50));
            objSqlCommand.Parameters["@ACCOUNTS_CODE"].Value = objACCOUNTS.ACCOUNTS_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = objACCOUNTS.COMPANY_ID;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();

            }
            catch { }
            finally
            {
                conn.Close();
            }
            return i;
        }
        public static int DeleteACCOUNT_SUBSIDIARY(ACCOUNT_SUBSIDIARY objACCOUNT_SUBSIDIARY)
        {
            int i = 0;
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_ACCOUNT_SUBSIDIARY_D";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@SUBSIDIARY_CODE", SqlDbType.VarChar, 50));
            objSqlCommand.Parameters["@SUBSIDIARY_CODE"].Value = objACCOUNT_SUBSIDIARY.SUBSIDIARY_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = objACCOUNT_SUBSIDIARY.COMPANY_ID;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();

            }
            catch { }
            finally
            {
                conn.Close();
            }
            return i;
        }
        public static int DeleteCONTROL_ACCOUNT(CONTROL_ACCOUNT objCONTROL_ACCOUNT)
        {
            int i = 0;
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_CONTROL_ACCOUNT_D";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@CONTROL_CODE", SqlDbType.VarChar, 50));
            objSqlCommand.Parameters["@CONTROL_CODE"].Value = objCONTROL_ACCOUNT.CONTROL_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = objCONTROL_ACCOUNT.COMPANY_ID;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();

            }
            catch { }
            finally
            {
                conn.Close();
            }
            return i;
        }
        public static int UpdateACCOUNTS(ACCOUNTS objACCOUNTS)
        {
            int i = 0;
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_ACCOUNTS_U";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@ACCOUNTS_CODE", SqlDbType.VarChar, 40));
            objSqlCommand.Parameters["@ACCOUNTS_CODE"].Value = objACCOUNTS.ACCOUNTS_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@DESCRIPTION", SqlDbType.VarChar, 40));
            objSqlCommand.Parameters["@DESCRIPTION"].Value = objACCOUNTS.DESCRIPTION;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = objACCOUNTS.COMPANY_ID;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();

            }
            catch { }
            finally
            {
                conn.Close();
            }
            return i;
        }
        public static int UpdateACCOUNT_SUBSIDIARY(ACCOUNT_SUBSIDIARY objACCOUNT_SUBSIDIARY)
        {
            int i = 0;
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_ACCOUNT_SUBSIDIARY_U";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@SUBSIDIARY_CODE", SqlDbType.VarChar, 40));
            objSqlCommand.Parameters["@SUBSIDIARY_CODE"].Value = objACCOUNT_SUBSIDIARY.SUBSIDIARY_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@SUBSIDIARY_NAME", SqlDbType.VarChar, 40));
            objSqlCommand.Parameters["@SUBSIDIARY_NAME"].Value = objACCOUNT_SUBSIDIARY.SUBSIDIARY_NAME;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = objACCOUNT_SUBSIDIARY.COMPANY_ID;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();

            }
            catch { }
            finally
            {
                conn.Close();
            }
            return i;
        }
        public static int UpdateCONTROL_ACCOUNT(CONTROL_ACCOUNT objCONTROL_ACCOUNT)
        {
            int i = 0;
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_CONTROL_ACCOUNT_U";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@CONTROL_CODE", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@CONTROL_CODE"].Value = objCONTROL_ACCOUNT.CONTROL_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@CONTROL_NAME", SqlDbType.VarChar, 50));
            objSqlCommand.Parameters["@CONTROL_NAME"].Value = objCONTROL_ACCOUNT.CONTROL_NAME;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = objCONTROL_ACCOUNT.COMPANY_ID;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();

            }
            catch { }
            finally
            {
                conn.Close();
            }
            return i;
        }
        public static int EditACCOUNT_GROUP(ACCOUNT_GROUP objACCOUNT_GROUP)
        {
            int i = 0;
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_ACCOUNT_GROUP_U";
            objSqlCommand.CommandType = CommandType.StoredProcedure;


            objSqlCommand.Parameters.Add(new SqlParameter("@ACCOUNT_GROUP_CODE", SqlDbType.VarChar, 150));
            objSqlCommand.Parameters["@ACCOUNT_GROUP_CODE"].Value = objACCOUNT_GROUP.ACCOUNT_GROUP_CODE;


            objSqlCommand.Parameters.Add(new SqlParameter("@ACCOUNT_GROUP_NAME", SqlDbType.VarChar, 150));
            objSqlCommand.Parameters["@ACCOUNT_GROUP_NAME"].Value = objACCOUNT_GROUP.ACCOUNT_GROUP_NAME;

            objSqlCommand.Parameters.Add(new SqlParameter("@ACCOUNT_GROUP_TYPE", SqlDbType.VarChar, 150));
            objSqlCommand.Parameters["@ACCOUNT_GROUP_TYPE"].Value = objACCOUNT_GROUP.ACCOUNT_GROUP_TYPE;


            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();

            }
            catch { }
            finally
            {
                conn.Close();
            }
            return i;
        }
        public static int SaveControlGroup(CONTROL_ACCOUNT objCONTROL_ACCOUNT)
        {
            int i = 0;
            SqlConnection con = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_CONTROL_ACCOUNT_I";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = con;
            objSqlCommand.Parameters.Add(new SqlParameter("@CONTROL_CODE", SqlDbType.VarChar, 5));
            objSqlCommand.Parameters["@CONTROL_CODE"].Value = objCONTROL_ACCOUNT.CONTROL_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@CONTROL_NAME", SqlDbType.VarChar, 50));
            objSqlCommand.Parameters["@CONTROL_NAME"].Value = objCONTROL_ACCOUNT.CONTROL_NAME;
            objSqlCommand.Parameters.Add(new SqlParameter("@ACCOUNT_GROUP_CODE", SqlDbType.VarChar, 3));
            objSqlCommand.Parameters["@ACCOUNT_GROUP_CODE"].Value = objCONTROL_ACCOUNT.ACCOUNT_GROUP_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.Int));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = objCONTROL_ACCOUNT.COMPANY_ID;

            objSqlCommand.Parameters.Add(new SqlParameter("@NAME_VALUE_LIST", SqlDbType.Char));
            objSqlCommand.Parameters["@NAME_VALUE_LIST"].Value = objCONTROL_ACCOUNT.NAME_VALUE_LIST;
            con.Open();
            //SqlDataReader dr = objSqlCommand.ExecuteReader();           
            try
            {
                //    while (dr.Read())
                //    {
                //        objCONTROL_ACCOUNT.CONTROL_CODE_NEW = dr["CONTROL_CODE_NEW"].ToString();
                //    }
                i = objSqlCommand.ExecuteNonQuery();
            }
            catch//(Exception ex) 
            { }
            finally
            {
                con.Close();
            }
            return i;
        }
        public static CONTROL_ACCOUNT SaveControlGroup1(CONTROL_ACCOUNT objCONTROL_ACCOUNT)
        {
            int i = 0;
            SqlConnection con = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_CONTROL_ACCOUNT_I";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = con;
            objSqlCommand.Parameters.Add("@CONTROL_CODE", SqlDbType.VarChar, 50);
            objSqlCommand.Parameters["@CONTROL_CODE"].Direction = ParameterDirection.Output;
            objSqlCommand.Parameters.Add(new SqlParameter("@CONTROL_NAME", SqlDbType.VarChar, 50));
            objSqlCommand.Parameters["@CONTROL_NAME"].Value = objCONTROL_ACCOUNT.CONTROL_NAME;
            objSqlCommand.Parameters.Add(new SqlParameter("@ACCOUNT_GROUP_CODE", SqlDbType.VarChar, 3));
            objSqlCommand.Parameters["@ACCOUNT_GROUP_CODE"].Value = objCONTROL_ACCOUNT.ACCOUNT_GROUP_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.Int));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = objCONTROL_ACCOUNT.COMPANY_ID;

            objSqlCommand.Parameters.Add(new SqlParameter("@NAME_VALUE_LIST", SqlDbType.Char));
            objSqlCommand.Parameters["@NAME_VALUE_LIST"].Value = objCONTROL_ACCOUNT.NAME_VALUE_LIST;
            con.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();
                objCONTROL_ACCOUNT.CONTROL_CODE = objSqlCommand.Parameters["@CONTROL_CODE"].Value.ToString();
            }
            catch//(Exception ex) 
            { }
            finally
            {
                con.Close();
            }
            return objCONTROL_ACCOUNT;
        }
        public static int SaveAccountGroup(ACCOUNT_GROUP objACCOUNT_GROUP)
        {
            int i = 0;
            SqlConnection con = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_ACCOUNT_GROUP_I";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = con;
            objSqlCommand.Parameters.Add(new SqlParameter("@ACCOUNT_GROUP_CODE", SqlDbType.VarChar, 5));
            objSqlCommand.Parameters["@ACCOUNT_GROUP_CODE"].Value = objACCOUNT_GROUP.ACCOUNT_GROUP_CODE;

            objSqlCommand.Parameters.Add(new SqlParameter("@ACCOUNT_GROUP_NAME", SqlDbType.VarChar, 50));
            objSqlCommand.Parameters["@ACCOUNT_GROUP_NAME"].Value = objACCOUNT_GROUP.ACCOUNT_GROUP_NAME;

            objSqlCommand.Parameters.Add(new SqlParameter("@ACCOUNT_GROUP_TYPE", SqlDbType.VarChar, 3));
            objSqlCommand.Parameters["@ACCOUNT_GROUP_TYPE"].Value = objACCOUNT_GROUP.ACCOUNT_GROUP_TYPE;

            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.Int));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = objACCOUNT_GROUP.COMPANY_ID;



            con.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();
            }
            catch//(Exception ex) 
            { }
            finally
            {
                con.Close();
            }
            return i;
        }
        public static ACCOUNT_SUBSIDIARY SaveSubsidiaryAccounts(ACCOUNT_SUBSIDIARY objACCOUNT_SUBSIDIARY)
        {
            int i = 0;
            SqlConnection con = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            //objSqlCommand.CommandText = "FSP_ACCOUNT_SUBSIDIARY_I";
            objSqlCommand.CommandText = "FSP_ACCOUNT_SUBSIDIARY_I";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = con;
            objSqlCommand.Parameters.Add("@SUBSIDIARY_CODE", SqlDbType.VarChar, 15);
            objSqlCommand.Parameters["@SUBSIDIARY_CODE"].Direction = ParameterDirection.Output;
            objSqlCommand.Parameters.Add(new SqlParameter("@ACCOUNT_GROUP_CODE", SqlDbType.VarChar, 5));
            objSqlCommand.Parameters["@ACCOUNT_GROUP_CODE"].Value = objACCOUNT_SUBSIDIARY.ACCOUNT_GROUP_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@CONTROL_CODE", SqlDbType.VarChar, 5));
            objSqlCommand.Parameters["@CONTROL_CODE"].Value = objACCOUNT_SUBSIDIARY.CONTROL_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@SUBSIDIARY_NAME", SqlDbType.VarChar, 50));
            objSqlCommand.Parameters["@SUBSIDIARY_NAME"].Value = objACCOUNT_SUBSIDIARY.SUBSIDIARY_NAME;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = objACCOUNT_SUBSIDIARY.COMPANY_ID;
            con.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();
                objACCOUNT_SUBSIDIARY.SUBSIDIARY_CODE = objSqlCommand.Parameters["@SUBSIDIARY_CODE"].Value.ToString();
            }
            catch//(Exception ex) 
            { }
            finally
            {
                con.Close();
            }
            return objACCOUNT_SUBSIDIARY;
        }
        public static ACCOUNTS SaveACCOUNTS(ACCOUNTS objACCOUNTS)
        {
            int i = 0;
            SqlConnection con = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_ACCOUNTS_I";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = con;
            objSqlCommand.Parameters.Add("@ACCOUNTS_CODE", SqlDbType.VarChar, 30);
            objSqlCommand.Parameters["@ACCOUNTS_CODE"].Direction = ParameterDirection.Output;
            objSqlCommand.Parameters.Add(new SqlParameter("@GROUP_CODE", SqlDbType.VarChar, 5));
            objSqlCommand.Parameters["@GROUP_CODE"].Value = objACCOUNTS.GROUP_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@CONTROL_CODE", SqlDbType.VarChar, 5));
            objSqlCommand.Parameters["@CONTROL_CODE"].Value = objACCOUNTS.CONTROL_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@SUBSIDIARY_CODE", SqlDbType.VarChar, 15));
            objSqlCommand.Parameters["@SUBSIDIARY_CODE"].Value = objACCOUNTS.SUBSIDIARY_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@DESCRIPTION", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@DESCRIPTION"].Value = objACCOUNTS.DESCRIPTION;
            objSqlCommand.Parameters.Add(new SqlParameter("@OPENING_DATE", SqlDbType.DateTime));
            objSqlCommand.Parameters["@OPENING_DATE"].Value = objACCOUNTS.OPENING_DATE;
            objSqlCommand.Parameters.Add(new SqlParameter("@OPENING_TIME", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@OPENING_TIME"].Value = objACCOUNTS.OPENING_TIME;
            objSqlCommand.Parameters.Add(new SqlParameter("@OPENING_BALANCE", SqlDbType.Decimal));
            objSqlCommand.Parameters["@OPENING_BALANCE"].Value = objACCOUNTS.OPENING_BALANCE;
            //objSqlCommand.Parameters.Add(new SqlParameter("@BALANCE_TYPE", SqlDbType.VarChar, 3));
            //objSqlCommand.Parameters["@BALANCE_TYPE"].Value = objACCOUNTS.BALANCE_TYPE;
            objSqlCommand.Parameters.Add(new SqlParameter("@STATEMENT_TYPE", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@STATEMENT_TYPE"].Value = objACCOUNTS.STATEMENT_TYPE;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = objACCOUNTS.COMPANY_ID;
            objSqlCommand.Parameters.Add(new SqlParameter("@NOTES", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@NOTES"].Value = objACCOUNTS.NOTES;
            objSqlCommand.Parameters.Add(new SqlParameter("@MANDATORY", SqlDbType.Int));
            objSqlCommand.Parameters["@MANDATORY"].Value = objACCOUNTS.MANDATORY;
            objSqlCommand.Parameters.Add(new SqlParameter("@BRANCH_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@BRANCH_ID"].Value = objACCOUNTS.BRANCH_ID;
            con.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();
                objACCOUNTS.ACCOUNTS_CODE = objSqlCommand.Parameters["@ACCOUNTS_CODE"].Value.ToString();
            }
            catch//(Exception ex) 
            { }
            finally
            {
                con.Close();
            }
            return objACCOUNTS;
        }
        public static ACCOUNTS SaveCASHACCOUNTS(ACCOUNTS objACCOUNTS)
        {
            int i = 0;
            SqlConnection con = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_CASH_ACCOUNTS_I";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = con;
            objSqlCommand.Parameters.Add("@ACCOUNTS_CODE", SqlDbType.VarChar, 30);
            objSqlCommand.Parameters["@ACCOUNTS_CODE"].Direction = ParameterDirection.Output;
            objSqlCommand.Parameters.Add(new SqlParameter("@GROUP_CODE", SqlDbType.VarChar, 5));
            objSqlCommand.Parameters["@GROUP_CODE"].Value = objACCOUNTS.GROUP_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@CONTROL_CODE", SqlDbType.VarChar, 5));
            objSqlCommand.Parameters["@CONTROL_CODE"].Value = objACCOUNTS.CONTROL_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@SUBSIDIARY_CODE", SqlDbType.VarChar, 15));
            objSqlCommand.Parameters["@SUBSIDIARY_CODE"].Value = objACCOUNTS.SUBSIDIARY_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@DESCRIPTION", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@DESCRIPTION"].Value = objACCOUNTS.DESCRIPTION;
            objSqlCommand.Parameters.Add(new SqlParameter("@OPENING_DATE", SqlDbType.DateTime));
            objSqlCommand.Parameters["@OPENING_DATE"].Value = objACCOUNTS.OPENING_DATE;
            objSqlCommand.Parameters.Add(new SqlParameter("@OPENING_TIME", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@OPENING_TIME"].Value = objACCOUNTS.OPENING_TIME;
            objSqlCommand.Parameters.Add(new SqlParameter("@OPENING_BALANCE", SqlDbType.Decimal));
            objSqlCommand.Parameters["@OPENING_BALANCE"].Value = objACCOUNTS.OPENING_BALANCE;
            //objSqlCommand.Parameters.Add(new SqlParameter("@BALANCE_TYPE", SqlDbType.VarChar, 3));
            //objSqlCommand.Parameters["@BALANCE_TYPE"].Value = objACCOUNTS.BALANCE_TYPE;
            objSqlCommand.Parameters.Add(new SqlParameter("@STATEMENT_TYPE", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@STATEMENT_TYPE"].Value = objACCOUNTS.STATEMENT_TYPE;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = objACCOUNTS.COMPANY_ID;
            objSqlCommand.Parameters.Add(new SqlParameter("@NOTES", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@NOTES"].Value = objACCOUNTS.NOTES;
            objSqlCommand.Parameters.Add(new SqlParameter("@MANDATORY", SqlDbType.Int));
            objSqlCommand.Parameters["@MANDATORY"].Value = objACCOUNTS.MANDATORY;
            objSqlCommand.Parameters.Add(new SqlParameter("@BRANCH_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@BRANCH_ID"].Value = objACCOUNTS.BRANCH_ID;

            con.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();
                objACCOUNTS.ACCOUNTS_CODE = objSqlCommand.Parameters["@ACCOUNTS_CODE"].Value.ToString();
            }
            catch//(Exception ex) 
            { }
            finally
            {
                con.Close();
            }
            return objACCOUNTS;
        }
        public static int UpdateBANK(BANK objBANK)
        {
            int i = 0;
            SqlConnection con = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_BANK_U";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = con;
            objSqlCommand.Parameters.Add("@ACCOUNTS_CODE", SqlDbType.VarChar, 30);
            objSqlCommand.Parameters["@ACCOUNTS_CODE"].Value = objBANK.ACCOUNTS_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@DESCRIPTION", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@DESCRIPTION"].Value = objBANK.BANK_NAME;
            objSqlCommand.Parameters.Add(new SqlParameter("@BANK_ADDRESS", SqlDbType.VarChar, 50));
            objSqlCommand.Parameters["@BANK_ADDRESS"].Value = objBANK.BANK_ADDRESS;
            objSqlCommand.Parameters.Add(new SqlParameter("@COUNTRY_ID", SqlDbType.UniqueIdentifier));
            objSqlCommand.Parameters["@COUNTRY_ID"].Value = objBANK.COUNTRY_ID;
            objSqlCommand.Parameters.Add(new SqlParameter("@BANK_FAX", SqlDbType.VarChar, 20));
            objSqlCommand.Parameters["@BANK_FAX"].Value = objBANK.BANK_FAX;
            objSqlCommand.Parameters.Add(new SqlParameter("@BANK_PHONE", SqlDbType.VarChar, 20));
            objSqlCommand.Parameters["@BANK_PHONE"].Value = objBANK.BANK_PHONE;
            objSqlCommand.Parameters.Add(new SqlParameter("@BANK_EMAIL", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@BANK_EMAIL"].Value = objBANK.BANK_EMAIL;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = objBANK.COMPANY_ID;
            con.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();


            }
            catch//(Exception ex) 
            { }
            finally
            {
                con.Close();
            }
            return i;
        }
        public static int UpdateBANK2(BANK objBANK)
        {
            int i = 0;
            SqlConnection con = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_BANKS_U";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = con;

            objSqlCommand.Parameters.Add("@ACCOUNTS_CODE", SqlDbType.VarChar, 30);
            objSqlCommand.Parameters["@ACCOUNTS_CODE"].Value = objBANK.ACCOUNTS_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@DESCRIPTION", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@DESCRIPTION"].Value = objBANK.BANK_NAME;
            con.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();


            }
            catch//(Exception ex) 
            { }
            finally
            {
                con.Close();
            }
            return i;
        }
        public static int SaveBank(ACCOUNTS objACCOUNTS, BANK objBANK)
        {

            int i = 0;
            SqlConnection con = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_BANK_I";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = con;
            objSqlCommand.Parameters.Add("@ACCOUNTS_CODE", SqlDbType.VarChar, 30);
            objSqlCommand.Parameters["@ACCOUNTS_CODE"].Direction = ParameterDirection.Output;
            objSqlCommand.Parameters.Add(new SqlParameter("@GROUP_CODE", SqlDbType.VarChar, 5));
            objSqlCommand.Parameters["@GROUP_CODE"].Value = objACCOUNTS.GROUP_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@CONTROL_CODE", SqlDbType.VarChar, 5));
            objSqlCommand.Parameters["@CONTROL_CODE"].Value = objACCOUNTS.CONTROL_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@SUBSIDIARY_CODE", SqlDbType.VarChar, 15));
            objSqlCommand.Parameters["@SUBSIDIARY_CODE"].Value = objACCOUNTS.SUBSIDIARY_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@DESCRIPTION", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@DESCRIPTION"].Value = objACCOUNTS.DESCRIPTION;
            objSqlCommand.Parameters.Add(new SqlParameter("@OPENING_DATE", SqlDbType.DateTime));
            objSqlCommand.Parameters["@OPENING_DATE"].Value = objACCOUNTS.OPENING_DATE;
            objSqlCommand.Parameters.Add(new SqlParameter("@OPENING_TIME", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@OPENING_TIME"].Value = objACCOUNTS.OPENING_TIME;
            objSqlCommand.Parameters.Add(new SqlParameter("@OPENING_BALANCE", SqlDbType.Decimal));
            objSqlCommand.Parameters["@OPENING_BALANCE"].Value = objACCOUNTS.OPENING_BALANCE;
            objSqlCommand.Parameters.Add(new SqlParameter("@STATEMENT_TYPE", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@STATEMENT_TYPE"].Value = objACCOUNTS.STATEMENT_TYPE;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = objACCOUNTS.COMPANY_ID;
            objSqlCommand.Parameters.Add(new SqlParameter("@NOTES", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@NOTES"].Value = objACCOUNTS.NOTES;
            objSqlCommand.Parameters.Add(new SqlParameter("@MANDATORY", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@MANDATORY"].Value = objACCOUNTS.MANDATORY;
            objSqlCommand.Parameters.Add(new SqlParameter("@BANK_ADDRESS", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@BANK_ADDRESS"].Value = objBANK.BANK_ADDRESS;
            objSqlCommand.Parameters.Add(new SqlParameter("@BANK_COUNTRY", SqlDbType.UniqueIdentifier));
            objSqlCommand.Parameters["@COUNTRY_ID"].Value = objBANK.COUNTRY_ID;
            objSqlCommand.Parameters.Add(new SqlParameter("@BANK_FAX", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@BANK_FAX"].Value = objBANK.BANK_FAX;
            objSqlCommand.Parameters.Add(new SqlParameter("@BANK_PHONE", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@BANK_PHONE"].Value = objBANK.BANK_PHONE;
            objSqlCommand.Parameters.Add(new SqlParameter("@BANK_EMAIL", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@BANK_EMAIL"].Value = objBANK.BANK_EMAIL;
            objSqlCommand.Parameters.Add(new SqlParameter("@BRANCH_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@BRANCH_ID"].Value = objACCOUNTS.BRANCH_ID;
            con.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();

                objACCOUNTS.ACCOUNTS_CODE = objSqlCommand.Parameters["@ACCOUNTS_CODE"].Value.ToString();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);

            }
            finally
            {
                con.Close();
            }
            return i;
        }
        public static int SaveOpeningBalance(ACCOUNTS objACCOUNTS, SqlCommand objSqlCommand, string VOUCHER_NO)
        {

            int i = 0;
            objSqlCommand.CommandText = "FSP_OPENING_BALANCE_SETUP";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Clear();
            objSqlCommand.Parameters.Add("@ACCOUNTS_CODE", SqlDbType.VarChar, 30);
            objSqlCommand.Parameters["@ACCOUNTS_CODE"].Value = objACCOUNTS.ACCOUNTS_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@OPENING_BALANCE", SqlDbType.Decimal));
            objSqlCommand.Parameters["@OPENING_BALANCE"].Value = objACCOUNTS.OPENING_BALANCE;
            objSqlCommand.Parameters.Add(new SqlParameter("@BALANCE_TYPE", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@BALANCE_TYPE"].Value = objACCOUNTS.BALANCE_TYPE;
            objSqlCommand.Parameters.Add(new SqlParameter("@VOUCHER_NO", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@VOUCHER_NO"].Value = VOUCHER_NO;//9 for VOUCHER_NO;
            objSqlCommand.Parameters.Add(new SqlParameter("@MAKE_BY", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@MAKE_BY"].Value = objACCOUNTS.MAKE_BY;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = objACCOUNTS.COMPANY_ID;
            objSqlCommand.Parameters.Add(new SqlParameter("@BRANCH_ID", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@BRANCH_ID"].Value = objACCOUNTS.BRANCH_ID;
            try
            {
                i = objSqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                i = 0; //MessageBox.Show(ex.Message);

            }
            finally
            {

            }
            return i;
        }
    }
}
