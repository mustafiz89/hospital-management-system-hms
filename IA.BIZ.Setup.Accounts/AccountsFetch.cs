﻿using System;
using System.Collections.Generic;
using System.Text;
using IA.Model.Setup.Accounts;
using System.Data;
using System.Data.SqlClient;
using IA.Provider;
using IA.Model.Setup.Other;


namespace IA.BIZ.Setup.Accounts
{
    public class AccountsFetch
    {
        public static ACCOUNT_GROUP GetACCOUNT_GROUP(string ACCOUNT_GROUP_CODE)
        {
            //List<COUNTRY> objListCOUNTRY = new List<COUNTRY>();
            ACCOUNT_GROUP objACCOUNT_GROUP = new ACCOUNT_GROUP();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_ACCOUNT_GROUP_GK";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@ACCOUNT_GROUP_CODE", SqlDbType.Int));
            objSqlCommand.Parameters["@ACCOUNT_GROUP_CODE"].Value = ACCOUNT_GROUP_CODE;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                SqlDataReader dr = objSqlCommand.ExecuteReader();
                while (dr.Read())
                {
                    // objCOUNTRY.COUNTRY_ID = Convert.ToInt32(dr["COUNTRY_ID"]);
                    objACCOUNT_GROUP.ACCOUNT_GROUP_NAME = dr["ACCOUNT_GROUP_NAME"].ToString();
                    objACCOUNT_GROUP.ACCOUNT_GROUP_TYPE = dr["ACCOUNT_GROUP_TYPE"].ToString();


                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objACCOUNT_GROUP;
        }
        public static List<ACCOUNT_GROUP> GetAllAccountGroup()
        {
            List<ACCOUNT_GROUP> objListACCOUNT_GROUP = new List<ACCOUNT_GROUP>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_ACCOUNT_GROUP_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                SqlDataReader dr = objSqlCommand.ExecuteReader();
                while (dr.Read())
                {
                    ACCOUNT_GROUP objACCOUNT_GROUP = new ACCOUNT_GROUP();
                    objACCOUNT_GROUP.ACCOUNT_GROUP_CODE = dr["ACCOUNT_GROUP_CODE"].ToString();
                    objACCOUNT_GROUP.ACCOUNT_GROUP_NAME = dr["ACCOUNT_GROUP_NAME"].ToString();
                    objACCOUNT_GROUP.ACCOUNT_GROUP_TYPE = dr["ACCOUNT_GROUP_TYPE"].ToString();
                    //objACCOUNT_GROUP.COMPANY_ID = dr["COMPANY_ID"].ToString();
                    objListACCOUNT_GROUP.Add(objACCOUNT_GROUP);
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objListACCOUNT_GROUP;
        }
        //public static List<CONTROL_ACCOUNT> GetAllControlAccount(string ACCOUNT_GROUP_CODE, string NAME_VALUE_LIST)
        //{
        //    List<CONTROL_ACCOUNT> objListCONTROL_ACCOUNT = new List<CONTROL_ACCOUNT>();
        //    SqlConnection conn = ConnectionClass.GetConnection();
        //    SqlCommand objSqlCommand = new SqlCommand();
        //    objSqlCommand.CommandText = "FSP_CONTROL_ACCOUNT_GA";
        //    objSqlCommand.CommandType = CommandType.StoredProcedure;
        //    objSqlCommand.Parameters.Add(new SqlParameter("@ACCOUNT_GROUP_CODE", SqlDbType.VarChar, 5));
        //    objSqlCommand.Parameters["@ACCOUNT_GROUP_CODE"].Value = ACCOUNT_GROUP_CODE;
        //    objSqlCommand.Parameters.Add(new SqlParameter("@NAME_VALUE_LIST", SqlDbType.VarChar, 5));
        //    objSqlCommand.Parameters["@NAME_VALUE_LIST"].Value = NAME_VALUE_LIST;
        //    objSqlCommand.Connection = conn;
        //    conn.Open();
        //    try
        //    {
        //        using (objSqlCommand)
        //        {
        //            SqlDataReader dr = objSqlCommand.ExecuteReader();
        //            while (dr.Read())
        //            {
        //                CONTROL_ACCOUNT objCONTROL_ACCOUNT = new CONTROL_ACCOUNT();
        //                objCONTROL_ACCOUNT.ACCOUNT_GROUP_CODE = dr["ACCOUNT_GROUP_CODE"].ToString();
        //                objCONTROL_ACCOUNT.CONTROL_CODE = dr["CONTROL_CODE"].ToString();
        //                objCONTROL_ACCOUNT.CONTROL_NAME = dr["CONTROL_NAME"].ToString();
        //                objCONTROL_ACCOUNT.ACCOUNT_GROUP_TYPE = dr["ACCOUNT_GROUP_TYPE"].ToString();
        //                objCONTROL_ACCOUNT.ACCOUNT_GROUP_NAME = dr["ACCOUNT_GROUP_NAME"].ToString();
        //                objCONTROL_ACCOUNT.COMPANY_ID = dr["COMPANY_ID"].ToString();
        //                objListCONTROL_ACCOUNT.Add(objCONTROL_ACCOUNT);
        //            }
        //        }
        //    }
        //    catch { }
        //    finally { conn.Close(); }


        //    //SqlConnection conn = ConnectionClass.GetConnection();
        //    //SqlCommand objSqlCommand = new SqlCommand();
        //    //objSqlCommand.CommandText = "FSP_CONTROL_ACCOUNT_GA";
        //    //objSqlCommand.CommandType = CommandType.StoredProcedure;
        //    //objSqlCommand.Parameters.Add(new SqlParameter("@ACCOUNT_GROUP_CODE", SqlDbType.VarChar, 5));
        //    //objSqlCommand.Parameters["@ACCOUNT_GROUP_CODE"].Value = ACCOUNT_GROUP_CODE;
        //    //objSqlCommand.Parameters.Add(new SqlParameter("@NAME_VALUE_LIST", SqlDbType.VarChar, 5));
        //    //objSqlCommand.Parameters["@NAME_VALUE_LIST"].Value = NAME_VALUE_LIST;
        //    //objSqlCommand.Connection = conn;
        //    //conn.Open();
        //    //try
        //    //{
        //    //    SqlDataReader dr = objSqlCommand.ExecuteReader();
        //    //    while (dr.Read())
        //    //    {
        //    //        CONTROL_ACCOUNT objCONTROL_ACCOUNT = new CONTROL_ACCOUNT();
        //    //        objCONTROL_ACCOUNT.ACCOUNT_GROUP_CODE = dr["ACCOUNT_GROUP_CODE"].ToString();
        //    //        objCONTROL_ACCOUNT.CONTROL_CODE = dr["CONTROL_CODE"].ToString();
        //    //        objCONTROL_ACCOUNT.CONTROL_NAME = dr["CONTROL_NAME"].ToString();
        //    //        objCONTROL_ACCOUNT.ACCOUNT_GROUP_TYPE = dr["ACCOUNT_GROUP_TYPE"].ToString();
        //    //        objCONTROL_ACCOUNT.ACCOUNT_GROUP_NAME = dr["ACCOUNT_GROUP_NAME"].ToString();
        //    //        objCONTROL_ACCOUNT.COMPANY_ID = dr["COMPANY_ID"].ToString();
        //    //        objListCONTROL_ACCOUNT.Add(objCONTROL_ACCOUNT);
        //    //    }
        //    //}
        //    //catch { }
        //    //finally
        //    //{
        //    //    conn.Close();
        //    //}
        //    return objListCONTROL_ACCOUNT;
        //}
        public static CONTROL_ACCOUNT GetControlAccount(string CONTROL_CODE, string COMPANY_ID)
        {
            CONTROL_ACCOUNT objCONTROL_ACCOUNT = new CONTROL_ACCOUNT();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_CONTROL_ACCOUNT_GK";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@CONTROL_CODE", SqlDbType.VarChar, 5));
            objSqlCommand.Parameters["@CONTROL_CODE"].Value = CONTROL_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = COMPANY_ID;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                SqlDataReader dr = objSqlCommand.ExecuteReader();
                while (dr.Read())
                {
                    objCONTROL_ACCOUNT.ACCOUNT_GROUP_CODE = dr["ACCOUNT_GROUP_CODE"].ToString();
                    objCONTROL_ACCOUNT.CONTROL_CODE = dr["CONTROL_CODE"].ToString();
                    objCONTROL_ACCOUNT.CONTROL_NAME = dr["CONTROL_NAME"].ToString();
                    objCONTROL_ACCOUNT.ACCOUNT_GROUP_TYPE = dr["ACCOUNT_GROUP_TYPE"].ToString();
                    objCONTROL_ACCOUNT.ACCOUNT_GROUP_NAME = dr["ACCOUNT_GROUP_NAME"].ToString();
                    objCONTROL_ACCOUNT.COMPANY_ID = dr["COMPANY_ID"].ToString();

                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objCONTROL_ACCOUNT;
        }
        public static List<ACCOUNT_SUBSIDIARY> GetAllSubsidiaryAccount(string _ACCOUNT_GROUP_CODE, string _CONTROL_CODE,string COMPANY_ID)
        {
            List<ACCOUNT_SUBSIDIARY> objListCONTROL_ACCOUNT = new List<ACCOUNT_SUBSIDIARY>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlcommand = new SqlCommand();
            objSqlcommand.CommandText = "select SUBSIDIARY_CODE,SUBSIDIARY_NAME from ACCOUNT_SUBSIDIARY where  CONTROL_CODE='" + _CONTROL_CODE + "' and COMPANY_ID='" + COMPANY_ID + "'";
            //objCommand.CommandType = CommandType.StoredProcedure;
            objSqlcommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlcommand)
                {
                    SqlDataReader dr = objSqlcommand.ExecuteReader();
                    while (dr.Read())
                    {
                        ACCOUNT_SUBSIDIARY objACCOUNT_SUBSIDIARY = new ACCOUNT_SUBSIDIARY();
                        objACCOUNT_SUBSIDIARY.SUBSIDIARY_CODE = dr["SUBSIDIARY_CODE"].ToString();
                        objACCOUNT_SUBSIDIARY.SUBSIDIARY_NAME = dr["SUBSIDIARY_NAME"].ToString();
                        objListCONTROL_ACCOUNT.Add(objACCOUNT_SUBSIDIARY);
                    }
                }
            }
            catch { }
            finally { conn.Close(); }
            return objListCONTROL_ACCOUNT;
        }
        public static List<ACCOUNT_SUBSIDIARY> GetAllSubsidiaryAccount2(string COMPANY_ID)
        {
            List<ACCOUNT_SUBSIDIARY> objListCONTROL_ACCOUNT = new List<ACCOUNT_SUBSIDIARY>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlcommand = new SqlCommand();
            objSqlcommand.CommandText = "select SUBSIDIARY_CODE,SUBSIDIARY_NAME,CONTROL_NAME from ACCOUNT_SUBSIDIARY s inner join CONTROL_ACCOUNT c on s.CONTROL_CODE=c.CONTROL_CODE and s.COMPANY_ID=c.COMPANY_ID where s.COMPANY_ID='" + COMPANY_ID + "' ";
            //objCommand.CommandType = CommandType.StoredProcedure;
            objSqlcommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlcommand)
                {
                    SqlDataReader dr = objSqlcommand.ExecuteReader();
                    while (dr.Read())
                    {
                        ACCOUNT_SUBSIDIARY objACCOUNT_SUBSIDIARY = new ACCOUNT_SUBSIDIARY();
                        objACCOUNT_SUBSIDIARY.SUBSIDIARY_CODE = dr["SUBSIDIARY_CODE"].ToString();
                        objACCOUNT_SUBSIDIARY.SUBSIDIARY_NAME = dr["SUBSIDIARY_NAME"].ToString();
                        objACCOUNT_SUBSIDIARY.CONTROL_NAME = dr["CONTROL_NAME"].ToString();

                        objListCONTROL_ACCOUNT.Add(objACCOUNT_SUBSIDIARY);
                    }
                }
            }
            catch { }
            finally { conn.Close(); }
            return objListCONTROL_ACCOUNT;
        }
        public static ACCOUNT_SUBSIDIARY GetSubsidiaryAccount(string SUBSIDIARY_CODE)
        {
            ACCOUNT_SUBSIDIARY objACCOUNT_SUBSIDIARY = new ACCOUNT_SUBSIDIARY();
            //List<ACCOUNT_SUBSIDIARY> objListCONTROL_ACCOUNT = new List<ACCOUNT_SUBSIDIARY>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlcommand = new SqlCommand();
            objSqlcommand.CommandText = "select SUBSIDIARY_CODE,SUBSIDIARY_NAME from ACCOUNT_SUBSIDIARY where SUBSIDIARY_CODE='" + SUBSIDIARY_CODE + "'  ";
            //objCommand.CommandType = CommandType.StoredProcedure;
            objSqlcommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlcommand)
                {
                    SqlDataReader dr = objSqlcommand.ExecuteReader();
                    while (dr.Read())
                    {

                        objACCOUNT_SUBSIDIARY.SUBSIDIARY_CODE = dr["SUBSIDIARY_CODE"].ToString();
                        objACCOUNT_SUBSIDIARY.SUBSIDIARY_NAME = dr["SUBSIDIARY_NAME"].ToString();
                        //objListCONTROL_ACCOUNT.Add(objACCOUNT_SUBSIDIARY);
                    }
                }
            }
            catch { }
            finally { conn.Close(); }
            return objACCOUNT_SUBSIDIARY;
        }
        public static List<ACCOUNTS> GetAccountTreeView(string COMPANY_ID, string BRANCH_ID)
        {
            List<ACCOUNTS> objListACCOUNTS = new List<ACCOUNTS>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlcommand = new SqlCommand();
            objSqlcommand.CommandText = "FSP_ACCOUNTS_TREE_VIEW";
            objSqlcommand.CommandType = CommandType.StoredProcedure;
            objSqlcommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar,10));
            objSqlcommand.Parameters["@COMPANY_ID"].Value = COMPANY_ID;
            objSqlcommand.Parameters.Add(new SqlParameter("@BRANCH_ID", SqlDbType.VarChar, 10));
            objSqlcommand.Parameters["@BRANCH_ID"].Value = BRANCH_ID;
            objSqlcommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlcommand)
                {
                    SqlDataReader dr = objSqlcommand.ExecuteReader();
                    while (dr.Read())
                    {

                        // g.ACCOUNT_GROUP_CODE, g.ACCOUNT_GROUP_NAME, c.CONTROL_CODE, c.CONTROL_NAME


                        ACCOUNTS objACCOUNTS = new ACCOUNTS();
                        //objACCOUNTS.ACCOUNT_GROUP_CODE = dr["ACCOUNT_GROUP_CODE"].ToString();
                        objACCOUNTS.ACCOUNT_GROUP_NAME = dr["ACCOUNT_GROUP_NAME"].ToString();
                        //objACCOUNTS.CONTROL_CODE = dr["CONTROL_CODE"].ToString();
                        objACCOUNTS.CONTROL_NAME = dr["CONTROL_NAME"].ToString();
                        //objACCOUNTS.SUBSIDIARY_CODE = dr["SUBSIDIARY_CODE"].ToString();
                        objACCOUNTS.SUBSIDIARY_NAME = dr["SUBSIDIARY_NAME"].ToString();
                        //objACCOUNTS.ACCOUNTS_CODE = dr["ACCOUNTS_CODE"].ToString();
                        objACCOUNTS.DESCRIPTION = dr["DESCRIPTION"].ToString();
                        objACCOUNTS.ACCOUNTS_BALANCE = Convert.ToDecimal(dr["ACCOUNTS_BALANCE"]);
                        //objACCOUNTS.BALANCE_TYPE = dr["BALANCE_TYPE"].ToString();
                        objListACCOUNTS.Add(objACCOUNTS);
                    }
                }
            }
            catch { }
            finally { conn.Close(); }
            return objListACCOUNTS;



        }
        public static List<ACCOUNTS> GetAccountOpeningBalance(string COMPANY_ID)
        {
            List<ACCOUNTS> objListACCOUNTS = new List<ACCOUNTS>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlcommand = new SqlCommand();
            objSqlcommand.CommandText = "FSP_ACCOUNT_OPENING_BALANCE_VIEW";
            objSqlcommand.CommandType = CommandType.StoredProcedure;
            objSqlcommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.Int));
            objSqlcommand.Parameters["@COMPANY_ID"].Value = COMPANY_ID;
            objSqlcommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlcommand)
                {
                    SqlDataReader dr = objSqlcommand.ExecuteReader();
                    while (dr.Read())
                    {

                        // g.ACCOUNT_GROUP_CODE, g.ACCOUNT_GROUP_NAME, c.CONTROL_CODE, c.CONTROL_NAME


                        ACCOUNTS objACCOUNTS = new ACCOUNTS();
                        //objACCOUNTS.ACCOUNT_GROUP_CODE = dr["ACCOUNT_GROUP_CODE"].ToString();
                        objACCOUNTS.ACCOUNT_GROUP_NAME = dr["ACCOUNT_GROUP_NAME"].ToString();
                        //objACCOUNTS.CONTROL_CODE = dr["CONTROL_CODE"].ToString();
                        objACCOUNTS.CONTROL_NAME = dr["CONTROL_NAME"].ToString();
                        //objACCOUNTS.SUBSIDIARY_CODE = dr["SUBSIDIARY_CODE"].ToString();
                        objACCOUNTS.SUBSIDIARY_NAME = dr["SUBSIDIARY_NAME"].ToString();
                        objACCOUNTS.ACCOUNTS_CODE = dr["ACCOUNTS_CODE"].ToString();
                        objACCOUNTS.DESCRIPTION = dr["DESCRIPTION"].ToString();
                        objACCOUNTS.OPENING_BALANCE = Convert.ToDecimal(dr["OPENING_BALANCE"]);
                        objACCOUNTS.OPENING_BALANCE_FLAG = Convert.ToInt32(dr["OPENING_BALANCE_FLAG"]);
                        objACCOUNTS.BALANCE_TYPE = dr["BALANCE_TYPE"].ToString();
                        objListACCOUNTS.Add(objACCOUNTS);
                    }
                }
            }
            catch { }
            finally { conn.Close(); }
            return objListACCOUNTS;
        }
        public static List<ACCOUNTS> GetAllAccount(string COMPANY_ID, string BRANCH_ID)
        {
            List<ACCOUNTS> objListACCOUNTS = new List<ACCOUNTS>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_ACCOUNTS_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.NVarChar, 10));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = COMPANY_ID;
            objSqlCommand.Parameters.Add(new SqlParameter("@BRANCH_ID", SqlDbType.NVarChar, 10));
            objSqlCommand.Parameters["@BRANCH_ID"].Value = BRANCH_ID;

            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                SqlDataReader dr = objSqlCommand.ExecuteReader();
                while (dr.Read())
                {
                    ACCOUNTS objACCOUNTS = new ACCOUNTS();
                    objACCOUNTS.DESCRIPTION = dr["DESCRIPTION"].ToString();
                    objACCOUNTS.ACCOUNTS_CODE = dr["ACCOUNTS_CODE"].ToString();
                    // objACCOUNT_GROUP.ACCOUNT_GROUP_TYPE = dr["ACCOUNT_GROUP_TYPE"].ToString();
                    //objACCOUNT_GROUP.COMPANY_ID = dr["COMPANY_ID"].ToString();
                    objListACCOUNTS.Add(objACCOUNTS);
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objListACCOUNTS;
        }
        public static BANK GetBank(string ACCOUNTS_CODE)
        {


            BANK objBANK = new BANK();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlcommand = new SqlCommand();
            objSqlcommand.CommandText = "FSP_BANK_GK";
            objSqlcommand.CommandType = CommandType.StoredProcedure;


            objSqlcommand.Parameters.Add(new SqlParameter("@ACCOUNTS_CODE", SqlDbType.VarChar, 50));
            objSqlcommand.Parameters["@ACCOUNTS_CODE"].Value = ACCOUNTS_CODE;

            objSqlcommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlcommand)
                {
                    SqlDataReader dr = objSqlcommand.ExecuteReader();
                    while (dr.Read())
                    {
                        objBANK.BANK_NAME = dr["BANK_NAME"].ToString();
                        objBANK.BANK_ADDRESS = dr["BANK_ADDRESS"].ToString();
                        objBANK.COUNTRY_ID = (dr["COUNTRY_ID"].ToString());
                        objBANK.BANK_EMAIL = dr["BANK_EMAIL"].ToString();
                        objBANK.BANK_PHONE = dr["BANK_PHONE"].ToString();
                        objBANK.BANK_FAX = dr["BANK_FAX"].ToString();

                    }
                }
            }
            catch { }
            finally { conn.Close(); }
            return objBANK;

        }
        public static List<ACCOUNTS> GetAllBank(string subsidiary_code, string COMPANY_ID)
        {
            List<ACCOUNTS> objListACCOUNTS = new List<ACCOUNTS>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlcommand = new SqlCommand();
            objSqlcommand.CommandText = "FSP_BANK_GA";
            objSqlcommand.CommandType = CommandType.StoredProcedure;
            objSqlcommand.Parameters.Add(new SqlParameter("@subsidiary_code", SqlDbType.VarChar, 50));
            objSqlcommand.Parameters["@subsidiary_code"].Value = subsidiary_code;
            objSqlcommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 50));
            objSqlcommand.Parameters["@COMPANY_ID"].Value = COMPANY_ID;
            objSqlcommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlcommand)
                {
                    SqlDataReader dr = objSqlcommand.ExecuteReader();
                    while (dr.Read())
                    {
                        // g.ACCOUNT_GROUP_CODE, g.ACCOUNT_GROUP_NAME, c.CONTROL_CODE, c.CONTROL_NAME
                        ACCOUNTS objACCOUNTS = new ACCOUNTS();
                        objACCOUNTS.ACCOUNTS_CODE = dr["ACCOUNTS_CODE"].ToString();
                        objACCOUNTS.DESCRIPTION = dr["DESCRIPTION"].ToString();
                        objACCOUNTS.ACCOUNTS_BALANCE = Convert.ToDecimal(dr["ACCOUNTS_BALANCE"].ToString());
                        objListACCOUNTS.Add(objACCOUNTS);
                    }
                }
            }
            catch { }
            finally { conn.Close(); }
            return objListACCOUNTS;
        }
        public static List<ACCOUNTS> GetCashAccount(string subsidiary_code1, string subsidiary_code2, string COMPANY_ID,string BRANCH_ID)
        {
            List<ACCOUNTS> objListACCOUNTS = new List<ACCOUNTS>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlcommand = new SqlCommand();
            objSqlcommand.CommandText = "FSP_ACCOUNTS_GSK";
            objSqlcommand.CommandType = CommandType.StoredProcedure;
            objSqlcommand.Parameters.Add(new SqlParameter("@subsidiary_code", SqlDbType.VarChar, 50));
            objSqlcommand.Parameters["@subsidiary_code"].Value = subsidiary_code1;
            objSqlcommand.Parameters.Add(new SqlParameter("@subsidiary_code2", SqlDbType.VarChar, 50));
            objSqlcommand.Parameters["@subsidiary_code2"].Value = subsidiary_code2;
            objSqlcommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.NVarChar, 10));
            objSqlcommand.Parameters["@COMPANY_ID"].Value = COMPANY_ID;
            objSqlcommand.Parameters.Add(new SqlParameter("@BRANCH_ID", SqlDbType.NVarChar, 10));
            objSqlcommand.Parameters["@BRANCH_ID"].Value = BRANCH_ID;
            objSqlcommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlcommand)
                {
                    SqlDataReader dr = objSqlcommand.ExecuteReader();
                    while (dr.Read())
                    {
                        // g.ACCOUNT_GROUP_CODE, g.ACCOUNT_GROUP_NAME, c.CONTROL_CODE, c.CONTROL_NAME
                        ACCOUNTS objACCOUNTS = new ACCOUNTS();
                        objACCOUNTS.DESCRIPTION = dr["DESCRIPTION"].ToString();
                        objACCOUNTS.ACCOUNTS_BALANCE = Convert.ToDecimal(dr["ACCOUNTS_BALANCE"].ToString());
                        objListACCOUNTS.Add(objACCOUNTS);
                    }
                }
            }
            catch { }
            finally { conn.Close(); }
            return objListACCOUNTS;
        }
        public static List<CONTROL_ACCOUNT> GetAllControlAccount(string ACCOUNT_GROUP_CODE, int NAME_VALUE_LIST, string COMPANY_ID)
        {
            List<CONTROL_ACCOUNT> objListCONTROL_ACCOUNT = new List<CONTROL_ACCOUNT>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_CONTROL_ACCOUNT_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@ACCOUNT_GROUP_CODE", SqlDbType.VarChar, 5));
            objSqlCommand.Parameters["@ACCOUNT_GROUP_CODE"].Value = ACCOUNT_GROUP_CODE;
            objSqlCommand.Parameters.Add(new SqlParameter("@NAME_VALUE_LIST", SqlDbType.VarChar, 5));
            objSqlCommand.Parameters["@NAME_VALUE_LIST"].Value = NAME_VALUE_LIST;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = COMPANY_ID;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                SqlDataReader dr = objSqlCommand.ExecuteReader();
                while (dr.Read())
                {
                    CONTROL_ACCOUNT objCONTROL_ACCOUNT = new CONTROL_ACCOUNT();
                    objCONTROL_ACCOUNT.ACCOUNT_GROUP_CODE = dr["ACCOUNT_GROUP_CODE"].ToString();
                    objCONTROL_ACCOUNT.CONTROL_CODE = dr["CONTROL_CODE"].ToString();
                    objCONTROL_ACCOUNT.CONTROL_NAME = dr["CONTROL_NAME"].ToString();
                    objCONTROL_ACCOUNT.ACCOUNT_GROUP_TYPE = dr["ACCOUNT_GROUP_TYPE"].ToString();
                    objCONTROL_ACCOUNT.ACCOUNT_GROUP_NAME = dr["ACCOUNT_GROUP_NAME"].ToString();
                    objCONTROL_ACCOUNT.COMPANY_ID = dr["COMPANY_ID"].ToString();
                    objListCONTROL_ACCOUNT.Add(objCONTROL_ACCOUNT);
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objListCONTROL_ACCOUNT;
        }



    }
}
