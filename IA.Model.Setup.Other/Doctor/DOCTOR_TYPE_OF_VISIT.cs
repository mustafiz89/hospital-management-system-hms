﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Doctor
{
    public class DOCTOR_TYPE_OF_VISIT
    {
        public Guid DOCTOR_TYPE_OF_VISIT_ID { get; set; }
        public string DOCTOR_TYPE_OF_VISIT_NAME { get; set; }
        public string STATUS_FLAG { get; set; }
    }
}
