﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Doctor
{
    public class OUTDOOR_DOCTOR
    {
        public Guid DOCTOR_ID { get; set; }
        public Guid DOCTOR_SPECIALIZATION_ID { get; set; }
        public Guid DOCTOR_TYPE_ID { get; set; }
        public string DOCTOR_NAME { get; set; }
        public string DOCTOR_CONTACT_PERSON { get; set; }
        public string DOCTOR_PERSONAL_ADDRESS { get; set; }
        public string DOCTOR_CHAMBER_ADDRESS { get; set; }
        public string DOCTOR_PERSONAL_CONTACT { get; set; }
        public string DOCTOR_PS_CONTACT { get; set; }
        public decimal DOCTOR_PATHOLOGY_COMMISSION { get; set; }
        public decimal DOCTOR_PATIENT_COMMISSION { get; set; }

    }
}
