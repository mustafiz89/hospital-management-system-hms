﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Doctor
{
    public class DOCTOR_SPECIALIZATION
    {
        public Guid DOCTOR_SPECIALIZATION_ID { get; set; }
        public string DOCTOR_SPECIALIZATION_NAME { get; set; }
        public string STATUS_FLAG { get; set; }
    }
}
