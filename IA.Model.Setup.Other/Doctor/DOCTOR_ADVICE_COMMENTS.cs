﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Doctor
{
    public class DOCTOR_ADVICE_COMMENTS
    {
        public Guid DOCTOR_ADVICE_COMMENTS_ID { get; set; }
        public string DOCTOR_ADVICE_COMMENTS_TEXT { get; set; }
    }
}
