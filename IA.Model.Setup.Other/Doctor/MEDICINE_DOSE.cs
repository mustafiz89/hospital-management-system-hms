﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Doctor
{
    public class MEDICINE_DOSE
    {
        public Guid MEDICINE_DOSE_ID { get; set; }
        public string MEDICINE_DOSE_NAME { get; set; }
        public string STATUS_FLAG { get; set; }
    }
}
