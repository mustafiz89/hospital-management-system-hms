﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IA.Model.Setup.Other.Settings
{
   public class COUNTRY
    {
                
        public Guid COUNTRY_ID {get; set;}
        public string COUNTRY_NAME {get; set;}
        public string STATUS_FLAG { get; set; }
        
    }
}
