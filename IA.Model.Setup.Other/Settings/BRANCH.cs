﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Settings
{
    public class BRANCH
    {
        public Guid BRANCH_ID { get; set; }
        public string BRANCH_NAME { get; set; }
        public string STATUS_FLAG { get; set; }
    }
}
