﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Settings
{
     public class City
    {
         public Guid CITY_ID { get; set; }
         public string CITY_NAME { get; set; }
         public string STATUS_FLAG { get; set; }
    }
}
