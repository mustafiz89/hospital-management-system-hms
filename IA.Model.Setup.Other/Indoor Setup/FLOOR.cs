﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Indoor_Setup
{
    public class FLOOR
    {
        public Guid FLOOR_ID { get; set; }
        public string FLOOR_NAME { get; set; }
        public string STATUS_FLAG { get; set; }
    }
}
