﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Indoor_Setup
{
    public class ROOM
    {
        public Guid ROOM_ID { get; set; }
        public Guid ROOM_CATEGORY_ID { get; set; }
        public Guid FLOOR_ID { get; set; }
        public string ROOM_NUMBER { get; set; }
        public string ROOM_PHONE { get; set; }
        public string STATUS_FLAG { get; set; }

        public string ROOM_CATEGORY_NAME { get; set; }
        public string FLOOR_NAME { get; set; }
    }
}
