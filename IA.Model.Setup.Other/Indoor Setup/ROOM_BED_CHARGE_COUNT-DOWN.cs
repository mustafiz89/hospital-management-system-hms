﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Indoor_Setup
{
    public class ROOM_BED_CHARGE_COUNT_DOWN
    {
        public Guid ROOM_BED_CHARGE_COUNT_DOWN_ID { get; set; }
        public DateTime TIME_START { get; set; }
        public DateTime TIME_END { get; set; }
    }
}
