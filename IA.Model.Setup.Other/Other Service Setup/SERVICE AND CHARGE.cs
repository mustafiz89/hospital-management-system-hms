﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Other_Service_Setup
{
    public class OTHER_SERVICE
    {
        public Guid OTHER_SERVICE_ID { get; set; }
        public string OTHER_SERVICE_NAME { get; set; }
        public decimal OTHER_SERVICE_CHARGE { get; set; }
        public string STATUS_FLAG { get; set; }
    }
}
