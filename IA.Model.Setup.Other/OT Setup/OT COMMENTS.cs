﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.OT_Setup
{
    public class OT_COMMENTS
    {
        public Guid OT_COMMENTS_ID { get; set; }
        public Guid OT_ID { get; set; }
        public string OT_COMMENT { get; set; }

        public string OT_NAME { get; set; }
    }
}
