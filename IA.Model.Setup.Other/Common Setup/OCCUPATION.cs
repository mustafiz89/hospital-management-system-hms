﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Common_Setup
{
    public class OCCUPATION
    {
        public Guid OCCUPATION_ID { get; set; }
        public string OCCUPATION_NAME { get; set; }
        public string STATUS_FLAG { get; set; }
    }
}
