﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Common_Setup
{
    public class GENDER
    {
        public Guid GENDER_ID { get; set;}
        public string GENDER_NAME { get; set; }
        public string STATUS_FLAG { get; set; }
    }
}
