﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Pathology_Setup
{
    public class PATHOLOGY_TEST_ITEM_SUB_CATEGORY
    {
        public Guid PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID {get; set;}
        public Guid PATHOLOGY_TEST_ITEM_CATEGORY_ID{get;set;}
        public string PATHOLOGY_TEST_ITEM_SUB_CATEGORY_NAME {get; set;}
        public string STATUS_FLAG { get; set; }

        public string PATHOLOGY_TEST_ITEM_CATEGORY_NAME { get; set; }
    }
}
