﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Pathology_Setup
{
    public class PATHOLOGY_REFERENCE_CATEGORY
    {
        public Guid PATHOLOGY_REFERENCE_CATEGORY_ID { get; set; }
        public string PATHOLOGY_REFERENCE_CATEGORY_NAME { get; set; }
        public string STATUS_FLAG { get; set; }
    }
}
