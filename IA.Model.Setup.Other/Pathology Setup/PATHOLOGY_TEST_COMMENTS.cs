﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Pathology_Setup
{
    public class PATHOLOGY_TEST_COMMENTS
    {
        public Guid PATHOLOGY_TEST_COMMENTS_ID { get; set; }
        public string PATHOLOGY_TEST_COMMENTS_COMMENTS { get; set; }
    }
}
