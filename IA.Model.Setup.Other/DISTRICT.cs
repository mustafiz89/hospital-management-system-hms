﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IA.Model.Setup.Other
{
    public class DISTRICT
    {
        private string _COMPANY_ID;

        public string COMPANY_ID
        {
            get { return _COMPANY_ID; }
            set { _COMPANY_ID = value; }
        }
        private int _DISTRICT_ID;

        public int DISTRICT_ID
        {
            get { return _DISTRICT_ID; }
            set { _DISTRICT_ID = value; }
        }
        private string _DISTRICT_NAME;

        public string DISTRICT_NAME
        {
            get { return _DISTRICT_NAME; }
            set { _DISTRICT_NAME = value; }
        }

        private int _IS_ACTIVE;

        public int IS_ACTIVE
        {
            get { return _IS_ACTIVE; }
            set { _IS_ACTIVE = value; }
        }

        private string _STATUS;

        public string STATUS
        {
            get { return _STATUS; }
            set { _STATUS = value; }
        }
    }
}
