﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IA.Model.Setup.Other
{
   public class BANK
    {
       public Guid BANK_ID {get;set;}
       public string COUNTRY_ID { get; set; }
       public string ACCOUNTS_CODE { get; set; }
       public string BANK_NAME { get; set; }
       public string BANK_ADDRESS { get; set; }       
       public string BANK_FAX { get; set; }
       public string BANK_PHONE { get; set; }
       public string BANK_EMAIL { get; set; }
       public string COMPANY_ID { get; set; }
       public string BRANCH_ID { get; set; }
       public string STATUS_FLAG { get; set; }
       
    }
}
