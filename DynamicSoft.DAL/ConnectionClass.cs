﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
namespace DynamicSoft.DAL
{
   public class ConnectionClass
    {
       public static SqlConnection GetConnection()
       {
           SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["application"].ConnectionString);
           return con;
       }
       public static SqlConnection GetConnectionMaster()
       {
           SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["applicationMaster"].ConnectionString);
           return conn;
       }
    }
}
