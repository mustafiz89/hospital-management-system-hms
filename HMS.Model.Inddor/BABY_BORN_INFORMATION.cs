﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HMS.Model.Inddor
{
   public class BABY_BORN_INFORMATION
    {
       public Guid BABY_BORN_INFORMATION_ID { get; set; }
       public string PATIENT_ADDMISSION_NO { get; set; }
       public string PATIENT_NAME { get; set; }
       public string BABY_NAME { get; set; }
       public string BABY_FATHER_NAME { get; set; }
       public string BABY_MOTHER_NAME { get; set; }
       public string PRESENT_ADDRESS { get; set; }
       public string PERMANENT_ADDRESS { get; set; }
       public DateTime BORN_DATE { get; set; }
       public Guid NATIONALITY_ID { get; set; }
       public Guid GENDER_ID { get; set; }
       public Guid BLOOD_GROUP_ID { get; set; }
       public Guid RELIGION_ID { get; set; }
    }
}
