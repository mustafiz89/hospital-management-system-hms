﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HMS.Model.Indoor
{
    public class PATIENT_ADMISSION
    {
        public string ADMISSION_NO { set; get; }
        public string PATIENT_REGISTRATION_NO { set; get; }
        public DateTime START_DATE { set; get; }
        public DateTime END_DATE { set; get; }
        public int REFFERENCE_ID { set; get; }
        public int ADMITTED_UNDER_ID { set; get; }
        public int ADMITTED_DEPARTMENT_ID { set; get; }
        public int BED_ID { set; get; }
        public int ROOM_CHANGE_STATUS { set; get; }
        public int ADMISSION_STATUS { set; get; }
        public string GURDIAN_NAME { set; get; }
        public string RELATION_GURDIAN { set; get; }
        public string GURDIAN_PHONE { set; get; }

        public decimal TOTAL { set; get; }
        public string SERVICE_NM { set; get; }
        public decimal LEDGER_DEBIT { set; get; }
        public decimal LEDGER_CREDIT { set; get; }

        public string SERVICE_TYPE { get; set; }
        public decimal COLLECTED_AMOUNT { get; set; }
        public string COLLECTION_DATE { get; set; }
        public string PAYMENT_TYPE_NAME { get; set; }

        public string DESCRIPTION { get; set; }

        public string FLOOR_NUMBER { get; set; }

        public string PATIENT_PHONE { get; set; }

        public string PATIENT_NAME { get; set; }

        public string ROOM_ID { get; set; }

        public string BED_NUMBER { get; set; }

        public decimal TOTAL_PAID { get; set; }

        public decimal TOTAL_DISCOUNT { get; set; }

        public decimal TOTAL_AMOUNT { get; set; }



        public string ERROR_CODE { get; set; }
        public string ERROR_MSG { get; set; }
        public decimal AMOUNT { get; set; }
        public decimal AMOUNT_RECEIVABLE_PAYABLE { get; set; }
        public decimal TOTAL_BILL { get; set; }
        public int AMOUNT_TYPE_ID { get; set; }
        public int BANK_ID { get; set; }
        public string CHEQUE_NO { get; set; }
        public string CHEQUE_DATE { get; set; }
        public string NARRATION { get; set; }
        public int CORPORATE_PATIENT_FLAG { get; set; }
        public int FINAL_DISCHARGE_FLAG { get; set; }
        public string COLLECTION_ID { get; set; }
        public string PAYMENT_ID { get; set; }
        public string VOUCHER_NO { get; set; }
        public string MAKE_BY { get; set; }
        public string ROW_COUNT { get; set; }

        public short RELEASE_STATUS { get; set; }

        public string RELEASE_STATUS_INFO { get; set; }

        public object COMPANY_ID { get; set; }
    }
}
