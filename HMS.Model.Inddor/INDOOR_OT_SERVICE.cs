﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HMS.Model.Inddor
{
    public class INDOOR_OT_SERVICE
    {
        public Guid INDOOR_OT_SERVICE_ID { get; set; }
        public string INVOICE_NO { get; set; }
        public string PATIENT_RESISTRATION_NO { get; set; }
        public Guid OT_COMMENTS_ID { get; set; }
        public string COLLECTION_MODE { get; set; }
        public decimal TOTAL_AMOUNT { get; set; }
        public decimal DISCOUNT { get; set; }
        public decimal COLLECTED_AMOUNT { get; set; }
        public Guid BANK_ID { get; set; }
        public decimal CHANGE_AMOUNT { get; set; }
        public decimal DUE_AMOUNT { get; set; }
        public decimal VAT { get; set; }
        public Guid OT_ID { get; set; }

    }
}
