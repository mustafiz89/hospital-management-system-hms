﻿namespace HMSDev.UI
{
    partial class Outdoor_Patient_Ticket
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnReport = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmbTypeOfVisit = new System.Windows.Forms.ComboBox();
            this.cmbDoctorName = new System.Windows.Forms.ComboBox();
            this.cmbDoctorType = new System.Windows.Forms.ComboBox();
            this.cmbDoctorSpecialization = new System.Windows.Forms.ComboBox();
            this.cmbCollectionMode = new System.Windows.Forms.ComboBox();
            this.txtDiscount = new System.Windows.Forms.TextBox();
            this.txtHospitalCharge = new System.Windows.Forms.TextBox();
            this.txtDueAmount = new System.Windows.Forms.TextBox();
            this.txtChangeAmount = new System.Windows.Forms.TextBox();
            this.txtCollectedAmount = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtDoctorCharge = new System.Windows.Forms.TextBox();
            this.txtTotalAmount = new System.Windows.Forms.TextBox();
            this.txtPatientName = new System.Windows.Forms.TextBox();
            this.txtPatientRegistrationNo = new System.Windows.Forms.TextBox();
            this.txtPatientTicketNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnReport);
            this.groupBox3.Controls.Add(this.btnClose);
            this.groupBox3.Controls.Add(this.btnAdd);
            this.groupBox3.Controls.Add(this.btnEdit);
            this.groupBox3.Controls.Add(this.btnDelete);
            this.groupBox3.Location = new System.Drawing.Point(3, 461);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1006, 59);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            // 
            // btnReport
            // 
            this.btnReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnReport.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnReport.Location = new System.Drawing.Point(228, 16);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(92, 29);
            this.btnReport.TabIndex = 6;
            this.btnReport.Text = "Report";
            this.btnReport.UseVisualStyleBackColor = false;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnClose.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnClose.Location = new System.Drawing.Point(432, 16);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(92, 29);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnAdd.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnAdd.Location = new System.Drawing.Point(20, 16);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(100, 29);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnEdit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnEdit.Location = new System.Drawing.Point(130, 16);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(92, 29);
            this.btnEdit.TabIndex = 4;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = false;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnDelete.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnDelete.Location = new System.Drawing.Point(326, 16);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 29);
            this.btnDelete.TabIndex = 5;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmbTypeOfVisit);
            this.groupBox2.Controls.Add(this.cmbDoctorName);
            this.groupBox2.Controls.Add(this.cmbDoctorType);
            this.groupBox2.Controls.Add(this.cmbDoctorSpecialization);
            this.groupBox2.Controls.Add(this.cmbCollectionMode);
            this.groupBox2.Controls.Add(this.txtDiscount);
            this.groupBox2.Controls.Add(this.txtHospitalCharge);
            this.groupBox2.Controls.Add(this.txtDueAmount);
            this.groupBox2.Controls.Add(this.txtChangeAmount);
            this.groupBox2.Controls.Add(this.txtCollectedAmount);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.txtDoctorCharge);
            this.groupBox2.Controls.Add(this.txtTotalAmount);
            this.groupBox2.Controls.Add(this.txtPatientName);
            this.groupBox2.Controls.Add(this.txtPatientRegistrationNo);
            this.groupBox2.Controls.Add(this.txtPatientTicketNo);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold);
            this.groupBox2.Location = new System.Drawing.Point(3, 18);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1006, 437);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Outdoor Patient Ticket";
            // 
            // cmbTypeOfVisit
            // 
            this.cmbTypeOfVisit.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbTypeOfVisit.FormattingEnabled = true;
            this.cmbTypeOfVisit.Location = new System.Drawing.Point(673, 39);
            this.cmbTypeOfVisit.Name = "cmbTypeOfVisit";
            this.cmbTypeOfVisit.Size = new System.Drawing.Size(213, 23);
            this.cmbTypeOfVisit.TabIndex = 37;
            // 
            // cmbDoctorName
            // 
            this.cmbDoctorName.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbDoctorName.FormattingEnabled = true;
            this.cmbDoctorName.Location = new System.Drawing.Point(671, 134);
            this.cmbDoctorName.Name = "cmbDoctorName";
            this.cmbDoctorName.Size = new System.Drawing.Size(215, 23);
            this.cmbDoctorName.TabIndex = 40;
            // 
            // cmbDoctorType
            // 
            this.cmbDoctorType.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbDoctorType.FormattingEnabled = true;
            this.cmbDoctorType.Location = new System.Drawing.Point(671, 101);
            this.cmbDoctorType.Name = "cmbDoctorType";
            this.cmbDoctorType.Size = new System.Drawing.Size(215, 23);
            this.cmbDoctorType.TabIndex = 40;
            // 
            // cmbDoctorSpecialization
            // 
            this.cmbDoctorSpecialization.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbDoctorSpecialization.FormattingEnabled = true;
            this.cmbDoctorSpecialization.Location = new System.Drawing.Point(671, 68);
            this.cmbDoctorSpecialization.Name = "cmbDoctorSpecialization";
            this.cmbDoctorSpecialization.Size = new System.Drawing.Size(215, 23);
            this.cmbDoctorSpecialization.TabIndex = 40;
            // 
            // cmbCollectionMode
            // 
            this.cmbCollectionMode.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbCollectionMode.FormattingEnabled = true;
            this.cmbCollectionMode.Location = new System.Drawing.Point(207, 273);
            this.cmbCollectionMode.Name = "cmbCollectionMode";
            this.cmbCollectionMode.Size = new System.Drawing.Size(129, 23);
            this.cmbCollectionMode.TabIndex = 40;
            this.cmbCollectionMode.SelectedIndexChanged += new System.EventHandler(this.comboBox6_SelectedIndexChanged);
            // 
            // txtDiscount
            // 
            this.txtDiscount.Location = new System.Drawing.Point(207, 238);
            this.txtDiscount.Name = "txtDiscount";
            this.txtDiscount.Size = new System.Drawing.Size(129, 25);
            this.txtDiscount.TabIndex = 29;
            // 
            // txtHospitalCharge
            // 
            this.txtHospitalCharge.Location = new System.Drawing.Point(207, 170);
            this.txtHospitalCharge.Name = "txtHospitalCharge";
            this.txtHospitalCharge.Size = new System.Drawing.Size(129, 25);
            this.txtHospitalCharge.TabIndex = 30;
            // 
            // txtDueAmount
            // 
            this.txtDueAmount.Location = new System.Drawing.Point(207, 366);
            this.txtDueAmount.Name = "txtDueAmount";
            this.txtDueAmount.Size = new System.Drawing.Size(129, 25);
            this.txtDueAmount.TabIndex = 25;
            // 
            // txtChangeAmount
            // 
            this.txtChangeAmount.Location = new System.Drawing.Point(207, 335);
            this.txtChangeAmount.Name = "txtChangeAmount";
            this.txtChangeAmount.Size = new System.Drawing.Size(129, 25);
            this.txtChangeAmount.TabIndex = 25;
            // 
            // txtCollectedAmount
            // 
            this.txtCollectedAmount.Location = new System.Drawing.Point(207, 303);
            this.txtCollectedAmount.Name = "txtCollectedAmount";
            this.txtCollectedAmount.Size = new System.Drawing.Size(129, 25);
            this.txtCollectedAmount.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(97, 370);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 16);
            this.label5.TabIndex = 19;
            this.label5.Text = "Due Amount :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(73, 339);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 16);
            this.label3.TabIndex = 19;
            this.label3.Text = "Change Amount :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(62, 307);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(128, 16);
            this.label17.TabIndex = 19;
            this.label17.Text = "Collected Amount :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(72, 275);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(118, 16);
            this.label18.TabIndex = 16;
            this.label18.Text = "Collection Mode :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(74, 174);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(116, 16);
            this.label19.TabIndex = 17;
            this.label19.Text = "Hospital Charge :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(121, 242);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(69, 16);
            this.label23.TabIndex = 22;
            this.label23.Text = "Discount :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(564, 134);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(97, 16);
            this.label24.TabIndex = 23;
            this.label24.Text = "Doctor Name :";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(570, 106);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(90, 16);
            this.label25.TabIndex = 20;
            this.label25.Text = "Doctor Type :";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(510, 72);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(151, 16);
            this.label26.TabIndex = 21;
            this.label26.Text = "Doctor Specialization :";
            // 
            // txtDoctorCharge
            // 
            this.txtDoctorCharge.Location = new System.Drawing.Point(207, 136);
            this.txtDoctorCharge.Name = "txtDoctorCharge";
            this.txtDoctorCharge.Size = new System.Drawing.Size(129, 25);
            this.txtDoctorCharge.TabIndex = 5;
            // 
            // txtTotalAmount
            // 
            this.txtTotalAmount.Location = new System.Drawing.Point(207, 203);
            this.txtTotalAmount.Name = "txtTotalAmount";
            this.txtTotalAmount.Size = new System.Drawing.Size(129, 25);
            this.txtTotalAmount.TabIndex = 5;
            // 
            // txtPatientName
            // 
            this.txtPatientName.Location = new System.Drawing.Point(207, 104);
            this.txtPatientName.Name = "txtPatientName";
            this.txtPatientName.Size = new System.Drawing.Size(256, 25);
            this.txtPatientName.TabIndex = 5;
            // 
            // txtPatientRegistrationNo
            // 
            this.txtPatientRegistrationNo.Location = new System.Drawing.Point(209, 70);
            this.txtPatientRegistrationNo.Name = "txtPatientRegistrationNo";
            this.txtPatientRegistrationNo.Size = new System.Drawing.Size(254, 25);
            this.txtPatientRegistrationNo.TabIndex = 5;
            // 
            // txtPatientTicketNo
            // 
            this.txtPatientTicketNo.Location = new System.Drawing.Point(209, 39);
            this.txtPatientTicketNo.Name = "txtPatientTicketNo";
            this.txtPatientTicketNo.Size = new System.Drawing.Size(254, 25);
            this.txtPatientTicketNo.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(89, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Patient Name :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(91, 207);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Total Amount :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(84, 139);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "Doctor Charge :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(36, 75);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(161, 16);
            this.label22.TabIndex = 3;
            this.label22.Text = "Patient Registration No :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(74, 43);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(123, 16);
            this.label21.TabIndex = 3;
            this.label21.Text = "Patient Ticket No :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(567, 43);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(94, 16);
            this.label20.TabIndex = 3;
            this.label20.Text = "Type Of Visit :";
            // 
            // Outdoor_Patient_Ticket
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(224)))), ((int)(((byte)(248)))));
            this.ClientSize = new System.Drawing.Size(1021, 525);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Name = "Outdoor_Patient_Ticket";
            this.Text = "Outdoor Patient Ticket";
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnReport;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtDoctorCharge;
        private System.Windows.Forms.TextBox txtTotalAmount;
        private System.Windows.Forms.TextBox txtPatientRegistrationNo;
        private System.Windows.Forms.TextBox txtPatientTicketNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtDiscount;
        private System.Windows.Forms.TextBox txtHospitalCharge;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtCollectedAmount;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cmbTypeOfVisit;
        private System.Windows.Forms.ComboBox cmbCollectionMode;
        private System.Windows.Forms.ComboBox cmbDoctorName;
        private System.Windows.Forms.ComboBox cmbDoctorType;
        private System.Windows.Forms.ComboBox cmbDoctorSpecialization;
        private System.Windows.Forms.TextBox txtPatientName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDueAmount;
        private System.Windows.Forms.TextBox txtChangeAmount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
    }
}