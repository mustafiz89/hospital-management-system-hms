﻿namespace HMSDev.UI.HR
{
    partial class Employee_Information
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label21 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtFatherName = new System.Windows.Forms.TextBox();
            this.txtMotherName = new System.Windows.Forms.TextBox();
            this.txthomePhone = new System.Windows.Forms.TextBox();
            this.txtDateOfBirth = new System.Windows.Forms.TextBox();
            this.txtNationalID = new System.Windows.Forms.TextBox();
            this.txtSpouseName = new System.Windows.Forms.TextBox();
            this.txtOfficePhone = new System.Windows.Forms.TextBox();
            this.txtEmployeeName = new System.Windows.Forms.TextBox();
            this.txtMobileNo = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.cmbSalution = new System.Windows.Forms.ComboBox();
            this.cmbDegree = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.cmbBloodGroup = new System.Windows.Forms.ComboBox();
            this.cmbMaritalStatus = new System.Windows.Forms.ComboBox();
            this.cmbNationality = new System.Windows.Forms.ComboBox();
            this.cmbReligion = new System.Windows.Forms.ComboBox();
            this.cmbGender = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtDutyHour = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnReport = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtParmanentPostoffice = new System.Windows.Forms.TextBox();
            this.txtPresentPostOffice = new System.Windows.Forms.TextBox();
            this.txtParmanentPoliceStation = new System.Windows.Forms.TextBox();
            this.txtPresentPoliceStation = new System.Windows.Forms.TextBox();
            this.txtParmanenthouseVillage = new System.Windows.Forms.TextBox();
            this.txtPresentHouseVillage = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtParmanentDistrict = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtParmanentCountry = new System.Windows.Forms.ComboBox();
            this.cmbPresentDistrict = new System.Windows.Forms.ComboBox();
            this.cmbPresentCountry = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.cmbDesignation = new System.Windows.Forms.ComboBox();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtEmailAddress = new System.Windows.Forms.TextBox();
            this.txtWeight = new System.Windows.Forms.TextBox();
            this.txtHeight = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(5, 78);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(97, 16);
            this.label21.TabIndex = 3;
            this.label21.Text = "Father Name :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(2, 107);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 16);
            this.label10.TabIndex = 3;
            this.label10.Text = "Mother Name :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(351, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 16);
            this.label8.TabIndex = 3;
            this.label8.Text = "Date Of Birth :";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(362, 186);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(85, 16);
            this.label26.TabIndex = 3;
            this.label26.Text = "National ID :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(344, 105);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(103, 16);
            this.label22.TabIndex = 3;
            this.label22.Text = "Spouse Name :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(202, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 16);
            this.label5.TabIndex = 3;
            this.label5.Text = "Employee Name :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(40, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 16);
            this.label7.TabIndex = 3;
            this.label7.Text = "Gender :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(26, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 16);
            this.label6.TabIndex = 3;
            this.label6.Text = "Saluation :";
            // 
            // txtFatherName
            // 
            this.txtFatherName.Location = new System.Drawing.Point(110, 75);
            this.txtFatherName.Name = "txtFatherName";
            this.txtFatherName.Size = new System.Drawing.Size(227, 25);
            this.txtFatherName.TabIndex = 3;
            // 
            // txtMotherName
            // 
            this.txtMotherName.Location = new System.Drawing.Point(110, 103);
            this.txtMotherName.Name = "txtMotherName";
            this.txtMotherName.Size = new System.Drawing.Size(227, 25);
            this.txtMotherName.TabIndex = 4;
            // 
            // txthomePhone
            // 
            this.txthomePhone.Location = new System.Drawing.Point(110, 156);
            this.txthomePhone.Name = "txthomePhone";
            this.txthomePhone.Size = new System.Drawing.Size(226, 25);
            this.txthomePhone.TabIndex = 6;
            // 
            // txtDateOfBirth
            // 
            this.txtDateOfBirth.Location = new System.Drawing.Point(449, 47);
            this.txtDateOfBirth.Name = "txtDateOfBirth";
            this.txtDateOfBirth.Size = new System.Drawing.Size(166, 25);
            this.txtDateOfBirth.TabIndex = 12;
            // 
            // txtNationalID
            // 
            this.txtNationalID.Location = new System.Drawing.Point(449, 182);
            this.txtNationalID.Name = "txtNationalID";
            this.txtNationalID.Size = new System.Drawing.Size(166, 25);
            this.txtNationalID.TabIndex = 17;
            // 
            // txtSpouseName
            // 
            this.txtSpouseName.Location = new System.Drawing.Point(449, 101);
            this.txtSpouseName.Name = "txtSpouseName";
            this.txtSpouseName.Size = new System.Drawing.Size(167, 25);
            this.txtSpouseName.TabIndex = 14;
            // 
            // txtOfficePhone
            // 
            this.txtOfficePhone.Location = new System.Drawing.Point(110, 183);
            this.txtOfficePhone.Name = "txtOfficePhone";
            this.txtOfficePhone.Size = new System.Drawing.Size(226, 25);
            this.txtOfficePhone.TabIndex = 7;
            // 
            // txtEmployeeName
            // 
            this.txtEmployeeName.Location = new System.Drawing.Point(332, 18);
            this.txtEmployeeName.Name = "txtEmployeeName";
            this.txtEmployeeName.Size = new System.Drawing.Size(283, 25);
            this.txtEmployeeName.TabIndex = 11;
            // 
            // txtMobileNo
            // 
            this.txtMobileNo.Location = new System.Drawing.Point(110, 210);
            this.txtMobileNo.Name = "txtMobileNo";
            this.txtMobileNo.Size = new System.Drawing.Size(226, 25);
            this.txtMobileNo.TabIndex = 8;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(626, 105);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 16);
            this.label9.TabIndex = 41;
            this.label9.Text = "Blood Group :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(345, 79);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(102, 16);
            this.label19.TabIndex = 41;
            this.label19.Text = "Marital Status :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(364, 132);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(83, 16);
            this.label24.TabIndex = 41;
            this.label24.Text = "Nationality :";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(379, 159);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(68, 16);
            this.label25.TabIndex = 41;
            this.label25.Text = "Religion :";
            // 
            // cmbSalution
            // 
            this.cmbSalution.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbSalution.FormattingEnabled = true;
            this.cmbSalution.Location = new System.Drawing.Point(110, 22);
            this.cmbSalution.Name = "cmbSalution";
            this.cmbSalution.Size = new System.Drawing.Size(87, 23);
            this.cmbSalution.TabIndex = 1;
            // 
            // cmbDegree
            // 
            this.cmbDegree.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbDegree.FormattingEnabled = true;
            this.cmbDegree.Location = new System.Drawing.Point(111, 131);
            this.cmbDegree.Name = "cmbDegree";
            this.cmbDegree.Size = new System.Drawing.Size(226, 23);
            this.cmbDegree.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(41, 133);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 16);
            this.label11.TabIndex = 3;
            this.label11.Text = "Degree :";
            // 
            // btnBrowse
            // 
            this.btnBrowse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnBrowse.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnBrowse.Location = new System.Drawing.Point(833, 178);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(79, 29);
            this.btnBrowse.TabIndex = 3;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = false;
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnCancel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnCancel.Location = new System.Drawing.Point(913, 178);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(61, 29);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(5, 160);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 16);
            this.label12.TabIndex = 3;
            this.label12.Text = "Home Phone :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(5, 187);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(97, 16);
            this.label15.TabIndex = 3;
            this.label15.Text = "Office Phone :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(23, 214);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 16);
            this.label16.TabIndex = 3;
            this.label16.Text = "Mobile No :";
            // 
            // cmbBloodGroup
            // 
            this.cmbBloodGroup.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbBloodGroup.FormattingEnabled = true;
            this.cmbBloodGroup.Location = new System.Drawing.Point(722, 103);
            this.cmbBloodGroup.Name = "cmbBloodGroup";
            this.cmbBloodGroup.Size = new System.Drawing.Size(87, 23);
            this.cmbBloodGroup.TabIndex = 23;
            // 
            // cmbMaritalStatus
            // 
            this.cmbMaritalStatus.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbMaritalStatus.FormattingEnabled = true;
            this.cmbMaritalStatus.Location = new System.Drawing.Point(449, 75);
            this.cmbMaritalStatus.Name = "cmbMaritalStatus";
            this.cmbMaritalStatus.Size = new System.Drawing.Size(166, 23);
            this.cmbMaritalStatus.TabIndex = 13;
            // 
            // cmbNationality
            // 
            this.cmbNationality.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbNationality.FormattingEnabled = true;
            this.cmbNationality.Location = new System.Drawing.Point(449, 128);
            this.cmbNationality.Name = "cmbNationality";
            this.cmbNationality.Size = new System.Drawing.Size(166, 23);
            this.cmbNationality.TabIndex = 15;
            // 
            // cmbReligion
            // 
            this.cmbReligion.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbReligion.FormattingEnabled = true;
            this.cmbReligion.Location = new System.Drawing.Point(449, 155);
            this.cmbReligion.Name = "cmbReligion";
            this.cmbReligion.Size = new System.Drawing.Size(166, 23);
            this.cmbReligion.TabIndex = 16;
            // 
            // cmbGender
            // 
            this.cmbGender.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbGender.FormattingEnabled = true;
            this.cmbGender.Location = new System.Drawing.Point(110, 49);
            this.cmbGender.Name = "cmbGender";
            this.cmbGender.Size = new System.Drawing.Size(140, 23);
            this.cmbGender.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label36);
            this.groupBox2.Controls.Add(this.txtDutyHour);
            this.groupBox2.Controls.Add(this.dateTimePicker1);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.cmbGender);
            this.groupBox2.Controls.Add(this.cmbCategory);
            this.groupBox2.Controls.Add(this.cmbDesignation);
            this.groupBox2.Controls.Add(this.cmbStatus);
            this.groupBox2.Controls.Add(this.cmbReligion);
            this.groupBox2.Controls.Add(this.cmbNationality);
            this.groupBox2.Controls.Add(this.cmbMaritalStatus);
            this.groupBox2.Controls.Add(this.cmbBloodGroup);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.btnCancel);
            this.groupBox2.Controls.Add(this.btnBrowse);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.cmbDegree);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.cmbSalution);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label32);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtMobileNo);
            this.groupBox2.Controls.Add(this.txtEmployeeName);
            this.groupBox2.Controls.Add(this.txtOfficePhone);
            this.groupBox2.Controls.Add(this.txtSpouseName);
            this.groupBox2.Controls.Add(this.txtEmailAddress);
            this.groupBox2.Controls.Add(this.txtNationalID);
            this.groupBox2.Controls.Add(this.txtWeight);
            this.groupBox2.Controls.Add(this.txtHeight);
            this.groupBox2.Controls.Add(this.txtDateOfBirth);
            this.groupBox2.Controls.Add(this.txthomePhone);
            this.groupBox2.Controls.Add(this.txtMotherName);
            this.groupBox2.Controls.Add(this.txtFatherName);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label35);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.label34);
            this.groupBox2.Controls.Add(this.label33);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold);
            this.groupBox2.Location = new System.Drawing.Point(12, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(984, 518);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Employee Information";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label36.Location = new System.Drawing.Point(665, 52);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(56, 16);
            this.label36.TabIndex = 62;
            this.label36.Text = "Height :";
            // 
            // txtDutyHour
            // 
            this.txtDutyHour.Location = new System.Drawing.Point(722, 130);
            this.txtDutyHour.Name = "txtDutyHour";
            this.txtDutyHour.Size = new System.Drawing.Size(87, 25);
            this.txtDutyHour.TabIndex = 24;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(449, 236);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(166, 25);
            this.dateTimePicker1.TabIndex = 19;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnReport);
            this.groupBox3.Controls.Add(this.btnClose);
            this.groupBox3.Controls.Add(this.btnAdd);
            this.groupBox3.Controls.Add(this.btnEdit);
            this.groupBox3.Controls.Add(this.btnDelete);
            this.groupBox3.Location = new System.Drawing.Point(788, 321);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(190, 191);
            this.groupBox3.TabIndex = 59;
            this.groupBox3.TabStop = false;
            // 
            // btnReport
            // 
            this.btnReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnReport.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnReport.Location = new System.Drawing.Point(97, 26);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(81, 29);
            this.btnReport.TabIndex = 6;
            this.btnReport.Text = "Report";
            this.btnReport.UseVisualStyleBackColor = false;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnClose.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnClose.Location = new System.Drawing.Point(97, 61);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(81, 29);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnAdd.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnAdd.Location = new System.Drawing.Point(10, 26);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(81, 29);
            this.btnAdd.TabIndex = 35;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnEdit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnEdit.Location = new System.Drawing.Point(10, 61);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(81, 29);
            this.btnEdit.TabIndex = 4;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = false;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnDelete.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnDelete.Location = new System.Drawing.Point(10, 96);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(81, 29);
            this.btnDelete.TabIndex = 5;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtParmanentPostoffice);
            this.groupBox1.Controls.Add(this.txtPresentPostOffice);
            this.groupBox1.Controls.Add(this.txtParmanentPoliceStation);
            this.groupBox1.Controls.Add(this.txtPresentPoliceStation);
            this.groupBox1.Controls.Add(this.txtParmanenthouseVillage);
            this.groupBox1.Controls.Add(this.txtPresentHouseVillage);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.txtParmanentDistrict);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.txtParmanentCountry);
            this.groupBox1.Controls.Add(this.cmbPresentDistrict);
            this.groupBox1.Controls.Add(this.cmbPresentCountry);
            this.groupBox1.Location = new System.Drawing.Point(7, 321);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(775, 191);
            this.groupBox1.TabIndex = 58;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Address";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.label13.Location = new System.Drawing.Point(412, 24);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(131, 16);
            this.label13.TabIndex = 41;
            this.label13.Text = "Parmanent Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.label3.Location = new System.Drawing.Point(18, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 16);
            this.label3.TabIndex = 41;
            this.label3.Text = "Present Address";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label31.Location = new System.Drawing.Point(451, 156);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(64, 16);
            this.label31.TabIndex = 41;
            this.label31.Text = "Country :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(55, 155);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 16);
            this.label14.TabIndex = 41;
            this.label14.Text = "Country :";
            // 
            // txtParmanentPostoffice
            // 
            this.txtParmanentPostoffice.Location = new System.Drawing.Point(522, 75);
            this.txtParmanentPostoffice.Name = "txtParmanentPostoffice";
            this.txtParmanentPostoffice.Size = new System.Drawing.Size(226, 25);
            this.txtParmanentPostoffice.TabIndex = 31;
            // 
            // txtPresentPostOffice
            // 
            this.txtPresentPostOffice.Location = new System.Drawing.Point(126, 74);
            this.txtPresentPostOffice.Name = "txtPresentPostOffice";
            this.txtPresentPostOffice.Size = new System.Drawing.Size(226, 25);
            this.txtPresentPostOffice.TabIndex = 26;
            // 
            // txtParmanentPoliceStation
            // 
            this.txtParmanentPoliceStation.Location = new System.Drawing.Point(522, 102);
            this.txtParmanentPoliceStation.Name = "txtParmanentPoliceStation";
            this.txtParmanentPoliceStation.Size = new System.Drawing.Size(226, 25);
            this.txtParmanentPoliceStation.TabIndex = 32;
            // 
            // txtPresentPoliceStation
            // 
            this.txtPresentPoliceStation.Location = new System.Drawing.Point(126, 101);
            this.txtPresentPoliceStation.Name = "txtPresentPoliceStation";
            this.txtPresentPoliceStation.Size = new System.Drawing.Size(226, 25);
            this.txtPresentPoliceStation.TabIndex = 27;
            // 
            // txtParmanenthouseVillage
            // 
            this.txtParmanenthouseVillage.Location = new System.Drawing.Point(522, 48);
            this.txtParmanenthouseVillage.Name = "txtParmanenthouseVillage";
            this.txtParmanenthouseVillage.Size = new System.Drawing.Size(226, 25);
            this.txtParmanenthouseVillage.TabIndex = 30;
            // 
            // txtPresentHouseVillage
            // 
            this.txtPresentHouseVillage.Location = new System.Drawing.Point(126, 47);
            this.txtPresentHouseVillage.Name = "txtPresentHouseVillage";
            this.txtPresentHouseVillage.Size = new System.Drawing.Size(226, 25);
            this.txtPresentHouseVillage.TabIndex = 25;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label30.Location = new System.Drawing.Point(412, 106);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(104, 16);
            this.label30.TabIndex = 3;
            this.label30.Text = "Police Station :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(16, 105);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(104, 16);
            this.label23.TabIndex = 3;
            this.label23.Text = "Police Station :";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label29.Location = new System.Drawing.Point(458, 131);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(58, 16);
            this.label29.TabIndex = 3;
            this.label29.Text = "District :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(62, 130);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(58, 16);
            this.label20.TabIndex = 3;
            this.label20.Text = "District :";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(433, 79);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(83, 16);
            this.label28.TabIndex = 3;
            this.label28.Text = "Post Office :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(37, 78);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 16);
            this.label18.TabIndex = 3;
            this.label18.Text = "Post Office :";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(413, 52);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(103, 16);
            this.label27.TabIndex = 3;
            this.label27.Text = "House/Village :";
            // 
            // txtParmanentDistrict
            // 
            this.txtParmanentDistrict.Font = new System.Drawing.Font("Arial", 9F);
            this.txtParmanentDistrict.FormattingEnabled = true;
            this.txtParmanentDistrict.Location = new System.Drawing.Point(522, 129);
            this.txtParmanentDistrict.Name = "txtParmanentDistrict";
            this.txtParmanentDistrict.Size = new System.Drawing.Size(226, 23);
            this.txtParmanentDistrict.TabIndex = 33;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(17, 51);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(103, 16);
            this.label17.TabIndex = 3;
            this.label17.Text = "House/Village :";
            // 
            // txtParmanentCountry
            // 
            this.txtParmanentCountry.Font = new System.Drawing.Font("Arial", 9F);
            this.txtParmanentCountry.FormattingEnabled = true;
            this.txtParmanentCountry.Location = new System.Drawing.Point(522, 154);
            this.txtParmanentCountry.Name = "txtParmanentCountry";
            this.txtParmanentCountry.Size = new System.Drawing.Size(226, 23);
            this.txtParmanentCountry.TabIndex = 34;
            // 
            // cmbPresentDistrict
            // 
            this.cmbPresentDistrict.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbPresentDistrict.FormattingEnabled = true;
            this.cmbPresentDistrict.Location = new System.Drawing.Point(126, 128);
            this.cmbPresentDistrict.Name = "cmbPresentDistrict";
            this.cmbPresentDistrict.Size = new System.Drawing.Size(226, 23);
            this.cmbPresentDistrict.TabIndex = 28;
            // 
            // cmbPresentCountry
            // 
            this.cmbPresentCountry.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbPresentCountry.FormattingEnabled = true;
            this.cmbPresentCountry.Location = new System.Drawing.Point(126, 153);
            this.cmbPresentCountry.Name = "cmbPresentCountry";
            this.cmbPresentCountry.Size = new System.Drawing.Size(226, 23);
            this.cmbPresentCountry.TabIndex = 29;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(834, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(138, 157);
            this.pictureBox1.TabIndex = 57;
            this.pictureBox1.TabStop = false;
            // 
            // cmbCategory
            // 
            this.cmbCategory.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Location = new System.Drawing.Point(110, 264);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(166, 23);
            this.cmbCategory.TabIndex = 10;
            // 
            // cmbDesignation
            // 
            this.cmbDesignation.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbDesignation.FormattingEnabled = true;
            this.cmbDesignation.Location = new System.Drawing.Point(449, 210);
            this.cmbDesignation.Name = "cmbDesignation";
            this.cmbDesignation.Size = new System.Drawing.Size(166, 23);
            this.cmbDesignation.TabIndex = 18;
            // 
            // cmbStatus
            // 
            this.cmbStatus.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Location = new System.Drawing.Point(110, 238);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(166, 23);
            this.cmbStatus.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(30, 271);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 16);
            this.label4.TabIndex = 41;
            this.label4.Text = "Category :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(357, 214);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 16);
            this.label2.TabIndex = 41;
            this.label2.Text = "Designation :";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label32.Location = new System.Drawing.Point(334, 238);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(113, 16);
            this.label32.TabIndex = 41;
            this.label32.Text = "Date Of Joining :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(48, 242);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 16);
            this.label1.TabIndex = 41;
            this.label1.Text = "Status :";
            // 
            // txtEmailAddress
            // 
            this.txtEmailAddress.Location = new System.Drawing.Point(449, 264);
            this.txtEmailAddress.Name = "txtEmailAddress";
            this.txtEmailAddress.Size = new System.Drawing.Size(166, 25);
            this.txtEmailAddress.TabIndex = 20;
            // 
            // txtWeight
            // 
            this.txtWeight.Location = new System.Drawing.Point(722, 75);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(87, 25);
            this.txtWeight.TabIndex = 22;
            // 
            // txtHeight
            // 
            this.txtHeight.Location = new System.Drawing.Point(722, 48);
            this.txtHeight.Name = "txtHeight";
            this.txtHeight.Size = new System.Drawing.Size(87, 25);
            this.txtHeight.TabIndex = 21;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label35.Location = new System.Drawing.Point(342, 266);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(105, 16);
            this.label35.TabIndex = 3;
            this.label35.Text = "Email Address :";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label34.Location = new System.Drawing.Point(661, 79);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(60, 16);
            this.label34.TabIndex = 3;
            this.label34.Text = "Weight :";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label33.Location = new System.Drawing.Point(644, 134);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(77, 16);
            this.label33.TabIndex = 3;
            this.label33.Text = "Duty Hour :";
            // 
            // Employee_Information
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(224)))), ((int)(((byte)(248)))));
            this.ClientSize = new System.Drawing.Size(1008, 535);
            this.Controls.Add(this.groupBox2);
            this.Name = "Employee_Information";
            this.Text = "Employee_Information";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtFatherName;
        private System.Windows.Forms.TextBox txtMotherName;
        private System.Windows.Forms.TextBox txthomePhone;
        private System.Windows.Forms.TextBox txtDateOfBirth;
        private System.Windows.Forms.TextBox txtNationalID;
        private System.Windows.Forms.TextBox txtSpouseName;
        private System.Windows.Forms.TextBox txtOfficePhone;
        private System.Windows.Forms.TextBox txtEmployeeName;
        private System.Windows.Forms.TextBox txtMobileNo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox cmbSalution;
        private System.Windows.Forms.ComboBox cmbDegree;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cmbBloodGroup;
        private System.Windows.Forms.ComboBox cmbMaritalStatus;
        private System.Windows.Forms.ComboBox cmbNationality;
        private System.Windows.Forms.ComboBox cmbReligion;
        private System.Windows.Forms.ComboBox cmbGender;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbDesignation;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbCategory;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtPresentPostOffice;
        private System.Windows.Forms.TextBox txtPresentPoliceStation;
        private System.Windows.Forms.TextBox txtPresentHouseVillage;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cmbPresentDistrict;
        private System.Windows.Forms.ComboBox cmbPresentCountry;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtParmanentPostoffice;
        private System.Windows.Forms.TextBox txtParmanentPoliceStation;
        private System.Windows.Forms.TextBox txtParmanenthouseVillage;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox txtParmanentDistrict;
        private System.Windows.Forms.ComboBox txtParmanentCountry;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnReport;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox txtWeight;
        private System.Windows.Forms.TextBox txtHeight;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtEmailAddress;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtDutyHour;

    }
}