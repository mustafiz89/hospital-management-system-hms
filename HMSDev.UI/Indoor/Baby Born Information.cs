﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using POS_SYSTEM;
using HMS_Helper.Model.Indoor;
using HMS.BIZ.Indoor;
using HMS.Model.Inddor;
using IA.BIZ.Setup.Common_Setup;

namespace HMSDev.UI.Indoor
{
    public partial class Baby_Born_Information : Form
    {
        public Baby_Born_Information()
        {
            InitializeComponent();
            BindComboGender();
            BindComboBloodGroup();
            BindComboReligion();
            BindComboNationality();
        }

        private void BindComboGender()
        {
            cmbGender.DataSource = CommonSetupFetch.getAllGENDER();
            cmbGender.ValueMember = "GENDER_ID";
            cmbGender.DisplayMember = "GENDER_NAME";
            cmbGender.SelectedIndex = 0;
        }

        

        private void BindComboBloodGroup()
        {
            cmbBloodGroup.DataSource = CommonSetupFetch.getAllBLOOD_GROUP();
            cmbBloodGroup.ValueMember = "BLOOD_GROUP_ID";
            cmbBloodGroup.DisplayMember = "BLOOD_GROUP_NAME";
            cmbBloodGroup.SelectedIndex = 0;
        }

        private void BindComboReligion()
        {
            cmbReligion.DataSource = CommonSetupFetch.getRELIGION();
            cmbReligion.ValueMember = "RELIGION_ID";
            cmbReligion.DisplayMember = "RELIGION_NAME";
            cmbReligion.SelectedIndex = 0;
        }


        private void BindComboNationality()
        {
            cmbNationality.DataSource = CommonSetupFetch.getAllNATIONALITY();
            cmbNationality.ValueMember = "NATIONALITY_ID";
            cmbNationality.DisplayMember = "NATIONALITY_NAME";
            cmbNationality.SelectedIndex = 0;
        }

        private void ClearTextbox()
        {
            txtBabyName.Text = "";
            txtFathersName.Text = "";
            txtMothersName.Text = "";
            txtPatientAddmissionNo.Text = "";
            txtpatientName.Text = "";
            txtPermanentAddress.Text = "";
            txtPresentAddress.Text = "";
        }

       

        private void btnAdd_Click(object sender, EventArgs e)
        {
            BABY_BORN_INFORMATION objBabyBornInformation = new BABY_BORN_INFORMATION();
            objBabyBornInformation.PATIENT_ADDMISSION_NO = txtPatientAddmissionNo.Text;
            objBabyBornInformation.PATIENT_NAME = txtpatientName.Text;
            objBabyBornInformation.BABY_NAME = txtBabyName.Text;
            objBabyBornInformation.BABY_FATHER_NAME = txtFathersName.Text;
            objBabyBornInformation.BABY_MOTHER_NAME = txtMothersName.Text;
            objBabyBornInformation.PRESENT_ADDRESS = txtPresentAddress.Text;
            objBabyBornInformation.PERMANENT_ADDRESS = txtPermanentAddress.Text;
            objBabyBornInformation.BORN_DATE = dateTimePicker1.Value;
            objBabyBornInformation.NATIONALITY_ID =new Guid (cmbNationality.SelectedValue.ToString());
            objBabyBornInformation.GENDER_ID =new Guid (cmbGender.SelectedValue.ToString());
            objBabyBornInformation.BLOOD_GROUP_ID =new Guid (cmbBloodGroup.SelectedValue.ToString());
            objBabyBornInformation.RELIGION_ID = new Guid(cmbReligion.SelectedValue.ToString());

            int i = Indoor_Task.SaveBabyBorninformation(objBabyBornInformation);

            if (i > 0)
            {
                MessageBox.Show("Save Successfully");

                ClearTextbox();
            }
            else
                MessageBox.Show("Save Not Successfully");
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
