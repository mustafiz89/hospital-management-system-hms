﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using POS_SYSTEM;
using HMS_Helper.Model.Indoor;
using HMS.BIZ.Indoor;
using IA.BIZ.Setup.Common_Setup;
using IA.BIZ.Setup.Doctor;
using IA.BIZ.Setup.Indoor_Setup;
using IA.BIZ.Setup.Pathology_Setup;
using IA.BIZ.Setup.Settings;

namespace HMSDev.UI
{
    public partial class Indoor_Patient_Addmission : Form
    {
        public Indoor_Patient_Addmission()
        {
            InitializeComponent();
            cmbDOBdate.SelectedIndex = 0;
            cmbDOBMonth.SelectedIndex = 0;
            cmbDOByear.SelectedIndex = 0;
            cmbCollectionType.SelectedIndex = 0;
            BindComboSalution();
            BindComboGender();
            BindComboMaritalStatus();
            BindComboBloodGroup();
            BindComboReligion();
            BindComboNationality();
            BindComboOccupation();
            BindComboIndoorDocotr();
            BindComboPatientDepartment();
            BindComboDisease();
            BindComboRoomCategory();
            BindComboBed();
            BindComboReferencePerson();
            BindComboCity();

        }

        private void BindComboSalution()
        {
            cmbSalution.DataSource = CommonSetupFetch.getALLSALUTION();
            cmbSalution.ValueMember = "SALUTION_ID";
            cmbSalution.DisplayMember = "SALUTION_NAME";
            cmbSalution.SelectedIndex = 0;
        }

        private void BindComboGender()
        {
            cmbGender.DataSource = CommonSetupFetch.getAllGENDER();
            cmbGender.ValueMember = "GENDER_ID";
            cmbGender.DisplayMember = "GENDER_NAME";
            cmbGender.SelectedIndex = 0;
        }

        private void BindComboMaritalStatus()
        {
            cmbmaritalStatus.DataSource = CommonSetupFetch.getAllMARITALSTATUS();
            cmbmaritalStatus.ValueMember = "MARITAL_STATUS_ID";
            cmbmaritalStatus.DisplayMember = "MARITAL_STATUS_NAME";
            cmbmaritalStatus.SelectedIndex = 0;
        }

        private void BindComboBloodGroup()
        {
            cmbBloodGroup.DataSource = CommonSetupFetch.getAllBLOOD_GROUP();
            cmbBloodGroup.ValueMember = "BLOOD_GROUP_ID";
            cmbBloodGroup.DisplayMember = "BLOOD_GROUP_NAME";
            cmbBloodGroup.SelectedIndex = 0;
        }

        private void BindComboReligion()
        {
            cmbReligion.DataSource = CommonSetupFetch.getRELIGION();
            cmbReligion.ValueMember = "RELIGION_ID";
            cmbReligion.DisplayMember = "RELIGION_NAME";
            cmbReligion.SelectedIndex = 0;
        }


        private void BindComboNationality()
        {
            cmbNationality.DataSource = CommonSetupFetch.getAllNATIONALITY();
            cmbNationality.ValueMember = "NATIONALITY_ID";
            cmbNationality.DisplayMember = "NATIONALITY_NAME";
            cmbNationality.SelectedIndex = 0;
        }


        private void BindComboOccupation()
        {
            cmbOccupation.DataSource = CommonSetupFetch.getALLOCCUPATION();
            cmbOccupation.ValueMember = "OCCUPATION_ID";
            cmbOccupation.DisplayMember = "OCCUPATION_NAME";
            cmbOccupation.SelectedIndex = 0;
        }

        private void BindComboIndoorDocotr()
        {
            cmbAdmittedUnder.DataSource = DoctorFetch.getALLIndoorDoctor();
            cmbAdmittedUnder.ValueMember = "DOCTOR_ID";
            cmbAdmittedUnder.DisplayMember = "DOCTOR_NAME";
            cmbAdmittedUnder.SelectedIndex = 0;
        }

        private void BindComboPatientDepartment()
        {
            cmbAdmittedDept.DataSource = IndoorFetch.getALLPatientDepartment();
            cmbAdmittedDept.ValueMember = "PATIENT_DEPARTMENT_ID";
            cmbAdmittedDept.DisplayMember = "PATIENT_DEPARTMENT_NAME";
            cmbAdmittedDept.SelectedIndex = 0;
        }

        private void BindComboDisease()
        {
            cmbDiseaseName.DataSource = IndoorFetch.getALLDisease();
            cmbDiseaseName.ValueMember = "DISEASE_ID";
            cmbDiseaseName.DisplayMember = "DISEASE_NAME";
            cmbDiseaseName.SelectedIndex = 0;
        }

        private void BindComboRoomCategory()
        {
            cmbRoomBedCategory.DataSource = IndoorFetch.getALLRoomCategory();
            cmbRoomBedCategory.ValueMember = "ROOM_CATEGORY_ID";
            cmbRoomBedCategory.DisplayMember = "ROOM_CATEGORY_NAME";
            cmbRoomBedCategory.SelectedIndex = 0;
        }

        private void BindComboBed()
        {
            dgvRoomBedInformation1.AutoGenerateColumns = false;
            dgvRoomBedInformation1.DataSource = IndoorFetch.getALLBed(new Guid( cmbRoomBedCategory.SelectedValue.ToString()));
        }

        private void cmbRoomBedCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindComboBed();
            }
            catch { }
        }

        private void BindComboReferencePerson()
        {
            cmbReferal.DataSource = PathologyFetch.getALLReferencePerson();
            cmbReferal.ValueMember = "PATHOLOGY_REFERENCE_PERSON_ID";
            cmbReferal.DisplayMember = "PATHOLOGY_REFERENCE_PERSON_NAME";
            cmbReferal.SelectedIndex = 0;
        }

        private void BindComboCity()
        {
            cmbCity.DataSource = SettingsFetch.getALLCity();
            cmbCity.ValueMember = "CITY_ID";
            cmbCity.DisplayMember = "CITY_NAME";
            cmbCity.SelectedIndex = 0;
        }



        private void btnSave_Click(object sender, EventArgs e)
        {
            string x = cmbDOBdate.Text.ToString();
            string y = cmbDOBMonth.Text.ToString();
            string z = cmbDOByear.Text.ToString();

            REGISTRATION_ADMISSION objRegistrationAddmission = new REGISTRATION_ADMISSION();
            objRegistrationAddmission.PATIENT_REGISTRATION_NO =int.Parse(txtRegistrationNo.Text);
            objRegistrationAddmission.ADMISSION_NO = txtAddmissionNo.Text;
            objRegistrationAddmission.START_DATE = dateTimePicker1.Value;
            objRegistrationAddmission.SALUTION_ID =new Guid (cmbSalution.SelectedValue.ToString());
            objRegistrationAddmission.PATIENT_NAME = txtPatientname.Text;
            objRegistrationAddmission.PATIENT_DOB = x + "." + y + "." + z;
            objRegistrationAddmission.PATIENT_AGE =int.Parse (txtAge.Text);
            objRegistrationAddmission.GENDER_ID = new Guid(cmbGender.SelectedValue.ToString());
            objRegistrationAddmission.MARITAL_STATUS_ID = new Guid(cmbmaritalStatus.SelectedValue.ToString());
            objRegistrationAddmission.BLOOD_GROUP_ID = new Guid(cmbBloodGroup.SelectedValue.ToString());
            objRegistrationAddmission.PATIENT_PHONE = txtMobile.Text;
            objRegistrationAddmission.PATIENT_PRESENT_ADDRESS = txtAddress.Text;
            objRegistrationAddmission.RELIGION_ID = new Guid(cmbReligion.SelectedValue.ToString());
            objRegistrationAddmission.CITY_ID = new Guid(cmbCity.SelectedValue.ToString());
            objRegistrationAddmission.NATIONALITY_ID = new Guid(cmbNationality.SelectedValue.ToString());
            objRegistrationAddmission.OCCUPATION_ID = new Guid(cmbOccupation.SelectedValue.ToString());
            objRegistrationAddmission.PATIENT_FATHER_NAME = txtFatherinfo.Text;
            objRegistrationAddmission.PATIENT_MOTHER_NAME = txtMotherinfo.Text;
            objRegistrationAddmission.RELATION_GURDIAN = txtRelation.Text;
            objRegistrationAddmission.GURDIAN_NAME = txtGurdianName.Text;
            objRegistrationAddmission.GURDIAN_PHONE = txtGurdianMobile.Text;
            objRegistrationAddmission.ADMITTED_UNDER_ID = new Guid(cmbAdmittedUnder.SelectedValue.ToString());
            objRegistrationAddmission.ADMITTED_DEPARTMENT_ID = new Guid(cmbAdmittedDept.SelectedValue.ToString());
            objRegistrationAddmission.PATIENT_DISEASE_ID = new Guid(cmbDiseaseName.SelectedValue.ToString());
            objRegistrationAddmission.REFFERENCE_ID = new Guid(cmbReferal.SelectedValue.ToString());
            objRegistrationAddmission.REFERENCE_ADDRESS = txtReferalAddress.Text;
            objRegistrationAddmission.COLLECTION_TYPE = cmbCollectionType.Text.ToString();
            objRegistrationAddmission.COLLECTED_AMOUNT =Convert.ToDecimal (txtCollectedAmount.Text);
            objRegistrationAddmission.ADDMISSION_CHARGE =int.Parse (txtAdmissionFee.Text);
            objRegistrationAddmission.BED_ID = new Guid(dgvRoomBedInformation2.SelectedRows[0].Cells[0].Value.ToString());


            int i = Indoor_Task.saveREGISTRATION_ADMISSION(objRegistrationAddmission);
            if (i > 0)
            {
                
                MessageBox.Show("Save Successfully");

            }
            else
                MessageBox.Show("Save Not Successfully");
        }

        private void dgvRoomBedInformation1_MouseClick(object sender, MouseEventArgs e)
        {

            foreach (DataGridViewRow row in this.dgvRoomBedInformation1.SelectedRows)
            {
                object[] rowData = new object[row.Cells.Count];
                for (int i = 0; i < rowData.Length; ++i)
                {
                    rowData[i] = row.Cells[i].Value;
                }
                this.dgvRoomBedInformation2.Rows.Add(rowData);
            }
                        
        }

    }
}
