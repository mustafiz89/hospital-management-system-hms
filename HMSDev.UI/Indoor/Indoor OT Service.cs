﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.BIZ.Setup.OT_Setup;
using IA.Model.Setup.Other.OT_Setup;

namespace HMSDev.UI
{
    public partial class Indoor_OT_Service : Form
    {
        public Indoor_OT_Service()
        {
            InitializeComponent();
            BindComboComments();
        }

        private void BindComboComments()
        {
            cmbComments.DataSource = OTFetch.getALLOTComments();
            cmbComments.ValueMember = "OT_COMMENTS_ID";
            cmbComments.DisplayMember = "OT_COMMENT";
            cmbComments.SelectedIndex = 0;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

        }
    }
}
