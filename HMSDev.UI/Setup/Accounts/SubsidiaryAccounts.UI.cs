﻿///*************************************************************************************
///	|| Creation History ||
///-------------------------------------------------------------------------------------
/// Copyright	  : Copyright© Dynamic Software Ltd. All rights reserved.
///	NameSpace	  :	IA.UI.Setup.Accounts
/// Class Name    : SubsidiaryAccounts
/// Inherits      : None
///	Author	      :	MD Billal 
///	Purpose	      :	This is a Class to save,update and delete SubsidiaryAccounts
///                 
///	Creation Date :	1/11/2013
/// ====================================================================================
/// || Modification History ||
///  -----------------------------------------------------------------------------------
/// Sl No.	Date:		Author:			Ver:	Area of Change:
/// 1        11/24/2013   Partha          1.01    save,update and delete Button
///	 -----------------------------------------------------------------------------------
///	************************************************************************************

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.BIZ.Setup.Accounts;
using IA.Model.Setup.Accounts;
using POS_SYSTEM;
using DynamicSoft.Security;

namespace IA.UI.Setup.Accounts
{
    public partial class SubsidiaryAccounts : Form
    {
        string user_id = "";
        public SubsidiaryAccounts(string _user_id)
        {
            InitializeComponent();
            BindCombo();
            BindComboControl();
            bindgrid();
            user_id = _user_id;
            lblUserID.Text = user_id;
            cmbAcccountGroup.Focus();
        }
        private void bindgrid()
        {

            dgSubsidiary.AutoGenerateColumns = false;
            dgSubsidiary.DataSource = AccountsFetch.GetAllSubsidiaryAccount2(DASSessionInfoClass.COMPANY_ID);

        }
        private void BindCombo()
        {
            try
            {
                cmbAcccountGroup.DataSource = AccountsFetch.GetAllAccountGroup();
                cmbAcccountGroup.ValueMember = "ACCOUNT_GROUP_CODE";
                cmbAcccountGroup.DisplayMember = "ACCOUNT_GROUP_NAME";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
        private void BindComboControl()
        {
            try
            {
                List<CONTROL_ACCOUNT> objCONTROL_ACCOUNT = AccountsFetch.GetAllControlAccount(cmbAcccountGroup.SelectedValue.ToString(), 1, DASSessionInfoClass.COMPANY_ID);
                if (objCONTROL_ACCOUNT.Count > 0)
                {
                    cmbControlName.DataSource = objCONTROL_ACCOUNT;
                    cmbControlName.ValueMember = "CONTROL_CODE";
                    cmbControlName.DisplayMember = "CONTROL_NAME";
                }
                else
                {
                    cmbControlName.DataSource = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
        private void cmbAcccountGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComboControl();
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int txtLength = txtSubsideryName.Text.Length;
                if (txtLength > 25)
                {
                    DSMessageBox objDSMessageBox = new DSMessageBox("SUBSIDIARY NAME is Too Big", 3);
                    objDSMessageBox.Show();
                    return;
                }
                if (txtSubsideryName.Text.Length == 0)
                {
                    DSMessageBox objDSMessageBox = new DSMessageBox("Insert SUBSIDIARY NAME", 3);
                    objDSMessageBox.Show();
                    return;
                }

                ACCOUNT_SUBSIDIARY objACCOUNT_SUBSIDIARY = new ACCOUNT_SUBSIDIARY();
                //objACCOUNT_SUBSIDIARY.SUBSIDIARY_CODE = "1";
                objACCOUNT_SUBSIDIARY.ACCOUNT_GROUP_CODE = cmbAcccountGroup.SelectedValue.ToString();
                objACCOUNT_SUBSIDIARY.CONTROL_CODE = cmbControlName.SelectedValue.ToString();
                objACCOUNT_SUBSIDIARY.SUBSIDIARY_NAME = txtSubsideryName.Text;
                objACCOUNT_SUBSIDIARY.COMPANY_ID = DASSessionInfoClass.COMPANY_ID;
                // objACCOUNT_SUBSIDIARY.NAME_VALUE_LIST = 'I';
                objACCOUNT_SUBSIDIARY = AccountSave.SaveSubsidiaryAccounts(objACCOUNT_SUBSIDIARY);
                if (!string.IsNullOrEmpty(objACCOUNT_SUBSIDIARY.SUBSIDIARY_CODE))
                {
                    bindgrid();
                    clearfield();
                    DSMessageBox objDSMessageBox = new DSMessageBox("Save Successfully", 1);
                    objDSMessageBox.Show();
                }
                else
                {
                    clearfield();
                    DSMessageBox objDSMessageBox = new DSMessageBox("Save Not Successfully", 2);
                    objDSMessageBox.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void dgSubsidiary_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ACCOUNT_SUBSIDIARY objACCOUNT_SUBSIDIARY = new ACCOUNT_SUBSIDIARY();
                objACCOUNT_SUBSIDIARY = AccountsFetch.GetSubsidiaryAccount(dgSubsidiary.Rows[e.RowIndex].Cells[0].Value.ToString());
                txtSubCode.Text = dgSubsidiary.Rows[e.RowIndex].Cells[0].Value.ToString();
                //cmbAcccountGroup.SelectedValue = objCONTROL_ACCOUNT.ACCOUNT_GROUP_CODE;
                txtSubsideryName.Text = objACCOUNT_SUBSIDIARY.SUBSIDIARY_NAME;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
        private void clearfield()
        {


            txtSubsideryName.Text = "";
            cmbAcccountGroup.SelectedIndex = 0;

        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                int txtLength = txtSubsideryName.Text.Length;
                if (txtLength > 25)
                {
                    DSMessageBox objDSMessageBox = new DSMessageBox("SUBSIDIARY NAME is Too Big", 3);
                    objDSMessageBox.Show();
                    return;
                }
                if (txtSubsideryName.Text.Length == 0)
                {
                    DSMessageBox objDSMessageBox = new DSMessageBox("Insert SUBSIDIARY NAME", 3);
                    objDSMessageBox.Show();
                    return;
                }
                if (txtSubsideryName.Text.Length == 0)
                {
                    DSMessageBox objDSMessageBox = new DSMessageBox("Insert SUBSIDIARY NAME", 3);
                    objDSMessageBox.Show();

                    // POS_SYSTEM.DSMessageBox objmsg = new POS_SYSTEM.DSMessageBox("Insert Data!!");
                    return;
                }
                string SUBSIDIARY_CODE = txtSubCode.Text;
                ACCOUNT_SUBSIDIARY objACCOUNT_SUBSIDIARY = new ACCOUNT_SUBSIDIARY();
                objACCOUNT_SUBSIDIARY.SUBSIDIARY_CODE = SUBSIDIARY_CODE;
                objACCOUNT_SUBSIDIARY.SUBSIDIARY_NAME = txtSubsideryName.Text;
                objACCOUNT_SUBSIDIARY.COMPANY_ID = DASSessionInfoClass.COMPANY_ID;
                int i = AccountSave.UpdateACCOUNT_SUBSIDIARY(objACCOUNT_SUBSIDIARY);
                if (i > 0)
                {
                    bindgrid();
                    DSMessageBox objDSMessageBox = new DSMessageBox("Update Successfully", 1);
                    objDSMessageBox.Show();
                    clearfield();
                }
                else
                {
                    DSMessageBox objDSMessageBox = new DSMessageBox("Update Not Successfully", 2);
                    objDSMessageBox.Show();
                    clearfield();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete this feature?",

        "Confirm Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    int i = 0;
                    //string SUBSIDIARY_CODE = txtSubCode.Text;
                    ACCOUNT_SUBSIDIARY objACCOUNT_SUBSIDIARY = new ACCOUNT_SUBSIDIARY();
                    objACCOUNT_SUBSIDIARY.SUBSIDIARY_CODE = txtSubCode.Text;
                    objACCOUNT_SUBSIDIARY.COMPANY_ID = DASSessionInfoClass.COMPANY_ID;
                    i = AccountSave.DeleteACCOUNT_SUBSIDIARY(objACCOUNT_SUBSIDIARY);
                    if (i > 0)
                    {
                        bindgrid();
                        clearfield();
                        DSMessageBox objDSMessageBox = new DSMessageBox("Delete Successfully", 1);
                        objDSMessageBox.Show();
                    }
                    else
                    {
                        DSMessageBox objDSMessageBox = new DSMessageBox("Delete Not Successfully", 2);
                        objDSMessageBox.Show();
                        clearfield();
                    }
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
        }
        private void txtSubsideryName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }
        private void btnAdd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }
        private void cmbAcccountGroup_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }
        private void cmbControlName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }
    }
}
