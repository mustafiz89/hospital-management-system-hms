﻿///*************************************************************************************
///	|| Creation History ||
///-------------------------------------------------------------------------------------
/// Copyright	  : Copyright© Dynamic Software Ltd. All rights reserved.
///	NameSpace	  :	IA.UI.Setup.Accounts
/// Class Name    : Accounts
/// Inherits      : None
///	Author	      :	MD Billal 
///	Purpose	      :	This is a Class to save,update and delete Accounts
///                 
///	Creation Date :	1/11/2013
/// ====================================================================================
/// || Modification History ||
///  -----------------------------------------------------------------------------------
/// Sl No.	Date:		Author:			Ver:	Area of Change:
/// 1        11/24/2013   Partha          1.01    save,update and delete Button
///	 -----------------------------------------------------------------------------------
///	************************************************************************************

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows.Forms;
using DynamicSoft.Security;
using IA.BIZ.Setup.Accounts;
using IA.Model.Setup.Accounts;
using IA.Model.Setup.Other;
using IA.Provider;
using POS_SYSTEM;

namespace IA.UI.Setup.Accounts
{
    public partial class Accounts : Form
    {
        string user_id = "";
        public Accounts(string _user_id)
        {
            InitializeComponent();
            BindCombo();

            BindComboACCOUNT_SUBSIDIARY();
            BindAccountgrid();
            bindtree();
            user_id = _user_id;
            lblUserID.Text = user_id;
        }
        private void clearfield()
        {
            cmbAcccountGroup.SelectedIndex = 0;
            txtAccountsDescription.Text = "";
            //txtOpeningBalance.Text = "";

        }
        //private void bindtree1() {


        //    treeView1.Nodes.Add("Node1");
        //    treeView1.Nodes[0].Nodes.Add("Node1.1");
        //    treeView1.Nodes[0].Nodes[0].Nodes.Add("Node1.1.1");
        //    treeView1.Nodes[0].Nodes[0].Nodes.Add("Node1.1.2");
        //    treeView1.Nodes[0].Nodes[0].Nodes.Add("Node1.1.3");
        //    treeView1.Nodes[0].Nodes[0].Nodes[1].Nodes.Add("Node1.1.2");
        //    treeView1.Nodes[0].Nodes[0].Nodes[1].Nodes.Add("Node1.1.2");
        //    treeView1.Nodes[0].Nodes[0].Nodes[2].Nodes.Add("Node1.1.3");
        //    treeView1.Nodes.Add("Node2");
        //    treeView1.Nodes[1].Nodes.Add("Node2.1");
        //    treeView1.Nodes[1].Nodes[0].Nodes.Add("Node2.1.1");
        //}
        //private void treeView1_NodeMouseClick_1(object sender, TreeNodeMouseClickEventArgs e)
        //{

        //txtAccountID.Text = e.Node.Text;

        //}
        private void bindtree()
        {
            //treeView1.Nodes.Add("Node1");
            //treeView1.Nodes.Add("Node2");
            //treeView1.Nodes[0].Nodes.Add("Node1.1");
            //treeView1.Nodes[1].Nodes.Add("Node2.1");
            //treeView1.Nodes[1].Nodes[0].Nodes.Add("Node2.1.2");
            SqlConnection conn = ConnectionClass.GetConnection();
            string query = "SELECT * from ACCOUNT_GROUP";
            using (SqlCommand objSqlCommand = new SqlCommand(query, conn))
            {
                int i = 0;
                int j = 0;
                int k = 0;
                conn.Open();
                SqlDataReader dr = objSqlCommand.ExecuteReader();
                while (dr.Read())
                {
                    j = 0;
                    treeView1.Nodes.Add(dr["ACCOUNT_GROUP_NAME"].ToString());
                    int categoryid = Convert.ToInt32(dr["ACCOUNT_GROUP_CODE"]);
                    SqlConnection conn1 = ConnectionClass.GetConnection();
                    string query1 = "SELECT * from CONTROL_ACCOUNT where ACCOUNT_GROUP_CODE='" + categoryid + "' and COMPANY_ID='" + DASSessionInfoClass.COMPANY_ID + "' ";
                    using (SqlCommand objSqlCommand1 = new SqlCommand(query1, conn1))
                    {
                        try
                        {
                            conn1.Open();
                            SqlDataReader dr1 = objSqlCommand1.ExecuteReader();
                            while (dr1.Read())
                            {
                                k = 0;
                                //txttree.Text = dr1["CONTROL_CODE"].ToString();
                                treeView1.Nodes[i].Nodes.Add(dr1["CONTROL_NAME"].ToString());
                                //treeView1.Nodes[i].Nodes.Add("sadsad");
                                SqlConnection conn2 = ConnectionClass.GetConnection();
                                string query2 = "SELECT * from ACCOUNT_SUBSIDIARY where CONTROL_CODE='" + Convert.ToInt32(dr1["CONTROL_CODE"]) + "' and COMPANY_ID='" + DASSessionInfoClass.COMPANY_ID + "' ";
                                using (SqlCommand objSqlCommand2 = new SqlCommand(query2, conn2))
                                {
                                    try
                                    {
                                        conn2.Open();
                                        SqlDataReader dr2 = objSqlCommand2.ExecuteReader();
                                        while (dr2.Read())
                                        {
                                            treeView1.Nodes[i].Nodes[j].Nodes.Add(dr2["SUBSIDIARY_NAME"].ToString());
                                            string subsidiarycode = dr2["SUBSIDIARY_CODE"].ToString();
                                            SqlConnection conn3 = ConnectionClass.GetConnection();
                                            string query3 = "SELECT * from ACCOUNTS where SUBSIDIARY_CODE='" + subsidiarycode + "' and COMPANY_ID='" + DASSessionInfoClass.COMPANY_ID + "' and BRANCH_ID='" + DASSessionInfoClass.BRANCH_ID + "' ";
                                            using (SqlCommand objSqlCommand3 = new SqlCommand(query3, conn3))
                                            {
                                                try
                                                {
                                                    conn3.Open();
                                                    SqlDataReader dr3 = objSqlCommand3.ExecuteReader();
                                                    while (dr3.Read())
                                                    {
                                                        treeView1.Nodes[i].Nodes[j].Nodes[k].Nodes.Add(dr3["DESCRIPTION"].ToString());
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    MessageBox.Show(ex.Message);
                                                }
                                            }
                                            k++;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.Message);
                                    }
                                }
                                j++;
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    i++;
                }
            }
            conn.Close();
        }
        private void BindAccountgrid()
        {
            dgAccounts.AutoGenerateColumns = false;
            dgAccounts.DataSource = AccountsFetch.GetAccountTreeView(DASSessionInfoClass.COMPANY_ID,DASSessionInfoClass.BRANCH_ID);
        }
        private void BindCombo()
        {
            cmbAcccountGroup.DataSource = AccountsFetch.GetAllAccountGroup();
            cmbAcccountGroup.ValueMember = "ACCOUNT_GROUP_CODE";
            cmbAcccountGroup.DisplayMember = "ACCOUNT_GROUP_NAME";
            cmbAcccountGroup.SelectedIndex = 0;
            BindComboControl();
            BindComboACCOUNT_SUBSIDIARY();
        }
        private void BindComboControl()
        {
            List<CONTROL_ACCOUNT> objCONTROL_ACCOUNT = AccountsFetch.GetAllControlAccount(cmbAcccountGroup.SelectedValue.ToString(), 1, DASSessionInfoClass.COMPANY_ID);
            if (objCONTROL_ACCOUNT.Count > 0)
            {
                cmbControlName.DataSource = objCONTROL_ACCOUNT;
                cmbControlName.ValueMember = "CONTROL_CODE";
                cmbControlName.DisplayMember = "CONTROL_NAME";
                BindComboACCOUNT_SUBSIDIARY();
            }
            else
            {
                cmbControlName.DataSource = null;
                dopSubsidiaryName.DataSource = null;
            }
        }
        private void BindComboACCOUNT_SUBSIDIARY()
        {
            try
            {
                List<ACCOUNT_SUBSIDIARY> objACCOUNT_SUBSIDIARY = AccountsFetch.GetAllSubsidiaryAccount(cmbAcccountGroup.SelectedValue.ToString(), cmbControlName.SelectedValue.ToString(), DASSessionInfoClass.COMPANY_ID);
                if (objACCOUNT_SUBSIDIARY.Count > 0)
                {
                    dopSubsidiaryName.DataSource = objACCOUNT_SUBSIDIARY;
                    dopSubsidiaryName.ValueMember = "SUBSIDIARY_CODE";
                    dopSubsidiaryName.DisplayMember = "SUBSIDIARY_NAME";
                }
                else
                {
                    dopSubsidiaryName.DataSource = null;
                }
            }
            catch { }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int txtLength = txtAccountsDescription.Text.Length;
                if (txtLength > 50)
                {
                    DSMessageBox objDSMessageBox = new DSMessageBox("ACCOUNTS DESCRIPTION is Too Big", 3);
                    objDSMessageBox.Show();
                    return;
                }
                if (txtAccountsDescription.Text.Length == 0)
                {
                    DSMessageBox objDSMessageBox = new DSMessageBox("Insert ACCOUNTS DESCRIPTION", 3);
                    objDSMessageBox.Show();
                    return;
                }
                ACCOUNTS objACCOUNTS = new ACCOUNTS();
                objACCOUNTS.CONTROL_CODE = cmbControlName.SelectedValue.ToString();
                objACCOUNTS.GROUP_CODE = cmbAcccountGroup.SelectedValue.ToString();
                objACCOUNTS.MANDATORY = 0;
                objACCOUNTS.OPENING_DATE = Convert.ToDateTime(txtOpenningDate.Value);
                objACCOUNTS.OPENING_TIME = DateTime.Now.ToShortTimeString();
                objACCOUNTS.OPENING_BALANCE = Convert.ToDecimal(txtOpeningBalance.Text);
                objACCOUNTS.NOTES = "Made By " + ' ' + DASSessionInfoClass.USER_ID;
                objACCOUNTS.STATEMENT_TYPE = "Debit";
                objACCOUNTS.DESCRIPTION = txtAccountsDescription.Text;
                if (!string.IsNullOrEmpty(dopSubsidiaryName.SelectedValue.ToString()))
                    objACCOUNTS.SUBSIDIARY_CODE = dopSubsidiaryName.SelectedValue.ToString();
                else
                    objACCOUNTS.SUBSIDIARY_CODE = "";
                objACCOUNTS.COMPANY_ID = DASSessionInfoClass.COMPANY_ID;
                objACCOUNTS.BRANCH_ID = DASSessionInfoClass.BRANCH_ID;
                //bank setup for subsidiary code 90190120003
                //if bank 2 table insert(accounts , bank) 
                if (dopSubsidiaryName.SelectedValue.ToString() == "90190120003")
                {
                    BANK objBANK = new BANK();
                    objBANK.BANK_ADDRESS = "";
                    objBANK.BANK_COUNTRY = 1;
                    objBANK.BANK_EMAIL = "";
                    objBANK.BANK_FAX = "";
                    objBANK.BANK_PHONE = "";
                    objBANK.BRANCH_ID = DASSessionInfoClass.BRANCH_ID;
                    int i = AccountSave.SaveBank(objACCOUNTS, objBANK);
                    if (i > 0)
                    {
                        BindAccountgrid();
                        clearfield();
                        treeView1.Nodes.Clear();
                        bindtree();
                        DSMessageBox objDSMessageBox = new DSMessageBox("Save Successfully", 1);
                        objDSMessageBox.Show();
                    }
                    else
                    {
                        //clearfield();
                        DSMessageBox objDSMessageBox = new DSMessageBox("Save Not Successfully", 2);
                        objDSMessageBox.Show();
                    }
                }
                //cash account
                else if (dopSubsidiaryName.SelectedValue.ToString() == "90190120002")
                {
                    objACCOUNTS = AccountSave.SaveCASHACCOUNTS(objACCOUNTS);
                    if (!string.IsNullOrEmpty(objACCOUNTS.ACCOUNTS_CODE))
                    {
                        treeView1.Nodes.Clear();
                        bindtree();
                        BindAccountgrid();
                        clearfield();
                        DSMessageBox objDSMessageBox = new DSMessageBox("Save Successfully", 1);
                        objDSMessageBox.Show();
                    }
                    else
                    {
                        //clearfield();
                        DSMessageBox objDSMessageBox = new DSMessageBox("Save Not Successfully", 2);
                        objDSMessageBox.Show();
                    }
                }

                    //other all account
                else
                {
                    objACCOUNTS = AccountSave.SaveACCOUNTS(objACCOUNTS);
                    if (!string.IsNullOrEmpty(objACCOUNTS.ACCOUNTS_CODE))
                    {
                        treeView1.Nodes.Clear();
                        bindtree();
                        BindAccountgrid();
                        clearfield();
                        DSMessageBox objDSMessageBox = new DSMessageBox("Save Successfully", 1);
                        objDSMessageBox.Show();
                    }
                    else
                    {
                        //clearfield();
                        DSMessageBox objDSMessageBox = new DSMessageBox("Save Not Successfully", 2);
                        objDSMessageBox.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void cmbControlName_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindComboACCOUNT_SUBSIDIARY();
        }
        private void cmbAcccountGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            //BindComboControl();
            //BindCombo();
        }
        private void txtAccountID_TextChanged(object sender, EventArgs e)
        {
            try
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                string query = "SELECT * from ACCOUNTS where DESCRIPTION='" + txtAccountID.Text + "'";
                using (SqlCommand objSqlCommand = new SqlCommand(query, conn))
                {
                    

                    conn.Open();
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {

                        txtAccountsDescription.Text = dr["DESCRIPTION"].ToString();
                        cmbAcccountGroup.SelectedValue = dr["GROUP_CODE"].ToString();
                        cmbControlName.SelectedValue = dr["CONTROL_CODE"].ToString();
                        dopSubsidiaryName.SelectedValue = dr["SUBSIDIARY_CODE"].ToString();
                        txtAccountCode.Text = dr["ACCOUNTS_CODE"].ToString();
                        txtOpeningBalance.Text = dr["OPENING_BALANCE"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }

        }
        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            txtAccountID.Text = e.Node.Text;
        }
        private void dgAccounts_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtAccountID.Text = dgAccounts.Rows[e.RowIndex].Cells[3].Value.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
        private void dgAccounts_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtAccountID.Text = dgAccounts.Rows[e.RowIndex].Cells[0].Value.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            clearfield();
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                int txtLength = txtAccountsDescription.Text.Length;
                if (txtLength > 50)
                {
                    DSMessageBox objDSMessageBox = new DSMessageBox("ACCOUNTS DESCRIPTION is Too Big", 3);
                    objDSMessageBox.Show();
                    return;
                }
                if (txtAccountsDescription.Text.Length == 0)
                {
                    DSMessageBox objDSMessageBox = new DSMessageBox("Insert ACCOUNTS DESCRIPTION", 3);
                    objDSMessageBox.Show();
                    return;
                }
                string ACCOUNTS_CODE = txtAccountCode.Text;
                ACCOUNTS objACCOUNTS = new ACCOUNTS();
                objACCOUNTS.DESCRIPTION = txtAccountsDescription.Text;
                objACCOUNTS.ACCOUNTS_CODE = ACCOUNTS_CODE;
                objACCOUNTS.COMPANY_ID = DASSessionInfoClass.COMPANY_ID;
                //if bank 2 table update(accounts , bank) 
                if (dopSubsidiaryName.SelectedValue.ToString() == "90190120003")
                {
                    BANK objBANK = new BANK();
                    objBANK.BANK_NAME = txtAccountsDescription.Text;
                    objBANK.ACCOUNTS_CODE = ACCOUNTS_CODE;
                    objBANK.COMPANY_ID = DASSessionInfoClass.COMPANY_ID;
                    int i = AccountSave.UpdateBANK2(objBANK);
                    if (i > 0)
                    {
                        treeView1.Nodes.Clear();
                        bindtree();
                        BindAccountgrid();
                        DSMessageBox objDSMessageBox = new DSMessageBox("Update Successfully", 1);
                        objDSMessageBox.Show();
                        clearfield();
                    }
                    else
                    {
                        DSMessageBox objDSMessageBox = new DSMessageBox("Update Not Successfully", 2);
                        objDSMessageBox.Show();
                        clearfield();
                    }                    
                }
                else//update  all account without bank
                {

                    int i = AccountSave.UpdateACCOUNTS(objACCOUNTS);
                    if (i > 0)
                    {
                        treeView1.Nodes.Clear();
                        bindtree();
                        BindAccountgrid();
                        DSMessageBox objDSMessageBox = new DSMessageBox("Update successfully", 1);
                        objDSMessageBox.Show();
                        clearfield();
                    }
                    else
                    {
                        DSMessageBox objDSMessageBox = new DSMessageBox("Update not successfully", 2);
                        objDSMessageBox.Show();
                        clearfield();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete this feature?",

        "Confirm Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    int i = 0;
                    ACCOUNTS objACCOUNTS = new ACCOUNTS();
                    objACCOUNTS.ACCOUNTS_CODE = txtAccountCode.Text;
                    objACCOUNTS.COMPANY_ID = DASSessionInfoClass.COMPANY_ID;
                    //bank delete 2 table (account,bank)
                    if (dopSubsidiaryName.SelectedValue.ToString() == "90190120003")
                    {
                        i = AccountSave.DeleteBANK(objACCOUNTS);
                        if (i > 0)
                        {
                            treeView1.Nodes.Clear();
                            bindtree();
                            DSMessageBox objDSMessageBox = new DSMessageBox("Delete Successfully", 1);
                            objDSMessageBox.Show();
                            BindAccountgrid();
                            clearfield();
                        }
                        else
                        {
                            DSMessageBox objDSMessageBox = new DSMessageBox("Delete Not Successfully", 2);
                            objDSMessageBox.Show();
                            clearfield();
                        }
                    }
                    else//delete all account without bank
                    {
                        i = AccountSave.DeleteACCOUNTS(objACCOUNTS);
                        if (i > 0)
                        {
                            treeView1.Nodes.Clear();
                            bindtree();
                            DSMessageBox objDSMessageBox = new DSMessageBox("Delete Successfully", 1);
                            objDSMessageBox.Show();
                            BindAccountgrid();
                            clearfield();
                        }
                        else
                        {
                            DSMessageBox objDSMessageBox = new DSMessageBox("Delete Not Successfully", 2);
                            objDSMessageBox.Show();
                        }
                    }
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
        }
    }
}
