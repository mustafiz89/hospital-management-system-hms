﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Pathology_Setup;
using IA.BIZ.Setup.Pathology_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Pathology
{
    public partial class Pathology_Speciman_Setup : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Pathology_Speciman_Setup()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvPathologySpecimen.AutoGenerateColumns = false;
            dgvPathologySpecimen.DataSource = PathologyFetch.getALLSpecimen();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtPathologySpecimenName.Text == "")
            {
                er++;
                ep.SetError(txtPathologySpecimenName, "Name Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                PATHOLOGY_SPECIMAN objPathologySpecimen = new PATHOLOGY_SPECIMAN();
                objPathologySpecimen.PATHOLOGY_SPECIMAN_NAME = txtPathologySpecimenName.Text;

                if (cmbActive.SelectedIndex == 0)
                    objPathologySpecimen.STATUS_FLAG = "A";
                else
                    objPathologySpecimen.STATUS_FLAG = "U";


                int i = PathologySetup.savePathologySpecimen(objPathologySpecimen);

                if (i > 0)
                {
                    dgvPathologySpecimen.AutoGenerateColumns = false;
                    dgvPathologySpecimen.DataSource = PathologyFetch.getALLSpecimen();
                    MessageBox.Show("Save Successfully");
                }
                else
                    MessageBox.Show("Save Not Successfully");

                txtPathologySpecimenName.Text = "";
            }
        }

        private void Pathology_Speciman_Setup_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            PATHOLOGY_SPECIMAN objPathologySpecimen = new PATHOLOGY_SPECIMAN();
            objPathologySpecimen.PATHOLOGY_SPECIMAN_ID =new Guid (txtSpecimenID.Text);
            objPathologySpecimen.PATHOLOGY_SPECIMAN_NAME = txtPathologySpecimenName.Text;

            if (cmbActive.SelectedIndex == 0)
                objPathologySpecimen.STATUS_FLAG = "A";
            else
                objPathologySpecimen.STATUS_FLAG = "U";


            int i = PathologySetup.UpdatePathologySpecimen(objPathologySpecimen);

            if (i > 0)
            {
                dgvPathologySpecimen.AutoGenerateColumns = false;
                dgvPathologySpecimen.DataSource = PathologyFetch.getALLSpecimen();
                MessageBox.Show("Update Successfully");

                txtSpecimenID.Text="";
                txtPathologySpecimenName.Text="";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvPathologySpecimen_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvPathologySpecimen.SelectedRows[0];
            txtSpecimenID.Text = dr.Cells[0].Value.ToString();
            txtPathologySpecimenName.Text = dr.Cells[1].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            PATHOLOGY_SPECIMAN objPathologySpecimen = new PATHOLOGY_SPECIMAN();
            objPathologySpecimen.PATHOLOGY_SPECIMAN_ID = new Guid(txtSpecimenID.Text);

            int i = PathologySetup.DeletePathologySpecimen(objPathologySpecimen);

            if (i > 0)
            {
                dgvPathologySpecimen.AutoGenerateColumns = false;
                dgvPathologySpecimen.DataSource = PathologyFetch.getALLSpecimen();
                MessageBox.Show("Delete Successfully");

                txtSpecimenID.Text = "";
                txtPathologySpecimenName.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
