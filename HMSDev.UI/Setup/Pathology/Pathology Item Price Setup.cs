﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Pathology_Setup;
using IA.BIZ.Setup.Pathology_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Pathology
{
    public partial class Pathology_Item_Price_Setup : Form
    {
        public Pathology_Item_Price_Setup()
        {
            InitializeComponent();
            BindComboPathologyCategoryName();
            BindComboPathologySubCategoryName();
            BindComboPathologySpecimenName();
            cmbActive.SelectedIndex = 0;
            dgvPathologyTestItemPriceSetup.AutoGenerateColumns = false;
            dgvPathologyTestItemPriceSetup.DataSource = PathologyFetch.getALLPathologyTestItem();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            PATHOLOGY_TEST_ITEM objPathologyTestItemPrice = new PATHOLOGY_TEST_ITEM();
            objPathologyTestItemPrice.PATHOLOGY_TEST_ITEM_CATEGORY_ID =new Guid (cmbPathologyCategoryName.SelectedValue.ToString());
            objPathologyTestItemPrice.PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID =new Guid (cmbPathologySubCategoryName.SelectedValue.ToString());
            objPathologyTestItemPrice.PATHOLOGY_TEST_ITEM_SPECIMEN_ID =new Guid (cmbPathologySpecimenName.SelectedValue.ToString());
            objPathologyTestItemPrice.PATHOLOGY_TEST_ITEM_NAME = txtPathologyName.Text;
            objPathologyTestItemPrice.PATHOLOGY_TEST_ITEM_PRICE_AMOUNT = Convert.ToDecimal(txtAmount.Text);
            objPathologyTestItemPrice.PATHOLOGY_TEST_ITEM_PRICE_DISCOUNT = Convert.ToDecimal(txtDiscount.Text);

            if (cmbActive.SelectedIndex == 0)
                objPathologyTestItemPrice.STATUS_FLAG = "A";
            else
                objPathologyTestItemPrice.STATUS_FLAG = "U";

            int i = PathologySetup.savePathologyTestItem(objPathologyTestItemPrice);

            if (i > 0)
            {
                dgvPathologyTestItemPriceSetup.AutoGenerateColumns = false;
                dgvPathologyTestItemPriceSetup.DataSource = PathologyFetch.getALLPathologyTestItem();
                MessageBox.Show("Save Successfully");

                txtPathologyName.Text = "";
                txtAmount.Text = "";
                txtDiscount.Text = "";
            }
            else
                MessageBox.Show("Save Not Successfully");
        }

        private void BindComboPathologyCategoryName()
        {
            cmbPathologyCategoryName.DataSource = PathologyFetch.getALLPathologyCategory();
            cmbPathologyCategoryName.ValueMember = "PATHOLOGY_TEST_ITEM_CATEGORY_ID";
            cmbPathologyCategoryName.DisplayMember = "PATHOLOGY_TEST_ITEM_CATEGORY_NAME";
            cmbPathologyCategoryName.SelectedIndex = 0;
        }

        private void BindComboPathologySubCategoryName()
        {
            cmbPathologySubCategoryName.DataSource = PathologyFetch.getALLPathologySubCategory(cmbPathologyCategoryName.SelectedValue.ToString());
            cmbPathologySubCategoryName.ValueMember = "PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID";
            cmbPathologySubCategoryName.DisplayMember = "PATHOLOGY_TEST_ITEM_SUB_CATEGORY_NAME";
        }

        private void BindComboPathologySpecimenName()
        {
            cmbPathologySpecimenName.DataSource = PathologyFetch.getALLSpecimen();
            cmbPathologySpecimenName.ValueMember = "PATHOLOGY_SPECIMAN_ID";
            cmbPathologySpecimenName.DisplayMember = "PATHOLOGY_SPECIMAN_NAME";
            cmbPathologySpecimenName.SelectedIndex = 0;
        }

        private void Pathology_Item_Price_Setup_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbPathologySubCategoryName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbPathologySpecimenName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtPathologyName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtAmount_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtDiscount_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cmbPathologySubCategoryName_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void cmbPathologyCategoryName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindComboPathologySubCategoryName();
            }
            catch { }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            PATHOLOGY_TEST_ITEM objPathologyTestItemPrice = new PATHOLOGY_TEST_ITEM();
            objPathologyTestItemPrice.PATHOLOGY_TEST_ITEM_ID =new Guid (txtPathologyID.Text);
            objPathologyTestItemPrice.PATHOLOGY_TEST_ITEM_CATEGORY_ID = new Guid(cmbPathologyCategoryName.SelectedValue.ToString());
            objPathologyTestItemPrice.PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID = new Guid(cmbPathologySubCategoryName.SelectedValue.ToString());
            objPathologyTestItemPrice.PATHOLOGY_TEST_ITEM_SPECIMEN_ID = new Guid(cmbPathologySpecimenName.SelectedValue.ToString());
            objPathologyTestItemPrice.PATHOLOGY_TEST_ITEM_NAME = txtPathologyName.Text;
            objPathologyTestItemPrice.PATHOLOGY_TEST_ITEM_PRICE_AMOUNT = Convert.ToDecimal(txtAmount.Text);
            objPathologyTestItemPrice.PATHOLOGY_TEST_ITEM_PRICE_DISCOUNT = Convert.ToDecimal(txtDiscount.Text);

            if (cmbActive.SelectedIndex == 0)
                objPathologyTestItemPrice.STATUS_FLAG = "A";
            else
                objPathologyTestItemPrice.STATUS_FLAG = "U";

            int i = PathologySetup.UpdatePathologyTestItem(objPathologyTestItemPrice);

            if (i > 0)
            {
                dgvPathologyTestItemPriceSetup.AutoGenerateColumns = false;
                dgvPathologyTestItemPriceSetup.DataSource = PathologyFetch.getALLPathologyTestItem();
                MessageBox.Show("Update Successfully");

                txtPathologyID.Text = "";
                txtPathologyName.Text = "";
                txtAmount.Text = "";
                txtDiscount.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvPathologyTestItemPriceSetup_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvPathologyTestItemPriceSetup.SelectedRows[0];
            txtPathologyID.Text = dr.Cells[0].Value.ToString();
            txtPathologyName.Text = dr.Cells[1].Value.ToString();
            cmbPathologyCategoryName.SelectedIndex = cmbPathologyCategoryName.FindString(dr.Cells[2].Value.ToString());
            cmbPathologySubCategoryName.SelectedIndex = cmbPathologySubCategoryName.FindString(dr.Cells[3].Value.ToString());
            cmbPathologySpecimenName.SelectedIndex = cmbPathologySpecimenName.FindString(dr.Cells[4].Value.ToString());
            txtAmount.Text = dr.Cells[5].Value.ToString();
            txtDiscount.Text = dr.Cells[6].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            PATHOLOGY_TEST_ITEM objPathologyTestItemPrice = new PATHOLOGY_TEST_ITEM();
            objPathologyTestItemPrice.PATHOLOGY_TEST_ITEM_ID = new Guid(txtPathologyID.Text);

            int i = PathologySetup.DeletePathologyTestItem(objPathologyTestItemPrice);

            if (i > 0)
            {
                dgvPathologyTestItemPriceSetup.AutoGenerateColumns = false;
                dgvPathologyTestItemPriceSetup.DataSource = PathologyFetch.getALLPathologyTestItem();
                MessageBox.Show("Delete Successfully");

                txtPathologyID.Text = "";
                txtPathologyName.Text = "";
                txtAmount.Text = "";
                txtDiscount.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
