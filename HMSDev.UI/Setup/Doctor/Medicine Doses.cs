﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Doctor;
using IA.BIZ.Setup.Doctor;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Doctor
{
    public partial class Medicine_Doses : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Medicine_Doses()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvMedicineDose.AutoGenerateColumns = false;
            dgvMedicineDose.DataSource = DoctorFetch.getALLMedicineDose();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtMedicineDoses.Text == "")
            {
                er++;
                ep.SetError(txtMedicineDoses, "Name Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                MEDICINE_DOSE objMedicineDose = new MEDICINE_DOSE();
                objMedicineDose.MEDICINE_DOSE_NAME = txtMedicineDoses.Text;
                if (cmbActive.SelectedIndex == 0)
                    objMedicineDose.STATUS_FLAG = "A";
                else
                    objMedicineDose.STATUS_FLAG = "U";
                int i = DoctorSave.saveMedicineDose(objMedicineDose);
                if (i > 0)
                {
                    dgvMedicineDose.AutoGenerateColumns = false;
                    dgvMedicineDose.DataSource = DoctorFetch.getALLMedicineDose();
                    MessageBox.Show("Save Successfully");
                }
                else
                    MessageBox.Show("Save Not Successfully");

                txtMedicineDoses.Text = "";
            }
        }

        private void txtMedicineDoses_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            MEDICINE_DOSE objMedicineDose = new MEDICINE_DOSE();
            objMedicineDose.MEDICINE_DOSE_ID =new Guid (txtDoseID.Text);
            objMedicineDose.MEDICINE_DOSE_NAME = txtMedicineDoses.Text;
            if (cmbActive.SelectedIndex == 0)
                objMedicineDose.STATUS_FLAG = "A";
            else
                objMedicineDose.STATUS_FLAG = "U";
            int i = DoctorSave.UpdateMedicineDose(objMedicineDose);
            if (i > 0)
            {
                dgvMedicineDose.AutoGenerateColumns = false;
                dgvMedicineDose.DataSource = DoctorFetch.getALLMedicineDose();
                MessageBox.Show("Update Successfully");
                txtDoseID.Text = "";
                txtMedicineDoses.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvMedicineDose_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvMedicineDose.SelectedRows[0];
            txtDoseID.Text = dr.Cells[0].Value.ToString();
            txtMedicineDoses.Text = dr.Cells[1].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            MEDICINE_DOSE objMedicineDose = new MEDICINE_DOSE();
            objMedicineDose.MEDICINE_DOSE_ID = new Guid(txtDoseID.Text);
          
            int i = DoctorSave.DeleteMedicineDose(objMedicineDose);
            if (i > 0)
            {
                dgvMedicineDose.AutoGenerateColumns = false;
                dgvMedicineDose.DataSource = DoctorFetch.getALLMedicineDose();
                MessageBox.Show("Delete Successfully");
                txtDoseID.Text = "";
                txtMedicineDoses.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
