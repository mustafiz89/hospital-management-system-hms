﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Doctor;
using IA.BIZ.Setup.Doctor;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Doctor
{
    public partial class Indoor_Doctor_Setup : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Indoor_Doctor_Setup()
        {
            InitializeComponent();
            cmbIndoorOutdoor.SelectedIndex = 0;
            BindComboDoctorSpecialization();
            BindComboDoctorType();
            dgvDoctorInfo.AutoGenerateColumns = false;
            dgvDoctorInfo.DataSource = DoctorFetch.getALLDoctor();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtDoctorName.Text == "")
            {
                er++;
                ep.SetError(txtDoctorName, "Name Required");
                //MessageBox.Show("required field needed");
            }
            else if (txtContactPerson.Text == "")
            {
                er++;
                ep.SetError(txtContactPerson, "Name Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                INDOOR_DOCTOR ObjIndoorDoctor = new INDOOR_DOCTOR();
                ObjIndoorDoctor.DOCTOR_SPECIALIZATION_ID = new Guid(cmbDoctorSpecialization.SelectedValue.ToString());
                ObjIndoorDoctor.DOCTOR_TYPE_ID =new Guid( cmbDoctorType.SelectedValue.ToString());
                ObjIndoorDoctor.DOCTOR_NAME = txtDoctorName.Text;
                ObjIndoorDoctor.DOCTOR_CONTACT_PERSON = txtContactPerson.Text;
                ObjIndoorDoctor.DOCTOR_PERSONAL_ADDRESS = txtPersonalAddress.Text;
                ObjIndoorDoctor.DOCTOR_CHAMBER_ADDRESS = txtChamberAddress.Text;
                ObjIndoorDoctor.DOCTOR_PERSONAL_CONTACT = txtDoctorContact.Text;
                ObjIndoorDoctor.DOCTOR_PS_CONTACT = txtDoctorPSContact.Text;
                ObjIndoorDoctor.DOCTOR_PATHOLOGY_COMMISSION = Convert.ToDecimal(txtPathologyCommission.Text);
                ObjIndoorDoctor.DOCTOR_PATIENT_COMMISSION = Convert.ToDecimal(txtPatientCommission.Text);

                if (cmbIndoorOutdoor.SelectedIndex == 0)
                {
                    ObjIndoorDoctor.INDOOR_OUTDOOR_FLAG = "I";
                }
                else
                    ObjIndoorDoctor.INDOOR_OUTDOOR_FLAG = "O";

                int i = DoctorSave.saveIndoorDoctor(ObjIndoorDoctor);
                if (i > 0)
                {
                    dgvDoctorInfo.AutoGenerateColumns = false;
                    dgvDoctorInfo.DataSource = DoctorFetch.getALLDoctor();
                    MessageBox.Show("Save Successfully");
                }
                else
                    MessageBox.Show("Save Not Successfully");

                txtDoctorName.Text = "";
                txtContactPerson.Text = "";
                txtPersonalAddress.Text = "";
                txtChamberAddress.Text = "";
                txtDoctorContact.Text = "";
                txtDoctorPSContact.Text = "";
                txtPathologyCommission.Text = "";
                txtPatientCommission.Text = "";

            }
        }

        private void cmbDoctorSpecialization_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbDoctorType_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtDoctorName_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtContactPerson_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtPersonalAddress_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtChamberAddress_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtDoctorContact_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtDoctorPSContact_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtPathologyCommission_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtPatientComission_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbDoctorSpecialization_SelectedIndexChanged(object sender, EventArgs e)
        {
            //BindComboDoctorSpecialization();
            
            //BindComboControl();
            //BindComboACCOUNT_SUBSIDIARY();
        
        }

        private void BindComboDoctorSpecialization()
        {
            cmbDoctorSpecialization.DataSource = DoctorFetch.getALLSpecialization();
            cmbDoctorSpecialization.ValueMember = "DOCTOR_SPECIALIZATION_ID";
            cmbDoctorSpecialization.DisplayMember = "DOCTOR_SPECIALIZATION_NAME";
            cmbDoctorSpecialization.SelectedIndex = 0;
        }

        private void BindComboDoctorType()
        {
            cmbDoctorType.DataSource = DoctorFetch.getALLDoctorType();
            cmbDoctorType.ValueMember = "DOCTOR_TYPE_ID";
            cmbDoctorType.DisplayMember = "DOCTOR_TYPE_NAME";
            cmbDoctorType.SelectedIndex = 0;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            INDOOR_DOCTOR ObjIndoorDoctor = new INDOOR_DOCTOR();
            ObjIndoorDoctor.DOCTOR_ID =new Guid (txtDocotrID.Text);
            ObjIndoorDoctor.DOCTOR_SPECIALIZATION_ID = new Guid(cmbDoctorSpecialization.SelectedValue.ToString());
            ObjIndoorDoctor.DOCTOR_TYPE_ID = new Guid(cmbDoctorType.SelectedValue.ToString());
            ObjIndoorDoctor.DOCTOR_NAME = txtDoctorName.Text;
            ObjIndoorDoctor.DOCTOR_CONTACT_PERSON = txtContactPerson.Text;
            ObjIndoorDoctor.DOCTOR_PERSONAL_ADDRESS = txtPersonalAddress.Text;
            ObjIndoorDoctor.DOCTOR_CHAMBER_ADDRESS = txtChamberAddress.Text;
            ObjIndoorDoctor.DOCTOR_PERSONAL_CONTACT = txtDoctorContact.Text;
            ObjIndoorDoctor.DOCTOR_PS_CONTACT = txtDoctorPSContact.Text;
            ObjIndoorDoctor.DOCTOR_PATHOLOGY_COMMISSION = Convert.ToDecimal(txtPathologyCommission.Text);
            ObjIndoorDoctor.DOCTOR_PATIENT_COMMISSION = Convert.ToDecimal(txtPatientCommission.Text);

            if (cmbIndoorOutdoor.SelectedIndex == 0)
            {
                ObjIndoorDoctor.INDOOR_OUTDOOR_FLAG = "I";
            }
            else
                ObjIndoorDoctor.INDOOR_OUTDOOR_FLAG = "O";

            int i = DoctorSave.UpdateIndoorDoctor(ObjIndoorDoctor);
            if (i > 0)
            {
                dgvDoctorInfo.AutoGenerateColumns = false;
                dgvDoctorInfo.DataSource = DoctorFetch.getALLDoctor();
                MessageBox.Show("Update Successfully");

                txtDocotrID.Text = "";
                txtDoctorName.Text = "";
                txtContactPerson.Text = "";
                txtPersonalAddress.Text = "";
                txtChamberAddress.Text = "";
                txtDoctorContact.Text = "";
                txtDoctorPSContact.Text = "";
                txtPathologyCommission.Text = "";
                txtPatientCommission.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");

        }

        private void dgvDoctorInfo_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvDoctorInfo.SelectedRows[0];
            txtDocotrID.Text = dr.Cells[0].Value.ToString();
            txtDoctorName.Text = dr.Cells[1].Value.ToString();
            cmbDoctorSpecialization.SelectedIndex =cmbDoctorSpecialization.FindString( dr.Cells[2].Value.ToString());
            cmbDoctorType.SelectedIndex =cmbDoctorType.FindString (dr.Cells[3].Value.ToString());
            txtContactPerson.Text = dr.Cells[4].Value.ToString();
            txtPersonalAddress.Text = dr.Cells[5].Value.ToString();
            txtChamberAddress.Text = dr.Cells[6].Value.ToString();
            txtDoctorContact.Text = dr.Cells[7].Value.ToString();
            txtDoctorPSContact.Text = dr.Cells[8].Value.ToString();
            txtPathologyCommission.Text = dr.Cells[9].Value.ToString();
            txtPatientCommission.Text = dr.Cells[10].Value.ToString();

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            INDOOR_DOCTOR ObjIndoorDoctor = new INDOOR_DOCTOR();
            ObjIndoorDoctor.DOCTOR_ID = new Guid(txtDocotrID.Text);

            int i = DoctorSave.DeleteDoctor(ObjIndoorDoctor);
            if (i > 0)
            {
                dgvDoctorInfo.AutoGenerateColumns = false;
                dgvDoctorInfo.DataSource = DoctorFetch.getALLDoctor();
                MessageBox.Show("Delete Successfully");

                txtDocotrID.Text = "";
                txtDoctorName.Text = "";
                txtContactPerson.Text = "";
                txtPersonalAddress.Text = "";
                txtChamberAddress.Text = "";
                txtDoctorContact.Text = "";
                txtDoctorPSContact.Text = "";
                txtPathologyCommission.Text = "";
                txtPatientCommission.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }

    }
}
