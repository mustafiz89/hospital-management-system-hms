﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Doctor;
using IA.BIZ.Setup.Doctor;
using POS_SYSTEM;
namespace HMSDev.UI.Setup.Doctor
{
    public partial class Doctor_Specialization : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Doctor_Specialization()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvSpecializationInfo.AutoGenerateColumns = false;
            dgvSpecializationInfo.DataSource = DoctorFetch.getALLSpecialization();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtDoctorSpecialization.Text == "")
            {
                er++;
                ep.SetError(txtDoctorSpecialization, "Specialization Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                DOCTOR_SPECIALIZATION objDoctorSpecialization = new DOCTOR_SPECIALIZATION();
                objDoctorSpecialization.DOCTOR_SPECIALIZATION_NAME = txtDoctorSpecialization.Text;
                if (cmbActive.SelectedIndex == 0)
                    objDoctorSpecialization.STATUS_FLAG = "A";
                else
                    objDoctorSpecialization.STATUS_FLAG = "U";
                int i = DoctorSave.saveDoctorSpecialization(objDoctorSpecialization);
                if (i > 0)
                {
                    dgvSpecializationInfo.AutoGenerateColumns = false;
                    dgvSpecializationInfo.DataSource = DoctorFetch.getALLSpecialization();
                    MessageBox.Show("Save Successfully");
                }
                else
                    MessageBox.Show("Save Not Successfully");

                txtDoctorSpecialization.Text = "";
            }
        }

        private void txtDoctorSpecialization_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DOCTOR_SPECIALIZATION objDoctorSpecialization = new DOCTOR_SPECIALIZATION();
            objDoctorSpecialization.DOCTOR_SPECIALIZATION_ID =new Guid (txtSpecializationID.Text);
            objDoctorSpecialization.DOCTOR_SPECIALIZATION_NAME = txtDoctorSpecialization.Text;
            if (cmbActive.SelectedIndex == 0)
                objDoctorSpecialization.STATUS_FLAG = "A";
            else
                objDoctorSpecialization.STATUS_FLAG = "U";
            int i = DoctorSave.UpdateDoctorSpecialization(objDoctorSpecialization);
            if (i > 0)
            {
                dgvSpecializationInfo.AutoGenerateColumns = false;
                dgvSpecializationInfo.DataSource = DoctorFetch.getALLSpecialization();
                MessageBox.Show("Save Successfully");

                txtSpecializationID.Text = "";
                txtDoctorSpecialization.Text = "";
            }
            else
                MessageBox.Show("Save Not Successfully");
                    
        }

        private void dgvSpecializationInfo_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvSpecializationInfo.SelectedRows[0];
            txtSpecializationID.Text = dr.Cells[0].Value.ToString();
            txtDoctorSpecialization.Text = dr.Cells[1].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DOCTOR_SPECIALIZATION objDoctorSpecialization = new DOCTOR_SPECIALIZATION();
            objDoctorSpecialization.DOCTOR_SPECIALIZATION_ID = new Guid(txtSpecializationID.Text);
           
            int i = DoctorSave.DeleteDoctorSpecialization(objDoctorSpecialization);
            if (i > 0)
            {
                dgvSpecializationInfo.AutoGenerateColumns = false;
                dgvSpecializationInfo.DataSource = DoctorFetch.getALLSpecialization();
                MessageBox.Show("Delete Successfully");

                txtSpecializationID.Text = "";
                txtDoctorSpecialization.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
