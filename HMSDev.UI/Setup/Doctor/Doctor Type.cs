﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Doctor;
using IA.BIZ.Setup.Doctor;
using POS_SYSTEM;
namespace HMSDev.UI.Setup.Doctor
{
    public partial class Doctor_Type : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Doctor_Type()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvDoctorTypeInfo.AutoGenerateColumns = false;
            dgvDoctorTypeInfo.DataSource = DoctorFetch.getALLDoctorType();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtDoctorType.Text == "")
            {
                er++;
                ep.SetError(txtDoctorType, "Name Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                DOCTOR_TYPE objDoctorType = new DOCTOR_TYPE();
                objDoctorType.DOCTOR_TYPE_NAME = txtDoctorType.Text;
                if (cmbActive.SelectedIndex == 0)
                    objDoctorType.STATUS_FLAG = "A";
                else
                    objDoctorType.STATUS_FLAG = "U";
                int i = DoctorSave.saveDoctorType(objDoctorType);
                if (i > 0)
                {
                    dgvDoctorTypeInfo.AutoGenerateColumns = false;
                    dgvDoctorTypeInfo.DataSource = DoctorFetch.getALLDoctorType();
                    MessageBox.Show("Save Successfully");
                }
                else
                    MessageBox.Show("Save Not Successfully");

                txtDoctorType.Text = "";
            }
        }

        private void txtDoctorType_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DOCTOR_TYPE objDoctorType = new DOCTOR_TYPE();
            objDoctorType.DOCTOR_TYPE_ID =new Guid (txtDoctorTypeID.Text);
            objDoctorType.DOCTOR_TYPE_NAME = txtDoctorType.Text;
            if (cmbActive.SelectedIndex == 0)
                objDoctorType.STATUS_FLAG = "A";
            else
                objDoctorType.STATUS_FLAG = "U";
            int i = DoctorSave.UpdateDoctorType(objDoctorType);
            if (i > 0)
            {
                dgvDoctorTypeInfo.AutoGenerateColumns = false;
                dgvDoctorTypeInfo.DataSource = DoctorFetch.getALLDoctorType();
                MessageBox.Show("Save Successfully");

                txtDoctorTypeID.Text = "";
                txtDoctorType.Text = "";
            }
            else
                MessageBox.Show("Save Not Successfully");

        }

        private void dgvDoctorTypeInfo_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvDoctorTypeInfo.SelectedRows[0];
            txtDoctorTypeID.Text = dr.Cells[0].Value.ToString();
            txtDoctorType.Text = dr.Cells[1].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DOCTOR_TYPE objDoctorType = new DOCTOR_TYPE();
            objDoctorType.DOCTOR_TYPE_ID = new Guid(txtDoctorTypeID.Text);
           ;
            int i = DoctorSave.DeleteDoctorType(objDoctorType);
            if (i > 0)
            {
                dgvDoctorTypeInfo.AutoGenerateColumns = false;
                dgvDoctorTypeInfo.DataSource = DoctorFetch.getALLDoctorType();
                MessageBox.Show("Delete Successfully");

                txtDoctorTypeID.Text = "";
                txtDoctorType.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
