﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Indoor_Setup;
using IA.BIZ.Setup.Indoor_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Indoor
{
    public partial class Patient_Department_Setup : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Patient_Department_Setup()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvPatientDepartment.AutoGenerateColumns = false;
            dgvPatientDepartment.DataSource = IndoorFetch.getALLPatientDepartment();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtPatientDepartment.Text == "")
            {
                er++;
                ep.SetError(txtPatientDepartment, "Patient Department Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                PATIENT_DEPARTMENT objPatientDepartment = new PATIENT_DEPARTMENT();
                objPatientDepartment.PATIENT_DEPARTMENT_NAME = txtPatientDepartment.Text;
                if (cmbActive.SelectedIndex == 0)
                    objPatientDepartment.STATUS_FLAG = "A";
                else
                    objPatientDepartment.STATUS_FLAG = "U";
                int i = IndoorSave.savePatientDepartment(objPatientDepartment);

                if (i > 0)
                {
                    dgvPatientDepartment.AutoGenerateColumns = false;
                    dgvPatientDepartment.DataSource = IndoorFetch.getALLPatientDepartment();
                    MessageBox.Show("Save Successfully");
                }
                else
                    MessageBox.Show("Save Not Successfully");

                txtPatientDepartment.Text = "";
            }
        }

        private void Patient_Department_Setup_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtPatientDepartment_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            PATIENT_DEPARTMENT objPatientDepartment = new PATIENT_DEPARTMENT();
            objPatientDepartment.PATIENT_DEPARTMENT_ID =new Guid (txtPatientDepartmentID.Text);
            objPatientDepartment.PATIENT_DEPARTMENT_NAME = txtPatientDepartment.Text;
            if (cmbActive.SelectedIndex == 0)
                objPatientDepartment.STATUS_FLAG = "A";
            else
                objPatientDepartment.STATUS_FLAG = "U";
            int i = IndoorSave.UpdatePatientDepartment(objPatientDepartment);

            if (i > 0)
            {
                dgvPatientDepartment.AutoGenerateColumns = false;
                dgvPatientDepartment.DataSource = IndoorFetch.getALLPatientDepartment();
                MessageBox.Show("Save Successfully");

                txtPatientDepartmentID.Text = "";
                txtPatientDepartment.Text = "";
            }
            else
                MessageBox.Show("Save Not Successfully");
        }

        private void dgvPatientDepartment_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvPatientDepartment.SelectedRows[0];
            txtPatientDepartmentID.Text = dr.Cells[0].Value.ToString();
            txtPatientDepartment.Text = dr.Cells[1].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            PATIENT_DEPARTMENT objPatientDepartment = new PATIENT_DEPARTMENT();
            objPatientDepartment.PATIENT_DEPARTMENT_ID = new Guid(txtPatientDepartmentID.Text);
          
            int i = IndoorSave.DeletePatientDepartment(objPatientDepartment);

            if (i > 0)
            {
                dgvPatientDepartment.AutoGenerateColumns = false;
                dgvPatientDepartment.DataSource = IndoorFetch.getALLPatientDepartment();
                MessageBox.Show("Delete Successfully");

                txtPatientDepartmentID.Text = "";
                txtPatientDepartment.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
