﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Indoor_Setup;
using IA.BIZ.Setup.Indoor_Setup;
using POS_SYSTEM;


namespace HMSDev.UI.Setup.Indoor
{
    public partial class Bed_Setup : Form
    {
        public Bed_Setup()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            BindComboRoomNo();
            dgvBedInformation.AutoGenerateColumns = false;
            dgvBedInformation.DataSource = IndoorFetch.getALLBed();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            BED objBed = new BED();
            objBed.BED_NUMBER = txtBedNo.Text;
            objBed.ROOM_ID =new Guid(cmbRoomNo.SelectedValue.ToString());
            objBed.CHARGE_PER_DAY = Convert.ToDecimal(txtChargePerDay.Text);
            if (cmbActive.SelectedIndex == 0)
                objBed.STATUS_FLAG = "A";
            else
                objBed.STATUS_FLAG = "U";
            int i = IndoorSave.saveBed(objBed);

            if (i > 0)
            {
                dgvBedInformation.AutoGenerateColumns = false;
                dgvBedInformation.DataSource = IndoorFetch.getALLBed();
                MessageBox.Show("Save Successfully");

                txtBedNo.Text = "";
                txtChargePerDay.Text = "";
            }
            else
                MessageBox.Show("Save Not Successfully");
        }

        private void txtBedNo_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbRoomCategory_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtChargePerDay_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbCompanyID_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }


        private void BindComboRoomNo()
        {
            cmbRoomNo.DataSource = IndoorFetch.getALLRoom();
            cmbRoomNo.ValueMember = "ROOM_ID";
            cmbRoomNo.DisplayMember = "ROOM_NUMBER";
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            BED objBed = new BED();
            objBed.BED_ID =new Guid (txtBedID.Text);
            objBed.BED_NUMBER = txtBedNo.Text;
            objBed.ROOM_ID = new Guid(cmbRoomNo.SelectedValue.ToString());
            objBed.CHARGE_PER_DAY = Convert.ToDecimal(txtChargePerDay.Text);
            if (cmbActive.SelectedIndex == 0)
                objBed.STATUS_FLAG = "A";
            else
                objBed.STATUS_FLAG = "U";
            int i = IndoorSave.UpdateBed(objBed);

            if (i > 0)
            {
                dgvBedInformation.AutoGenerateColumns = false;
                dgvBedInformation.DataSource = IndoorFetch.getALLBed();
                MessageBox.Show("Update Successfully");

                txtBedID.Text = "";
                txtBedNo.Text = "";
                txtChargePerDay.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvBedInformation_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvBedInformation.SelectedRows[0];
            txtBedID.Text = dr.Cells[0].Value.ToString();
            txtBedNo.Text = dr.Cells[1].Value.ToString();
            cmbRoomNo.SelectedIndex = cmbRoomNo.FindString(dr.Cells[2].Value.ToString());
            txtChargePerDay.Text = dr.Cells[3].Value.ToString();

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            BED objBed = new BED();
            objBed.BED_ID = new Guid(txtBedID.Text);
            
            int i = IndoorSave.DeleteBed(objBed);

            if (i > 0)
            {
                dgvBedInformation.AutoGenerateColumns = false;
                dgvBedInformation.DataSource = IndoorFetch.getALLBed();
                MessageBox.Show("Delete Successfully");

                txtBedID.Text = "";
                txtBedNo.Text = "";
                txtChargePerDay.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }

        private void cmbRoomNo_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }
    }
}
