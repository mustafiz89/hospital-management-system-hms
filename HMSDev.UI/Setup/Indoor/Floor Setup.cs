﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Indoor_Setup;
using IA.BIZ.Setup.Indoor_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Indoor
{
    public partial class Floor_Setup : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Floor_Setup()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvFloorInfo.AutoGenerateColumns = false;
            dgvFloorInfo.DataSource = IndoorFetch.getALLFLOOR();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtFloorName.Text == "")
            {
                er++;
                ep.SetError(txtFloorName, "Floor Name Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                FLOOR objFloor = new FLOOR();
                objFloor.FLOOR_NAME = txtFloorName.Text;

                if (cmbActive.SelectedIndex == 0)
                    objFloor.STATUS_FLAG = "A";
                else
                    objFloor.STATUS_FLAG = "U";

                int i = IndoorSave.saveFloor(objFloor);

                if (i > 0)
                {
                    dgvFloorInfo.AutoGenerateColumns = false;
                    dgvFloorInfo.DataSource = IndoorFetch.getALLFLOOR();
                    MessageBox.Show("Save Successfully");
                }
                else
                    MessageBox.Show("Save Not Successfully");

                txtFloorName.Text = "";
            }
        }

        private void Floor_Setup_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            FLOOR objFloor = new FLOOR();
            objFloor.FLOOR_ID =new Guid (txtFloorID.Text);
            objFloor.FLOOR_NAME = txtFloorName.Text;

            if (cmbActive.SelectedIndex == 0)
                objFloor.STATUS_FLAG = "A";
            else
                objFloor.STATUS_FLAG = "U";

            int i = IndoorSave.UpdateFloor(objFloor);

            if (i > 0)
            {
                dgvFloorInfo.AutoGenerateColumns = false;
                dgvFloorInfo.DataSource = IndoorFetch.getALLFLOOR();
                MessageBox.Show("Update Successfully");

                txtFloorID.Text = "";
                txtFloorName.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvFloorInfo_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvFloorInfo.SelectedRows[0];
            txtFloorID.Text = dr.Cells[0].Value.ToString();
            txtFloorName.Text = dr.Cells[1].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            FLOOR objFloor = new FLOOR();
            objFloor.FLOOR_ID = new Guid(txtFloorID.Text);

            int i = IndoorSave.DeleteFloor(objFloor);

            if (i > 0)
            {
                dgvFloorInfo.AutoGenerateColumns = false;
                dgvFloorInfo.DataSource = IndoorFetch.getALLFLOOR();
                MessageBox.Show("Delete Successfully");

                txtFloorID.Text = "";
                txtFloorName.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }

        private void txtFloorName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }
    }
}
