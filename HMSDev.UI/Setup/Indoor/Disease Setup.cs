﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Indoor_Setup;
using IA.BIZ.Setup.Indoor_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Indoor
{
    public partial class Disease_Setup : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Disease_Setup()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvDiseaseSetup.AutoGenerateColumns = false;
            dgvDiseaseSetup.DataSource = IndoorFetch.getALLDisease();
        }

        

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtDiseaseName.Text == "")
            {
                er++;
                ep.SetError(txtDiseaseName, "Name Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                DISEASE objDisease = new DISEASE();
                objDisease.DISEASE_NAME = txtDiseaseName.Text;

                if (cmbActive.SelectedIndex == 0)
                    objDisease.STATUS_FLAG = "A";
                else
                    objDisease.STATUS_FLAG = "U";

                int i = IndoorSave.saveDisease(objDisease);

                if (i > 0)
                {
                    dgvDiseaseSetup.AutoGenerateColumns = false;
                    dgvDiseaseSetup.DataSource = IndoorFetch.getALLDisease();
                    MessageBox.Show("Save Successfully");
                }
                else
                    MessageBox.Show("Save Not Successfully");

                txtDiseaseName.Text = "";
            }
        }

        private void Disease_Setup_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DISEASE objDisease = new DISEASE();
            objDisease.DISEASE_ID =new Guid (txtID.Text);
            objDisease.DISEASE_NAME = txtDiseaseName.Text;

            if (cmbActive.SelectedIndex == 0)
                objDisease.STATUS_FLAG = "A";
            else
                objDisease.STATUS_FLAG = "U";

            int i = IndoorSave.UpdateDisease(objDisease);

            if (i > 0)
            {
                dgvDiseaseSetup.AutoGenerateColumns = false;
                dgvDiseaseSetup.DataSource = IndoorFetch.getALLDisease();
                MessageBox.Show("Save Successfully");

                txtID.Text = "";
                txtDiseaseName.Text = "";
            }
            else
                MessageBox.Show("Save Not Successfully");

        }

        private void dgvDiseaseSetup_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvDiseaseSetup.SelectedRows[0];
            txtID.Text = dr.Cells[0].Value.ToString();
            txtDiseaseName.Text = dr.Cells[1].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DISEASE objDisease = new DISEASE();
            objDisease.DISEASE_ID = new Guid(txtID.Text);

            int i = IndoorSave.DeleteDisease(objDisease);

            if (i > 0)
            {
                dgvDiseaseSetup.AutoGenerateColumns = false;
                dgvDiseaseSetup.DataSource = IndoorFetch.getALLDisease();
                MessageBox.Show("Delete Successfully");

                txtID.Text = "";
                txtDiseaseName.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }

        private void txtDiseaseName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }
    }
}
