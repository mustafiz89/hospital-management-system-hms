﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Indoor_Setup;
using IA.BIZ.Setup.Indoor_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Indoor
{
    public partial class Room_Setup : Form
    {
        public Room_Setup()
        {
            InitializeComponent();
            BindComboRoomCategory();
            BindComboFloorNumber();
            cmbActive.SelectedIndex = 0;
            dgvRoomInformation.AutoGenerateColumns = false;
            dgvRoomInformation.DataSource = IndoorFetch.getALLRoom();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ROOM objRoom = new ROOM();
            objRoom.ROOM_CATEGORY_ID =new Guid(cmbRoomCategory.SelectedValue.ToString());
            objRoom.FLOOR_ID =new Guid(cmbFloorNumber.SelectedValue.ToString());
            objRoom.ROOM_NUMBER = txtRoomNumber.Text;
            objRoom.ROOM_PHONE = txtPhone.Text;

            if (cmbActive.SelectedIndex == 0)
                objRoom.STATUS_FLAG = "A";
            else
                objRoom.STATUS_FLAG = "U";

            int i = IndoorSave.saveRoom(objRoom);

            if (i > 0)
            {
                dgvRoomInformation.AutoGenerateColumns = false;
                dgvRoomInformation.DataSource = IndoorFetch.getALLRoom();
                MessageBox.Show("Save Successfully");
            }
            else
                MessageBox.Show("Save Not Successfully");
        }

        private void Room_Setup_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbFloorNumber_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbBedType_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtRoomNumber_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtPhone_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtRoomCharge_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void BindComboRoomCategory()
        {
            cmbRoomCategory.DataSource = IndoorFetch.getALLRoomCategory();
            cmbRoomCategory.ValueMember = "ROOM_CATEGORY_ID";
            cmbRoomCategory.DisplayMember = "ROOM_CATEGORY_NAME";
            cmbRoomCategory.SelectedIndex = 0;
        }

        private void BindComboFloorNumber()
        {
            cmbFloorNumber.DataSource = IndoorFetch.getALLFLOOR();
            cmbFloorNumber.ValueMember = "FLOOR_ID";
            cmbFloorNumber.DisplayMember = "FLOOR_NAME";
            cmbFloorNumber.SelectedIndex = 0;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            ROOM objRoom = new ROOM();
            objRoom.ROOM_ID =new Guid (txtRoomID.Text);
            objRoom.ROOM_CATEGORY_ID = new Guid(cmbRoomCategory.SelectedValue.ToString());
            objRoom.FLOOR_ID = new Guid(cmbFloorNumber.SelectedValue.ToString());
            objRoom.ROOM_NUMBER = txtRoomNumber.Text;
            objRoom.ROOM_PHONE = txtPhone.Text;

            if (cmbActive.SelectedIndex == 0)
                objRoom.STATUS_FLAG = "A";
            else
                objRoom.STATUS_FLAG = "U";

            int i = IndoorSave.UpdateRoom(objRoom);

            if (i > 0)
            {
                dgvRoomInformation.AutoGenerateColumns = false;
                dgvRoomInformation.DataSource = IndoorFetch.getALLRoom();
                MessageBox.Show("Save Successfully");

                txtRoomID.Text = "";
                txtRoomNumber.Text = "";
                txtPhone.Text = "";
            }
            else
                MessageBox.Show("Save Not Successfully");
        }

        private void dgvRoomInformation_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvRoomInformation.SelectedRows[0];
            txtRoomID.Text = dr.Cells[0].Value.ToString();
            txtRoomNumber.Text = dr.Cells[1].Value.ToString();
            cmbRoomCategory.SelectedIndex= cmbRoomCategory.FindString( dr.Cells[2].Value.ToString());
            cmbFloorNumber.SelectedIndex = cmbFloorNumber.FindString(dr.Cells[3].Value.ToString());
            txtPhone.Text = dr.Cells[4].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            ROOM objRoom = new ROOM();
            objRoom.ROOM_ID = new Guid(txtRoomID.Text);

            int i = IndoorSave.DeleteRoom(objRoom);

            if (i > 0)
            {
                dgvRoomInformation.AutoGenerateColumns = false;
                dgvRoomInformation.DataSource = IndoorFetch.getALLRoom();
                MessageBox.Show("Delete Successfully");

                txtRoomID.Text = "";
                txtRoomNumber.Text = "";
                txtPhone.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }

        private void cmbRoomCategory_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }
    }
}
