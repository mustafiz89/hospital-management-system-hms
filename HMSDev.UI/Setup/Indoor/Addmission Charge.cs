﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Indoor_Setup;
using IA.BIZ.Setup.Indoor_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Indoor
{
    public partial class Addmission_Charge : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Addmission_Charge()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvAdmissionCharge.AutoGenerateColumns = false;
            dgvAdmissionCharge.DataSource = IndoorFetch.getALLAdmissionCharge();
        }

        
        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtCharge.Text == "")
            {
                er++;
                ep.SetError(txtCharge, "Charge Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                ADDMISSION_CHARGE objAddmissionCharge = new ADDMISSION_CHARGE();
                objAddmissionCharge.ADMISSION_CHARGE_AMOUNT = Convert.ToDecimal(txtCharge.Text);
                if (cmbActive.SelectedIndex == 0)
                    objAddmissionCharge.STATUS_FLAG = "A";
                else
                    objAddmissionCharge.STATUS_FLAG = "U";
                int i = IndoorSave.saveAddmissionCharge(objAddmissionCharge);

                if (i > 0)
                {
                    dgvAdmissionCharge.AutoGenerateColumns = false;
                    dgvAdmissionCharge.DataSource = IndoorFetch.getALLAdmissionCharge();
                    MessageBox.Show("Save Successfully");
                }
                else
                    MessageBox.Show("Save Not Successfully");

                txtCharge.Text = "";
            }
        }

        private void txtCharge_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            ADDMISSION_CHARGE objAddmissionCharge = new ADDMISSION_CHARGE();
            objAddmissionCharge.ADMISSION_CHARGE_ID =new Guid (txtChargeID.Text);
            objAddmissionCharge.ADMISSION_CHARGE_AMOUNT = Convert.ToDecimal(txtCharge.Text);
            if (cmbActive.SelectedIndex == 0)
                objAddmissionCharge.STATUS_FLAG = "A";
            else
                objAddmissionCharge.STATUS_FLAG = "U";
            int i = IndoorSave.saveAddmissionCharge(objAddmissionCharge);

            if (i > 0)
            {
                dgvAdmissionCharge.AutoGenerateColumns = false;
                dgvAdmissionCharge.DataSource = IndoorFetch.getALLAdmissionCharge();
                MessageBox.Show("Save Successfully");
            }
            else
                MessageBox.Show("Save Not Successfully");
        }

        private void dgvAdmissionCharge_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvAdmissionCharge.SelectedRows[0];
            txtChargeID.Text = dr.Cells[0].Value.ToString();
            txtCharge.Text = dr.Cells[1].Value.ToString();
        }
    }
}
