﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Common_Setup;
using IA.BIZ.Setup.Common_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Common_Setup
{
    public partial class Salution_Setup : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Salution_Setup()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvSalutionInfo.AutoGenerateColumns = false;
            dgvSalutionInfo.DataSource = CommonSetupFetch.getALLSALUTION();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtSalution.Text == "")
            {
                er++;
                ep.SetError(txtSalution,"Salution Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                SALUTION objSalution = new SALUTION();
                objSalution.SALUTION_NAME = txtSalution.Text;
                if (cmbActive.SelectedIndex == 0)
                    objSalution.STATUS_FLAG = "A";
                else
                    objSalution.STATUS_FLAG = "A";
                int i = CommonSetupSave.saveSalution(objSalution);
                if (i > 0)
                {
                    dgvSalutionInfo.AutoGenerateColumns = false;
                    dgvSalutionInfo.DataSource = CommonSetupFetch.getALLSALUTION();
                    MessageBox.Show("Save Successfully");

                    txtSalution.Text = "";

                }
                else
                    MessageBox.Show("Save Not Successfully");

            }
        }

        private void txtSalution_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            SALUTION objSalution = new SALUTION();
            objSalution.SALUTION_ID =new Guid (txtSalutionID.Text);
            objSalution.SALUTION_NAME = txtSalution.Text;
            if (cmbActive.SelectedIndex == 0)
                objSalution.STATUS_FLAG = "A";
            else
                objSalution.STATUS_FLAG = "A";
            int i = CommonSetupSave.UpadateSalution(objSalution);
            if (i > 0)
            {
                dgvSalutionInfo.AutoGenerateColumns = false;
                dgvSalutionInfo.DataSource = CommonSetupFetch.getALLSALUTION();
                MessageBox.Show("Update Successfully");

                txtSalution.Text = "";
                txtSalutionID.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");

        }

        private void dgvSalutionInfo_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvSalutionInfo.SelectedRows[0];
            txtSalutionID.Text = dr.Cells[0].Value.ToString();
            txtSalution.Text = dr.Cells[1].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            SALUTION objSalution = new SALUTION();
            objSalution.SALUTION_ID = new Guid(txtSalutionID.Text);
            
            int i = CommonSetupSave.DeleteSalution(objSalution);
            if (i > 0)
            {
                dgvSalutionInfo.AutoGenerateColumns = false;
                dgvSalutionInfo.DataSource = CommonSetupFetch.getALLSALUTION();
                MessageBox.Show("Delete Successfully");

                txtSalution.Text = "";
                txtSalutionID.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
