﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Common_Setup;
using IA.BIZ.Setup.Common_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Common_Setup
{
    public partial class Nationality_Setup : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Nationality_Setup()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvNationalityInfo.AutoGenerateColumns = false;
            dgvNationalityInfo.DataSource = CommonSetupFetch.getAllNATIONALITY();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtNationality.Text == "")
            {
                er++;
                ep.SetError(txtNationality, "Name Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                NATIONALITY objNATIONALITY = new NATIONALITY();
                objNATIONALITY.NATIONALITY_NAME = txtNationality.Text;
                if (cmbActive.SelectedIndex == 0)
                    objNATIONALITY.STATUS_FLAG = "A";
                else
                    objNATIONALITY.STATUS_FLAG = "U";
                int i = CommonSetupSave.saveNationality(objNATIONALITY);
                if (i > 0)
                {
                    dgvNationalityInfo.AutoGenerateColumns = false;
                    dgvNationalityInfo.DataSource = CommonSetupFetch.getAllNATIONALITY();
                    MessageBox.Show("Save Successfully");

                    txtNationality.Text = "";
                }
                else
                    MessageBox.Show("Save Not Successfully");

            }
        }

        private void txtNationality_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            NATIONALITY objNATIONALITY = new NATIONALITY();
            objNATIONALITY.NATIONALITY_ID =new Guid (txtNationalityID.Text);
            objNATIONALITY.NATIONALITY_NAME = txtNationality.Text;
            if (cmbActive.SelectedIndex == 0)
                objNATIONALITY.STATUS_FLAG = "A";
            else
                objNATIONALITY.STATUS_FLAG = "U";
            int i = CommonSetupSave.UpdateNationality(objNATIONALITY);
            if (i > 0)
            {
                dgvNationalityInfo.AutoGenerateColumns = false;
                dgvNationalityInfo.DataSource = CommonSetupFetch.getAllNATIONALITY();
                MessageBox.Show("Update Successfully");

                txtNationality.Text = "";
                txtNationalityID.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");

        }

        private void dgvNationalityInfo_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvNationalityInfo.SelectedRows[0];
            txtNationalityID.Text = dr.Cells[0].Value.ToString();
            txtNationality.Text = dr.Cells[1].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            NATIONALITY objNATIONALITY = new NATIONALITY();
            objNATIONALITY.NATIONALITY_ID = new Guid(txtNationalityID.Text);
       
            int i = CommonSetupSave.DeleteNationality(objNATIONALITY);
            if (i > 0)
            {
                dgvNationalityInfo.AutoGenerateColumns = false;
                dgvNationalityInfo.DataSource = CommonSetupFetch.getAllNATIONALITY();
                MessageBox.Show("Delete Successfully");

                txtNationality.Text = "";
                txtNationalityID.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
