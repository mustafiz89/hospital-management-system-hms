﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Common_Setup;
using IA.BIZ.Setup.Common_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Common_Setup
{
    public partial class Gender_Setup : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Gender_Setup()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvGenderInfo.AutoGenerateColumns = false;
            dgvGenderInfo.DataSource = CommonSetupFetch.getAllGENDER();
        }

       
        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtGender.Text == "")
            {
                er++;
                ep.SetError(txtGender, "Name Required");
            }
            else
            {
                GENDER objGender = new GENDER();
                objGender.GENDER_NAME = txtGender.Text;
                if (cmbActive.SelectedIndex == 0)
                    objGender.STATUS_FLAG = "A";
                else
                    objGender.STATUS_FLAG = "U";
                int i = CommonSetupSave.saveGender(objGender);
                if (i > 0)
                {
                    dgvGenderInfo.AutoGenerateColumns = false;
                    dgvGenderInfo.DataSource = CommonSetupFetch.getAllGENDER();
                    MessageBox.Show("Save Successfully");

                    txtGender.Text = "";
                }
                else
                    MessageBox.Show("Save Not Successfully");

            }
        }

        private void txtGender_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            GENDER objGender = new GENDER();
            objGender.GENDER_ID =new Guid (txtGenderID.Text);
            objGender.GENDER_NAME = txtGender.Text;
            if (cmbActive.SelectedIndex == 0)
                objGender.STATUS_FLAG = "A";
            else
                objGender.STATUS_FLAG = "U";
            int i = CommonSetupSave.UpdateGender(objGender);
            if (i > 0)
            {
                dgvGenderInfo.AutoGenerateColumns = false;
                dgvGenderInfo.DataSource = CommonSetupFetch.getAllGENDER();
                MessageBox.Show("Update Successfully");

                txtGenderID.Text = "";
                txtGender.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");

        }

        private void dgvGenderInfo_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvGenderInfo.SelectedRows[0];
            txtGenderID.Text = dr.Cells[0].Value.ToString();
            txtGender.Text = dr.Cells[1].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            GENDER objGender = new GENDER();
            objGender.GENDER_ID = new Guid(txtGenderID.Text);
            
            int i = CommonSetupSave.DeleteGender(objGender);
            if (i > 0)
            {
                dgvGenderInfo.AutoGenerateColumns = false;
                dgvGenderInfo.DataSource = CommonSetupFetch.getAllGENDER();
                MessageBox.Show("Delete Successfully");

                txtGenderID.Text = "";
                txtGender.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
