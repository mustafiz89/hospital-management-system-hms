﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Common_Setup;
using IA.BIZ.Setup.Common_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Common_Setup
{
    public partial class Religion_Setup : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Religion_Setup()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvReligionInfo.AutoGenerateColumns = false;
            dgvReligionInfo.DataSource = CommonSetupFetch.getRELIGION();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtReligion.Text == "")
            {
                er++;
                ep.SetError(txtReligion, "Name Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                RELIGION objReligion = new RELIGION();
                objReligion.RELIGION_NAME = txtReligion.Text;
                if (cmbActive.SelectedIndex == 0)
                    objReligion.STATUS_FLAG = "A";
                else
                    objReligion.STATUS_FLAG = "U";
                int i = CommonSetupSave.saveReligion(objReligion);
                if (i > 0)
                {
                    dgvReligionInfo.AutoGenerateColumns = false;
                    dgvReligionInfo.DataSource = CommonSetupFetch.getRELIGION();
                    MessageBox.Show("Save Successfully");

                    txtReligion.Text = "";
                }
                else
                    MessageBox.Show("Save Not Successfully");

            }
        }

        private void txtReligion_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            RELIGION objReligion = new RELIGION();
            objReligion.RELIGION_ID =new Guid (txtReligionID.Text);
            objReligion.RELIGION_NAME = txtReligion.Text;
            if (cmbActive.SelectedIndex == 0)
                objReligion.STATUS_FLAG = "A";
            else
                objReligion.STATUS_FLAG = "U";
            int i = CommonSetupSave.UpdateReligion(objReligion);
            if (i > 0)
            {
                dgvReligionInfo.AutoGenerateColumns = false;
                dgvReligionInfo.DataSource = CommonSetupFetch.getRELIGION();
                MessageBox.Show("Update Successfully");

                txtReligion.Text = "";
                txtReligionID.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvReligionInfo_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvReligionInfo.SelectedRows[0];
            txtReligionID.Text = dr.Cells[0].Value.ToString();
            txtReligion.Text = dr.Cells[1].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            RELIGION objReligion = new RELIGION();
            objReligion.RELIGION_ID = new Guid(txtReligionID.Text);
          
            int i = CommonSetupSave.DeleteReligion(objReligion);
            if (i > 0)
            {
                dgvReligionInfo.AutoGenerateColumns = false;
                dgvReligionInfo.DataSource = CommonSetupFetch.getRELIGION();
                MessageBox.Show("Delete Successfully");

                txtReligion.Text = "";
                txtReligionID.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
