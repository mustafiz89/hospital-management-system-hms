﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Common_Setup;
using IA.BIZ.Setup.Common_Setup;
using POS_SYSTEM;
using System.Data.SqlClient;

namespace HMSDev.UI.Setup.Common_Setup
{
    public partial class Blood_Group_Setup : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Blood_Group_Setup()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvBloodGroupInfo.AutoGenerateColumns = false;
            dgvBloodGroupInfo.DataSource = CommonSetupFetch.getAllBLOOD_GROUP();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtBloodgropuName.Text == "")
            {
                er++;
                ep.SetError(txtBloodgropuName,"Name Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                BLOOD_GROUP objBLOOD_GROUP = new BLOOD_GROUP();
                objBLOOD_GROUP.BLOOD_GROUP_NAME = txtBloodgropuName.Text;
                if (cmbActive.SelectedIndex == 0)
                    objBLOOD_GROUP.STATUS_FLAG = "A";
                else
                    objBLOOD_GROUP.STATUS_FLAG = "U";
                int i = CommonSetupSave.saveBLOOD_GROUP(objBLOOD_GROUP);
                if (i > 0)
                {
                    dgvBloodGroupInfo.AutoGenerateColumns = false;
                    dgvBloodGroupInfo.DataSource = CommonSetupFetch.getAllBLOOD_GROUP();
                    MessageBox.Show("Save Successfully");                    

                    txtBloodgropuName.Text = "";
                }
                else
                    MessageBox.Show("Save Not Successfully");
            }

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            BLOOD_GROUP objBLOOD_GROUP = new BLOOD_GROUP();

            objBLOOD_GROUP.BLOOD_GROUP_ID = new Guid(txtBloodGroupID.Text);
            objBLOOD_GROUP.BLOOD_GROUP_NAME = txtBloodgropuName.Text;

            if (cmbActive.SelectedIndex == 0)
                objBLOOD_GROUP.STATUS_FLAG = "A";
            else
                objBLOOD_GROUP.STATUS_FLAG = "U";
          
            int i = CommonSetupSave.UpdateBloodGroup(objBLOOD_GROUP);
            if (i > 0)
            {
                dgvBloodGroupInfo.AutoGenerateColumns = false;
                dgvBloodGroupInfo.DataSource = CommonSetupFetch.getAllBLOOD_GROUP();
                MessageBox.Show("Update Successfully");

                txtBloodgropuName.Text = "";
                txtBloodGroupID.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");

        }

        private void txtBloodgropuName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dgvBloodGroupInfo_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvBloodGroupInfo.SelectedRows[0];
            txtBloodGroupID.Text = dr.Cells[0].Value.ToString();
            txtBloodgropuName.Text = dr.Cells[1].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            BLOOD_GROUP objBLOOD_GROUP = new BLOOD_GROUP();

            objBLOOD_GROUP.BLOOD_GROUP_ID = new Guid(txtBloodGroupID.Text);

            int i = CommonSetupSave.DeleteBloodGroup(objBLOOD_GROUP);
            if (i > 0)
            {
                dgvBloodGroupInfo.AutoGenerateColumns = false;
                dgvBloodGroupInfo.DataSource = CommonSetupFetch.getAllBLOOD_GROUP();
                MessageBox.Show("Delete Successfully");

                txtBloodgropuName.Text = "";
                txtBloodGroupID.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
