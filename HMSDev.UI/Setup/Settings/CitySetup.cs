﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Settings;
using IA.BIZ.Setup.Settings;
using POS_SYSTEM;
using System.Data.SqlClient;

namespace HMSDev.UI.Setup.Settings
{
    public partial class CitySetup : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public CitySetup()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvCityInfo.AutoGenerateColumns = false;
            dgvCityInfo.DataSource = SettingsFetch.getALLCity();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtCityName.Text == "")
            {
                er++;
                ep.SetError(txtCityName,"Name Required");
                //MessageBox.Show("required field needed");
            }

            else
            {           
                City objCity = new City();
                objCity.CITY_NAME = txtCityName.Text;
                if (cmbActive.SelectedIndex == 0)
                    objCity.STATUS_FLAG = "A";
                else
                    objCity.STATUS_FLAG = "U";
                int i = SettingsSetup.SaveCity(objCity);
                if (i > 0)
                {
                    dgvCityInfo.AutoGenerateColumns = false;
                    dgvCityInfo.DataSource = SettingsFetch.getALLCity();
                    MessageBox.Show("Save Successfully");

                    txtCityName.Text = "";
                }
                else
                    MessageBox.Show("Save Not Successfully");
            }
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            City objCity = new City();

            objCity.CITY_ID = new Guid(txtCityID.Text);
            objCity.CITY_NAME = txtCityName.Text;

            if (cmbActive.SelectedIndex == 0)
                objCity.STATUS_FLAG = "A";
            else
                objCity.STATUS_FLAG = "U";

            int i = SettingsSetup.UpdateCity(objCity);
            if (i > 0)
            {
                dgvCityInfo.AutoGenerateColumns = false;
                dgvCityInfo.DataSource = SettingsFetch.getALLCity();
                MessageBox.Show("Update Successfully");

                txtCityName.Text = "";
                txtCityID.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            City objCity = new City();

            objCity.CITY_ID = new Guid(txtCityID.Text);

            int i = SettingsSetup.DeleteCity(objCity);
            if (i > 0)
            {
                dgvCityInfo.AutoGenerateColumns = false;
                dgvCityInfo.DataSource = SettingsFetch.getALLCity();
                MessageBox.Show("Delete Successfully");

                txtCityID.Text = "";
                txtCityName.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }

        private void dgvCityInfo_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvCityInfo.SelectedRows[0];
            txtCityID.Text = dr.Cells[0].Value.ToString();
            txtCityName.Text = dr.Cells[1].Value.ToString();
        }

    }
}
