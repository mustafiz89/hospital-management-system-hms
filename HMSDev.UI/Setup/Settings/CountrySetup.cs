﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Settings;
using IA.BIZ.Setup.Settings;
using POS_SYSTEM;
using System.Data.SqlClient;

namespace HMSDev.UI.Setup.Settings
{
    public partial class CountrySetup : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public CountrySetup()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvCountryInfo.AutoGenerateColumns = false;
            dgvCountryInfo.DataSource = SettingsFetch.getALLCountry();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtCountryName.Text == "")
            {
                er++;
                ep.SetError(txtCountryName, "Name Required");
                //MessageBox.Show("required field needed");
            }

            else
            {
                COUNTRY objCountry = new COUNTRY();
                objCountry.COUNTRY_NAME = txtCountryName.Text;
                if (cmbActive.SelectedIndex == 0)
                    objCountry.STATUS_FLAG = "A";
                else
                    objCountry.STATUS_FLAG = "U";
                int i = SettingsSetup.SaveCountry(objCountry);
                if (i > 0)
                {
                    dgvCountryInfo.AutoGenerateColumns = false;
                    dgvCountryInfo.DataSource = SettingsFetch.getALLCountry();
                    MessageBox.Show("Save Successfully");

                    txtCountryName.Text = "";
                    cmbActive.SelectedIndex = 0;
                }
                else
                    MessageBox.Show("Save Not Successfully");
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            COUNTRY objCountry = new COUNTRY();
            objCountry.COUNTRY_ID =new Guid (txtCountryID.Text);
            objCountry.COUNTRY_NAME = txtCountryName.Text;
            if (cmbActive.SelectedIndex == 0)
                objCountry.STATUS_FLAG = "A";
            else
                objCountry.STATUS_FLAG = "U";
            int i = SettingsSetup.UpdateCountry(objCountry);
            if (i > 0)
            {
                dgvCountryInfo.AutoGenerateColumns = false;
                dgvCountryInfo.DataSource = SettingsFetch.getALLCountry();
                MessageBox.Show("Update Successfully");

                txtCountryID.Text = "";
                txtCountryName.Text = "";
                cmbActive.SelectedIndex = 0;
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            COUNTRY objCountry = new COUNTRY();
            objCountry.COUNTRY_ID = new Guid(txtCountryID.Text);
           
            int i = SettingsSetup.DeleteCountry(objCountry);
            if (i > 0)
            {
                dgvCountryInfo.AutoGenerateColumns = false;
                dgvCountryInfo.DataSource = SettingsFetch.getALLCountry();
                MessageBox.Show("Delete Successfully");

                txtCountryID.Text = "";
                txtCountryName.Text = "";
                cmbActive.SelectedIndex = 0;
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }

        private void dgvCountryInfo_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvCountryInfo.SelectedRows[0];
            txtCountryID.Text = dr.Cells[0].Value.ToString();
            txtCountryName.Text = dr.Cells[1].Value.ToString();
        }
    }
}
