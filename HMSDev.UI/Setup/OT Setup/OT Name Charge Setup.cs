﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.OT_Setup;
using IA.BIZ.Setup.OT_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.OT_Setup
{
    public partial class OT_Charge_Setup : Form
    {
        public OT_Charge_Setup()
        {
            InitializeComponent();
            OTName();
            dgvOTChargeinfo.AutoGenerateColumns = false;
            dgvOTChargeinfo.DataSource = OTFetch.getALLOTCharge();
        }

        

        private void btnAdd_Click(object sender, EventArgs e)
        {
            OT_CHARGE objOTCharge = new OT_CHARGE();
            objOTCharge.OT_ID =new Guid (cmbOTName.SelectedValue.ToString());
            objOTCharge.OT_AMOUNT = Convert.ToDecimal(txtAmount.Text);
            objOTCharge.OT_DISCOUNT = Convert.ToDecimal(txtDiscount.Text);


            int i = OTSetup.saveOTCharge(objOTCharge);
            if (i > 0)
            {
                dgvOTChargeinfo.AutoGenerateColumns = false;
                dgvOTChargeinfo.DataSource = OTFetch.getALLOTCharge();
                MessageBox.Show("Save Successfully");

                txtAmount.Text = "";
                txtDiscount.Text = "";
            }
            else
                MessageBox.Show("Save Not Successfully");
        }


        private void OTName()
        {
            cmbOTName.DataSource = OTFetch.getALLOT();
            cmbOTName.ValueMember = "OT_ID";
            cmbOTName.DisplayMember = "OT_NAME";
        }

        private void cmbOTCategoryName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbOTName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtAmount_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtDiscount_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void btnEdit_Click(object sender, EventArgs e)
        {
            OT_CHARGE objOTCharge = new OT_CHARGE();
            objOTCharge.OT_CHARGE_ID =new Guid (txtOTChargeID.Text);
            objOTCharge.OT_ID = new Guid(cmbOTName.SelectedValue.ToString());
            objOTCharge.OT_AMOUNT = Convert.ToDecimal(txtAmount.Text);
            objOTCharge.OT_DISCOUNT = Convert.ToDecimal(txtDiscount.Text);


            int i = OTSetup.UpdateOTCharge(objOTCharge);
            if (i > 0)
            {
                dgvOTChargeinfo.AutoGenerateColumns = false;
                dgvOTChargeinfo.DataSource = OTFetch.getALLOTCharge();
                MessageBox.Show("Update Successfully");

                txtOTChargeID.Text = "";
                txtAmount.Text = "";
                txtDiscount.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvOTChargeinfo_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvOTChargeinfo.SelectedRows[0];
            txtOTChargeID.Text = dr.Cells[0].Value.ToString();
            cmbOTName.SelectedIndex = cmbOTName.FindString(dr.Cells[1].Value.ToString());
            txtAmount.Text = dr.Cells[2].Value.ToString();
            txtDiscount.Text = dr.Cells[3].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            OT_CHARGE objOTCharge = new OT_CHARGE();
            objOTCharge.OT_CHARGE_ID = new Guid(txtOTChargeID.Text);

            int i = OTSetup.DeleteOTCharge(objOTCharge);
            if (i > 0)
            {
                dgvOTChargeinfo.AutoGenerateColumns = false;
                dgvOTChargeinfo.DataSource = OTFetch.getALLOTCharge();
                MessageBox.Show("Delete Successfully");

                txtOTChargeID.Text = "";
                txtAmount.Text = "";
                txtDiscount.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
