﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace POS_SYSTEM
{
    public partial class DSMessageBox : Form
    {
        public DSMessageBox(string message,int a)
        {
      
            InitializeComponent();
            lblMessage.Text = message;
            picOK.Visible = false;
            picCancel.Visible = false;
            picWarning.Visible = false;
          
            if (a==1)
            { 
            picOK.Visible = true;
            }
            if (a == 2)
            {
                picCancel.Visible = true;
            }
            if (a == 3)
            {
                picWarning.Visible = true;
            }

            lblMessage.MaximumSize = new Size(380, 0);
            lblMessage.AutoSize = true;
            btnOK.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            return;
        }

    }
}
