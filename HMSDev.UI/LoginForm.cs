﻿using System.Windows.Forms;
using HMSDev.UI;

namespace IA.UI
{
    public partial class LoginForm : Form
    {

        public LoginForm()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, System.EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            HomePage obj = new HomePage();
            obj.Show();
            this.Hide();
        }
    }
}
            //BindComboCompanyName();
//            txtUserName.Focus();
//            txtUserName.Text = user_id;
//            txtUserPassword.Text = password;
//            try
//            {
//                if (company_id == "")
//                {
//                    cmbCompany.DataSource = SettingsFatch.GetAllCompany();
//                    cmbCompany.DisplayMember = "COMPANY_NAME";
//                    cmbCompany.ValueMember = "COMPANY_ID";

//                    cmbBranch.DataSource = SettingsFatch.GetAllBranch(cmbCompany.SelectedValue.ToString());
//                    cmbBranch.DisplayMember = "BRANCH_NAME";
//                    cmbBranch.ValueMember = "BRANCH_ID";
//                }
//                else
//                {
//                    COMPANY objCOMPANY = SettingsFatch.GetCompany(company_id);
//                    List<COMPANY> objListCOMPANY = new List<COMPANY>();
//                    objListCOMPANY.Add(objCOMPANY);
//                    cmbCompany.DataSource = objListCOMPANY;
//                    cmbCompany.DisplayMember = "COMPANY_NAME";
//                    cmbCompany.ValueMember = "COMPANY_ID";
//                    cmbBranch.DataSource = SettingsFatch.GetAllBranch(cmbCompany.SelectedValue.ToString());
//                    cmbBranch.DisplayMember = "BRANCH_NAME";
//                    cmbBranch.ValueMember = "BRANCH_ID";
//                    cmbCompany.Enabled = false;
//                }
//            }
//            catch { }
//        }
//        private void BindComboCompanyName()
//        {
//            //cmbCompanyName.DataSource = SettingsFatch.GetAllCompany();
//            //cmbCompanyName.DisplayMember = "COMPANY_NAME";
//            //cmbCompanyName.ValueMember = "COMPANY_ID";
//        }
//        private void BindComboCompanyLocation()
//        {
//            //cmbCompanyLocation.DataSource = SettingsFatch.GetCompany(cmbCompanyName.SelectedValue.ToString());
//            //cmbCompanyLocation.DisplayMember = "ADDRESS";
//            //cmbCompanyLocation.ValueMember = "ADDRESS";
//        }
//        private void button1_Click(object sender, EventArgs e)
//        {

//            #region database restore    

//            string bd_name = SmsUserManagement.TestSqlConncetion();
//            if (string.IsNullOrEmpty(bd_name))
//            {
//                POS_SYSTEM.Others.DatabaseRestore obj = new POS_SYSTEM.Others.DatabaseRestore();
//                obj.Show();
//                this.Hide();
//                return;
//            }
//            else
//            {

//            }
//            #endregion

//            #region security and 15 days demo
//            string value = SmsUserManagement.createDemoLicense();
//            if (!string.IsNullOrEmpty(value))
//            {
//                return;
//            }
//            #endregion



//            string lblInfo = "";
//            string password = "";
//            if (txtUserName.Text.Length == 0)
//            {
//                txtUserName.Focus();
//                lblInfo = "User ID can not be empty";
//                MessageBox.Show(lblInfo);
//                return;
//            }
//            if (txtUserPassword.Text.Length == 0)
//            {
//                txtUserPassword.Focus();
//                lblInfo = "Password can not be empty";
//                MessageBox.Show(lblInfo);
//                return;
//            }
//            USER_PROFILE objUSER_PROFILE = new USER_PROFILE();
//            objUSER_PROFILE.USER_ID = txtUserName.Text;
//            objUSER_PROFILE.PASSWORD = DSCryptorEngine.Encrypt(txtUserPassword.Text, true);
//            objUSER_PROFILE.COMPANY_ID = cmbCompany.SelectedValue.ToString();
//            objUSER_PROFILE.TERMINAL_IP_ADDRESS = "";// Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"];
//            string TERMINAL_IP_ADDRESS = "";// Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"];
//            objUSER_PROFILE.SESSION_ID = "";//this.Session.SessionID;
//            objUSER_PROFILE = SmsUserManagement.USER_LOGIN(objUSER_PROFILE);
//            if (!string.IsNullOrEmpty(objUSER_PROFILE.PASSWORD))
//            {
//                password = DSCryptorEngine.Decrypt(objUSER_PROFILE.PASSWORD, true);
//            }
//            //if (!string.IsNullOrEmpty(objUSER_PROFILE.USER_ID))
//            //{
//            //    if (objUSER_PROFILE.USER_ID != txtUserName.Text)
//            //    {
//            //        txtUserName.Focus();
//            //        lblInfo = objUSER_PROFILE.USER_ID + " is login from this terminal " + objUSER_PROFILE.TERMINAL_IP_ADDRESS + ".";
//            //        MessageBox.Show(lblInfo); 
//            //        return;
//            //    }
//            //}
//            //if (!string.IsNullOrEmpty(objUSER_PROFILE.TERMINAL_IP_ADDRESS) && TERMINAL_IP_ADDRESS != objUSER_PROFILE.TERMINAL_IP_ADDRESS)
//            //{
//            //    if (objUSER_PROFILE.USER_ID == txtUserName.Text)
//            //    {
//            //        txtUserName.Focus();
//            //        lblInfo = objUSER_PROFILE.USER_ID + " is login from terminal " + objUSER_PROFILE.TERMINAL_IP_ADDRESS + ".";
//            //        MessageBox.Show(lblInfo); 
//            //        return;
//            //    }
//            //}
//            if (password == txtUserPassword.Text)
//                if (!string.IsNullOrEmpty(objUSER_PROFILE.USER_ID))
//                {
//                    if (objUSER_PROFILE.USER_ID == txtUserName.Text)
//                    {
//                        string vMsg = SmsUserManagement.CheckLicense(objUSER_PROFILE.COMPANY_ID, objUSER_PROFILE.USER_ID);
//                        if (string.IsNullOrEmpty(vMsg))
//                        {
//                            string system_key = KeyFetch.System_Key();
//                            string database_key = KeyFetch.Database_Key();
//                            if (system_key != database_key)
//                            {
//                                txtUserPassword.Focus();
//                                lblInfo = "New Year";
//                                MessageBox.Show(lblInfo);
//                                return;
//                            }

//                            DASSessionInfoClass.USER_ID = txtUserName.Text;
//                            DASSessionInfoClass.COMPANY_ID = objUSER_PROFILE.COMPANY_ID;
//                            DASSessionInfoClass.BRANCH_ID = objUSER_PROFILE.BRANCH_ID;
//                            DASSessionInfoClass.COMPANY_NAME = objUSER_PROFILE.COMPANY_NAME;
//                            DASSessionInfoClass.ADDRESS = objUSER_PROFILE.ADDRESS;
//                            DASSessionInfoClass.BRANCH_ID = objUSER_PROFILE.BRANCH_ID;
//                            DASSessionInfoClass.BRANCH_NAME = objUSER_PROFILE.BRANCH_NAME;
//                            DASSessionInfoClass.HO_FLAG = objUSER_PROFILE.HO_FLAG;
//                            DASSessionInfoClass.WARE_HOUSE_FLAG = objUSER_PROFILE.WARE_HOUSE_FLAG;


//                            DASSessionInfoClass.MIDICINE_MODULE_FLAG = objUSER_PROFILE.MIDICINE_MODULE_FLAG;
//                            DASSessionInfoClass.PRODUCTION_MODULE_FLAG = objUSER_PROFILE.PRODUCTION_MODULE_FLAG;
//                            DASSessionInfoClass.WAREHOUSE_MODULE_FLAG = objUSER_PROFILE.WAREHOUSE_MODULE_FLAG;

//                            HomePage obj = new HomePage(txtUserName.Text);
//                            obj.Show();
//                            this.Hide();
//                        }
//                        else
//                        {
//                            lblInfo = vMsg;
//                            //if (lblInfo == "SQL:20001-Invaid License Key" || lblInfo == "SQL:20003-Date Expired" || lblInfo == "SQL:20000-Invaid License Key" || lblInfo == "SQL:20006-Invalid System Date" || lblInfo == "SQL:20007-Invaid License Key")
//                            //{
//                            LicenseInfoPopUpUI objLicenseInfoPopUpUI = new LicenseInfoPopUpUI(lblInfo);
//                            objLicenseInfoPopUpUI.Show();
//                            this.Hide();
//                            //}                           
//                            //else
//                            //{
//                            //    MessageBox.Show(lblInfo);
//                            //    txtUserName.Focus();
//                            //}

//                        }
//                    }
//                    else
//                    {
//                        txtUserName.Focus();
//                        txtUserPassword.Text = "";
//                        lblInfo = "Invalid user ID or Password";
//                        MessageBox.Show(lblInfo);
//                    }
//                }
//                else
//                {
//                    txtUserName.Focus();
//                    txtUserPassword.Text = "";
//                    lblInfo = "Invalid user ID or Password";
//                    MessageBox.Show(lblInfo);
//                }
//            else
//            {
//                txtUserPassword.Text = "";
//                lblInfo = "Invalid user ID or Password";
//                MessageBox.Show(lblInfo);
//            }
//        }
//        private void button2_Click(object sender, EventArgs e)
//        {
//            Application.Exit();
//        }

//        private void cmbCompanyName_TextChanged(object sender, EventArgs e)
//        {
//            BindComboCompanyLocation();
//        }

//        private void button3_Click(object sender, EventArgs e)
//        {
//            Application.Exit();
//        }

//        private void button1_MouseHover(object sender, EventArgs e)
//        {

//        }

//        private void button1_MouseLeave(object sender, EventArgs e)
//        {
//            button1.BackColor = System.Drawing.Color.WhiteSmoke;
//            button1.ForeColor = System.Drawing.Color.Black;
//        }

//        private void button1_MouseMove(object sender, MouseEventArgs e)
//        {
//            button1.BackColor = System.Drawing.Color.Blue;
//            button1.ForeColor = System.Drawing.Color.White;
//        }

//        private void txtUserName_KeyPress(object sender, KeyPressEventArgs e)
//        {
//            if (e.KeyChar == '\r')
//            {
//                e.Handled = true;
//                SendKeys.Send("{TAB}");
//            }
//        }

//        private void txtUserPassword_KeyPress(object sender, KeyPressEventArgs e)
//        {
//            if (e.KeyChar == '\r')
//            {
//                e.Handled = true;
//                SendKeys.Send("{TAB}");
//            }
//        }

//        private void groupBox1_Enter(object sender, EventArgs e)
//        {

//        }

//        private void LoginForm_Load(object sender, EventArgs e)
//        {

//        }

//        private void cmbCompany_SelectedIndexChanged(object sender, EventArgs e)
//        {
//            cmbBranch.DataSource = SettingsFatch.GetAllBranch(cmbCompany.SelectedValue.ToString());
//            cmbBranch.DisplayMember = "BRANCH_NAME";
//            cmbBranch.ValueMember = "BRANCH_ID";
//        }

//        private void cmbCompany_KeyUp(object sender, KeyEventArgs e)
//        {
//            try
//            {
//                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
//                {
//                    cmbBranch.Focus();
//                }
//            }
//            catch { }
//        }

//        private void cmbBranch_KeyUp(object sender, KeyEventArgs e)
//        {
//            try
//            {
//                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
//                {
//                    txtUserName.Focus();
//                }
//            }
//            catch { }
//        }

//        private void txtUserName_KeyUp(object sender, KeyEventArgs e)
//        {
//            try
//            {
//                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
//                {
//                    txtUserPassword.Focus();
//                }
//            }
//            catch { }
//        }

//        private void txtUserPassword_KeyUp(object sender, KeyEventArgs e)
//        {
//            try
//            {
//                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
//                {
//                    button1.Focus();
//                }
//            }
//            catch { }
//        }
//    }
//}
