﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IA.Model.Setup.Accounts;
using IA.Provider;
using System.Data.SqlClient;
using System.Data;

namespace IA.BIZ.Task.Accounts
{
    public class VoucherFetch
    {
        public static List<ACCOUNT_LEDGER> GetVoucher(string VOUCHER_NO, string COMPANY_ID, string BRANCH_ID)
        {
            List<ACCOUNT_LEDGER> objListACCOUNT_LEDGER = new List<ACCOUNT_LEDGER>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_ACCOUNT_LEDGER_GK";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@VOUCHER_NO", SqlDbType.VarChar, 50));
            objSqlCommand.Parameters["@VOUCHER_NO"].Value = VOUCHER_NO;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = COMPANY_ID;
            objSqlCommand.Parameters.Add(new SqlParameter("@BRANCH_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@BRANCH_ID"].Value = BRANCH_ID;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                SqlDataReader dr = objSqlCommand.ExecuteReader();
                while (dr.Read())
                {
                    ACCOUNT_LEDGER objACCOUNT_LEDGER = new ACCOUNT_LEDGER();
                    objACCOUNT_LEDGER.ACCOUNT_ID = dr["ACCOUNT_ID"].ToString();
                    objACCOUNT_LEDGER.NOTE = dr["NOTE"].ToString();
                    if (!string.IsNullOrEmpty(dr["LEDGER_DEBIT"].ToString()))
                        objACCOUNT_LEDGER.LEDGER_DEBIT = Convert.ToDecimal(dr["LEDGER_DEBIT"]);
                    if (!string.IsNullOrEmpty(dr["LEDGER_CREDIT"].ToString()))
                        objACCOUNT_LEDGER.LEDGER_CREDIT = Convert.ToDecimal(dr["LEDGER_CREDIT"]);
                    objACCOUNT_LEDGER.DESCRIPTION = dr["DESCRIPTION"].ToString();

                    objListACCOUNT_LEDGER.Add(objACCOUNT_LEDGER);
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objListACCOUNT_LEDGER;
        }

        public static List<ACCOUNT_LEDGER> GetNonVoucherPost(string COMPANY_ID, string BRANCH_ID)
        {
            List<ACCOUNT_LEDGER> objListACCOUNT_LEDGER = new List<ACCOUNT_LEDGER>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_ACCOUNT_LEDGER_GU";
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = COMPANY_ID;
            objSqlCommand.Parameters.Add(new SqlParameter("@BRANCH_ID", SqlDbType.VarChar, 50));
            objSqlCommand.Parameters["@BRANCH_ID"].Value = BRANCH_ID;
            objSqlCommand.CommandType = CommandType.StoredProcedure;


            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                SqlDataReader dr = objSqlCommand.ExecuteReader();
                while (dr.Read())
                {
                    ACCOUNT_LEDGER objACCOUNT_LEDGER = new ACCOUNT_LEDGER();
                    //objACCOUNT_LEDGER.ACCOUNT_ID = dr["ACCOUNT_ID"].ToString();
                    // objACCOUNT_LEDGER.NOTE = dr["NOTE"].ToString();
                    //if (!string.IsNullOrEmpty(dr["LEDGER_DEBIT"].ToString()))
                    // objACCOUNT_LEDGER.LEDGER_DEBIT =Convert.ToDecimal(dr["LEDGER_DEBIT"]);
                    // if (!string.IsNullOrEmpty(dr["LEDGER_CREDIT"].ToString()))
                    //objACCOUNT_LEDGER.LEDGER_CREDIT = Convert.ToDecimal(dr["LEDGER_CREDIT"]);
                    objACCOUNT_LEDGER.VOUCHER_NO = dr["VOUCHER_NO"].ToString();


                    objListACCOUNT_LEDGER.Add(objACCOUNT_LEDGER);
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objListACCOUNT_LEDGER;
        }
        public static List<ACCOUNT_LEDGER> GetVoucherPost(string COMPANY_ID,string BRANCH_ID )
        {
            List<ACCOUNT_LEDGER> objListACCOUNT_LEDGER = new List<ACCOUNT_LEDGER>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_ACCOUNT_LEDGER_POST_A";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = COMPANY_ID;
            objSqlCommand.Parameters.Add(new SqlParameter("@BRANCH_ID", SqlDbType.VarChar, 50));
            objSqlCommand.Parameters["@BRANCH_ID"].Value = BRANCH_ID;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                SqlDataReader dr = objSqlCommand.ExecuteReader();
                while (dr.Read())
                {
                    ACCOUNT_LEDGER objACCOUNT_LEDGER = new ACCOUNT_LEDGER();
                    objACCOUNT_LEDGER.VOUCHER_NO = dr["VOUCHER_NO"].ToString();
                    objListACCOUNT_LEDGER.Add(objACCOUNT_LEDGER);
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objListACCOUNT_LEDGER;
        }
    }
}
