﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IA.Model.Setup.Other
{
    public class BUYER_SUPPLIER_INFO
    {
        private decimal _BS_ACCOUNT_BALANCE;

        public decimal BS_ACCOUNT_BALANCE
        {
            get { return _BS_ACCOUNT_BALANCE; }
            set { _BS_ACCOUNT_BALANCE = value; }
        }


        private int _DISTRICT_ID;

        public int DISTRICT_ID
        {
            get { return _DISTRICT_ID; }
            set { _DISTRICT_ID = value; }
        }
        
        private string _ZONE_NAME;

        public string ZONE_NAME
        {
            get { return _ZONE_NAME; }
            set { _ZONE_NAME = value; }
        }

        private int _BUYER_SUPPLIER_ID;

        public int BUYER_SUPPLIER_ID
        {
            get { return _BUYER_SUPPLIER_ID; }
            set { _BUYER_SUPPLIER_ID = value; }
        }

        private string _BUYER_SUPPLIER_NAME;

        public string BUYER_SUPPLIER_NAME
        {
            get { return _BUYER_SUPPLIER_NAME; }
            set { _BUYER_SUPPLIER_NAME = value; }
        }
        private string _COMPANY_NAME;

        public string COMPANY_NAME
        {
            get { return _COMPANY_NAME; }
            set { _COMPANY_NAME = value; }
        }
        private string _ADDRESS;


        public string ADDRESS
        {
            get { return _ADDRESS; }
            set { _ADDRESS = value; }
        }
        private int _ZONE_ID;

        public int ZONE_ID
        {
            get { return _ZONE_ID; }
            set { _ZONE_ID = value; }
        }
        private int _COUNTRY_ID;

        public int COUNTRY_ID
        {
            get { return _COUNTRY_ID; }
            set { _COUNTRY_ID = value; }
        }
        private int _PROFESSION_ID;

        public int PROFESSION_ID
        {
            get { return _PROFESSION_ID; }
            set { _PROFESSION_ID = value; }
        }
        private string _PHONE;

        public string PHONE
        {
            get { return _PHONE; }
            set { _PHONE = value; }
        }
        private DateTime _OPENNING_DATE;

        public DateTime OPENNING_DATE
        {
            get { return _OPENNING_DATE; }
            set { _OPENNING_DATE = value; }
        }
        private Decimal _OPENNING_BALANCE;

        public Decimal OPENNING_BALANCE
        {
            get { return _OPENNING_BALANCE; }
            set { _OPENNING_BALANCE = value; }
        }
        private string _COMPANY_ID;

        public string COMPANY_ID
        {
            get { return _COMPANY_ID; }
            set { _COMPANY_ID = value; }
        }
        private int _IS_ACTIVE;

        public int IS_ACTIVE
        {
            get { return _IS_ACTIVE; }
            set { _IS_ACTIVE = value; }
        }
        private string _POST_CODE;

        public string POST_CODE
        {
            get { return _POST_CODE; }
            set { _POST_CODE = value; }
        }
        private string _BUYER_SUPPLIER_FLAG;

        public string BUYER_SUPPLIER_FLAG
        {
            get { return _BUYER_SUPPLIER_FLAG; }
            set { _BUYER_SUPPLIER_FLAG = value; }
        }
        private string _MAKE_BY;

        public string MAKE_BY
        {
            get { return _MAKE_BY; }
            set { _MAKE_BY = value; }
        }
        private DateTime _MAKE_DT;

        public DateTime MAKE_DT
        {
            get { return _MAKE_DT; }
            set { _MAKE_DT = value; }
        }
        public int MEDICINE_SUPPLIER_FLAG { get; set; }
        public string BRANCH_ID { get; set; }
        public string MEDICINE_SUPPLIER { get; set; }
        

    }
}
