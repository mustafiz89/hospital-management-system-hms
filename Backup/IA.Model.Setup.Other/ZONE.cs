﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IA.Model.Setup.Other
{
    public class ZONE
    {
        private int _ZONE_ID;

        public int ZONE_ID
        {
            get { return _ZONE_ID; }
            set { _ZONE_ID = value; }
        }

        private string _ZONE_NAME;

        public string ZONE_NAME
        {
            get { return _ZONE_NAME; }
            set { _ZONE_NAME = value; }
        }

        private int _IS_ACTIVE;

        public int IS_ACTIVE
        {
            get { return _IS_ACTIVE; }
            set { _IS_ACTIVE = value; }
        }

        private string _STATUS;

        public string STATUS
        {
            get { return _STATUS; }
            set { _STATUS = value; }
        }

        private string _COMPANY_ID;

        public string COMPANY_ID
        {
            get { return _COMPANY_ID; }
            set { _COMPANY_ID = value; }
        }



    }
}
