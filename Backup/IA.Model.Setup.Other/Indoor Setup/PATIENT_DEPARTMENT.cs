﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Indoor_Setup
{
    public class PATIENT_DEPARTMENT
    {
        public Guid PATIENT_DEPARTMENT_ID { get; set; }
        public string PATIENT_DEPARTMENT_NAME { get; set; }
        public string STATUS_FLAG { get; set; }
    }
}
