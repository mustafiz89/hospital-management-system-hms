﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Indoor_Setup
{
    public class BED
    {
        public Guid BED_ID { get; set; }
        public string BED_NUMBER { get; set; }
        public Guid ROOM_CATEGORY_ID { get; set; }
        public Guid ROOM_ID { get; set; }
        public decimal CHARGE_PER_DAY { get; set; }
        public string STATUS_FLAG { get; set; }

        public string ROOM_NUMBER { get; set; }
        public string ROOM_CATEGORY_NAME { get; set; }
    }
}
