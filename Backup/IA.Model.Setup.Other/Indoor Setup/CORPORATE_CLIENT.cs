﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Indoor_Setup
{
    public class CORPORATE_CLIENT
    {
        public Guid CORPORATE_CLIENT_CODE { get; set; }
        public string CORPORATE_CLIENT_NAME { get; set; }
        public string PERSON_NAME { get; set; }
        public string CORPORATE_CLIENT_DESIGNATION { get; set; }
        public string CORPORATE_CLIENT_ADDRESS { get; set; }
        public string PERSON_ADDRESS { get; set; }
        public string CORPORATE_CLIENT_PHONE { get; set; }
        public string PERSON_PHONE { get; set; }
        public DateTime CORPORATE_CLIENT_STARTING_DATE { get; set; }
        public DateTime CORPORATE_CLIENT_EXPIRE_DATE { get; set; }
        public decimal CORPORATE_CLIENT_DISCOUNT { get; set; }
        public decimal CORPORATE_CLIENT_BALANCE{get;set;}
    }
}
