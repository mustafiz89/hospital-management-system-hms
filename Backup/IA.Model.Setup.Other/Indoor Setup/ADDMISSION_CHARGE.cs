﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace IA.Model.Setup.Other.Indoor_Setup
{
    public class ADDMISSION_CHARGE
    {
        public Guid ADMISSION_CHARGE_ID { get; set; }
        public decimal ADMISSION_CHARGE_AMOUNT { get; set; }
        public string STATUS_FLAG { get; set; }

    }
}
