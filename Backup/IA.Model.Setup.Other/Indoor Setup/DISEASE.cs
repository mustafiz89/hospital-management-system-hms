﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Indoor_Setup
{
    public class DISEASE
    {
        public Guid DISEASE_ID { get; set; }
        public string DISEASE_NAME { get; set; }
        public string STATUS_FLAG { get; set; }

    }
}
