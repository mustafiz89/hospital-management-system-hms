﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Pathology_Setup
{
    public class PATHOLOGY_REFERENCE_PERSON
    {
        public Guid PATHOLOGY_REFERENCE_PERSON_ID { get; set; }
        public Guid PATHOLOGY_REFERENCE_CATEGORY_ID { get; set; }
        public string PATHOLOGY_REFERENCE_PERSON_NAME { get; set; }
        public string PATHOLOGY_REFERENCE_PERSON_ADDRESS { get; set; }
        public string PATHOLOGY_REFERENCE_PERSON_CONTACT { get; set; }
        public decimal PATHOLOGY_TESTWISE_COMMISSION { get; set; }

        public string PATHOLOGY_REFERENCE_CATEGORY_NAME { get; set; }
    }
}
