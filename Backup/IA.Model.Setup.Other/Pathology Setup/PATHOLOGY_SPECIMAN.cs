﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Pathology_Setup
{
    public class PATHOLOGY_SPECIMAN
    {
        public Guid PATHOLOGY_SPECIMAN_ID { get; set; }
        public string PATHOLOGY_SPECIMAN_NAME { get; set; }
        public string STATUS_FLAG { get; set; }
    }
}
