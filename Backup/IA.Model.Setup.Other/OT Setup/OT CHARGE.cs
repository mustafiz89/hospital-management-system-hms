﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.OT_Setup
{
    public class OT_CHARGE
    {
        public Guid OT_CHARGE_ID { get; set; }
        public Guid OT_ID { get; set; }
        public decimal OT_AMOUNT { get; set; }
        public decimal OT_DISCOUNT { get; set; }

        public string OT_NAME { get; set; }
    }
}
