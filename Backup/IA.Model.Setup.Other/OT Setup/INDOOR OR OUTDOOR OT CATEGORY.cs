﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace IA.Model.Setup.Other.OT_Setup
{
    public class OT_CATEGORY
    {
        public Guid OT_CATEGORY_ID { get; set; }
        public string OT_CATEGORY_NAME { get; set; }
        public string STATUS_FLAG { get; set; }
    }
}
