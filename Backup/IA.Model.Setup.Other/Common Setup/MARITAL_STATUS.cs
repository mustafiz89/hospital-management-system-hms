﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Common_Setup
{
    public class MARITAL_STATUS
    {
        public Guid MARITAL_STATUS_ID { get; set; }
        public string MARITAL_STATUS_NAME { get; set; }
        public string STATUS_FLAG { get; set; }
    }
}
