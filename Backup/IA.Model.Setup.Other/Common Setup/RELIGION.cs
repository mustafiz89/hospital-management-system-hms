﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Common_Setup
{
    public class RELIGION
    {
        public Guid RELIGION_ID { get; set; }
        public string RELIGION_NAME { get; set; }
        public string STATUS_FLAG { get; set; }
    }
}
