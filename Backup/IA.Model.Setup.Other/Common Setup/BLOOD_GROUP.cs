﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Common_Setup
{
    public class BLOOD_GROUP
    {
        public Guid BLOOD_GROUP_ID { get; set; }
        public string BLOOD_GROUP_NAME { get; set; }
        public string STATUS_FLAG { get; set; }
    }
}
