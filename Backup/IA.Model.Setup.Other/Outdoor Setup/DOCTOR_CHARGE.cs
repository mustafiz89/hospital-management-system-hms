﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Outdoor_Setup
{
    public class OUTDORR_DOCTOR_CHARGE
    {
        public Guid OUTDORR_DOCTOR_CHARGE_ID { get; set; }
        public Guid DOCTOR_ID { get; set; }
        public decimal OUTDORR_DOCTOR_NEW_VISIT_CHARGE { get; set; }
        public decimal OUTDORR_DOCTOR_FOLLOW_UP_CHARGE { get; set; }
        public decimal OUTDORR_DOCTOR_CALL_ON_CHARGE { get; set; }
        public string STATUS_FLAG { get; set; }

        public string DOCTOR_NAME { get; set; }
    }
}
