﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Nurse_Setup
{
    public class NURSE
    {
        public Guid NURSE_ID { get; set; }
        public string  NURSE_NAME { get; set; }
        public Guid FLOOR_ID { get; set; }
        public string NURSE_ADDRESS { get; set; }
        public string NURSE_CONTACT { get; set; }

        public string FLOOR_NAME { get; set; }
    }
}
