﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IA.Model.Setup.Other
{
    public class PROFESSION
    {
        private string _COMPANY_ID;

        public string COMPANY_ID
        {
            get { return _COMPANY_ID; }
            set { _COMPANY_ID = value; }
        }
        private int _PROFESSION_ID;

        public int PROFESSION_ID
        {
            get { return _PROFESSION_ID; }
            set { _PROFESSION_ID = value; }
        }
        private string _PROFESSION_NAME;

        public string PROFESSION_NAME
        {
            get { return _PROFESSION_NAME; }
            set { _PROFESSION_NAME = value; }
        }


        private int _IS_ACTIVE;

        public int IS_ACTIVE
        {
            get { return _IS_ACTIVE; }
            set { _IS_ACTIVE = value; }
        }

        private string _STATUS;

        public string STATUS
        {
            get { return _STATUS; }
            set { _STATUS = value; }
        }

    }
}
