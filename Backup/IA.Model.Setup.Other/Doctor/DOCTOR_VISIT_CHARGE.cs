﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IA.Model.Setup.Other.Doctor
{
    public class DOCTOR_CHARGE
    {
        public Guid DOCTOR_CHARGE_ID { get; set; }
        public Guid DOCTOR_ID { get; set; }
        public decimal DOCTOR_NEW_VISIT_CHARGE { get; set; }
        public decimal DOCTOR_FOLLOW_UP_CHARGE { get; set; }
        public decimal DOCTOR_CALL_ON_CHARGE { get; set; }
        public string STATUS_FLAG { get; set; }
        public string DOCTOR_NAME { get; set; }
        

    }
}
