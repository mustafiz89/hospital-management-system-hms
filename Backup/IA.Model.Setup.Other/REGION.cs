﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IA.Model.Setup.Other
{
    public class REGION
    {
        private int _REGION_ID;

        public int REGION_ID
        {
            get { return _REGION_ID; }
            set { _REGION_ID = value; }
        }
        private string _REGION_NAME;

        public string REGION_NAME
        {
            get { return _REGION_NAME; }
            set { _REGION_NAME = value; }
        }
    }
}
