﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IA.Model.Setup.Other
{
   public class BANK
    {
        private string _ACCOUNTS_CODE;

        public string ACCOUNTS_CODE
        {
            get { return _ACCOUNTS_CODE; }
            set { _ACCOUNTS_CODE = value; }
        }

        private string _BANK_NAME;

        public string BANK_NAME
        {
            get { return _BANK_NAME; }
            set { _BANK_NAME = value; }
        }

        private string _BANK_ADDRESS;

        public string BANK_ADDRESS
        {
            get { return _BANK_ADDRESS; }
            set { _BANK_ADDRESS = value; }
        }

        private int _BANK_COUNTRY;

        public int BANK_COUNTRY
        {
            get { return _BANK_COUNTRY; }
            set { _BANK_COUNTRY = value; }
        }

        private string _BANK_FAX;

        public string BANK_FAX
        {
            get { return _BANK_FAX; }
            set { _BANK_FAX = value; }
        }

        private string _BANK_PHONE;

        public string BANK_PHONE
        {
            get { return _BANK_PHONE; }
            set { _BANK_PHONE = value; }
        }

        private string _BANK_EMAIL;

        public string BANK_EMAIL
        {
            get { return _BANK_EMAIL; }
            set { _BANK_EMAIL = value; }
        }

        private string _COMPANY_ID;

        public string COMPANY_ID
        {
            get { return _COMPANY_ID; }
            set { _COMPANY_ID = value; }
        }

        private int _IS_ACTIVE;

        public int IS_ACTIVE
        {
            get { return _IS_ACTIVE; }
            set { _IS_ACTIVE = value; }
        }
        public string BRANCH_ID { get; set; }

    }
}
