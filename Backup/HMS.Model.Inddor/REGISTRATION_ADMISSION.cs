﻿///*************************************************************************************
///	|| Creation History ||
///-------------------------------------------------------------------------------------
/// Copyright	  : Copyright© Dynamic Software Ltd. All rights reserved.
///	NameSpace	  :	HMS.HMS_Helper.Model.Indoor
/// Class Name    : REGISTRATION_ADMISSION
/// Inherits      : None
///	Author	      :	MD Billal 
///	Purpose	      :	This is a Model Class 
///                 
///	Creation Date :	1/11/2013
/// ====================================================================================
/// || Modification History ||
///  -----------------------------------------------------------------------------------
/// Sl No.	Date:		 Author:			Ver:	Area of Change:    Description:
/// 1       04/01/2014   Anik Islam         1.01    PATIENT_AGE        Before only store age with only year but this time will store age with year , month and day
/// 2       04/01/2014   Anik Islam         1.01    ROOM_NO            ROOM_NO Converted to BED_ID
/// 3       04/01/2014   Anik Islam         1.01    MARITAL_STATUS     Data Type of MARITAL_STATUS converted from int to string
/// 4       14/01/2014   Anik Islam         1.01    TOTAL_AMOUNT       newly Added in database
/// 4       14/01/2014   Anik Islam         1.01    TOTAL_DISCOUNT     newly Added in database 
/// 4       14/01/2014   Anik Islam         1.01    TOTAL_PAID         newly Added in database
///	 -----------------------------------------------------------------------------------
///	************************************************************************************


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HMS_Helper.Model.Indoor
{
    public class REGISTRATION_ADMISSION
    {

        public DateTime START_DATE { get; set; }
        public Guid PATIENT_SALUTION_ID { get; set; }
        public string PATIENT_NAME { get; set; }
        public string PATIENT_DOB { set; get; }
        public string PATIENT_AGE { get; set; }
        public string PATIENT_GENDER { get; set; }
        public string MARITAL_STATUS { get; set; }
        public string PATIENT_BLOOD_GROUP { get; set; }
        public string PATIENT_PHONE { get; set; }
        public string PATIENT_PRESENT_ADDRESS { get; set; }
        public string PATIENT_RELIGION { get; set; }
        public int CITY_ID { get; set; }
        public string PATIENT_NATIONALITY { get; set; }
        public int OCCUPATION_ID { get; set; }
        public string PATIENT_FATHER_NAME { get; set; }
        public string PATIENT_MOTHER_NAME { get; set; }
        public string RELATION_GURDIAN { get; set; }
        public string GURDIAN_NAME { get; set; }
        public string GURDIAN_PHONE { get; set; }
        
        
        public string PATIENT_PERMANENT_ADDRESS { get; set; }
        
        
        
        
        
        
        
        
        
        public int REFFERENCE_ID { get; set; }
        public int ADMITTED_UNDER_ID { get; set; }
        public int ADMITTED_DEPARTMENT_ID { get; set; }
        public int BED_ID { get; set; }
        
        public decimal PATIENT_BALANCE { set; get; }
        public decimal PATIENT_TOTAL_PAID { set; get; }

        public decimal TOTAL_AMOUNT { set; get; }
        public decimal TOTAL_DISCOUNT { set; get; }
        public decimal TOTAL_PAID { set; get; }
        public decimal ADMISSION_FEE { set; get; }
        // extra

        public string PATIENT_REGISTRATION_NO { set; get; }
        public string ADMISSION_NO { set; get; }
        public int ROOM_CHANGE_STATUS { set; get; }
        public int ADMISSION_STATUS { set; get; }

        public string PATHOLOGY_TEST_ITEM_NAME { get; set; }
    }
}
