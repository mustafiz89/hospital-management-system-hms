﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IA.Model.Setup.Accounts
{
    public class ACCOUNT_LEDGER
    {
        public string LEDGER_ID { get; set; }
        public string VOUCHER_NO { get; set; }
        public string PURCHASE_SALES_INVOICE_NO { get; set; }
        public string ACCOUNT_ID { get; set; }
        public int BUYER_SUPPLIER_ID { get; set; }
        public DateTime DATE { get; set; }
        public decimal LEDGER_DEBIT { get; set; }
        public decimal LEDGER_CREDIT { get; set; }
        public decimal NEW_AMOUNT { get; set; }
        public int COMPANY_ID { get; set; }
        public string MAKE_BY { get; set; }
        public DateTime MAKE_DATE { get; set; }
        public string NOTE { get; set; }
        public string SYSTEM_GENERATION_FLAG { get; set; }
        public string AUTHORIZE_STATUS { get; set; }
        private string _DESCRIPTION;

        public string DESCRIPTION
        {
            get { return _DESCRIPTION; }
            set { _DESCRIPTION = value; }
        }

    }
}
