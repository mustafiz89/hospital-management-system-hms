﻿using System;

namespace IA.Model.Setup.Accounts
{
    [Serializable]
    public class ACCOUNTS
    {
        #region

        private string _GROUP_CODE;

        public string GROUP_CODE
        {
            get { return _GROUP_CODE; }
            set { _GROUP_CODE = value; }
        }
        private string _CONTROL_CODE;

        public string CONTROL_CODE
        {
            get { return _CONTROL_CODE; }
            set { _CONTROL_CODE = value; }
        }
        private string _SUBSIDIARY_CODE;

        public string SUBSIDIARY_CODE
        {
            get { return _SUBSIDIARY_CODE; }
            set { _SUBSIDIARY_CODE = value; }
        }
        private string _ACCOUNTS_CODE;

        public string ACCOUNTS_CODE
        {
            get { return _ACCOUNTS_CODE; }
            set { _ACCOUNTS_CODE = value; }
        }
        private string _DESCRIPTION;

        public string DESCRIPTION
        {
            get { return _DESCRIPTION; }
            set { _DESCRIPTION = value; }
        }
        private DateTime _OPENING_DATE;

        public DateTime OPENING_DATE
        {
            get { return _OPENING_DATE; }
            set { _OPENING_DATE = value; }
        }
        private String _OPENING_TIME;

        public String OPENING_TIME
        {
            get { return _OPENING_TIME; }
            set { _OPENING_TIME = value; }
        }
        private decimal _OPENING_BALANCE;

        public decimal OPENING_BALANCE
        {
            get { return _OPENING_BALANCE; }
            set { _OPENING_BALANCE = value; }
        }
        private string _BALANCE_TYPE;

        public string BALANCE_TYPE
        {
            get { return _BALANCE_TYPE; }
            set { _BALANCE_TYPE = value; }
        }
        private string _STATEMENT_TYPE;

        public string STATEMENT_TYPE
        {
            get { return _STATEMENT_TYPE; }
            set { _STATEMENT_TYPE = value; }
        }
        private string _COMPANY_ID;

        public string COMPANY_ID
        {
            get { return _COMPANY_ID; }
            set { _COMPANY_ID = value; }
        }
        private string _NOTES;

        public string NOTES
        {
            get { return _NOTES; }
            set { _NOTES = value; }
        }
        private int _MANDATORY;

        public int MANDATORY
        {
            get { return _MANDATORY; }
            set { _MANDATORY = value; }
        }
        #endregion

        public string CONTROL_NAME { get; set; }

        public string ACCOUNT_GROUP_CODE { get; set; }

        public string ACCOUNT_GROUP_NAME { get; set; }

        public string SUBSIDIARY_NAME { get; set; }
        public decimal ACCOUNTS_BALANCE { get; set; }
        public int OPENING_BALANCE_FLAG { get; set; }
        public string MAKE_BY { get; set; }
        public string BRANCH_ID { get; set; }
    }
}
