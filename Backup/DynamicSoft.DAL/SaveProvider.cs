﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DynamicSoft.DAL
{
    public class SaveProvider
    {
        public static int getMaxID(string TABLE_NAME, string NAME_VALUE_LIST)
        {
            int i = 0;
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "GET_MEX_SERIAL_NO";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@TABLE_NAME", SqlDbType.VarChar, 50));
            objSqlCommand.Parameters["@TABLE_NAME"].Value = TABLE_NAME;
            objSqlCommand.Parameters.Add(new SqlParameter("@NAME_VALUE_LIST", SqlDbType.Char));
            objSqlCommand.Parameters["@NAME_VALUE_LIST"].Value = NAME_VALUE_LIST;
            objSqlCommand.Connection = conn;
            conn.Open();
            SqlDataReader dr = objSqlCommand.ExecuteReader();
            while(dr.Read())
            {
                i = Convert.ToInt32(dr["serial"]);
            }
            conn.Close();            
            return i;
        }
    }
}
