﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using DynamicSoft.Security.MODEL;
using DynamicSoft.DAL;
//using DynamicSoft.Utility;
using System.Net;
using Microsoft.Win32;
using System.Management;
using System.Net.Sockets;
using System.Security.AccessControl;
using System.Configuration;
using System.Windows.Forms;
using System.Web.Configuration;


namespace DynamicSoft.Security.BIZ
{
    public class SmsUserManagement
    {
        public static int saveUSER_PROFILE(USER_PROFILE objUSER_PROFILE)
        {
            int i = 0;
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_USER_PROFILE_I";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@USER_ID", SqlDbType.VarChar, 20));
            objSqlCommand.Parameters["@USER_ID"].Value = objUSER_PROFILE.USER_ID;
            objSqlCommand.Parameters.Add(new SqlParameter("@USER_NAME", SqlDbType.VarChar, 50));
            objSqlCommand.Parameters["@USER_NAME"].Value = objUSER_PROFILE.USER_NAME;
            objSqlCommand.Parameters.Add(new SqlParameter("@PASSWORD", SqlDbType.VarChar, 128));
            objSqlCommand.Parameters["@PASSWORD"].Value = objUSER_PROFILE.PASSWORD;
            objSqlCommand.Parameters.Add(new SqlParameter("@IS_ACTIVE", SqlDbType.Int));
            objSqlCommand.Parameters["@IS_ACTIVE"].Value = objUSER_PROFILE.IS_ACTIVE;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMMENT", SqlDbType.Text));
            objSqlCommand.Parameters["@COMMENT"].Value = objUSER_PROFILE.COMMENT;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.Int));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = objUSER_PROFILE.COMPANY_ID;
            objSqlCommand.Parameters.Add(new SqlParameter("@MAKE_BY", SqlDbType.VarChar, 20));
            objSqlCommand.Parameters["@MAKE_BY"].Value = objUSER_PROFILE.MAKE_BY;
            objSqlCommand.Parameters.Add(new SqlParameter("@NAME_VALUE_LIST", SqlDbType.Char, 1));
            objSqlCommand.Parameters["@NAME_VALUE_LIST"].Value = objUSER_PROFILE.NAME_VALUE_LIST;
            objSqlCommand.Parameters.Add(new SqlParameter("@BRANCH_ID", SqlDbType.Int));
            objSqlCommand.Parameters["@BRANCH_ID"].Value = objUSER_PROFILE.BRANCH_ID;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return i;
        }
        public static int saveUSER_MENU_INSERT(string USER_ID,string COMPANY_ID)
        {
            int i = 0;
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_USER_FIRST_MENU";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@USER_ID", SqlDbType.VarChar, 20));
            objSqlCommand.Parameters["@USER_ID"].Value = USER_ID;
            objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 10));
            objSqlCommand.Parameters["@COMPANY_ID"].Value = COMPANY_ID;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return i;
        }
        public static int SaveROLE(ROLE_FUNCTION objROLE_FUNCTION)
        {
            int i = 0;
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_SMS_ROLE_I";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@ROLE_NAME", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@ROLE_NAME"].Value = objROLE_FUNCTION.ROLE_NAME;
            objSqlCommand.Parameters.Add(new SqlParameter("@ROLE_DESCRIP", SqlDbType.VarChar, 150));
            objSqlCommand.Parameters["@ROLE_DESCRIP"].Value = objROLE_FUNCTION.ROLE_DESCRIP;
            objSqlCommand.Parameters.Add(new SqlParameter("@MAKE_BY1", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@MAKE_BY1"].Value = objROLE_FUNCTION.MAKE_BY;
            objSqlCommand.Parameters.Add(new SqlParameter("@NAME_VALUE_LIST", SqlDbType.Char));
            objSqlCommand.Parameters["@NAME_VALUE_LIST"].Value = objROLE_FUNCTION.NAME_VALUE_LIST;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return i;
        }
        public static int saveGRANT_ROLES(ROLE_FUNCTION objROLE_FUNCTION)
        {
            int i = 0;
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_SMS_GRANT_ROLES_I";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@ROLE_NAME", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@ROLE_NAME"].Value = objROLE_FUNCTION.ROLE_NAME;
            objSqlCommand.Parameters.Add(new SqlParameter("@USER_ID", SqlDbType.VarChar, 50));
            objSqlCommand.Parameters["@USER_ID"].Value = objROLE_FUNCTION.USER_ID;
            objSqlCommand.Parameters.Add(new SqlParameter("@MAKE_BY1", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@MAKE_BY1"].Value = objROLE_FUNCTION.MAKE_BY;
            objSqlCommand.Parameters.Add(new SqlParameter("@NAME_VALUE_LIST", SqlDbType.Char));
            objSqlCommand.Parameters["@NAME_VALUE_LIST"].Value = objROLE_FUNCTION.NAME_VALUE_LIST;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return i;
        }
        public static int saveROLE_FUNCTIONS(ROLE_FUNCTION objROLE_FUNCTION)
        {
            int i = 0;
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_SMS_ROLE_FUNCTIONS_I";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@ROLE_NAME", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@ROLE_NAME"].Value = objROLE_FUNCTION.ROLE_NAME;
            objSqlCommand.Parameters.Add(new SqlParameter("@FUNCTION_ID", SqlDbType.VarChar, 20));
            objSqlCommand.Parameters["@FUNCTION_ID"].Value = objROLE_FUNCTION.FUNCTION_ID;
            objSqlCommand.Parameters.Add(new SqlParameter("@MAKE_BY1", SqlDbType.VarChar, 30));
            objSqlCommand.Parameters["@MAKE_BY1"].Value = objROLE_FUNCTION.MAKE_BY;
            objSqlCommand.Parameters.Add(new SqlParameter("@NAME_VALUE_LIST", SqlDbType.Char));
            objSqlCommand.Parameters["@NAME_VALUE_LIST"].Value = objROLE_FUNCTION.NAME_VALUE_LIST;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return i;
        }
        public static List<ROLE_FUNCTION> GetRole()
        {
            List<ROLE_FUNCTION> objListROLE_FUNCTION = new List<ROLE_FUNCTION>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_SMS_ROLE_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            SqlDataReader DR = objSqlCommand.ExecuteReader();
            while (DR.Read())
            {
                ROLE_FUNCTION objROLE_FUNCTION = new ROLE_FUNCTION();
                objROLE_FUNCTION.ROLE_NAME = DR["ROLE_NAME"].ToString();
                objROLE_FUNCTION.ROLE_DESCRIP = DR["ROLE_DESCRIP"].ToString();
                objListROLE_FUNCTION.Add(objROLE_FUNCTION);
            }
            conn.Close();
            return objListROLE_FUNCTION;
        }
        public static USER_PROFILE USER_LOGIN(USER_PROFILE objUSER_PROFILE)
        {

            using (SqlConnection conn = ConnectionClass.GetConnection())
            {
                SqlCommand objSqlCommand = new SqlCommand();
                //objSqlCommand.CommandText = "FSP_USER_LOGIN";
                objSqlCommand.CommandText = "FSP_USER_LOGIN_NEW";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add(new SqlParameter("@USER_ID", SqlDbType.VarChar, 20));
                objSqlCommand.Parameters["@USER_ID"].Value = objUSER_PROFILE.USER_ID;
                objSqlCommand.Parameters.Add(new SqlParameter("@PASSWORD", SqlDbType.VarChar, 128));
                objSqlCommand.Parameters["@PASSWORD"].Value = objUSER_PROFILE.PASSWORD;
                objSqlCommand.Parameters.Add(new SqlParameter("@TERMINAL_IP_ADDRESS", SqlDbType.VarChar, 128));
                objSqlCommand.Parameters["@TERMINAL_IP_ADDRESS"].Value = objUSER_PROFILE.TERMINAL_IP_ADDRESS;
                objSqlCommand.Parameters.Add(new SqlParameter("@SESSION_ID", SqlDbType.VarChar, 128));
                objSqlCommand.Parameters["@SESSION_ID"].Value = objUSER_PROFILE.SESSION_ID;
                objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 128));
                objSqlCommand.Parameters["@COMPANY_ID"].Value = objUSER_PROFILE.COMPANY_ID;

                objSqlCommand.Connection = conn;
                conn.Open();
                SqlDataReader dr = objSqlCommand.ExecuteReader();
                objUSER_PROFILE.USER_ID = null;
                while (dr.Read())
                {
                    objUSER_PROFILE.USER_ID = dr["USER_ID"].ToString();
                    objUSER_PROFILE.TERMINAL_IP_ADDRESS = dr["TERMINAL_IP_ADDRESS"].ToString();
                    objUSER_PROFILE.PASSWORD = dr["PASSWORD"].ToString();
                    objUSER_PROFILE.COMPANY_ID = dr["COMPANY_ID"].ToString();
                    objUSER_PROFILE.COMPANY_NAME = dr["COMPANY_NAME"].ToString();
                    objUSER_PROFILE.ADDRESS = dr["ADDRESS"].ToString();
                    objUSER_PROFILE.BRANCH_ID = dr["BRANCH_ID"].ToString();
                    objUSER_PROFILE.BRANCH_NAME = dr["BRANCH_NAME"].ToString();
                    objUSER_PROFILE.HO_FLAG =Convert.ToInt16(dr["HO_FLAG"]);
                    objUSER_PROFILE.WARE_HOUSE_FLAG = Convert.ToInt16(dr["WARE_HOUSE_FLAG"]);
                    objUSER_PROFILE.MIDICINE_MODULE_FLAG = Convert.ToInt16(dr["MIDICINE_MODULE_FLAG"]);
                    objUSER_PROFILE.PRODUCTION_MODULE_FLAG = Convert.ToInt16(dr["PRODUCTION_MODULE_FLAG"]);
                    objUSER_PROFILE.WAREHOUSE_MODULE_FLAG = Convert.ToInt16(dr["WAREHOUSE_MODULE_FLAG"]);
                    
                    //DSSessionUtility.DSSessionContainer.USER_ID = dr["USER_ID"].ToString();
                    //DSSessionUtility.DSSessionContainer.COMPANY_ID = dr["COMPANY_ID"].ToString();
                }
                return objUSER_PROFILE;
            }
        }
        public static int USER_LOGOUT(USER_PROFILE objUSER_PROFILE)
        {
            int i = 0;
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_LOGOUT";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@USER_ID", SqlDbType.VarChar, 20));
            objSqlCommand.Parameters["@USER_ID"].Value = objUSER_PROFILE.USER_ID;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                i = objSqlCommand.ExecuteNonQuery();
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return i;
        }
        public static DateTime GETDATE()
        {
            DateTime currnet_date = DateTime.Now;
            using (SqlConnection conn = ConnectionClass.GetConnection())
            {
                SqlCommand objSqlCommand = new SqlCommand();
                objSqlCommand.CommandText = "FSP_GETDATE";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = conn;
                conn.Open();
                SqlDataReader dr = objSqlCommand.ExecuteReader();
                while (dr.Read())
                {
                    currnet_date = Convert.ToDateTime(dr["CURRENTDATE"]);
                }
                return currnet_date;
            }
        }
        public static DateTime GET_MAX_DATE(string COMPANY_ID)
        {
            DateTime currnet_date = DateTime.Now;
            using (SqlConnection conn = ConnectionClass.GetConnection())
            {
                SqlCommand objSqlCommand = new SqlCommand();
                objSqlCommand.CommandText = "FSP_MAX_LOGIN_TIME";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.NVarChar, 20));
                objSqlCommand.Parameters["@COMPANY_ID"].Value = COMPANY_ID;
                objSqlCommand.Connection = conn;
                conn.Open();
                SqlDataReader dr = objSqlCommand.ExecuteReader();
                while (dr.Read())
                {
                    currnet_date = Convert.ToDateTime(dr["MAX_LOGIN_TIME"]);
                }
                return currnet_date;
            }
        }
        public static int GetOnline_User(string company_id)
        {
            int online_user = 0;
            using (SqlConnection conn = ConnectionClass.GetConnection())
            {
                SqlCommand objSqlCommand = new SqlCommand();
                objSqlCommand.CommandText = "FSP_GET_ONLINE_USER_INFO";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 20));
                objSqlCommand.Parameters["@COMPANY_ID"].Value = company_id;
                objSqlCommand.Connection = conn;
                conn.Open();
                SqlDataReader dr = objSqlCommand.ExecuteReader();
                while (dr.Read())
                {
                    online_user = Convert.ToInt32(dr["ONLINE_USER"]);
                }
                return online_user;
            }
        }
        public static int GetMaxLoginInADay(string company_id, string USER_ID, DateTime currnet_date)
        {
            int online_user = 0;
            using (SqlConnection conn = ConnectionClass.GetConnection())
            {
                SqlCommand objSqlCommand = new SqlCommand();
                objSqlCommand.CommandText = "FSP_USER_LOGIN_COUNT";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add(new SqlParameter("@USER_ID", SqlDbType.VarChar, 30));
                objSqlCommand.Parameters["@USER_ID"].Value = USER_ID;
                objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 10));
                objSqlCommand.Parameters["@COMPANY_ID"].Value = company_id;
                objSqlCommand.Parameters.Add(new SqlParameter("@CURRENT_DATE", SqlDbType.DateTime));
                objSqlCommand.Parameters["@CURRENT_DATE"].Value = currnet_date;
                objSqlCommand.Parameters.Add(new SqlParameter("@MAX_LOGIN_FLAG_USERWISE", SqlDbType.Int));
                objSqlCommand.Parameters["@MAX_LOGIN_FLAG_USERWISE"].Value = company_id;
                objSqlCommand.Connection = conn;
                conn.Open();
                SqlDataReader dr = objSqlCommand.ExecuteReader();
                while (dr.Read())
                {
                    online_user = Convert.ToInt32(dr["LOGIN_COUNT"]);
                }
                return online_user;
            }
        }
        public static string GetIP()
        {
            //string Str = "";
            //Str = System.Net.Dns.GetHostName();
            //IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(Str);
            //IPAddress[] addr = ipEntry.AddressList;
            //return addr[addr.Length - 2].ToString();

            string ip = "";
            IPHostEntry ipEntry = Dns.GetHostEntry(GetCompCode());
            IPAddress[] addr = ipEntry.AddressList;
            ip = addr[2].ToString();
            return ip;
        }
        public static string GetCompCode()  // Get Computer Name
        {
            string strHostName = "";
            strHostName = Dns.GetHostName();
            return strHostName;
        }
        public static List<USER_PROFILE> USER_Active_List(string company_id)
        {
            List<USER_PROFILE> objListUSER_PROFILE = new List<USER_PROFILE>();
            using (SqlConnection conn = ConnectionClass.GetConnection())
            {
                SqlCommand objSqlCommand = new SqlCommand();
                objSqlCommand.CommandText = "FSP_User_Active_Session";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 20));
                objSqlCommand.Parameters["@COMPANY_ID"].Value = company_id;
                objSqlCommand.Connection = conn;
                conn.Open();
                SqlDataReader dr = objSqlCommand.ExecuteReader();
                while (dr.Read())
                {
                    USER_PROFILE objUSER_PROFILE = new USER_PROFILE();
                    objUSER_PROFILE.USER_ID = dr["USER_ID"].ToString();
                    objUSER_PROFILE.TERMINAL_IP_ADDRESS = dr["TERMINAL_IP_ADDRESS"].ToString();
                    objUSER_PROFILE.USER_NAME = dr["USER_NAME"].ToString();
                    objUSER_PROFILE.COMPANY_NAME = dr["COMPANY_NAME"].ToString();
                    objListUSER_PROFILE.Add(objUSER_PROFILE);
                }
                return objListUSER_PROFILE;
            }
        }
        public static string identifier(string wmiClass, string wmiProperty) //Return a hardware identifier       
        {
            string result = "";

            System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);
            System.Management.ManagementObjectCollection moc = mc.GetInstances();
            foreach (System.Management.ManagementObject mo in moc)
            {
                //Only get the first one
                if (result == "")
                {
                    try
                    {
                        result = mo[wmiProperty].ToString();
                        break;
                    }
                    catch
                    {
                    }
                }
            }
            return result;
        }
        public static string TestSqlConncetion()
        {
            string database_name = "";
            try
            {
                using (SqlConnection conn = ConnectionClass.GetConnectionMaster())
                {
                    SqlCommand objSqlCommand = new SqlCommand();
                    objSqlCommand.CommandText = "use Master; Select NAME from SYSDatabases where  name IN ('SHOP_MANAGEMENT','SHOP_MANAGEMENT_DEV')";
                    objSqlCommand.Connection = conn;
                    try
                    {
                        conn.Open();

                        SqlDataReader dr = objSqlCommand.ExecuteReader();
                        while (dr.Read())
                        {
                            database_name = dr["name"].ToString();
                        }
                    }
                    catch
                    {
                        try
                        {
                            string constr = "Password=123;User ID=sa;Initial Catalog=SHOP_MANAGEMENT;Data Source=" + System.Environment.MachineName.ToUpper() + "";
                            string constrMaster = "Password=123;User ID=sa;Initial Catalog=master;Data Source=" + System.Environment.MachineName.ToUpper() + "";
                            
                            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                            config.ConnectionStrings.ConnectionStrings["application"].ConnectionString = constr; //CONCATINATE YOUR FIELDS TOGETHER HERE
                            config.Save(ConfigurationSaveMode.Modified, true);
                            //ConfigurationManager.RefreshSection("connectionStrings");

                            config.ConnectionStrings.ConnectionStrings["applicationMaster"].ConnectionString = constrMaster; //CONCATINATE YOUR FIELDS TOGETHER HERE
                            config.Save(ConfigurationSaveMode.Modified, true);
                            //ConfigurationManager.RefreshSection("connectionStrings");

                            config.ConnectionStrings.ConnectionStrings["POS_SYSTEM.Properties.Settings.SHOP_MANAGEMENTConnectionString"].ConnectionString = constr; //CONCATINATE YOUR FIELDS TOGETHER HERE
                            config.Save(ConfigurationSaveMode.Modified, true);
                            ConfigurationManager.RefreshSection("connectionStrings");


                            System.Configuration.ConfigurationManager.AppSettings.Set("PC_NAME", System.Environment.MachineName.ToUpper());
                        }
                        catch { }
                    }
                    finally
                    {
                        conn.Close();
                        //var configuration = WebConfigurationManager.OpenWebConfiguration("C:\\Program Files\\Dynamic POS\\");
                        //var section = (ConnectionStringsSection)configuration.GetSection("connectionStrings");
                        //section.ConnectionStrings["application"].ConnectionString
                        //    = "Password=123;User ID=sa;Initial Catalog=SHOP_MANAGEMENT;Data Source=" + System.Environment.MachineName.ToUpper() + "";
                        //configuration.Save();
                        //section.ConnectionStrings["applicationMaster"].ConnectionString
                        //    = "Password=123;User ID=sa;Initial Catalog=master;Data Source=" + System.Environment.MachineName.ToUpper() + "";
                        //configuration.Save();
                    }                  
                }
            }
            catch 
            {
                string constr = "Password=123;User ID=sa;Initial Catalog=SHOP_MANAGEMENT;Data Source=" + System.Environment.MachineName.ToUpper() + "";
                string constrMaster = "Password=123;User ID=sa;Initial Catalog=master;Data Source=" + System.Environment.MachineName.ToUpper() + "";

                var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                config.ConnectionStrings.ConnectionStrings["application"].ConnectionString = constr; //CONCATINATE YOUR FIELDS TOGETHER HERE
                config.Save(ConfigurationSaveMode.Modified, true);
                ConfigurationManager.RefreshSection("connectionStrings");

                config.ConnectionStrings.ConnectionStrings["applicationMaster"].ConnectionString = constrMaster; //CONCATINATE YOUR FIELDS TOGETHER HERE
                config.Save(ConfigurationSaveMode.Modified, true);
                ConfigurationManager.RefreshSection("connectionStrings");

                config.ConnectionStrings.ConnectionStrings["POS_SYSTEM.Properties.Settings.SHOP_MANAGEMENTConnectionString"].ConnectionString = constr; //CONCATINATE YOUR FIELDS TOGETHER HERE
                config.Save(ConfigurationSaveMode.Modified, true);
                ConfigurationManager.RefreshSection("connectionStrings");


                System.Configuration.ConfigurationManager.AppSettings.Set("PC_NAME", System.Environment.MachineName.ToUpper());
                
            }
            return database_name;
        }
        public static string CheckLicense(string COMPANY_ID, string USER_ID)
        {
            string vMsg = "";
            string InstallPath = (string)Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\DAS", "DASKEY", null);
            //string InstallPath = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\DAS", "DASKEY", null);
            if (string.IsNullOrEmpty(InstallPath))
            {
                vMsg = "SQL:20000-Invaid License Key";
                return vMsg;
            }
            string[] arr1;
            try
            {
                arr1 = DSCryptorEngine.DecryptLicense(InstallPath, true).Split('|');
            }
            catch (Exception ex) { return "SQL:20000-" + ex.Message; }

            string MachineName = arr1[1];
            DateTime StartDate;
            try
            {
                StartDate = Convert.ToDateTime(arr1[2]);
            }
            catch
            {
                vMsg = "invalid StartDate";
                return vMsg;
            }
            DateTime EndDate;
            try
            {
                EndDate = Convert.ToDateTime(Convert.ToDateTime(arr1[3]).ToString("dd/MMM/yyyy"));
            }
            catch
            {
                vMsg = "invalid EndDate";
                return vMsg;
            }
            int MaxUser = Convert.ToInt32(arr1[4]);
            int MaxLoginInADay = Convert.ToInt32(arr1[5]);//0 for Unlimited login in a day
            int MaxLoginFlagUserWise = Convert.ToInt32(arr1[6]);//0=All userwise Max Login and 1=Single userwise Max Login
            DateTime currnet_date;
            try
            {
                currnet_date = Convert.ToDateTime(GETDATE().ToString("dd/MMM/yyyy"));
            }
            catch
            {
                vMsg = "invalid currnet_date";
                return vMsg;
            }
            int Online_user = GetOnline_User(COMPANY_ID);


            string modelNo = identifier("Win32_DiskDrive", "Model");
            //GetMaxLoginInADay
            //First Check
            if (MachineName.ToUpper() != System.Environment.MachineName.ToUpper())
            {
                vMsg = "SQL:20001-Invaid License Key";
            }
            if (StartDate > currnet_date)
            {
                vMsg = "SQL:20002-Invalid Current Date";
            }
            if (EndDate < currnet_date)
            {
                vMsg = "SQL:20003-Date Expired";
            }
            
            //if (Online_user > MaxUser)
            //{
            //    vMsg = "SQL:20004-Maximum User Limit Exceed";
            //}

            int MaxLoginInADay_Count = GetMaxLoginInADay(COMPANY_ID, USER_ID, currnet_date);
            if (MaxLoginInADay != 0)
            {
                if (MaxLoginInADay_Count != 0 && MaxLoginInADay_Count > MaxLoginInADay)
                {
                    vMsg = "SQL:20005-Maximum Login Limit Exceed In a Day";
                }
            }
            
            DateTime MAX_LOGIN_TIME = Convert.ToDateTime(GET_MAX_DATE(COMPANY_ID).ToString("dd/MMM/yyyy"));
            if (MAX_LOGIN_TIME > currnet_date)
            {
                vMsg = "SQL:20006-Invalid System Date";
            }
            
            string segnature = arr1[7].ToString();
            if (segnature != SmsUserManagement.identifier("Win32_DiskDrive", "Signature"))
            {
                vMsg = "SQL:20007-Invaid License Key";
                return vMsg;
            }

            //////////////////////////////////////////////////////////////////////////////////////
            string LICENSE_TYPE = "";
            try
            {
              LICENSE_TYPE= arr1[8].ToString();
            }
            catch { }


            using (SqlConnection conn = ConnectionClass.GetConnection())
            {
                int i = 0;
                int COLUMN_COUNT = 0;
                int COUNT = 0;
                SqlCommand objSqlCommand = new SqlCommand();
                objSqlCommand.CommandText = "FSP_USER_LOGIN_AL";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add(new SqlParameter("@START_DATE", SqlDbType.DateTime));
                objSqlCommand.Parameters["@START_DATE"].Value = StartDate;
                objSqlCommand.Connection = conn;
                conn.Open();
                try
                {
                    SqlDataReader DR = objSqlCommand.ExecuteReader();
                    while (DR.Read())
                    {
                        COLUMN_COUNT = Convert.ToInt16(DR["COLUMN_FIRST"]);
                        COUNT = Convert.ToInt16(DR["COUNT"]);
                    }
                }
                catch (Exception ex)
                {
                    //vMsg = "SQL:20008-Invaid License Key";
                    //return vMsg;
                }
                finally
                {
                    conn.Close();
                }
                if (LICENSE_TYPE == "D")
                {
                    if (COLUMN_COUNT != 2)
                    {
                        vMsg = "SQL:20009-Invaid License Key";
                        return vMsg;
                    }
                    if (COUNT > 0)
                    {
                        vMsg = "SQL:20010-Invaid License Key";
                        return vMsg;
                    }
                }
            }
            /////////////////////////////////////////////////////////////////////////////////////////
            
            return vMsg;
        }
        public static string createDemoLicense()
        {
            string startdate = DateTime.Now.ToString("MM/dd/yyyy");
            string enddate = DateTime.Now.AddDays(15).ToString("MM/dd/yyyy");////// CHANGE VALUE FOR DEMO DAYS
            string signature = SmsUserManagement.identifier("Win32_DiskDrive", "Signature");
            string PC_NAME = System.Environment.MachineName.ToUpper();
                      
            string value = "";
            string Key = "";
            string InstallPath = (string)Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\DAS", "DASKEY", null);
            //string InstallPath = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\DAS", "DASKEY", null);
            if (string.IsNullOrEmpty(InstallPath))
            {
                if (MessageBox.Show("No license found! Are you sure you want to use demo version for 15 days? All Your Present Data Will Erase(" + PC_NAME + " - " + signature + ")", "Confirm Reset", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    ///////////////
                    using (SqlConnection conn = ConnectionClass.GetConnection())
                    {
                        int i = 0;
                        SqlCommand objSqlCommand = new SqlCommand();
                        objSqlCommand.CommandText = "RESET_DATA";
                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                        objSqlCommand.Parameters.Add(new SqlParameter("@RESET_CODE", SqlDbType.VarChar, 40));
                        objSqlCommand.Parameters["@RESET_CODE"].Value = "billal";
                        objSqlCommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.VarChar, 40));
                        objSqlCommand.Parameters["@COMPANY_ID"].Value = "";
                        objSqlCommand.Parameters.Add(new SqlParameter("@BRANCH_ID", SqlDbType.VarChar, 40));
                        objSqlCommand.Parameters["@BRANCH_ID"].Value = "";
                        objSqlCommand.Connection = conn;
                        conn.Open();
                        try
                        {
                            i = objSqlCommand.ExecuteNonQuery();
                            Random rnd = new Random();
                            string month = rnd.Next(1, 9).ToString(); // creates a number between 1 and 12
                            string dice = rnd.Next(1, 9).ToString(); // creates a number between 1 and 6
                            string card = rnd.Next(9).ToString(); // creates a number between 0 and 9
                            string prefix = month + dice + card;
                            string licenseType = "D";
                            Key = prefix + "|" + PC_NAME + "|" + startdate + "|" + enddate + "|" + 1 + "|" + 0 + "|" + 0 + "|" + signature + "|" + licenseType;
                            //txtKey.Text = key;
                            Key = DSCryptorEngine.EncryptLicense(Key, true);
                            ///////////
                            string user = Environment.UserDomainName + "\\" + Environment.UserName;
                            RegistrySecurity rs = new RegistrySecurity();
                            // Allow the current user to read and delete the key.
                            rs.AddAccessRule(new RegistryAccessRule(user,
                                RegistryRights.ReadKey | RegistryRights.Delete | RegistryRights.WriteKey,
                                InheritanceFlags.None,
                                PropagationFlags.None,
                                AccessControlType.Allow));
                            string subKey = "SOFTWARE\\DAS";// +Application.ProductName.ToUpper();
                            RegistryKey baseRegistryKey = Registry.CurrentUser;//LocalMachine;
                            RegistryKey rk = baseRegistryKey;
                            RegistryKey sk1 = rk.OpenSubKey(subKey);
                            sk1 = rk.CreateSubKey(subKey);
                            sk1.SetValue("DASKEY", Key);

                        }
                        catch (Exception ex)
                        {
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                    using (SqlConnection conn = ConnectionClass.GetConnection())
                    {
                        int i = 0;
                        SqlCommand objSqlCommand = new SqlCommand();
                        objSqlCommand.CommandText = "FSP_USER_LOGIN_BL";
                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                        objSqlCommand.Parameters.Add(new SqlParameter("@NAME_VALUE_LIST", SqlDbType.Int));
                        objSqlCommand.Parameters["@NAME_VALUE_LIST"].Value = "0";
                        objSqlCommand.Connection = conn;
                        conn.Open();
                        try
                        {
                            i = objSqlCommand.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                }
                else
                {
                    value = "no";
                }
            }
            return value;
        }
    }
}