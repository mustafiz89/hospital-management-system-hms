﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynamicSoft.Security.MODEL
{
    public class USER_PROFILE
    {
        #region variable
        private string _USER_ID;
        private string _USER_NAME;
        private string _PASSWORD;
        private string _PASSWORD_FORMAT;
        private string _PASSWORD_SALT;
        private string _MOBILE_PIN;
        private string _EMAIL;
        private string _LOWERED_EMAIL;
        private string _PASSWORD_QUESTION;
        private string _PASSWORD_ANSWER;
        private string _IS_ACTIVE;
        private string _IS_LOCKED_OUT;
        private string _MAKE_DT;
        private string _LAST_LOGIN_DATE;
        private string _LAST_PASSWORD_CHANGED_DATE;
        private string _LAST_LOCKOUT_DATE;
        private string _FAILED_PASSWORD_ATTEMPT_COUNT;
        private string _FAILED_PASSWORD_ATTEMPT_WINDOW_START;
        private string _FAILED_PASSWORD_ANSWER_ATTEMPT_COUNT;
        private string _FAILED_PASSWORD_ANSWER_ATTEMPT_WINDOW_START;
        private string _TERMINAL_IP_ADDRESS;
        private string _APPLICATION_ID;
        private string _COMMENT;
        #endregion

        #region property
        public string USER_ID
        {
            get { return _USER_ID; }
            set { _USER_ID = value; }
        }

        public string USER_NAME
        {
            get { return _USER_NAME; }
            set { _USER_NAME = value; }
        }
        public string PASSWORD
        {
            get { return _PASSWORD; }
            set { _PASSWORD = value; }
        }

        public string PASSWORD_FORMAT
        {
            get { return _PASSWORD_FORMAT; }
            set { _PASSWORD_FORMAT = value; }
        }

        public string PASSWORD_SALT
        {
            get { return _PASSWORD_SALT; }
            set { _PASSWORD_SALT = value; }
        }

        public string MOBILE_PIN
        {
            get { return _MOBILE_PIN; }
            set { _MOBILE_PIN = value; }
        }

        public string EMAIL
        {
            get { return _EMAIL; }
            set { _EMAIL = value; }
        }

        public string LOWERED_EMAIL
        {
            get { return _LOWERED_EMAIL; }
            set { _LOWERED_EMAIL = value; }
        }

        public string PASSWORD_QUESTION
        {
            get { return _PASSWORD_QUESTION; }
            set { _PASSWORD_QUESTION = value; }
        }

        public string PASSWORD_ANSWER
        {
            get { return _PASSWORD_ANSWER; }
            set { _PASSWORD_ANSWER = value; }
        }

        public string IS_ACTIVE
        {
            get { return _IS_ACTIVE; }
            set { _IS_ACTIVE = value; }
        }

        public string IS_LOCKED_OUT
        {
            get { return _IS_LOCKED_OUT; }
            set { _IS_LOCKED_OUT = value; }
        }
        public string MAKE_DT
        {
            get { return _MAKE_DT; }
            set { _MAKE_DT = value; }
        }

        public string LAST_LOGIN_DATE
        {
            get { return _LAST_LOGIN_DATE; }
            set { _LAST_LOGIN_DATE = value; }
        }

        public string LAST_PASSWORD_CHANGED_DATE
        {
            get { return _LAST_PASSWORD_CHANGED_DATE; }
            set { _LAST_PASSWORD_CHANGED_DATE = value; }
        }

        public string LAST_LOCKOUT_DATE
        {
            get { return _LAST_LOCKOUT_DATE; }
            set { _LAST_LOCKOUT_DATE = value; }
        }

        public string FAILED_PASSWORD_ATTEMPT_COUNT
        {
            get { return _FAILED_PASSWORD_ATTEMPT_COUNT; }
            set { _FAILED_PASSWORD_ATTEMPT_COUNT = value; }
        }

        public string FAILED_PASSWORD_ATTEMPT_WINDOW_START
        {
            get { return _FAILED_PASSWORD_ATTEMPT_WINDOW_START; }
            set { _FAILED_PASSWORD_ATTEMPT_WINDOW_START = value; }
        }

        public string FAILED_PASSWORD_ANSWER_ATTEMPT_COUNT
        {
            get { return _FAILED_PASSWORD_ANSWER_ATTEMPT_COUNT; }
            set { _FAILED_PASSWORD_ANSWER_ATTEMPT_COUNT = value; }
        }

        public string FAILED_PASSWORD_ANSWER_ATTEMPT_WINDOW_START
        {
            get { return _FAILED_PASSWORD_ANSWER_ATTEMPT_WINDOW_START; }
            set { _FAILED_PASSWORD_ANSWER_ATTEMPT_WINDOW_START = value; }
        }

        public string TERMINAL_IP_ADDRESS
        {
            get { return _TERMINAL_IP_ADDRESS; }
            set { _TERMINAL_IP_ADDRESS = value; }
        }
        public string APPLICATION_ID
        {
            get { return _APPLICATION_ID; }
            set { _APPLICATION_ID = value; }
        }
        public string COMMENT
        {
            get { return _COMMENT; }
            set { _COMMENT = value; }
        }
        #endregion

        public string NAME_VALUE_LIST { get; set; }

        public string COMPANY_ID { get; set; }

        public string MAKE_BY { get; set; }

        public string SESSION_ID { get; set; }

        public string COMPANY_NAME { get; set; }
        public string BRANCH_ID { get; set; }
        public string BRANCH_NAME { get; set; }
        public string ADDRESS { get; set; }

        public int HO_FLAG { get; set; }
        public int WARE_HOUSE_FLAG { get; set; }


        public int MIDICINE_MODULE_FLAG { get; set; }
        public int PRODUCTION_MODULE_FLAG { get; set; }
        public int WAREHOUSE_MODULE_FLAG { get; set; }
    }
}
