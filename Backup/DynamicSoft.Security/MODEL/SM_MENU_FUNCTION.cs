﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DynamicSoft.Security.MODEL
{
    public class SM_MENU_FUNCTION
    {

        public string FUNCTION_NAME { get; set; }
        public string FUNCTION_BACK_NAME { get; set; }
        public string MENU_NAME { get; set; }
        public string MENU_BACK_NAME { get; set; }
        public int MENU_ID { get; set; }
        public int HAS_CHIELD { get; set; }
        public string USER_ID { get; set; }
        public bool ASSIGN_VALUE { get; set; }
    }
}
