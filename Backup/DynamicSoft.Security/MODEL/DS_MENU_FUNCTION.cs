﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynamicSoft.Security.MODEL
{
    public class DS_MENU_FUNCTION
    {
        #region variable
        private string _SERVICE_NM;
        private string _MODULE_NM;
        private string _FUNCTION_NM;
        private string _TARGET_PATH;
        private string _MENU_NM;
        private int _MENU_ID;
        private int _PARENT_MENU_ID;
        private int _MENU_LEVEL;
        private string _ITEM_TYPE;
        private string _FUNCTION_ID;
        private string _FAST_PATH_NO;
        public string USER_ID { get; set; }
        public string COMPANY_ID { get; set; }

        #endregion

        #region property

        public string SERVICE_NM
        {
            get { return _SERVICE_NM; }
            set { _SERVICE_NM = value; }
        }


        public string MODULE_NM
        {
            get { return _MODULE_NM; }
            set { _MODULE_NM = value; }
        }


        public string FUNCTION_NM
        {
            get { return _FUNCTION_NM; }
            set { _FUNCTION_NM = value; }
        }


        public string TARGET_PATH
        {
            get { return _TARGET_PATH; }
            set { _TARGET_PATH = value; }
        }


        public string MENU_NM
        {
            get { return _MENU_NM; }
            set { _MENU_NM = value; }
        }


        public int MENU_ID
        {
            get { return _MENU_ID; }
            set { _MENU_ID = value; }
        }


        public int PARENT_MENU_ID
        {
            get { return _PARENT_MENU_ID; }
            set { _PARENT_MENU_ID = value; }
        }


        public int MENU_LEVEL
        {
            get { return _MENU_LEVEL; }
            set { _MENU_LEVEL = value; }
        }


        public string ITEM_TYPE
        {
            get { return _ITEM_TYPE; }
            set { _ITEM_TYPE = value; }
        }


        public string FUNCTION_ID
        {
            get { return _FUNCTION_ID; }
            set { _FUNCTION_ID = value; }
        }


        public string FAST_PATH_NO
        {
            get { return _FAST_PATH_NO; }
            set { _FAST_PATH_NO = value; }
        }
        #endregion



        public int FUNCTION_ASSIGN_FLAG { get; set; }


        //public string FUNCTION_NAME { get; set; }
        //public string FUNCTION_BACK_NAME { get; set; }
        //public string MENU_NAME { get; set; }
        //public string MENU_BACK_NAME { get; set; }
        //public int MENU_ID { get; set; }
        //public int HAS_CHIELD { get; set; }
        //public string USER_ID { get; set; }
        //public int ASSIGN_VALUE { get; set; }
        
    }
}
