﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynamicSoft.Security.MODEL
{
    public class ROLE_FUNCTION
    {
        private string _ROLE_NAME;
        private string _ROLE_DESCRIP;
        private string _USER_ID;
        private string _FUNCTION_ID;
        private string _MAKE_BY;   
        private string _NAME_VALUE_LIST;


        public string ROLE_NAME
        {
            get { return _ROLE_NAME; }
            set { _ROLE_NAME = value; }
        }
        public string ROLE_DESCRIP
        {
            get { return _ROLE_DESCRIP; }
            set { _ROLE_DESCRIP = value; }
        }
        public string USER_ID
        {
            get { return _USER_ID; }
            set { _USER_ID = value; }
        }
        public string FUNCTION_ID
        {
            get { return _FUNCTION_ID; }
            set { _FUNCTION_ID = value; }
        }
        public string MAKE_BY
        {
            get { return _MAKE_BY; }
            set { _MAKE_BY = value; }
        }
        public string NAME_VALUE_LIST
        {
            get { return _NAME_VALUE_LIST; }
            set { _NAME_VALUE_LIST = value; }
        }


        public string FAST_PATH_NO { get; set; }

        public string FUNCTION_NM { get; set; }

        public bool CHECK { get; set; }
    }
}
