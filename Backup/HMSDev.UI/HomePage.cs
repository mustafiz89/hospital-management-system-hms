﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HMSDev.UI.Setup.Common_Setup;
using HMSDev.UI.Setup.Indoor;
using HMSDev.UI.Setup.Outdoor;
using HMSDev.UI.Setup.Pathology;
using HMSDev.UI.Setup.OT_Setup;
using HMSDev.UI.Setup;
using HMSDev.UI.Setup.Doctor;
using HMSDev.UI.Setup.Naurse;
using HMSDev.UI.Setup.Other;

namespace HMSDev.UI
{
    public partial class HomePage : Form
    {
        public HomePage()
        {
            InitializeComponent();
        }

        private void controlAccountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IA.UI.Setup.Accounts.AccountControl obj = new IA.UI.Setup.Accounts.AccountControl("");
            obj.MdiParent = this;
            obj.Show();
        }

        private void subsidiaryAccountsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IA.UI.Setup.Accounts.SubsidiaryAccounts obj = new IA.UI.Setup.Accounts.SubsidiaryAccounts("");
            obj.MdiParent = this;
            obj.Show();
        }

        private void accountsSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IA.UI.Setup.Accounts.Accounts obj = new IA.UI.Setup.Accounts.Accounts("");
            obj.MdiParent = this;
            obj.Show();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void HomePage_Load(object sender, EventArgs e)
        {

        }

        private void inventorySetupToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void catageruToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void inventoryProductPurchageToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void oTServicesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cancelCollectionToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void doctorAdviceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Doctor_Advice_Text obj = new Doctor_Advice_Text();
            obj.MdiParent = this;
            obj.Show();
        }

        private void nurseSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {

        }

        private void inventoryProductPurchageReportToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void setupToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void indoorOTReportToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void bedChargeToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void indoorPaiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HMSDev.UI.Indoor_Patient_Addmission ipa = new Indoor_Patient_Addmission();
            //if(ipa.IsDisposed)
            //{
            //    ipa = new Indoor_Patient_Addmission();
            //}
            ipa.MdiParent = this;
            ipa.BringToFront();
            ipa.Show();

        }

        private void indoorPatientDischargeCollectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HMSDev.UI.Indoor_Patient_Discharge ipd = new Indoor_Patient_Discharge();
            ipd.Show();
            ipd.MdiParent = this;
            ipd.BringToFront();
        }

        private void outdoorOTServicesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HMSDev.UI.Outdoor_OT_Service oos = new Outdoor_OT_Service();
            oos.Show();
            oos.MdiParent = this;
            oos.BringToFront();
        }

        private void indoorOTServicesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HMSDev.UI.Indoor_OT_Service ios = new Indoor_OT_Service();
            ios.Show();
            ios.MdiParent = this;
            ios.BringToFront();
        }

        private void outdoorPatientAddmissionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HMSDev.UI.Outdoor_Patient_Registration opr = new Outdoor_Patient_Registration();
            opr.Show();
            opr.MdiParent = this;
            opr.BringToFront();
        }

        private void outdoorPatientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HMSDev.UI.Outdoor_Patient_Ticket opt = new Outdoor_Patient_Ticket();
            opt.Show();
            opt.MdiParent = this;
            opt.BringToFront();
        }

        private void indoorPatientBedChangeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HMSDev.UI.Indoor_Patient_Bed_Room_Transfer ipbrt = new Indoor_Patient_Bed_Room_Transfer();
            ipbrt.Show();
            ipbrt.MdiParent = this;
            ipbrt.BringToFront();
        }

        private void indoorPatientDoctorChangeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HMSDev.UI.Indoor_Patient_Doctor_Transfer ipdt = new Indoor_Patient_Doctor_Transfer();
            ipdt.Show();
            ipdt.MdiParent = this;
            ipdt.BringToFront();
        }

        private void addPathologyInvestigationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HMSDev.UI.Add_Pathology_Investigation api = new Add_Pathology_Investigation();
            api.Show();
            api.MdiParent = this;
            api.BringToFront();

        }

        private void pathologyTestResultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HMSDev.UI.Pathology_Test_Result ptr = new Pathology_Test_Result();
            ptr.Show();
            ptr.MdiParent = this;
            ptr.BringToFront();
        }

        private void pathologyTestResultEditToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HMSDev.UI.Pathology_Test_Result_Edit ptre = new Pathology_Test_Result_Edit();
            ptre.Show();
            ptre.MdiParent = this;
            ptre.BringToFront();
        }

        private void oToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void inventoryProductPurchageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HMSDev.UI.Inventory.Inventory_Product_Purchase ipp = new HMSDev.UI.Inventory.Inventory_Product_Purchase();
            ipp.Show();
            ipp.MdiParent = this;
            ipp.BringToFront();
        }

        private void doctorChargeToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void indoorOTCategoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OT__Category obj = new OT__Category();
            obj.MdiParent = this;
            obj.Show();
        }

        private void doctorChargeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Doctor_Visit_Charge_Setup obj = new Doctor_Visit_Charge_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void floorSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Floor_Setup obj = new Floor_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void cancelToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {

        }

        private void profitLossToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void advanceCollectionReportToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void oTReportsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void outdoorOTReportsToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void inventoryStockReportToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void hRToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void outdoorPatientReportToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void totalExpenseReportToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void individualPaymentReportToolStripMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem17_Click(object sender, EventArgs e)
        {
            Blood_Group_Setup obj = new Blood_Group_Setup();
            obj.MdiParent = this;
            obj.Show();

        }

        private void toolStripMenuItem15_Click(object sender, EventArgs e)
        {
            Nationality_Setup obj = new Nationality_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void toolStripMenuItem16_Click(object sender, EventArgs e)
        {
            Gender_Setup obj = new Gender_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void toolStripMenuItem18_Click(object sender, EventArgs e)
        {
            Occupation_Setup obj = new Occupation_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void toolStripMenuItem19_Click(object sender, EventArgs e)
        {
            Religion_Setup obj = new Religion_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void toolStripMenuItem20_Click(object sender, EventArgs e)
        {
            Salution_Setup obj = new Salution_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void toolStripMenuItem21_Click(object sender, EventArgs e)
        {
            Marital_Status_Setup obj = new Marital_Status_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void admissionChargeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Addmission_Charge obj = new Addmission_Charge();
            obj.MdiParent = this;
            obj.Show();
        }

        private void roomToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Room_Setup obj = new Room_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void bedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bed_Setup obj = new Bed_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void roomCategoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Room_Category_Setup obj = new Room_Category_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void roomBedChargeCountDownHourSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Room_and_Bed_Charge_Count_Down_Hour_Setup obj = new Room_and_Bed_Charge_Count_Down_Hour_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void patientDepartmentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Patient_Department_Setup obj = new Patient_Department_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void diseaseSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Disease_Setup obj = new Disease_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void corporateClientSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Corporate_Client_Setup obj = new Corporate_Client_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void doctorChargeToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Doctor_Charge_Setup obj = new Doctor_Charge_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void referenceCategoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reference_Category_Setup obj = new Reference_Category_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void referenceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reference_Person_Info_Setup obj = new Reference_Person_Info_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void pathologyTestItemCategoryInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Pathology_Test_Item_Category_Setup obj = new Pathology_Test_Item_Category_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void pathologyTestItemSubCategoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Pathology_Test_Item_Sub_Category_Setup obj = new Pathology_Test_Item_Sub_Category_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void pathologySpecimenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Pathology_Speciman_Setup obj = new Pathology_Speciman_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void pathologyItemPriceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Pathology_Item_Price_Setup obj = new Pathology_Item_Price_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void pathologyTestCommentsSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Pathology_Test_Comments_Setup obj = new Pathology_Test_Comments_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void oTSetupToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            OT_Setup obj = new OT_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void oTNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OT_Charge_Setup obj = new OT_Charge_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void oTNoteTemplateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OT_Comments_Setup obj = new OT_Comments_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void doctorNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Indoor_Doctor_Setup obj = new Indoor_Doctor_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void outdoorDoctorSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Outdoor_Doctor_Setup obj = new Outdoor_Doctor_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void doctorSpecializationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Doctor_Specialization obj = new Doctor_Specialization();
            obj.MdiParent = this;
            obj.Show();
        }

        private void doctorTypeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Doctor_Type obj = new Doctor_Type();
            obj.MdiParent = this;
            obj.Show();
        }

        private void medicineDosesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Medicine_Doses obj = new Medicine_Doses();
            obj.MdiParent = this;
            obj.Show();
        }

        private void typeOfVisitSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Doctor_Type_of_Visit obj = new Doctor_Type_of_Visit();
            obj.MdiParent = this;
            obj.Show();
        }

        private void nurseSetupToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Nurse_Setup obj = new Nurse_Setup();
            obj.MdiParent = this;
            obj.Show();
        }

        private void serviceNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Service_Name_Or_Charge_Setup obj = new Service_Name_Or_Charge_Setup();
            obj.MdiParent = this;
            obj.Show();
        }
    }
}
