﻿namespace HMSDev.UI
{
    partial class HomePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.setupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.controlAccountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subsidiaryAccountsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountsSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indoorSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.admissionChargeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.roomAndBedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.roomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.roomCategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.roomBedChargeCountDownHourSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.patientDepartmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.diseaseSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.corporateClientSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.floorSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outdoorSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doctorChargeToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologySetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.referenceCategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.referenceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologyTestItemCategoryInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologyTestItemSubCategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologySpecimenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologyItemPriceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologyTestCommentsSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oTSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indoorOTCategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oTSetupToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.oTNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oTNoteTemplateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventorySetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.catageruToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subCategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.brandToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unitTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventorySupplierSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doctorSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doctorNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outdoorDoctorSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doctorSpecializationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doctorTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doctorChargeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.doctorAdviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.medicineDosesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.typeOfVisitSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nurseSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nurseSetupToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.otherServicesSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.serviceNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem17 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem18 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem19 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem20 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem21 = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.companyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bankToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.taskToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outdoorDoctorPaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indoorDoctorPaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refaralDoctorPaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologyReferelCommissionPaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.collectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indoorPatientCollectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outdoorPatientCollectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologyPatientCollectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oTCollectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outdoorOTCollectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherServicesCollectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.corporateClientCollectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.voucherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.voucherPostingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.voucherCancelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryProductPurchageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryPStockOutUseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productPurchaseReturnToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.laundryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indoorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indoorPaiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indoorPatientDischargeCollectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indoorPatientBedChangeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indoorPatientDoctorChangeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indoorOTServicesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.patientDemographicInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outdoorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outdoorPatientAddmissionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outdoorPatientToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outdoorOTServicesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addPathologyInvestigationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologyTestResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologyTestResultEditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologySampleCollectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.radiologyAndImagingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.electronicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doctorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doctorWorkStationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doctorPrescriptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doctorRoasterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doctorAppointmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.followUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.referForAdmissionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nurseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nurseWorkStationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nurseRoasterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherServicesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addOtherServiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indoorCancelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelIndoorPatientAddmissionToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelIndoorPatientDischargeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelIndoorOTServicesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelIndoorOTCollectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelIndoorPatientCollectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelIndoorDoctorCommissionPaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outdoorCancelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelOutdoorOTServicesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelOutdoorPatientRegistrationToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelOutdoorPatientTicketToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelOutdoorOTCollectionToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelOutdoorPatientCollectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelOutdoorDoctorCommissionPaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologyCancelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelPathologyTestResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelPathologyCollectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelPathologyCommissionPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryCancelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelInventoryProductPurchageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelInventoryPStockOutUseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelInventorySupplierPaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherServiceCancelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelAddOtherServiceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelOtherServicesCollectionToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelVoucherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indoorReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.advanceCollectionReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.individualPaymentReportToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.individualCollectionReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.individualPatientReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indoorPatientDischargeListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.patientIDCardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.patientListReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indoorOTReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indoorPatientCollectionReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indoorOTCollectionReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indoorDoctorLedgerReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indoorDoctorPaymentReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refaralDoctorComissionPaymentReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.referalDoctorLedgerReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.deToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.corporateClientLedgerReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.loginWToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loginWiseAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allPatientBloodGroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.patientClinicalHistoryAndVisitHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.automaticPathologyReportGenerationFromAnalyserMachineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outdoorReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outdoorPatientListReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outdoorDoctorLedgerReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.outdoorIDCardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outdoorOTReportsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.outdoorDoctorPaymentReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outdoorPatientCollectionReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outdoorOTCollectionReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.individualPatientReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.individualPatientLedgerReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.individualCollectionReportToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.individualPaymentReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.refaralDoctorComissionPaymentReportsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.referalDoctorLedgerReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.advanceCollectionReportToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.allPatientBloodGroupToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologyReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologyServiceSalesReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologyCatagoryListReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologySubCatagoryListReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologyTestResultReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologyDeliveryReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologyUndeliveryReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologyInvestigationChargeReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.individualInvoiceReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologyCollectionReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.individualCollectionReportToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.advanceCollectionReportToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.refaralDoctorComissionPaymentReportsToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologyReferelCommissionPaymentReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.individualPaymentReportToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.referalDoctorLedgerReportToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryProductPurchageReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryPStockOutUseReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryProductPurchageReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.inventoryStockReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventorySupplierLedgerReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.individualPaymentReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.individualPatientLedgerReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.individualCollectionReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outstandingReprotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indoorPatientReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outdoorPatientReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pathologyOutstandingReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherServiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.medicineReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indoorOTReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outdoorOTReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.corporateClientOutstandingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountsReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userOrDeskWiseCollectionReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hospitalTotalCollectionReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.totalExpenseReportToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.profitLossToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.patientDiscountReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.totalPaymentReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.patientManagementExpensesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherServicesReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherServiceSellReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherServicesCollectionReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doctorReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doctorListReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doctorCommissionOutstandingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doctorWisePatientListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nurseReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancelReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.certificateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.birthCertificateReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deathCertificateReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dischargeCertificateReprotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hRReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indivisdualSalaryReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hRSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeDepartmentSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeGradeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeDesignationSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeReligionSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employCategorySetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeEducationSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeStatusSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeEdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeSalaryIncrementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.promotionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeSalaryInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeSalaryGenerateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeSalaryGenerateCancelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeActiveOrInactiveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.procurementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.userManagementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changePasswordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.securityAssignToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.othersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.databaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supportCenterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.licenceUpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.babyBornInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // setupToolStripMenuItem
            // 
            this.setupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accountsToolStripMenuItem,
            this.indoorSetupToolStripMenuItem,
            this.outdoorSetupToolStripMenuItem,
            this.pathologySetupToolStripMenuItem,
            this.oTSetupToolStripMenuItem,
            this.inventorySetupToolStripMenuItem,
            this.doctorSetupToolStripMenuItem,
            this.nurseSetupToolStripMenuItem,
            this.otherServicesSetupToolStripMenuItem,
            this.toolStripMenuItem14,
            this.settingsToolStripMenuItem});
            this.setupToolStripMenuItem.Name = "setupToolStripMenuItem";
            this.setupToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.setupToolStripMenuItem.Text = "Setup";
            this.setupToolStripMenuItem.Click += new System.EventHandler(this.setupToolStripMenuItem_Click);
            // 
            // accountsToolStripMenuItem
            // 
            this.accountsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.controlAccountToolStripMenuItem,
            this.subsidiaryAccountsToolStripMenuItem,
            this.accountsSetupToolStripMenuItem});
            this.accountsToolStripMenuItem.Name = "accountsToolStripMenuItem";
            this.accountsToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.accountsToolStripMenuItem.Text = "Accounts";
            // 
            // controlAccountToolStripMenuItem
            // 
            this.controlAccountToolStripMenuItem.Name = "controlAccountToolStripMenuItem";
            this.controlAccountToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.controlAccountToolStripMenuItem.Text = "Control Account";
            this.controlAccountToolStripMenuItem.Click += new System.EventHandler(this.controlAccountToolStripMenuItem_Click);
            // 
            // subsidiaryAccountsToolStripMenuItem
            // 
            this.subsidiaryAccountsToolStripMenuItem.Name = "subsidiaryAccountsToolStripMenuItem";
            this.subsidiaryAccountsToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.subsidiaryAccountsToolStripMenuItem.Text = "Subsidiary Accounts";
            this.subsidiaryAccountsToolStripMenuItem.Click += new System.EventHandler(this.subsidiaryAccountsToolStripMenuItem_Click);
            // 
            // accountsSetupToolStripMenuItem
            // 
            this.accountsSetupToolStripMenuItem.Name = "accountsSetupToolStripMenuItem";
            this.accountsSetupToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.accountsSetupToolStripMenuItem.Text = "Accounts Setup";
            this.accountsSetupToolStripMenuItem.Click += new System.EventHandler(this.accountsSetupToolStripMenuItem_Click);
            // 
            // indoorSetupToolStripMenuItem
            // 
            this.indoorSetupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.admissionChargeToolStripMenuItem,
            this.roomAndBedToolStripMenuItem,
            this.patientDepartmentToolStripMenuItem,
            this.diseaseSetupToolStripMenuItem,
            this.corporateClientSetupToolStripMenuItem,
            this.floorSetupToolStripMenuItem});
            this.indoorSetupToolStripMenuItem.Name = "indoorSetupToolStripMenuItem";
            this.indoorSetupToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.indoorSetupToolStripMenuItem.Text = "Indoor Setup";
            // 
            // admissionChargeToolStripMenuItem
            // 
            this.admissionChargeToolStripMenuItem.Name = "admissionChargeToolStripMenuItem";
            this.admissionChargeToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.admissionChargeToolStripMenuItem.Text = "Admission Charge Setup";
            this.admissionChargeToolStripMenuItem.Click += new System.EventHandler(this.admissionChargeToolStripMenuItem_Click);
            // 
            // roomAndBedToolStripMenuItem
            // 
            this.roomAndBedToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.roomToolStripMenuItem,
            this.bedToolStripMenuItem,
            this.roomCategoryToolStripMenuItem,
            this.roomBedChargeCountDownHourSetupToolStripMenuItem});
            this.roomAndBedToolStripMenuItem.Name = "roomAndBedToolStripMenuItem";
            this.roomAndBedToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.roomAndBedToolStripMenuItem.Text = "Room And Bed Setup";
            // 
            // roomToolStripMenuItem
            // 
            this.roomToolStripMenuItem.Name = "roomToolStripMenuItem";
            this.roomToolStripMenuItem.Size = new System.Drawing.Size(317, 22);
            this.roomToolStripMenuItem.Text = "Room Setup";
            this.roomToolStripMenuItem.Click += new System.EventHandler(this.roomToolStripMenuItem_Click);
            // 
            // bedToolStripMenuItem
            // 
            this.bedToolStripMenuItem.Name = "bedToolStripMenuItem";
            this.bedToolStripMenuItem.Size = new System.Drawing.Size(317, 22);
            this.bedToolStripMenuItem.Text = "Bed Setup";
            this.bedToolStripMenuItem.Click += new System.EventHandler(this.bedToolStripMenuItem_Click);
            // 
            // roomCategoryToolStripMenuItem
            // 
            this.roomCategoryToolStripMenuItem.Name = "roomCategoryToolStripMenuItem";
            this.roomCategoryToolStripMenuItem.Size = new System.Drawing.Size(317, 22);
            this.roomCategoryToolStripMenuItem.Text = "Room/Bed Category Setup";
            this.roomCategoryToolStripMenuItem.Click += new System.EventHandler(this.roomCategoryToolStripMenuItem_Click);
            // 
            // roomBedChargeCountDownHourSetupToolStripMenuItem
            // 
            this.roomBedChargeCountDownHourSetupToolStripMenuItem.Name = "roomBedChargeCountDownHourSetupToolStripMenuItem";
            this.roomBedChargeCountDownHourSetupToolStripMenuItem.Size = new System.Drawing.Size(317, 22);
            this.roomBedChargeCountDownHourSetupToolStripMenuItem.Text = "Room/Bed Charge Count-Down Hour Setup";
            this.roomBedChargeCountDownHourSetupToolStripMenuItem.Click += new System.EventHandler(this.roomBedChargeCountDownHourSetupToolStripMenuItem_Click);
            // 
            // patientDepartmentToolStripMenuItem
            // 
            this.patientDepartmentToolStripMenuItem.Name = "patientDepartmentToolStripMenuItem";
            this.patientDepartmentToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.patientDepartmentToolStripMenuItem.Text = "Patient Department Setup ";
            this.patientDepartmentToolStripMenuItem.Click += new System.EventHandler(this.patientDepartmentToolStripMenuItem_Click);
            // 
            // diseaseSetupToolStripMenuItem
            // 
            this.diseaseSetupToolStripMenuItem.Name = "diseaseSetupToolStripMenuItem";
            this.diseaseSetupToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.diseaseSetupToolStripMenuItem.Text = "Disease Setup";
            this.diseaseSetupToolStripMenuItem.Click += new System.EventHandler(this.diseaseSetupToolStripMenuItem_Click);
            // 
            // corporateClientSetupToolStripMenuItem
            // 
            this.corporateClientSetupToolStripMenuItem.Name = "corporateClientSetupToolStripMenuItem";
            this.corporateClientSetupToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.corporateClientSetupToolStripMenuItem.Text = "Corporate Client Setup";
            this.corporateClientSetupToolStripMenuItem.Click += new System.EventHandler(this.corporateClientSetupToolStripMenuItem_Click);
            // 
            // floorSetupToolStripMenuItem
            // 
            this.floorSetupToolStripMenuItem.Name = "floorSetupToolStripMenuItem";
            this.floorSetupToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.floorSetupToolStripMenuItem.Text = "Floor Setup";
            this.floorSetupToolStripMenuItem.Click += new System.EventHandler(this.floorSetupToolStripMenuItem_Click);
            // 
            // outdoorSetupToolStripMenuItem
            // 
            this.outdoorSetupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.doctorChargeToolStripMenuItem2});
            this.outdoorSetupToolStripMenuItem.Name = "outdoorSetupToolStripMenuItem";
            this.outdoorSetupToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.outdoorSetupToolStripMenuItem.Text = "Outdoor Setup";
            // 
            // doctorChargeToolStripMenuItem2
            // 
            this.doctorChargeToolStripMenuItem2.Name = "doctorChargeToolStripMenuItem2";
            this.doctorChargeToolStripMenuItem2.Size = new System.Drawing.Size(191, 22);
            this.doctorChargeToolStripMenuItem2.Text = "Doctor Charge Setup";
            this.doctorChargeToolStripMenuItem2.Click += new System.EventHandler(this.doctorChargeToolStripMenuItem2_Click);
            // 
            // pathologySetupToolStripMenuItem
            // 
            this.pathologySetupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.referenceCategoryToolStripMenuItem,
            this.referenceToolStripMenuItem,
            this.pathologyTestItemCategoryInfoToolStripMenuItem,
            this.pathologyTestItemSubCategoryToolStripMenuItem,
            this.pathologySpecimenToolStripMenuItem,
            this.pathologyItemPriceToolStripMenuItem,
            this.pathologyTestCommentsSetupToolStripMenuItem});
            this.pathologySetupToolStripMenuItem.Name = "pathologySetupToolStripMenuItem";
            this.pathologySetupToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.pathologySetupToolStripMenuItem.Text = "Pathology Setup";
            // 
            // referenceCategoryToolStripMenuItem
            // 
            this.referenceCategoryToolStripMenuItem.Name = "referenceCategoryToolStripMenuItem";
            this.referenceCategoryToolStripMenuItem.Size = new System.Drawing.Size(299, 22);
            this.referenceCategoryToolStripMenuItem.Text = "Pathology Reference Category Setup ";
            this.referenceCategoryToolStripMenuItem.Click += new System.EventHandler(this.referenceCategoryToolStripMenuItem_Click);
            // 
            // referenceToolStripMenuItem
            // 
            this.referenceToolStripMenuItem.Name = "referenceToolStripMenuItem";
            this.referenceToolStripMenuItem.Size = new System.Drawing.Size(299, 22);
            this.referenceToolStripMenuItem.Text = "Pathology Reference Person Info Setup";
            this.referenceToolStripMenuItem.Click += new System.EventHandler(this.referenceToolStripMenuItem_Click);
            // 
            // pathologyTestItemCategoryInfoToolStripMenuItem
            // 
            this.pathologyTestItemCategoryInfoToolStripMenuItem.Name = "pathologyTestItemCategoryInfoToolStripMenuItem";
            this.pathologyTestItemCategoryInfoToolStripMenuItem.Size = new System.Drawing.Size(299, 22);
            this.pathologyTestItemCategoryInfoToolStripMenuItem.Text = "Pathology Test Item Category Setup";
            this.pathologyTestItemCategoryInfoToolStripMenuItem.Click += new System.EventHandler(this.pathologyTestItemCategoryInfoToolStripMenuItem_Click);
            // 
            // pathologyTestItemSubCategoryToolStripMenuItem
            // 
            this.pathologyTestItemSubCategoryToolStripMenuItem.Name = "pathologyTestItemSubCategoryToolStripMenuItem";
            this.pathologyTestItemSubCategoryToolStripMenuItem.Size = new System.Drawing.Size(299, 22);
            this.pathologyTestItemSubCategoryToolStripMenuItem.Text = "Pathology Test Item Sub Category Setup";
            this.pathologyTestItemSubCategoryToolStripMenuItem.Click += new System.EventHandler(this.pathologyTestItemSubCategoryToolStripMenuItem_Click);
            // 
            // pathologySpecimenToolStripMenuItem
            // 
            this.pathologySpecimenToolStripMenuItem.Name = "pathologySpecimenToolStripMenuItem";
            this.pathologySpecimenToolStripMenuItem.Size = new System.Drawing.Size(299, 22);
            this.pathologySpecimenToolStripMenuItem.Text = "Pathology Specimen Setup";
            this.pathologySpecimenToolStripMenuItem.Click += new System.EventHandler(this.pathologySpecimenToolStripMenuItem_Click);
            // 
            // pathologyItemPriceToolStripMenuItem
            // 
            this.pathologyItemPriceToolStripMenuItem.Name = "pathologyItemPriceToolStripMenuItem";
            this.pathologyItemPriceToolStripMenuItem.Size = new System.Drawing.Size(299, 22);
            this.pathologyItemPriceToolStripMenuItem.Text = "Pathology Item Price Setup";
            this.pathologyItemPriceToolStripMenuItem.Click += new System.EventHandler(this.pathologyItemPriceToolStripMenuItem_Click);
            // 
            // pathologyTestCommentsSetupToolStripMenuItem
            // 
            this.pathologyTestCommentsSetupToolStripMenuItem.Name = "pathologyTestCommentsSetupToolStripMenuItem";
            this.pathologyTestCommentsSetupToolStripMenuItem.Size = new System.Drawing.Size(299, 22);
            this.pathologyTestCommentsSetupToolStripMenuItem.Text = "Pathology Test Comments Setup";
            this.pathologyTestCommentsSetupToolStripMenuItem.Click += new System.EventHandler(this.pathologyTestCommentsSetupToolStripMenuItem_Click);
            // 
            // oTSetupToolStripMenuItem
            // 
            this.oTSetupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.indoorOTCategoryToolStripMenuItem,
            this.oTSetupToolStripMenuItem1,
            this.oTNameToolStripMenuItem,
            this.oTNoteTemplateToolStripMenuItem});
            this.oTSetupToolStripMenuItem.Name = "oTSetupToolStripMenuItem";
            this.oTSetupToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.oTSetupToolStripMenuItem.Text = "OT Setup";
            // 
            // indoorOTCategoryToolStripMenuItem
            // 
            this.indoorOTCategoryToolStripMenuItem.Name = "indoorOTCategoryToolStripMenuItem";
            this.indoorOTCategoryToolStripMenuItem.Size = new System.Drawing.Size(307, 22);
            this.indoorOTCategoryToolStripMenuItem.Text = "Indoor/Outdoor OT Name Category Setup";
            this.indoorOTCategoryToolStripMenuItem.Click += new System.EventHandler(this.indoorOTCategoryToolStripMenuItem_Click);
            // 
            // oTSetupToolStripMenuItem1
            // 
            this.oTSetupToolStripMenuItem1.Name = "oTSetupToolStripMenuItem1";
            this.oTSetupToolStripMenuItem1.Size = new System.Drawing.Size(307, 22);
            this.oTSetupToolStripMenuItem1.Text = "OT Setup";
            this.oTSetupToolStripMenuItem1.Click += new System.EventHandler(this.oTSetupToolStripMenuItem1_Click);
            // 
            // oTNameToolStripMenuItem
            // 
            this.oTNameToolStripMenuItem.Name = "oTNameToolStripMenuItem";
            this.oTNameToolStripMenuItem.Size = new System.Drawing.Size(307, 22);
            this.oTNameToolStripMenuItem.Text = "OT Charge Setup";
            this.oTNameToolStripMenuItem.Click += new System.EventHandler(this.oTNameToolStripMenuItem_Click);
            // 
            // oTNoteTemplateToolStripMenuItem
            // 
            this.oTNoteTemplateToolStripMenuItem.Name = "oTNoteTemplateToolStripMenuItem";
            this.oTNoteTemplateToolStripMenuItem.Size = new System.Drawing.Size(307, 22);
            this.oTNoteTemplateToolStripMenuItem.Text = "OT Comments Setup";
            this.oTNoteTemplateToolStripMenuItem.Click += new System.EventHandler(this.oTNoteTemplateToolStripMenuItem_Click);
            // 
            // inventorySetupToolStripMenuItem
            // 
            this.inventorySetupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.catageruToolStripMenuItem,
            this.subCategoryToolStripMenuItem,
            this.brandToolStripMenuItem,
            this.productToolStripMenuItem,
            this.unitTypeToolStripMenuItem,
            this.inventorySupplierSetupToolStripMenuItem});
            this.inventorySetupToolStripMenuItem.Name = "inventorySetupToolStripMenuItem";
            this.inventorySetupToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.inventorySetupToolStripMenuItem.Text = "Inventory Setup";
            this.inventorySetupToolStripMenuItem.Click += new System.EventHandler(this.inventorySetupToolStripMenuItem_Click);
            // 
            // catageruToolStripMenuItem
            // 
            this.catageruToolStripMenuItem.Name = "catageruToolStripMenuItem";
            this.catageruToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.catageruToolStripMenuItem.Text = "Category";
            this.catageruToolStripMenuItem.Click += new System.EventHandler(this.catageruToolStripMenuItem_Click);
            // 
            // subCategoryToolStripMenuItem
            // 
            this.subCategoryToolStripMenuItem.Name = "subCategoryToolStripMenuItem";
            this.subCategoryToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.subCategoryToolStripMenuItem.Text = "Sub Category ";
            // 
            // brandToolStripMenuItem
            // 
            this.brandToolStripMenuItem.Name = "brandToolStripMenuItem";
            this.brandToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.brandToolStripMenuItem.Text = "Brand ";
            // 
            // productToolStripMenuItem
            // 
            this.productToolStripMenuItem.Name = "productToolStripMenuItem";
            this.productToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.productToolStripMenuItem.Text = "Product";
            // 
            // unitTypeToolStripMenuItem
            // 
            this.unitTypeToolStripMenuItem.Name = "unitTypeToolStripMenuItem";
            this.unitTypeToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.unitTypeToolStripMenuItem.Text = "Unit Type";
            // 
            // inventorySupplierSetupToolStripMenuItem
            // 
            this.inventorySupplierSetupToolStripMenuItem.Name = "inventorySupplierSetupToolStripMenuItem";
            this.inventorySupplierSetupToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.inventorySupplierSetupToolStripMenuItem.Text = "Inventory Supplier Setup";
            // 
            // doctorSetupToolStripMenuItem
            // 
            this.doctorSetupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.doctorNameToolStripMenuItem,
            this.outdoorDoctorSetupToolStripMenuItem,
            this.doctorSpecializationToolStripMenuItem,
            this.doctorTypeToolStripMenuItem,
            this.doctorChargeToolStripMenuItem1,
            this.doctorAdviceToolStripMenuItem,
            this.medicineDosesToolStripMenuItem,
            this.typeOfVisitSetupToolStripMenuItem});
            this.doctorSetupToolStripMenuItem.Name = "doctorSetupToolStripMenuItem";
            this.doctorSetupToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.doctorSetupToolStripMenuItem.Text = "Doctor Setup";
            // 
            // doctorNameToolStripMenuItem
            // 
            this.doctorNameToolStripMenuItem.Name = "doctorNameToolStripMenuItem";
            this.doctorNameToolStripMenuItem.Size = new System.Drawing.Size(228, 22);
            this.doctorNameToolStripMenuItem.Text = "Indoor Doctor Setup";
            this.doctorNameToolStripMenuItem.Click += new System.EventHandler(this.doctorNameToolStripMenuItem_Click);
            // 
            // outdoorDoctorSetupToolStripMenuItem
            // 
            this.outdoorDoctorSetupToolStripMenuItem.Name = "outdoorDoctorSetupToolStripMenuItem";
            this.outdoorDoctorSetupToolStripMenuItem.Size = new System.Drawing.Size(228, 22);
            this.outdoorDoctorSetupToolStripMenuItem.Text = "Outdoor Doctor Setup";
            this.outdoorDoctorSetupToolStripMenuItem.Click += new System.EventHandler(this.outdoorDoctorSetupToolStripMenuItem_Click);
            // 
            // doctorSpecializationToolStripMenuItem
            // 
            this.doctorSpecializationToolStripMenuItem.Name = "doctorSpecializationToolStripMenuItem";
            this.doctorSpecializationToolStripMenuItem.Size = new System.Drawing.Size(228, 22);
            this.doctorSpecializationToolStripMenuItem.Text = "Doctor Specialization Setup";
            this.doctorSpecializationToolStripMenuItem.Click += new System.EventHandler(this.doctorSpecializationToolStripMenuItem_Click);
            // 
            // doctorTypeToolStripMenuItem
            // 
            this.doctorTypeToolStripMenuItem.Name = "doctorTypeToolStripMenuItem";
            this.doctorTypeToolStripMenuItem.Size = new System.Drawing.Size(228, 22);
            this.doctorTypeToolStripMenuItem.Text = "Doctor Type Setup";
            this.doctorTypeToolStripMenuItem.Click += new System.EventHandler(this.doctorTypeToolStripMenuItem_Click);
            // 
            // doctorChargeToolStripMenuItem1
            // 
            this.doctorChargeToolStripMenuItem1.Name = "doctorChargeToolStripMenuItem1";
            this.doctorChargeToolStripMenuItem1.Size = new System.Drawing.Size(228, 22);
            this.doctorChargeToolStripMenuItem1.Text = "Doctor Visit Charge Setup";
            this.doctorChargeToolStripMenuItem1.Click += new System.EventHandler(this.doctorChargeToolStripMenuItem1_Click);
            // 
            // doctorAdviceToolStripMenuItem
            // 
            this.doctorAdviceToolStripMenuItem.Name = "doctorAdviceToolStripMenuItem";
            this.doctorAdviceToolStripMenuItem.Size = new System.Drawing.Size(228, 22);
            this.doctorAdviceToolStripMenuItem.Text = "Doctor Advice Text Setup";
            this.doctorAdviceToolStripMenuItem.Click += new System.EventHandler(this.doctorAdviceToolStripMenuItem_Click);
            // 
            // medicineDosesToolStripMenuItem
            // 
            this.medicineDosesToolStripMenuItem.Name = "medicineDosesToolStripMenuItem";
            this.medicineDosesToolStripMenuItem.Size = new System.Drawing.Size(228, 22);
            this.medicineDosesToolStripMenuItem.Text = "Medicine Doses Setup";
            this.medicineDosesToolStripMenuItem.Click += new System.EventHandler(this.medicineDosesToolStripMenuItem_Click);
            // 
            // typeOfVisitSetupToolStripMenuItem
            // 
            this.typeOfVisitSetupToolStripMenuItem.Name = "typeOfVisitSetupToolStripMenuItem";
            this.typeOfVisitSetupToolStripMenuItem.Size = new System.Drawing.Size(228, 22);
            this.typeOfVisitSetupToolStripMenuItem.Text = "Type of Visit Setup";
            this.typeOfVisitSetupToolStripMenuItem.Click += new System.EventHandler(this.typeOfVisitSetupToolStripMenuItem_Click);
            // 
            // nurseSetupToolStripMenuItem
            // 
            this.nurseSetupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nurseSetupToolStripMenuItem1});
            this.nurseSetupToolStripMenuItem.Name = "nurseSetupToolStripMenuItem";
            this.nurseSetupToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.nurseSetupToolStripMenuItem.Text = "Nurse Setup";
            this.nurseSetupToolStripMenuItem.Click += new System.EventHandler(this.nurseSetupToolStripMenuItem_Click);
            // 
            // nurseSetupToolStripMenuItem1
            // 
            this.nurseSetupToolStripMenuItem1.Name = "nurseSetupToolStripMenuItem1";
            this.nurseSetupToolStripMenuItem1.Size = new System.Drawing.Size(143, 22);
            this.nurseSetupToolStripMenuItem1.Text = "Nurse Setup";
            this.nurseSetupToolStripMenuItem1.Click += new System.EventHandler(this.nurseSetupToolStripMenuItem1_Click);
            // 
            // otherServicesSetupToolStripMenuItem
            // 
            this.otherServicesSetupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.serviceNameToolStripMenuItem});
            this.otherServicesSetupToolStripMenuItem.Name = "otherServicesSetupToolStripMenuItem";
            this.otherServicesSetupToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.otherServicesSetupToolStripMenuItem.Text = "Other Services Setup";
            // 
            // serviceNameToolStripMenuItem
            // 
            this.serviceNameToolStripMenuItem.Name = "serviceNameToolStripMenuItem";
            this.serviceNameToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.serviceNameToolStripMenuItem.Text = "Service Name/Charge Setup ";
            this.serviceNameToolStripMenuItem.Click += new System.EventHandler(this.serviceNameToolStripMenuItem_Click);
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem15,
            this.toolStripMenuItem16,
            this.toolStripMenuItem17,
            this.toolStripMenuItem18,
            this.toolStripMenuItem19,
            this.toolStripMenuItem20,
            this.toolStripMenuItem21});
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(193, 22);
            this.toolStripMenuItem14.Text = "Common Setup";
            // 
            // toolStripMenuItem15
            // 
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            this.toolStripMenuItem15.Size = new System.Drawing.Size(187, 22);
            this.toolStripMenuItem15.Text = "Nationality Setup";
            this.toolStripMenuItem15.Click += new System.EventHandler(this.toolStripMenuItem15_Click);
            // 
            // toolStripMenuItem16
            // 
            this.toolStripMenuItem16.Name = "toolStripMenuItem16";
            this.toolStripMenuItem16.Size = new System.Drawing.Size(187, 22);
            this.toolStripMenuItem16.Text = "Gender Setup";
            this.toolStripMenuItem16.Click += new System.EventHandler(this.toolStripMenuItem16_Click);
            // 
            // toolStripMenuItem17
            // 
            this.toolStripMenuItem17.Name = "toolStripMenuItem17";
            this.toolStripMenuItem17.Size = new System.Drawing.Size(187, 22);
            this.toolStripMenuItem17.Text = "Blood Group Setup";
            this.toolStripMenuItem17.Click += new System.EventHandler(this.toolStripMenuItem17_Click);
            // 
            // toolStripMenuItem18
            // 
            this.toolStripMenuItem18.Name = "toolStripMenuItem18";
            this.toolStripMenuItem18.Size = new System.Drawing.Size(187, 22);
            this.toolStripMenuItem18.Text = "Occupation Setup";
            this.toolStripMenuItem18.Click += new System.EventHandler(this.toolStripMenuItem18_Click);
            // 
            // toolStripMenuItem19
            // 
            this.toolStripMenuItem19.Name = "toolStripMenuItem19";
            this.toolStripMenuItem19.Size = new System.Drawing.Size(187, 22);
            this.toolStripMenuItem19.Text = "Religion Setup";
            this.toolStripMenuItem19.Click += new System.EventHandler(this.toolStripMenuItem19_Click);
            // 
            // toolStripMenuItem20
            // 
            this.toolStripMenuItem20.Name = "toolStripMenuItem20";
            this.toolStripMenuItem20.Size = new System.Drawing.Size(187, 22);
            this.toolStripMenuItem20.Text = "Salution Setup";
            this.toolStripMenuItem20.Click += new System.EventHandler(this.toolStripMenuItem20_Click);
            // 
            // toolStripMenuItem21
            // 
            this.toolStripMenuItem21.Name = "toolStripMenuItem21";
            this.toolStripMenuItem21.Size = new System.Drawing.Size(187, 22);
            this.toolStripMenuItem21.Text = "Marital Status Setup";
            this.toolStripMenuItem21.Click += new System.EventHandler(this.toolStripMenuItem21_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.companyToolStripMenuItem,
            this.bankToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // companyToolStripMenuItem
            // 
            this.companyToolStripMenuItem.Name = "companyToolStripMenuItem";
            this.companyToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.companyToolStripMenuItem.Text = "Company";
            // 
            // bankToolStripMenuItem
            // 
            this.bankToolStripMenuItem.Name = "bankToolStripMenuItem";
            this.bankToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.bankToolStripMenuItem.Text = "Bank";
            // 
            // taskToolStripMenuItem
            // 
            this.taskToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.paymentToolStripMenuItem,
            this.collectionToolStripMenuItem,
            this.accountsToolStripMenuItem1});
            this.taskToolStripMenuItem.Name = "taskToolStripMenuItem";
            this.taskToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.taskToolStripMenuItem.Text = "&Task";
            // 
            // paymentToolStripMenuItem
            // 
            this.paymentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.outdoorDoctorPaymentToolStripMenuItem,
            this.indoorDoctorPaymentToolStripMenuItem,
            this.refaralDoctorPaymentToolStripMenuItem,
            this.pathologyReferelCommissionPaymentToolStripMenuItem});
            this.paymentToolStripMenuItem.Name = "paymentToolStripMenuItem";
            this.paymentToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.paymentToolStripMenuItem.Text = "Payment";
            // 
            // outdoorDoctorPaymentToolStripMenuItem
            // 
            this.outdoorDoctorPaymentToolStripMenuItem.Name = "outdoorDoctorPaymentToolStripMenuItem";
            this.outdoorDoctorPaymentToolStripMenuItem.Size = new System.Drawing.Size(298, 22);
            this.outdoorDoctorPaymentToolStripMenuItem.Text = "Outdoor Doctor Payment";
            // 
            // indoorDoctorPaymentToolStripMenuItem
            // 
            this.indoorDoctorPaymentToolStripMenuItem.Name = "indoorDoctorPaymentToolStripMenuItem";
            this.indoorDoctorPaymentToolStripMenuItem.Size = new System.Drawing.Size(298, 22);
            this.indoorDoctorPaymentToolStripMenuItem.Text = "Indoor Doctor Payment";
            // 
            // refaralDoctorPaymentToolStripMenuItem
            // 
            this.refaralDoctorPaymentToolStripMenuItem.Name = "refaralDoctorPaymentToolStripMenuItem";
            this.refaralDoctorPaymentToolStripMenuItem.Size = new System.Drawing.Size(298, 22);
            this.refaralDoctorPaymentToolStripMenuItem.Text = "Refaral Doctor Comission Payment ";
            // 
            // pathologyReferelCommissionPaymentToolStripMenuItem
            // 
            this.pathologyReferelCommissionPaymentToolStripMenuItem.Name = "pathologyReferelCommissionPaymentToolStripMenuItem";
            this.pathologyReferelCommissionPaymentToolStripMenuItem.Size = new System.Drawing.Size(298, 22);
            this.pathologyReferelCommissionPaymentToolStripMenuItem.Text = "Pathology Referel Commission Payment ";
            // 
            // collectionToolStripMenuItem
            // 
            this.collectionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.indoorPatientCollectionToolStripMenuItem,
            this.outdoorPatientCollectionToolStripMenuItem,
            this.pathologyPatientCollectionToolStripMenuItem,
            this.oTCollectionToolStripMenuItem,
            this.outdoorOTCollectionToolStripMenuItem,
            this.otherServicesCollectionToolStripMenuItem,
            this.corporateClientCollectionToolStripMenuItem});
            this.collectionToolStripMenuItem.Name = "collectionToolStripMenuItem";
            this.collectionToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.collectionToolStripMenuItem.Text = "Collection";
            // 
            // indoorPatientCollectionToolStripMenuItem
            // 
            this.indoorPatientCollectionToolStripMenuItem.Name = "indoorPatientCollectionToolStripMenuItem";
            this.indoorPatientCollectionToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.indoorPatientCollectionToolStripMenuItem.Text = "Indoor Patient &Collection";
            // 
            // outdoorPatientCollectionToolStripMenuItem
            // 
            this.outdoorPatientCollectionToolStripMenuItem.Name = "outdoorPatientCollectionToolStripMenuItem";
            this.outdoorPatientCollectionToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.outdoorPatientCollectionToolStripMenuItem.Text = "Outdoor Patient &Collection";
            // 
            // pathologyPatientCollectionToolStripMenuItem
            // 
            this.pathologyPatientCollectionToolStripMenuItem.Name = "pathologyPatientCollectionToolStripMenuItem";
            this.pathologyPatientCollectionToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.pathologyPatientCollectionToolStripMenuItem.Text = "Pathology Collection";
            // 
            // oTCollectionToolStripMenuItem
            // 
            this.oTCollectionToolStripMenuItem.Name = "oTCollectionToolStripMenuItem";
            this.oTCollectionToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.oTCollectionToolStripMenuItem.Text = "Indoor OT Collection";
            // 
            // outdoorOTCollectionToolStripMenuItem
            // 
            this.outdoorOTCollectionToolStripMenuItem.Name = "outdoorOTCollectionToolStripMenuItem";
            this.outdoorOTCollectionToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.outdoorOTCollectionToolStripMenuItem.Text = "Outdoor OT Collection";
            // 
            // otherServicesCollectionToolStripMenuItem
            // 
            this.otherServicesCollectionToolStripMenuItem.Name = "otherServicesCollectionToolStripMenuItem";
            this.otherServicesCollectionToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.otherServicesCollectionToolStripMenuItem.Text = "Other Services Collection";
            // 
            // corporateClientCollectionToolStripMenuItem
            // 
            this.corporateClientCollectionToolStripMenuItem.Name = "corporateClientCollectionToolStripMenuItem";
            this.corporateClientCollectionToolStripMenuItem.Size = new System.Drawing.Size(223, 22);
            this.corporateClientCollectionToolStripMenuItem.Text = "Corporate Client Collection";
            // 
            // accountsToolStripMenuItem1
            // 
            this.accountsToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.voucherToolStripMenuItem,
            this.voucherPostingToolStripMenuItem,
            this.voucherCancelToolStripMenuItem});
            this.accountsToolStripMenuItem1.Name = "accountsToolStripMenuItem1";
            this.accountsToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.accountsToolStripMenuItem1.Text = "Accounts";
            // 
            // voucherToolStripMenuItem
            // 
            this.voucherToolStripMenuItem.Name = "voucherToolStripMenuItem";
            this.voucherToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.voucherToolStripMenuItem.Text = "Voucher";
            // 
            // voucherPostingToolStripMenuItem
            // 
            this.voucherPostingToolStripMenuItem.Name = "voucherPostingToolStripMenuItem";
            this.voucherPostingToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.voucherPostingToolStripMenuItem.Text = "Voucher Posting";
            // 
            // voucherCancelToolStripMenuItem
            // 
            this.voucherCancelToolStripMenuItem.Name = "voucherCancelToolStripMenuItem";
            this.voucherCancelToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.voucherCancelToolStripMenuItem.Text = "Voucher Cancel";
            // 
            // inventoryToolStripMenuItem
            // 
            this.inventoryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inventoryProductPurchageToolStripMenuItem,
            this.inventoryPStockOutUseToolStripMenuItem,
            this.productPurchaseReturnToolStripMenuItem1,
            this.toolStripMenuItem1,
            this.laundryToolStripMenuItem});
            this.inventoryToolStripMenuItem.Name = "inventoryToolStripMenuItem";
            this.inventoryToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.inventoryToolStripMenuItem.Text = "I&nventory ";
            // 
            // inventoryProductPurchageToolStripMenuItem
            // 
            this.inventoryProductPurchageToolStripMenuItem.Name = "inventoryProductPurchageToolStripMenuItem";
            this.inventoryProductPurchageToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.inventoryProductPurchageToolStripMenuItem.Text = "Inventory Product Purchase";
            this.inventoryProductPurchageToolStripMenuItem.Click += new System.EventHandler(this.inventoryProductPurchageToolStripMenuItem_Click);
            // 
            // inventoryPStockOutUseToolStripMenuItem
            // 
            this.inventoryPStockOutUseToolStripMenuItem.Name = "inventoryPStockOutUseToolStripMenuItem";
            this.inventoryPStockOutUseToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.inventoryPStockOutUseToolStripMenuItem.Text = "Inventory P. Stock Out/Use";
            // 
            // productPurchaseReturnToolStripMenuItem1
            // 
            this.productPurchaseReturnToolStripMenuItem1.Name = "productPurchaseReturnToolStripMenuItem1";
            this.productPurchaseReturnToolStripMenuItem1.Size = new System.Drawing.Size(229, 22);
            this.productPurchaseReturnToolStripMenuItem1.Text = "Product Purchase Return";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.toolStripMenuItem6});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(229, 22);
            this.toolStripMenuItem1.Text = "Purchase Order";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(311, 22);
            this.toolStripMenuItem2.Text = "Purchase Requisitoin";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(311, 22);
            this.toolStripMenuItem3.Text = "Purchase Requisition Receive";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(311, 22);
            this.toolStripMenuItem4.Text = "Purchase Requisition Supplierwise";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(311, 22);
            this.toolStripMenuItem5.Text = "Purchase Requisition Supplierwise Receive";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(311, 22);
            this.toolStripMenuItem6.Text = "Purchase Requisition Approvel";
            // 
            // laundryToolStripMenuItem
            // 
            this.laundryToolStripMenuItem.Name = "laundryToolStripMenuItem";
            this.laundryToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.laundryToolStripMenuItem.Text = "Laundry/Linen";
            // 
            // indoorToolStripMenuItem
            // 
            this.indoorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.indoorPaiToolStripMenuItem,
            this.indoorPatientDischargeCollectionToolStripMenuItem,
            this.indoorPatientBedChangeToolStripMenuItem,
            this.indoorPatientDoctorChangeToolStripMenuItem,
            this.indoorOTServicesToolStripMenuItem,
            this.patientDemographicInformationToolStripMenuItem,
            this.babyBornInformationToolStripMenuItem});
            this.indoorToolStripMenuItem.Name = "indoorToolStripMenuItem";
            this.indoorToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.indoorToolStripMenuItem.Text = "&Indoor";
            // 
            // indoorPaiToolStripMenuItem
            // 
            this.indoorPaiToolStripMenuItem.Name = "indoorPaiToolStripMenuItem";
            this.indoorPaiToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.indoorPaiToolStripMenuItem.Text = "Indoor Patient &Addmission";
            this.indoorPaiToolStripMenuItem.Click += new System.EventHandler(this.indoorPaiToolStripMenuItem_Click);
            // 
            // indoorPatientDischargeCollectionToolStripMenuItem
            // 
            this.indoorPatientDischargeCollectionToolStripMenuItem.Name = "indoorPatientDischargeCollectionToolStripMenuItem";
            this.indoorPatientDischargeCollectionToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.indoorPatientDischargeCollectionToolStripMenuItem.Text = "Indoor Patient &Discharge/Collection";
            this.indoorPatientDischargeCollectionToolStripMenuItem.Click += new System.EventHandler(this.indoorPatientDischargeCollectionToolStripMenuItem_Click);
            // 
            // indoorPatientBedChangeToolStripMenuItem
            // 
            this.indoorPatientBedChangeToolStripMenuItem.Name = "indoorPatientBedChangeToolStripMenuItem";
            this.indoorPatientBedChangeToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.indoorPatientBedChangeToolStripMenuItem.Text = "Indoor Patient Bed/Room Transfer ";
            this.indoorPatientBedChangeToolStripMenuItem.Click += new System.EventHandler(this.indoorPatientBedChangeToolStripMenuItem_Click);
            // 
            // indoorPatientDoctorChangeToolStripMenuItem
            // 
            this.indoorPatientDoctorChangeToolStripMenuItem.Name = "indoorPatientDoctorChangeToolStripMenuItem";
            this.indoorPatientDoctorChangeToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.indoorPatientDoctorChangeToolStripMenuItem.Text = "Indoor Patient Doctor Transfer ";
            this.indoorPatientDoctorChangeToolStripMenuItem.Click += new System.EventHandler(this.indoorPatientDoctorChangeToolStripMenuItem_Click);
            // 
            // indoorOTServicesToolStripMenuItem
            // 
            this.indoorOTServicesToolStripMenuItem.Name = "indoorOTServicesToolStripMenuItem";
            this.indoorOTServicesToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.indoorOTServicesToolStripMenuItem.Text = "Indoor OT Services";
            this.indoorOTServicesToolStripMenuItem.Click += new System.EventHandler(this.indoorOTServicesToolStripMenuItem_Click);
            // 
            // patientDemographicInformationToolStripMenuItem
            // 
            this.patientDemographicInformationToolStripMenuItem.Name = "patientDemographicInformationToolStripMenuItem";
            this.patientDemographicInformationToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.patientDemographicInformationToolStripMenuItem.Text = "Patient Demographic Information";
            // 
            // outdoorToolStripMenuItem
            // 
            this.outdoorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.outdoorPatientAddmissionToolStripMenuItem,
            this.outdoorPatientToolStripMenuItem,
            this.outdoorOTServicesToolStripMenuItem});
            this.outdoorToolStripMenuItem.Name = "outdoorToolStripMenuItem";
            this.outdoorToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.outdoorToolStripMenuItem.Text = "&Outdoor";
            // 
            // outdoorPatientAddmissionToolStripMenuItem
            // 
            this.outdoorPatientAddmissionToolStripMenuItem.Name = "outdoorPatientAddmissionToolStripMenuItem";
            this.outdoorPatientAddmissionToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.outdoorPatientAddmissionToolStripMenuItem.Text = "Outdoor Patient Registration";
            this.outdoorPatientAddmissionToolStripMenuItem.Click += new System.EventHandler(this.outdoorPatientAddmissionToolStripMenuItem_Click);
            // 
            // outdoorPatientToolStripMenuItem
            // 
            this.outdoorPatientToolStripMenuItem.Name = "outdoorPatientToolStripMenuItem";
            this.outdoorPatientToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.outdoorPatientToolStripMenuItem.Text = "Outdoor Patient Ticket";
            this.outdoorPatientToolStripMenuItem.Click += new System.EventHandler(this.outdoorPatientToolStripMenuItem_Click);
            // 
            // outdoorOTServicesToolStripMenuItem
            // 
            this.outdoorOTServicesToolStripMenuItem.Name = "outdoorOTServicesToolStripMenuItem";
            this.outdoorOTServicesToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.outdoorOTServicesToolStripMenuItem.Text = "Outdoor OT Services ";
            this.outdoorOTServicesToolStripMenuItem.Click += new System.EventHandler(this.outdoorOTServicesToolStripMenuItem_Click);
            // 
            // pathologyToolStripMenuItem
            // 
            this.pathologyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addPathologyInvestigationToolStripMenuItem,
            this.pathologyTestResultToolStripMenuItem,
            this.pathologyTestResultEditToolStripMenuItem,
            this.pathologySampleCollectionToolStripMenuItem,
            this.radiologyAndImagingToolStripMenuItem,
            this.electronicToolStripMenuItem});
            this.pathologyToolStripMenuItem.Name = "pathologyToolStripMenuItem";
            this.pathologyToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.pathologyToolStripMenuItem.Text = "&Pathology";
            // 
            // addPathologyInvestigationToolStripMenuItem
            // 
            this.addPathologyInvestigationToolStripMenuItem.Name = "addPathologyInvestigationToolStripMenuItem";
            this.addPathologyInvestigationToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.addPathologyInvestigationToolStripMenuItem.Text = "Add Pathology Investigation";
            this.addPathologyInvestigationToolStripMenuItem.Click += new System.EventHandler(this.addPathologyInvestigationToolStripMenuItem_Click);
            // 
            // pathologyTestResultToolStripMenuItem
            // 
            this.pathologyTestResultToolStripMenuItem.Name = "pathologyTestResultToolStripMenuItem";
            this.pathologyTestResultToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.pathologyTestResultToolStripMenuItem.Text = "Pathology Test Result";
            this.pathologyTestResultToolStripMenuItem.Click += new System.EventHandler(this.pathologyTestResultToolStripMenuItem_Click);
            // 
            // pathologyTestResultEditToolStripMenuItem
            // 
            this.pathologyTestResultEditToolStripMenuItem.Name = "pathologyTestResultEditToolStripMenuItem";
            this.pathologyTestResultEditToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.pathologyTestResultEditToolStripMenuItem.Text = "Pathology Test Result Edit";
            this.pathologyTestResultEditToolStripMenuItem.Click += new System.EventHandler(this.pathologyTestResultEditToolStripMenuItem_Click);
            // 
            // pathologySampleCollectionToolStripMenuItem
            // 
            this.pathologySampleCollectionToolStripMenuItem.Name = "pathologySampleCollectionToolStripMenuItem";
            this.pathologySampleCollectionToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.pathologySampleCollectionToolStripMenuItem.Text = "Pathology Sample Collection";
            // 
            // radiologyAndImagingToolStripMenuItem
            // 
            this.radiologyAndImagingToolStripMenuItem.Name = "radiologyAndImagingToolStripMenuItem";
            this.radiologyAndImagingToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.radiologyAndImagingToolStripMenuItem.Text = "Radiology And Imaging";
            // 
            // electronicToolStripMenuItem
            // 
            this.electronicToolStripMenuItem.Name = "electronicToolStripMenuItem";
            this.electronicToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.electronicToolStripMenuItem.Text = "Electronic Medical Record (EMR)";
            // 
            // doctorToolStripMenuItem
            // 
            this.doctorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.doctorWorkStationToolStripMenuItem,
            this.doctorPrescriptionToolStripMenuItem,
            this.doctorRoasterToolStripMenuItem,
            this.doctorAppointmentToolStripMenuItem,
            this.followUpToolStripMenuItem,
            this.referForAdmissionToolStripMenuItem});
            this.doctorToolStripMenuItem.Name = "doctorToolStripMenuItem";
            this.doctorToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.doctorToolStripMenuItem.Text = "&Doctor";
            // 
            // doctorWorkStationToolStripMenuItem
            // 
            this.doctorWorkStationToolStripMenuItem.Name = "doctorWorkStationToolStripMenuItem";
            this.doctorWorkStationToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.doctorWorkStationToolStripMenuItem.Text = "Doctor Work Station";
            // 
            // doctorPrescriptionToolStripMenuItem
            // 
            this.doctorPrescriptionToolStripMenuItem.Name = "doctorPrescriptionToolStripMenuItem";
            this.doctorPrescriptionToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.doctorPrescriptionToolStripMenuItem.Text = "Doctor Prescription";
            // 
            // doctorRoasterToolStripMenuItem
            // 
            this.doctorRoasterToolStripMenuItem.Name = "doctorRoasterToolStripMenuItem";
            this.doctorRoasterToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.doctorRoasterToolStripMenuItem.Text = "Doctor Roaster";
            // 
            // doctorAppointmentToolStripMenuItem
            // 
            this.doctorAppointmentToolStripMenuItem.Name = "doctorAppointmentToolStripMenuItem";
            this.doctorAppointmentToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.doctorAppointmentToolStripMenuItem.Text = "Doctor Appointment";
            // 
            // followUpToolStripMenuItem
            // 
            this.followUpToolStripMenuItem.Name = "followUpToolStripMenuItem";
            this.followUpToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.followUpToolStripMenuItem.Text = "Follow Up";
            // 
            // referForAdmissionToolStripMenuItem
            // 
            this.referForAdmissionToolStripMenuItem.Name = "referForAdmissionToolStripMenuItem";
            this.referForAdmissionToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.referForAdmissionToolStripMenuItem.Text = "Refer for Admission";
            // 
            // nurseToolStripMenuItem
            // 
            this.nurseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nurseWorkStationToolStripMenuItem,
            this.nurseRoasterToolStripMenuItem});
            this.nurseToolStripMenuItem.Name = "nurseToolStripMenuItem";
            this.nurseToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.nurseToolStripMenuItem.Text = "&Nurse";
            // 
            // nurseWorkStationToolStripMenuItem
            // 
            this.nurseWorkStationToolStripMenuItem.Name = "nurseWorkStationToolStripMenuItem";
            this.nurseWorkStationToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.nurseWorkStationToolStripMenuItem.Text = "Nurse Work Station";
            // 
            // nurseRoasterToolStripMenuItem
            // 
            this.nurseRoasterToolStripMenuItem.Name = "nurseRoasterToolStripMenuItem";
            this.nurseRoasterToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.nurseRoasterToolStripMenuItem.Text = "Nurse Roaster";
            // 
            // otherServicesToolStripMenuItem
            // 
            this.otherServicesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addOtherServiceToolStripMenuItem});
            this.otherServicesToolStripMenuItem.Name = "otherServicesToolStripMenuItem";
            this.otherServicesToolStripMenuItem.Size = new System.Drawing.Size(102, 20);
            this.otherServicesToolStripMenuItem.Text = "Other S&ervices";
            // 
            // addOtherServiceToolStripMenuItem
            // 
            this.addOtherServiceToolStripMenuItem.Name = "addOtherServiceToolStripMenuItem";
            this.addOtherServiceToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.addOtherServiceToolStripMenuItem.Text = "Add Other Service";
            // 
            // cancelToolStripMenuItem
            // 
            this.cancelToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.indoorCancelToolStripMenuItem,
            this.outdoorCancelToolStripMenuItem,
            this.pathologyCancelToolStripMenuItem,
            this.inventoryCancelToolStripMenuItem,
            this.otherServiceCancelToolStripMenuItem,
            this.cancelVoucherToolStripMenuItem});
            this.cancelToolStripMenuItem.Name = "cancelToolStripMenuItem";
            this.cancelToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.cancelToolStripMenuItem.Text = "&Cancel";
            this.cancelToolStripMenuItem.Click += new System.EventHandler(this.cancelToolStripMenuItem_Click);
            // 
            // indoorCancelToolStripMenuItem
            // 
            this.indoorCancelToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cancelIndoorPatientAddmissionToolStripMenuItem1,
            this.cancelIndoorPatientDischargeToolStripMenuItem,
            this.cancelIndoorOTServicesToolStripMenuItem,
            this.cancelIndoorOTCollectionToolStripMenuItem,
            this.cancelIndoorPatientCollectionToolStripMenuItem,
            this.cancelIndoorDoctorCommissionPaymentToolStripMenuItem});
            this.indoorCancelToolStripMenuItem.Name = "indoorCancelToolStripMenuItem";
            this.indoorCancelToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.indoorCancelToolStripMenuItem.Text = "Indoor Cancel";
            // 
            // cancelIndoorPatientAddmissionToolStripMenuItem1
            // 
            this.cancelIndoorPatientAddmissionToolStripMenuItem1.Name = "cancelIndoorPatientAddmissionToolStripMenuItem1";
            this.cancelIndoorPatientAddmissionToolStripMenuItem1.Size = new System.Drawing.Size(313, 22);
            this.cancelIndoorPatientAddmissionToolStripMenuItem1.Text = "Cancel Indoor Patient &Addmission";
            // 
            // cancelIndoorPatientDischargeToolStripMenuItem
            // 
            this.cancelIndoorPatientDischargeToolStripMenuItem.Name = "cancelIndoorPatientDischargeToolStripMenuItem";
            this.cancelIndoorPatientDischargeToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.cancelIndoorPatientDischargeToolStripMenuItem.Text = "Cancel Indoor Patient &Discharge";
            // 
            // cancelIndoorOTServicesToolStripMenuItem
            // 
            this.cancelIndoorOTServicesToolStripMenuItem.Name = "cancelIndoorOTServicesToolStripMenuItem";
            this.cancelIndoorOTServicesToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.cancelIndoorOTServicesToolStripMenuItem.Text = "Cancel Indoor  OT Services";
            // 
            // cancelIndoorOTCollectionToolStripMenuItem
            // 
            this.cancelIndoorOTCollectionToolStripMenuItem.Name = "cancelIndoorOTCollectionToolStripMenuItem";
            this.cancelIndoorOTCollectionToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.cancelIndoorOTCollectionToolStripMenuItem.Text = "Cancel Indoor OT &Collection";
            // 
            // cancelIndoorPatientCollectionToolStripMenuItem
            // 
            this.cancelIndoorPatientCollectionToolStripMenuItem.Name = "cancelIndoorPatientCollectionToolStripMenuItem";
            this.cancelIndoorPatientCollectionToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.cancelIndoorPatientCollectionToolStripMenuItem.Text = "Cancel Indoor Patient &Collection";
            // 
            // cancelIndoorDoctorCommissionPaymentToolStripMenuItem
            // 
            this.cancelIndoorDoctorCommissionPaymentToolStripMenuItem.Name = "cancelIndoorDoctorCommissionPaymentToolStripMenuItem";
            this.cancelIndoorDoctorCommissionPaymentToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.cancelIndoorDoctorCommissionPaymentToolStripMenuItem.Text = "Cancel Indoor Doctor Commission Payment";
            // 
            // outdoorCancelToolStripMenuItem
            // 
            this.outdoorCancelToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cancelOutdoorOTServicesToolStripMenuItem1,
            this.cancelOutdoorPatientRegistrationToolStripMenuItem1,
            this.cancelOutdoorPatientTicketToolStripMenuItem1,
            this.cancelOutdoorOTCollectionToolStripMenuItem1,
            this.cancelOutdoorPatientCollectionToolStripMenuItem,
            this.cancelOutdoorDoctorCommissionPaymentToolStripMenuItem});
            this.outdoorCancelToolStripMenuItem.Name = "outdoorCancelToolStripMenuItem";
            this.outdoorCancelToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.outdoorCancelToolStripMenuItem.Text = "Outdoor Cancel";
            // 
            // cancelOutdoorOTServicesToolStripMenuItem1
            // 
            this.cancelOutdoorOTServicesToolStripMenuItem1.Name = "cancelOutdoorOTServicesToolStripMenuItem1";
            this.cancelOutdoorOTServicesToolStripMenuItem1.Size = new System.Drawing.Size(323, 22);
            this.cancelOutdoorOTServicesToolStripMenuItem1.Text = "Cancel Outdoor OT Services";
            // 
            // cancelOutdoorPatientRegistrationToolStripMenuItem1
            // 
            this.cancelOutdoorPatientRegistrationToolStripMenuItem1.Name = "cancelOutdoorPatientRegistrationToolStripMenuItem1";
            this.cancelOutdoorPatientRegistrationToolStripMenuItem1.Size = new System.Drawing.Size(323, 22);
            this.cancelOutdoorPatientRegistrationToolStripMenuItem1.Text = "Cancel Outdoor Patient Registration";
            // 
            // cancelOutdoorPatientTicketToolStripMenuItem1
            // 
            this.cancelOutdoorPatientTicketToolStripMenuItem1.Name = "cancelOutdoorPatientTicketToolStripMenuItem1";
            this.cancelOutdoorPatientTicketToolStripMenuItem1.Size = new System.Drawing.Size(323, 22);
            this.cancelOutdoorPatientTicketToolStripMenuItem1.Text = "Cancel Outdoor Patient Ticket";
            // 
            // cancelOutdoorOTCollectionToolStripMenuItem1
            // 
            this.cancelOutdoorOTCollectionToolStripMenuItem1.Name = "cancelOutdoorOTCollectionToolStripMenuItem1";
            this.cancelOutdoorOTCollectionToolStripMenuItem1.Size = new System.Drawing.Size(323, 22);
            this.cancelOutdoorOTCollectionToolStripMenuItem1.Text = "Cancel Outdoor OT &Collection";
            // 
            // cancelOutdoorPatientCollectionToolStripMenuItem
            // 
            this.cancelOutdoorPatientCollectionToolStripMenuItem.Name = "cancelOutdoorPatientCollectionToolStripMenuItem";
            this.cancelOutdoorPatientCollectionToolStripMenuItem.Size = new System.Drawing.Size(323, 22);
            this.cancelOutdoorPatientCollectionToolStripMenuItem.Text = "Cancel Outdoor Patient &Collection";
            // 
            // cancelOutdoorDoctorCommissionPaymentToolStripMenuItem
            // 
            this.cancelOutdoorDoctorCommissionPaymentToolStripMenuItem.Name = "cancelOutdoorDoctorCommissionPaymentToolStripMenuItem";
            this.cancelOutdoorDoctorCommissionPaymentToolStripMenuItem.Size = new System.Drawing.Size(323, 22);
            this.cancelOutdoorDoctorCommissionPaymentToolStripMenuItem.Text = "Cancel Outdoor Doctor Commission Payment";
            // 
            // pathologyCancelToolStripMenuItem
            // 
            this.pathologyCancelToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cancelPathologyTestResultToolStripMenuItem,
            this.cancelPathologyCollectionToolStripMenuItem,
            this.cancelPathologyCommissionPToolStripMenuItem});
            this.pathologyCancelToolStripMenuItem.Name = "pathologyCancelToolStripMenuItem";
            this.pathologyCancelToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.pathologyCancelToolStripMenuItem.Text = "Pathology Cancel";
            // 
            // cancelPathologyTestResultToolStripMenuItem
            // 
            this.cancelPathologyTestResultToolStripMenuItem.Name = "cancelPathologyTestResultToolStripMenuItem";
            this.cancelPathologyTestResultToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.cancelPathologyTestResultToolStripMenuItem.Text = "Cancel Pathology Test Result";
            // 
            // cancelPathologyCollectionToolStripMenuItem
            // 
            this.cancelPathologyCollectionToolStripMenuItem.Name = "cancelPathologyCollectionToolStripMenuItem";
            this.cancelPathologyCollectionToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.cancelPathologyCollectionToolStripMenuItem.Text = "Cancel Pathology Collection";
            // 
            // cancelPathologyCommissionPToolStripMenuItem
            // 
            this.cancelPathologyCommissionPToolStripMenuItem.Name = "cancelPathologyCommissionPToolStripMenuItem";
            this.cancelPathologyCommissionPToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.cancelPathologyCommissionPToolStripMenuItem.Text = "Cancel Pathology Commission Payment";
            // 
            // inventoryCancelToolStripMenuItem
            // 
            this.inventoryCancelToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cancelInventoryProductPurchageToolStripMenuItem,
            this.cancelInventoryPStockOutUseToolStripMenuItem,
            this.cancelInventorySupplierPaymentToolStripMenuItem});
            this.inventoryCancelToolStripMenuItem.Name = "inventoryCancelToolStripMenuItem";
            this.inventoryCancelToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.inventoryCancelToolStripMenuItem.Text = "Inventory Cancel";
            // 
            // cancelInventoryProductPurchageToolStripMenuItem
            // 
            this.cancelInventoryProductPurchageToolStripMenuItem.Name = "cancelInventoryProductPurchageToolStripMenuItem";
            this.cancelInventoryProductPurchageToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.cancelInventoryProductPurchageToolStripMenuItem.Text = "Cancel Inventory Product Purchage";
            // 
            // cancelInventoryPStockOutUseToolStripMenuItem
            // 
            this.cancelInventoryPStockOutUseToolStripMenuItem.Name = "cancelInventoryPStockOutUseToolStripMenuItem";
            this.cancelInventoryPStockOutUseToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.cancelInventoryPStockOutUseToolStripMenuItem.Text = "Cancel Inventory P. Stock Out/Use";
            // 
            // cancelInventorySupplierPaymentToolStripMenuItem
            // 
            this.cancelInventorySupplierPaymentToolStripMenuItem.Name = "cancelInventorySupplierPaymentToolStripMenuItem";
            this.cancelInventorySupplierPaymentToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.cancelInventorySupplierPaymentToolStripMenuItem.Text = "Cancel Inventory Supplier Payment";
            // 
            // otherServiceCancelToolStripMenuItem
            // 
            this.otherServiceCancelToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cancelAddOtherServiceToolStripMenuItem1,
            this.cancelOtherServicesCollectionToolStripMenuItem1});
            this.otherServiceCancelToolStripMenuItem.Name = "otherServiceCancelToolStripMenuItem";
            this.otherServiceCancelToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.otherServiceCancelToolStripMenuItem.Text = "Other Service Cancel";
            // 
            // cancelAddOtherServiceToolStripMenuItem1
            // 
            this.cancelAddOtherServiceToolStripMenuItem1.Name = "cancelAddOtherServiceToolStripMenuItem1";
            this.cancelAddOtherServiceToolStripMenuItem1.Size = new System.Drawing.Size(254, 22);
            this.cancelAddOtherServiceToolStripMenuItem1.Text = "Cancel [ Add Other Service]";
            // 
            // cancelOtherServicesCollectionToolStripMenuItem1
            // 
            this.cancelOtherServicesCollectionToolStripMenuItem1.Name = "cancelOtherServicesCollectionToolStripMenuItem1";
            this.cancelOtherServicesCollectionToolStripMenuItem1.Size = new System.Drawing.Size(254, 22);
            this.cancelOtherServicesCollectionToolStripMenuItem1.Text = "Cancel Other Services Collection";
            // 
            // cancelVoucherToolStripMenuItem
            // 
            this.cancelVoucherToolStripMenuItem.Name = "cancelVoucherToolStripMenuItem";
            this.cancelVoucherToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.cancelVoucherToolStripMenuItem.Text = "Cancel Voucher ";
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.indoorReportsToolStripMenuItem,
            this.outdoorReportsToolStripMenuItem,
            this.pathologyReportsToolStripMenuItem,
            this.inventoryProductPurchageReportToolStripMenuItem,
            this.outstandingReprotToolStripMenuItem,
            this.accountsReportToolStripMenuItem,
            this.otherServicesReportsToolStripMenuItem,
            this.doctorReportToolStripMenuItem,
            this.nurseReportToolStripMenuItem,
            this.cancelReportsToolStripMenuItem,
            this.certificateToolStripMenuItem,
            this.hRReportToolStripMenuItem});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.reportsToolStripMenuItem.Text = "&Reports";
            // 
            // indoorReportsToolStripMenuItem
            // 
            this.indoorReportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.advanceCollectionReportToolStripMenuItem1,
            this.individualPaymentReportToolStripMenuItem2,
            this.individualCollectionReportToolStripMenuItem1,
            this.individualPatientReportToolStripMenuItem,
            this.indoorPatientDischargeListToolStripMenuItem,
            this.patientIDCardToolStripMenuItem,
            this.patientListReportToolStripMenuItem,
            this.indoorOTReportsToolStripMenuItem,
            this.indoorPatientCollectionReportsToolStripMenuItem,
            this.indoorOTCollectionReportsToolStripMenuItem,
            this.indoorDoctorLedgerReportToolStripMenuItem,
            this.indoorDoctorPaymentReportsToolStripMenuItem,
            this.refaralDoctorComissionPaymentReportsToolStripMenuItem,
            this.referalDoctorLedgerReportToolStripMenuItem1,
            this.deToolStripMenuItem,
            this.corporateClientLedgerReportToolStripMenuItem1,
            this.loginWToolStripMenuItem,
            this.loginWiseAToolStripMenuItem,
            this.allPatientBloodGroupToolStripMenuItem,
            this.patientClinicalHistoryAndVisitHistoryToolStripMenuItem,
            this.automaticPathologyReportGenerationFromAnalyserMachineToolStripMenuItem});
            this.indoorReportsToolStripMenuItem.Name = "indoorReportsToolStripMenuItem";
            this.indoorReportsToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.indoorReportsToolStripMenuItem.Text = "Indoor Reports";
            // 
            // advanceCollectionReportToolStripMenuItem1
            // 
            this.advanceCollectionReportToolStripMenuItem1.Name = "advanceCollectionReportToolStripMenuItem1";
            this.advanceCollectionReportToolStripMenuItem1.Size = new System.Drawing.Size(344, 22);
            this.advanceCollectionReportToolStripMenuItem1.Text = "Advance Collection Report";
            // 
            // individualPaymentReportToolStripMenuItem2
            // 
            this.individualPaymentReportToolStripMenuItem2.Name = "individualPaymentReportToolStripMenuItem2";
            this.individualPaymentReportToolStripMenuItem2.Size = new System.Drawing.Size(344, 22);
            this.individualPaymentReportToolStripMenuItem2.Text = "Individual Payment Report";
            this.individualPaymentReportToolStripMenuItem2.Click += new System.EventHandler(this.individualPaymentReportToolStripMenuItem2_Click);
            // 
            // individualCollectionReportToolStripMenuItem1
            // 
            this.individualCollectionReportToolStripMenuItem1.Name = "individualCollectionReportToolStripMenuItem1";
            this.individualCollectionReportToolStripMenuItem1.Size = new System.Drawing.Size(344, 22);
            this.individualCollectionReportToolStripMenuItem1.Text = "Individual Collection Report";
            // 
            // individualPatientReportToolStripMenuItem
            // 
            this.individualPatientReportToolStripMenuItem.Name = "individualPatientReportToolStripMenuItem";
            this.individualPatientReportToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.individualPatientReportToolStripMenuItem.Text = "Individual Patient Invoice  Report";
            // 
            // indoorPatientDischargeListToolStripMenuItem
            // 
            this.indoorPatientDischargeListToolStripMenuItem.Name = "indoorPatientDischargeListToolStripMenuItem";
            this.indoorPatientDischargeListToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.indoorPatientDischargeListToolStripMenuItem.Text = "Indoor Patient Discharge List";
            // 
            // patientIDCardToolStripMenuItem
            // 
            this.patientIDCardToolStripMenuItem.Name = "patientIDCardToolStripMenuItem";
            this.patientIDCardToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.patientIDCardToolStripMenuItem.Text = "Indoor Patient ID Card/Barcode Print";
            // 
            // patientListReportToolStripMenuItem
            // 
            this.patientListReportToolStripMenuItem.Name = "patientListReportToolStripMenuItem";
            this.patientListReportToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.patientListReportToolStripMenuItem.Text = "Indoor Patient List Report";
            // 
            // indoorOTReportsToolStripMenuItem
            // 
            this.indoorOTReportsToolStripMenuItem.Name = "indoorOTReportsToolStripMenuItem";
            this.indoorOTReportsToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.indoorOTReportsToolStripMenuItem.Text = "Indoor OT Reports";
            // 
            // indoorPatientCollectionReportsToolStripMenuItem
            // 
            this.indoorPatientCollectionReportsToolStripMenuItem.Name = "indoorPatientCollectionReportsToolStripMenuItem";
            this.indoorPatientCollectionReportsToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.indoorPatientCollectionReportsToolStripMenuItem.Text = "Indoor Patient &Collection Reports";
            // 
            // indoorOTCollectionReportsToolStripMenuItem
            // 
            this.indoorOTCollectionReportsToolStripMenuItem.Name = "indoorOTCollectionReportsToolStripMenuItem";
            this.indoorOTCollectionReportsToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.indoorOTCollectionReportsToolStripMenuItem.Text = "Indoor OT Collection Reports";
            // 
            // indoorDoctorLedgerReportToolStripMenuItem
            // 
            this.indoorDoctorLedgerReportToolStripMenuItem.Name = "indoorDoctorLedgerReportToolStripMenuItem";
            this.indoorDoctorLedgerReportToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.indoorDoctorLedgerReportToolStripMenuItem.Text = "Indoor Doctor Ledger Report";
            // 
            // indoorDoctorPaymentReportsToolStripMenuItem
            // 
            this.indoorDoctorPaymentReportsToolStripMenuItem.Name = "indoorDoctorPaymentReportsToolStripMenuItem";
            this.indoorDoctorPaymentReportsToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.indoorDoctorPaymentReportsToolStripMenuItem.Text = "Indoor Doctor Payment Reports";
            // 
            // refaralDoctorComissionPaymentReportsToolStripMenuItem
            // 
            this.refaralDoctorComissionPaymentReportsToolStripMenuItem.Name = "refaralDoctorComissionPaymentReportsToolStripMenuItem";
            this.refaralDoctorComissionPaymentReportsToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.refaralDoctorComissionPaymentReportsToolStripMenuItem.Text = "Refaral Doctor Comission Payment Reports";
            // 
            // referalDoctorLedgerReportToolStripMenuItem1
            // 
            this.referalDoctorLedgerReportToolStripMenuItem1.Name = "referalDoctorLedgerReportToolStripMenuItem1";
            this.referalDoctorLedgerReportToolStripMenuItem1.Size = new System.Drawing.Size(344, 22);
            this.referalDoctorLedgerReportToolStripMenuItem1.Text = "Referal Doctor Ledger Report";
            // 
            // deToolStripMenuItem
            // 
            this.deToolStripMenuItem.Name = "deToolStripMenuItem";
            this.deToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.deToolStripMenuItem.Text = "Disease Wise Bill Statement";
            // 
            // corporateClientLedgerReportToolStripMenuItem1
            // 
            this.corporateClientLedgerReportToolStripMenuItem1.Name = "corporateClientLedgerReportToolStripMenuItem1";
            this.corporateClientLedgerReportToolStripMenuItem1.Size = new System.Drawing.Size(344, 22);
            this.corporateClientLedgerReportToolStripMenuItem1.Text = "Corporate Client Ledger Report";
            // 
            // loginWToolStripMenuItem
            // 
            this.loginWToolStripMenuItem.Name = "loginWToolStripMenuItem";
            this.loginWToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.loginWToolStripMenuItem.Text = "Login Wise Collection";
            // 
            // loginWiseAToolStripMenuItem
            // 
            this.loginWiseAToolStripMenuItem.Name = "loginWiseAToolStripMenuItem";
            this.loginWiseAToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.loginWiseAToolStripMenuItem.Text = "Login Wise Addmission";
            // 
            // allPatientBloodGroupToolStripMenuItem
            // 
            this.allPatientBloodGroupToolStripMenuItem.Name = "allPatientBloodGroupToolStripMenuItem";
            this.allPatientBloodGroupToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.allPatientBloodGroupToolStripMenuItem.Text = "All Patient Blood Group";
            // 
            // patientClinicalHistoryAndVisitHistoryToolStripMenuItem
            // 
            this.patientClinicalHistoryAndVisitHistoryToolStripMenuItem.Name = "patientClinicalHistoryAndVisitHistoryToolStripMenuItem";
            this.patientClinicalHistoryAndVisitHistoryToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.patientClinicalHistoryAndVisitHistoryToolStripMenuItem.Text = "Patient Clinical History And Visit History";
            // 
            // automaticPathologyReportGenerationFromAnalyserMachineToolStripMenuItem
            // 
            this.automaticPathologyReportGenerationFromAnalyserMachineToolStripMenuItem.Name = "automaticPathologyReportGenerationFromAnalyserMachineToolStripMenuItem";
            this.automaticPathologyReportGenerationFromAnalyserMachineToolStripMenuItem.Size = new System.Drawing.Size(344, 22);
            this.automaticPathologyReportGenerationFromAnalyserMachineToolStripMenuItem.Text = "Auto. Report Generation From Analyser Machine";
            // 
            // outdoorReportsToolStripMenuItem
            // 
            this.outdoorReportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.outdoorPatientListReportToolStripMenuItem,
            this.outdoorDoctorLedgerReportToolStripMenuItem1,
            this.outdoorIDCardToolStripMenuItem,
            this.outdoorOTReportsToolStripMenuItem1,
            this.outdoorDoctorPaymentReportsToolStripMenuItem,
            this.outdoorPatientCollectionReportsToolStripMenuItem,
            this.outdoorOTCollectionReportsToolStripMenuItem,
            this.individualPatientReportToolStripMenuItem1,
            this.individualPatientLedgerReportToolStripMenuItem,
            this.individualCollectionReportToolStripMenuItem2,
            this.individualPaymentReportToolStripMenuItem1,
            this.refaralDoctorComissionPaymentReportsToolStripMenuItem1,
            this.referalDoctorLedgerReportToolStripMenuItem,
            this.advanceCollectionReportToolStripMenuItem2,
            this.allPatientBloodGroupToolStripMenuItem1});
            this.outdoorReportsToolStripMenuItem.Name = "outdoorReportsToolStripMenuItem";
            this.outdoorReportsToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.outdoorReportsToolStripMenuItem.Text = "Outdoor Reports";
            // 
            // outdoorPatientListReportToolStripMenuItem
            // 
            this.outdoorPatientListReportToolStripMenuItem.Name = "outdoorPatientListReportToolStripMenuItem";
            this.outdoorPatientListReportToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.outdoorPatientListReportToolStripMenuItem.Text = "Outdoor Patient List Report";
            // 
            // outdoorDoctorLedgerReportToolStripMenuItem1
            // 
            this.outdoorDoctorLedgerReportToolStripMenuItem1.Name = "outdoorDoctorLedgerReportToolStripMenuItem1";
            this.outdoorDoctorLedgerReportToolStripMenuItem1.Size = new System.Drawing.Size(313, 22);
            this.outdoorDoctorLedgerReportToolStripMenuItem1.Text = "Outdoor  Doctor Ledger Report";
            // 
            // outdoorIDCardToolStripMenuItem
            // 
            this.outdoorIDCardToolStripMenuItem.Name = "outdoorIDCardToolStripMenuItem";
            this.outdoorIDCardToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.outdoorIDCardToolStripMenuItem.Text = "Outdoor ID Card";
            // 
            // outdoorOTReportsToolStripMenuItem1
            // 
            this.outdoorOTReportsToolStripMenuItem1.Name = "outdoorOTReportsToolStripMenuItem1";
            this.outdoorOTReportsToolStripMenuItem1.Size = new System.Drawing.Size(313, 22);
            this.outdoorOTReportsToolStripMenuItem1.Text = "Outdoor OT Reports";
            this.outdoorOTReportsToolStripMenuItem1.Click += new System.EventHandler(this.outdoorOTReportsToolStripMenuItem1_Click);
            // 
            // outdoorDoctorPaymentReportsToolStripMenuItem
            // 
            this.outdoorDoctorPaymentReportsToolStripMenuItem.Name = "outdoorDoctorPaymentReportsToolStripMenuItem";
            this.outdoorDoctorPaymentReportsToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.outdoorDoctorPaymentReportsToolStripMenuItem.Text = "Outdoor Doctor Payment Reports";
            // 
            // outdoorPatientCollectionReportsToolStripMenuItem
            // 
            this.outdoorPatientCollectionReportsToolStripMenuItem.Name = "outdoorPatientCollectionReportsToolStripMenuItem";
            this.outdoorPatientCollectionReportsToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.outdoorPatientCollectionReportsToolStripMenuItem.Text = "Outdoor Patient &Collection Reports";
            // 
            // outdoorOTCollectionReportsToolStripMenuItem
            // 
            this.outdoorOTCollectionReportsToolStripMenuItem.Name = "outdoorOTCollectionReportsToolStripMenuItem";
            this.outdoorOTCollectionReportsToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.outdoorOTCollectionReportsToolStripMenuItem.Text = "Outdoor OT Collection Reports";
            // 
            // individualPatientReportToolStripMenuItem1
            // 
            this.individualPatientReportToolStripMenuItem1.Name = "individualPatientReportToolStripMenuItem1";
            this.individualPatientReportToolStripMenuItem1.Size = new System.Drawing.Size(313, 22);
            this.individualPatientReportToolStripMenuItem1.Text = "Individual Outdoor Patient Invoice Report";
            // 
            // individualPatientLedgerReportToolStripMenuItem
            // 
            this.individualPatientLedgerReportToolStripMenuItem.Name = "individualPatientLedgerReportToolStripMenuItem";
            this.individualPatientLedgerReportToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.individualPatientLedgerReportToolStripMenuItem.Text = "Individual Patient Ledger Report";
            // 
            // individualCollectionReportToolStripMenuItem2
            // 
            this.individualCollectionReportToolStripMenuItem2.Name = "individualCollectionReportToolStripMenuItem2";
            this.individualCollectionReportToolStripMenuItem2.Size = new System.Drawing.Size(313, 22);
            this.individualCollectionReportToolStripMenuItem2.Text = "Individual Collection Report";
            // 
            // individualPaymentReportToolStripMenuItem1
            // 
            this.individualPaymentReportToolStripMenuItem1.Name = "individualPaymentReportToolStripMenuItem1";
            this.individualPaymentReportToolStripMenuItem1.Size = new System.Drawing.Size(313, 22);
            this.individualPaymentReportToolStripMenuItem1.Text = "Individual Payment Report";
            // 
            // refaralDoctorComissionPaymentReportsToolStripMenuItem1
            // 
            this.refaralDoctorComissionPaymentReportsToolStripMenuItem1.Name = "refaralDoctorComissionPaymentReportsToolStripMenuItem1";
            this.refaralDoctorComissionPaymentReportsToolStripMenuItem1.Size = new System.Drawing.Size(313, 22);
            this.refaralDoctorComissionPaymentReportsToolStripMenuItem1.Text = "Refaral Doctor Comission Payment Reports";
            // 
            // referalDoctorLedgerReportToolStripMenuItem
            // 
            this.referalDoctorLedgerReportToolStripMenuItem.Name = "referalDoctorLedgerReportToolStripMenuItem";
            this.referalDoctorLedgerReportToolStripMenuItem.Size = new System.Drawing.Size(313, 22);
            this.referalDoctorLedgerReportToolStripMenuItem.Text = "Referal Doctor Ledger Report";
            // 
            // advanceCollectionReportToolStripMenuItem2
            // 
            this.advanceCollectionReportToolStripMenuItem2.Name = "advanceCollectionReportToolStripMenuItem2";
            this.advanceCollectionReportToolStripMenuItem2.Size = new System.Drawing.Size(313, 22);
            this.advanceCollectionReportToolStripMenuItem2.Text = "Advance Collection Report";
            // 
            // allPatientBloodGroupToolStripMenuItem1
            // 
            this.allPatientBloodGroupToolStripMenuItem1.Name = "allPatientBloodGroupToolStripMenuItem1";
            this.allPatientBloodGroupToolStripMenuItem1.Size = new System.Drawing.Size(313, 22);
            this.allPatientBloodGroupToolStripMenuItem1.Text = "All Patient Blood Group";
            // 
            // pathologyReportsToolStripMenuItem
            // 
            this.pathologyReportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pathologyServiceSalesReportToolStripMenuItem,
            this.pathologyCatagoryListReportToolStripMenuItem,
            this.pathologySubCatagoryListReportToolStripMenuItem,
            this.pathologyTestResultReportToolStripMenuItem,
            this.pathologyDeliveryReportToolStripMenuItem,
            this.pathologyUndeliveryReportToolStripMenuItem,
            this.pathologyInvestigationChargeReportToolStripMenuItem,
            this.individualInvoiceReportToolStripMenuItem,
            this.pathologyCollectionReportsToolStripMenuItem,
            this.individualCollectionReportToolStripMenuItem3,
            this.advanceCollectionReportToolStripMenuItem3,
            this.refaralDoctorComissionPaymentReportsToolStripMenuItem2,
            this.pathologyReferelCommissionPaymentReportsToolStripMenuItem,
            this.individualPaymentReportToolStripMenuItem3,
            this.referalDoctorLedgerReportToolStripMenuItem2});
            this.pathologyReportsToolStripMenuItem.Name = "pathologyReportsToolStripMenuItem";
            this.pathologyReportsToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.pathologyReportsToolStripMenuItem.Text = "Pathology Reports ";
            // 
            // pathologyServiceSalesReportToolStripMenuItem
            // 
            this.pathologyServiceSalesReportToolStripMenuItem.Name = "pathologyServiceSalesReportToolStripMenuItem";
            this.pathologyServiceSalesReportToolStripMenuItem.Size = new System.Drawing.Size(345, 22);
            this.pathologyServiceSalesReportToolStripMenuItem.Text = "Pathology Service Sales Report";
            // 
            // pathologyCatagoryListReportToolStripMenuItem
            // 
            this.pathologyCatagoryListReportToolStripMenuItem.Name = "pathologyCatagoryListReportToolStripMenuItem";
            this.pathologyCatagoryListReportToolStripMenuItem.Size = new System.Drawing.Size(345, 22);
            this.pathologyCatagoryListReportToolStripMenuItem.Text = "Pathology Catagory List Report";
            // 
            // pathologySubCatagoryListReportToolStripMenuItem
            // 
            this.pathologySubCatagoryListReportToolStripMenuItem.Name = "pathologySubCatagoryListReportToolStripMenuItem";
            this.pathologySubCatagoryListReportToolStripMenuItem.Size = new System.Drawing.Size(345, 22);
            this.pathologySubCatagoryListReportToolStripMenuItem.Text = "Pathology Sub Catagory List Report";
            // 
            // pathologyTestResultReportToolStripMenuItem
            // 
            this.pathologyTestResultReportToolStripMenuItem.Name = "pathologyTestResultReportToolStripMenuItem";
            this.pathologyTestResultReportToolStripMenuItem.Size = new System.Drawing.Size(345, 22);
            this.pathologyTestResultReportToolStripMenuItem.Text = "Pathology Test Result Report";
            // 
            // pathologyDeliveryReportToolStripMenuItem
            // 
            this.pathologyDeliveryReportToolStripMenuItem.Name = "pathologyDeliveryReportToolStripMenuItem";
            this.pathologyDeliveryReportToolStripMenuItem.Size = new System.Drawing.Size(345, 22);
            this.pathologyDeliveryReportToolStripMenuItem.Text = "Pathology Test Delivery Report";
            // 
            // pathologyUndeliveryReportToolStripMenuItem
            // 
            this.pathologyUndeliveryReportToolStripMenuItem.Name = "pathologyUndeliveryReportToolStripMenuItem";
            this.pathologyUndeliveryReportToolStripMenuItem.Size = new System.Drawing.Size(345, 22);
            this.pathologyUndeliveryReportToolStripMenuItem.Text = "Pathology Test UnDelivery Report";
            // 
            // pathologyInvestigationChargeReportToolStripMenuItem
            // 
            this.pathologyInvestigationChargeReportToolStripMenuItem.Name = "pathologyInvestigationChargeReportToolStripMenuItem";
            this.pathologyInvestigationChargeReportToolStripMenuItem.Size = new System.Drawing.Size(345, 22);
            this.pathologyInvestigationChargeReportToolStripMenuItem.Text = "Pathology Investigation Charge Report";
            // 
            // individualInvoiceReportToolStripMenuItem
            // 
            this.individualInvoiceReportToolStripMenuItem.Name = "individualInvoiceReportToolStripMenuItem";
            this.individualInvoiceReportToolStripMenuItem.Size = new System.Drawing.Size(345, 22);
            this.individualInvoiceReportToolStripMenuItem.Text = "Individual Invoice Report";
            // 
            // pathologyCollectionReportsToolStripMenuItem
            // 
            this.pathologyCollectionReportsToolStripMenuItem.Name = "pathologyCollectionReportsToolStripMenuItem";
            this.pathologyCollectionReportsToolStripMenuItem.Size = new System.Drawing.Size(345, 22);
            this.pathologyCollectionReportsToolStripMenuItem.Text = "Pathology Collection Reports";
            // 
            // individualCollectionReportToolStripMenuItem3
            // 
            this.individualCollectionReportToolStripMenuItem3.Name = "individualCollectionReportToolStripMenuItem3";
            this.individualCollectionReportToolStripMenuItem3.Size = new System.Drawing.Size(345, 22);
            this.individualCollectionReportToolStripMenuItem3.Text = "Individual Collection Report";
            // 
            // advanceCollectionReportToolStripMenuItem3
            // 
            this.advanceCollectionReportToolStripMenuItem3.Name = "advanceCollectionReportToolStripMenuItem3";
            this.advanceCollectionReportToolStripMenuItem3.Size = new System.Drawing.Size(345, 22);
            this.advanceCollectionReportToolStripMenuItem3.Text = "Advance Collection Report";
            // 
            // refaralDoctorComissionPaymentReportsToolStripMenuItem2
            // 
            this.refaralDoctorComissionPaymentReportsToolStripMenuItem2.Name = "refaralDoctorComissionPaymentReportsToolStripMenuItem2";
            this.refaralDoctorComissionPaymentReportsToolStripMenuItem2.Size = new System.Drawing.Size(345, 22);
            this.refaralDoctorComissionPaymentReportsToolStripMenuItem2.Text = "Refaral Doctor Comission Payment Reports";
            // 
            // pathologyReferelCommissionPaymentReportsToolStripMenuItem
            // 
            this.pathologyReferelCommissionPaymentReportsToolStripMenuItem.Name = "pathologyReferelCommissionPaymentReportsToolStripMenuItem";
            this.pathologyReferelCommissionPaymentReportsToolStripMenuItem.Size = new System.Drawing.Size(345, 22);
            this.pathologyReferelCommissionPaymentReportsToolStripMenuItem.Text = "Pathology Referel Commission Payment Reports ";
            // 
            // individualPaymentReportToolStripMenuItem3
            // 
            this.individualPaymentReportToolStripMenuItem3.Name = "individualPaymentReportToolStripMenuItem3";
            this.individualPaymentReportToolStripMenuItem3.Size = new System.Drawing.Size(345, 22);
            this.individualPaymentReportToolStripMenuItem3.Text = "Individual Payment Report";
            // 
            // referalDoctorLedgerReportToolStripMenuItem2
            // 
            this.referalDoctorLedgerReportToolStripMenuItem2.Name = "referalDoctorLedgerReportToolStripMenuItem2";
            this.referalDoctorLedgerReportToolStripMenuItem2.Size = new System.Drawing.Size(345, 22);
            this.referalDoctorLedgerReportToolStripMenuItem2.Text = "Referal Doctor Ledger Report";
            // 
            // inventoryProductPurchageReportToolStripMenuItem
            // 
            this.inventoryProductPurchageReportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inventoryPStockOutUseReportToolStripMenuItem,
            this.inventoryProductPurchageReportToolStripMenuItem1,
            this.inventoryStockReportToolStripMenuItem,
            this.inventorySupplierLedgerReportToolStripMenuItem,
            this.individualPaymentReportToolStripMenuItem,
            this.individualPatientLedgerReportToolStripMenuItem1,
            this.individualCollectionReportToolStripMenuItem});
            this.inventoryProductPurchageReportToolStripMenuItem.Name = "inventoryProductPurchageReportToolStripMenuItem";
            this.inventoryProductPurchageReportToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.inventoryProductPurchageReportToolStripMenuItem.Text = "Inventory Report";
            this.inventoryProductPurchageReportToolStripMenuItem.Click += new System.EventHandler(this.inventoryProductPurchageReportToolStripMenuItem_Click);
            // 
            // inventoryPStockOutUseReportToolStripMenuItem
            // 
            this.inventoryPStockOutUseReportToolStripMenuItem.Name = "inventoryPStockOutUseReportToolStripMenuItem";
            this.inventoryPStockOutUseReportToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.inventoryPStockOutUseReportToolStripMenuItem.Text = "Inventory P. Stock Out/Use Report";
            // 
            // inventoryProductPurchageReportToolStripMenuItem1
            // 
            this.inventoryProductPurchageReportToolStripMenuItem1.Name = "inventoryProductPurchageReportToolStripMenuItem1";
            this.inventoryProductPurchageReportToolStripMenuItem1.Size = new System.Drawing.Size(271, 22);
            this.inventoryProductPurchageReportToolStripMenuItem1.Text = "Inventory Product Purchase Report";
            // 
            // inventoryStockReportToolStripMenuItem
            // 
            this.inventoryStockReportToolStripMenuItem.Name = "inventoryStockReportToolStripMenuItem";
            this.inventoryStockReportToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.inventoryStockReportToolStripMenuItem.Text = "Inventory Stock Report";
            this.inventoryStockReportToolStripMenuItem.Click += new System.EventHandler(this.inventoryStockReportToolStripMenuItem_Click);
            // 
            // inventorySupplierLedgerReportToolStripMenuItem
            // 
            this.inventorySupplierLedgerReportToolStripMenuItem.Name = "inventorySupplierLedgerReportToolStripMenuItem";
            this.inventorySupplierLedgerReportToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.inventorySupplierLedgerReportToolStripMenuItem.Text = "Inventory Supplier Ledger Report";
            // 
            // individualPaymentReportToolStripMenuItem
            // 
            this.individualPaymentReportToolStripMenuItem.Name = "individualPaymentReportToolStripMenuItem";
            this.individualPaymentReportToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.individualPaymentReportToolStripMenuItem.Text = "Individual Payment Report";
            // 
            // individualPatientLedgerReportToolStripMenuItem1
            // 
            this.individualPatientLedgerReportToolStripMenuItem1.Name = "individualPatientLedgerReportToolStripMenuItem1";
            this.individualPatientLedgerReportToolStripMenuItem1.Size = new System.Drawing.Size(271, 22);
            this.individualPatientLedgerReportToolStripMenuItem1.Text = "Individual Patient Ledger Report";
            // 
            // individualCollectionReportToolStripMenuItem
            // 
            this.individualCollectionReportToolStripMenuItem.Name = "individualCollectionReportToolStripMenuItem";
            this.individualCollectionReportToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.individualCollectionReportToolStripMenuItem.Text = "Individual Collection Report";
            // 
            // outstandingReprotToolStripMenuItem
            // 
            this.outstandingReprotToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.indoorPatientReportToolStripMenuItem,
            this.outdoorPatientReportToolStripMenuItem,
            this.pathologyOutstandingReportToolStripMenuItem,
            this.otherServiceToolStripMenuItem,
            this.medicineReportToolStripMenuItem,
            this.indoorOTReportToolStripMenuItem,
            this.outdoorOTReportToolStripMenuItem,
            this.corporateClientOutstandingToolStripMenuItem});
            this.outstandingReprotToolStripMenuItem.Name = "outstandingReprotToolStripMenuItem";
            this.outstandingReprotToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.outstandingReprotToolStripMenuItem.Text = "Patient Outstanding Report";
            // 
            // indoorPatientReportToolStripMenuItem
            // 
            this.indoorPatientReportToolStripMenuItem.Name = "indoorPatientReportToolStripMenuItem";
            this.indoorPatientReportToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.indoorPatientReportToolStripMenuItem.Text = "Indoor Patient Outstanding";
            // 
            // outdoorPatientReportToolStripMenuItem
            // 
            this.outdoorPatientReportToolStripMenuItem.Name = "outdoorPatientReportToolStripMenuItem";
            this.outdoorPatientReportToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.outdoorPatientReportToolStripMenuItem.Text = "Outdoor Patient Outstanding ";
            this.outdoorPatientReportToolStripMenuItem.Click += new System.EventHandler(this.outdoorPatientReportToolStripMenuItem_Click);
            // 
            // pathologyOutstandingReportToolStripMenuItem
            // 
            this.pathologyOutstandingReportToolStripMenuItem.Name = "pathologyOutstandingReportToolStripMenuItem";
            this.pathologyOutstandingReportToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.pathologyOutstandingReportToolStripMenuItem.Text = "Pathology Outstanding Report";
            // 
            // otherServiceToolStripMenuItem
            // 
            this.otherServiceToolStripMenuItem.Name = "otherServiceToolStripMenuItem";
            this.otherServiceToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.otherServiceToolStripMenuItem.Text = "Other Service Outstanding";
            // 
            // medicineReportToolStripMenuItem
            // 
            this.medicineReportToolStripMenuItem.Name = "medicineReportToolStripMenuItem";
            this.medicineReportToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.medicineReportToolStripMenuItem.Text = "Medicine Outstanding";
            // 
            // indoorOTReportToolStripMenuItem
            // 
            this.indoorOTReportToolStripMenuItem.Name = "indoorOTReportToolStripMenuItem";
            this.indoorOTReportToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.indoorOTReportToolStripMenuItem.Text = "Indoor OT Outstanding";
            this.indoorOTReportToolStripMenuItem.Click += new System.EventHandler(this.indoorOTReportToolStripMenuItem_Click);
            // 
            // outdoorOTReportToolStripMenuItem
            // 
            this.outdoorOTReportToolStripMenuItem.Name = "outdoorOTReportToolStripMenuItem";
            this.outdoorOTReportToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.outdoorOTReportToolStripMenuItem.Text = "Outdoor OT Outstanding ";
            // 
            // corporateClientOutstandingToolStripMenuItem
            // 
            this.corporateClientOutstandingToolStripMenuItem.Name = "corporateClientOutstandingToolStripMenuItem";
            this.corporateClientOutstandingToolStripMenuItem.Size = new System.Drawing.Size(242, 22);
            this.corporateClientOutstandingToolStripMenuItem.Text = "Corporate Client Outstanding ";
            // 
            // accountsReportToolStripMenuItem
            // 
            this.accountsReportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userOrDeskWiseCollectionReportToolStripMenuItem,
            this.hospitalTotalCollectionReportToolStripMenuItem,
            this.totalExpenseReportToolStripMenuItem1,
            this.profitLossToolStripMenuItem,
            this.patientDiscountReportToolStripMenuItem,
            this.totalPaymentReportsToolStripMenuItem,
            this.patientManagementExpensesToolStripMenuItem});
            this.accountsReportToolStripMenuItem.Name = "accountsReportToolStripMenuItem";
            this.accountsReportToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.accountsReportToolStripMenuItem.Text = "Accounts Report";
            // 
            // userOrDeskWiseCollectionReportToolStripMenuItem
            // 
            this.userOrDeskWiseCollectionReportToolStripMenuItem.Name = "userOrDeskWiseCollectionReportToolStripMenuItem";
            this.userOrDeskWiseCollectionReportToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.userOrDeskWiseCollectionReportToolStripMenuItem.Text = "User or Desk  Wise Collection Report";
            // 
            // hospitalTotalCollectionReportToolStripMenuItem
            // 
            this.hospitalTotalCollectionReportToolStripMenuItem.Name = "hospitalTotalCollectionReportToolStripMenuItem";
            this.hospitalTotalCollectionReportToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.hospitalTotalCollectionReportToolStripMenuItem.Text = "Total Collection Report";
            // 
            // totalExpenseReportToolStripMenuItem1
            // 
            this.totalExpenseReportToolStripMenuItem1.Name = "totalExpenseReportToolStripMenuItem1";
            this.totalExpenseReportToolStripMenuItem1.Size = new System.Drawing.Size(279, 22);
            this.totalExpenseReportToolStripMenuItem1.Text = "Total Expense Report";
            // 
            // profitLossToolStripMenuItem
            // 
            this.profitLossToolStripMenuItem.Name = "profitLossToolStripMenuItem";
            this.profitLossToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.profitLossToolStripMenuItem.Text = "Profit and Loss";
            // 
            // patientDiscountReportToolStripMenuItem
            // 
            this.patientDiscountReportToolStripMenuItem.Name = "patientDiscountReportToolStripMenuItem";
            this.patientDiscountReportToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.patientDiscountReportToolStripMenuItem.Text = "Patient Discount Report";
            // 
            // totalPaymentReportsToolStripMenuItem
            // 
            this.totalPaymentReportsToolStripMenuItem.Name = "totalPaymentReportsToolStripMenuItem";
            this.totalPaymentReportsToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.totalPaymentReportsToolStripMenuItem.Text = "Total Payment Reports";
            // 
            // patientManagementExpensesToolStripMenuItem
            // 
            this.patientManagementExpensesToolStripMenuItem.Name = "patientManagementExpensesToolStripMenuItem";
            this.patientManagementExpensesToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.patientManagementExpensesToolStripMenuItem.Text = "Patient Management Expenses";
            // 
            // otherServicesReportsToolStripMenuItem
            // 
            this.otherServicesReportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.otherServiceSellReportToolStripMenuItem,
            this.otherServicesCollectionReportsToolStripMenuItem});
            this.otherServicesReportsToolStripMenuItem.Name = "otherServicesReportsToolStripMenuItem";
            this.otherServicesReportsToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.otherServicesReportsToolStripMenuItem.Text = "Other Services Reports ";
            // 
            // otherServiceSellReportToolStripMenuItem
            // 
            this.otherServiceSellReportToolStripMenuItem.Name = "otherServiceSellReportToolStripMenuItem";
            this.otherServiceSellReportToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.otherServiceSellReportToolStripMenuItem.Text = "Other Service Sales Report";
            // 
            // otherServicesCollectionReportsToolStripMenuItem
            // 
            this.otherServicesCollectionReportsToolStripMenuItem.Name = "otherServicesCollectionReportsToolStripMenuItem";
            this.otherServicesCollectionReportsToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.otherServicesCollectionReportsToolStripMenuItem.Text = "Other Services Collection Reports";
            // 
            // doctorReportToolStripMenuItem
            // 
            this.doctorReportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.doctorListReportToolStripMenuItem,
            this.doctorCommissionOutstandingToolStripMenuItem,
            this.doctorWisePatientListToolStripMenuItem});
            this.doctorReportToolStripMenuItem.Name = "doctorReportToolStripMenuItem";
            this.doctorReportToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.doctorReportToolStripMenuItem.Text = "Doctor Report";
            // 
            // doctorListReportToolStripMenuItem
            // 
            this.doctorListReportToolStripMenuItem.Name = "doctorListReportToolStripMenuItem";
            this.doctorListReportToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.doctorListReportToolStripMenuItem.Text = "Doctor List Report";
            // 
            // doctorCommissionOutstandingToolStripMenuItem
            // 
            this.doctorCommissionOutstandingToolStripMenuItem.Name = "doctorCommissionOutstandingToolStripMenuItem";
            this.doctorCommissionOutstandingToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.doctorCommissionOutstandingToolStripMenuItem.Text = "Doctor Commission Outstanding";
            // 
            // doctorWisePatientListToolStripMenuItem
            // 
            this.doctorWisePatientListToolStripMenuItem.Name = "doctorWisePatientListToolStripMenuItem";
            this.doctorWisePatientListToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.doctorWisePatientListToolStripMenuItem.Text = "Doctor Wise Patient List";
            // 
            // nurseReportToolStripMenuItem
            // 
            this.nurseReportToolStripMenuItem.Name = "nurseReportToolStripMenuItem";
            this.nurseReportToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.nurseReportToolStripMenuItem.Text = "Nurse List Report ";
            // 
            // cancelReportsToolStripMenuItem
            // 
            this.cancelReportsToolStripMenuItem.Name = "cancelReportsToolStripMenuItem";
            this.cancelReportsToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.cancelReportsToolStripMenuItem.Text = "Cancel Reports";
            // 
            // certificateToolStripMenuItem
            // 
            this.certificateToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.birthCertificateReportToolStripMenuItem,
            this.deathCertificateReportToolStripMenuItem,
            this.dischargeCertificateReprotToolStripMenuItem});
            this.certificateToolStripMenuItem.Name = "certificateToolStripMenuItem";
            this.certificateToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.certificateToolStripMenuItem.Text = "Certificate";
            // 
            // birthCertificateReportToolStripMenuItem
            // 
            this.birthCertificateReportToolStripMenuItem.Name = "birthCertificateReportToolStripMenuItem";
            this.birthCertificateReportToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.birthCertificateReportToolStripMenuItem.Text = "Birth Certificate Report";
            // 
            // deathCertificateReportToolStripMenuItem
            // 
            this.deathCertificateReportToolStripMenuItem.Name = "deathCertificateReportToolStripMenuItem";
            this.deathCertificateReportToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.deathCertificateReportToolStripMenuItem.Text = "Death Certificate Report";
            // 
            // dischargeCertificateReprotToolStripMenuItem
            // 
            this.dischargeCertificateReprotToolStripMenuItem.Name = "dischargeCertificateReprotToolStripMenuItem";
            this.dischargeCertificateReprotToolStripMenuItem.Size = new System.Drawing.Size(233, 22);
            this.dischargeCertificateReprotToolStripMenuItem.Text = "Discharge Certificate Reprot";
            // 
            // hRReportToolStripMenuItem
            // 
            this.hRReportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.indivisdualSalaryReportToolStripMenuItem});
            this.hRReportToolStripMenuItem.Name = "hRReportToolStripMenuItem";
            this.hRReportToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.hRReportToolStripMenuItem.Text = "HR Report";
            // 
            // indivisdualSalaryReportToolStripMenuItem
            // 
            this.indivisdualSalaryReportToolStripMenuItem.Name = "indivisdualSalaryReportToolStripMenuItem";
            this.indivisdualSalaryReportToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.indivisdualSalaryReportToolStripMenuItem.Text = "Indivisdual Salary Report";
            // 
            // hRToolStripMenuItem
            // 
            this.hRToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hRSetupToolStripMenuItem,
            this.employeeInformationToolStripMenuItem,
            this.employeeSalaryIncrementToolStripMenuItem,
            this.promotionToolStripMenuItem,
            this.employeeSalaryInformationToolStripMenuItem,
            this.employeeSalaryGenerateToolStripMenuItem,
            this.employeeSalaryGenerateCancelToolStripMenuItem,
            this.employeeActiveOrInactiveToolStripMenuItem,
            this.procurementToolStripMenuItem});
            this.hRToolStripMenuItem.Name = "hRToolStripMenuItem";
            this.hRToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.hRToolStripMenuItem.Text = "HR / Payroll";
            this.hRToolStripMenuItem.Click += new System.EventHandler(this.hRToolStripMenuItem_Click);
            // 
            // hRSetupToolStripMenuItem
            // 
            this.hRSetupToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.employeeDepartmentSetupToolStripMenuItem,
            this.employeeGradeToolStripMenuItem,
            this.employeeDesignationSetupToolStripMenuItem,
            this.employeeReligionSetupToolStripMenuItem,
            this.employCategorySetupToolStripMenuItem,
            this.employeeEducationSetupToolStripMenuItem,
            this.employeeStatusSetupToolStripMenuItem,
            this.employeeEdToolStripMenuItem});
            this.hRSetupToolStripMenuItem.Name = "hRSetupToolStripMenuItem";
            this.hRSetupToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.hRSetupToolStripMenuItem.Text = "HR Setup";
            // 
            // employeeDepartmentSetupToolStripMenuItem
            // 
            this.employeeDepartmentSetupToolStripMenuItem.Name = "employeeDepartmentSetupToolStripMenuItem";
            this.employeeDepartmentSetupToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.employeeDepartmentSetupToolStripMenuItem.Text = "Employee Department Setup";
            // 
            // employeeGradeToolStripMenuItem
            // 
            this.employeeGradeToolStripMenuItem.Name = "employeeGradeToolStripMenuItem";
            this.employeeGradeToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.employeeGradeToolStripMenuItem.Text = "Employee Grade Setup";
            // 
            // employeeDesignationSetupToolStripMenuItem
            // 
            this.employeeDesignationSetupToolStripMenuItem.Name = "employeeDesignationSetupToolStripMenuItem";
            this.employeeDesignationSetupToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.employeeDesignationSetupToolStripMenuItem.Text = "Employee Designation Setup";
            // 
            // employeeReligionSetupToolStripMenuItem
            // 
            this.employeeReligionSetupToolStripMenuItem.Name = "employeeReligionSetupToolStripMenuItem";
            this.employeeReligionSetupToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.employeeReligionSetupToolStripMenuItem.Text = "Employee Religion Setup";
            // 
            // employCategorySetupToolStripMenuItem
            // 
            this.employCategorySetupToolStripMenuItem.Name = "employCategorySetupToolStripMenuItem";
            this.employCategorySetupToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.employCategorySetupToolStripMenuItem.Text = "Employee  Category Setup";
            // 
            // employeeEducationSetupToolStripMenuItem
            // 
            this.employeeEducationSetupToolStripMenuItem.Name = "employeeEducationSetupToolStripMenuItem";
            this.employeeEducationSetupToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.employeeEducationSetupToolStripMenuItem.Text = "Employee Education Setup";
            // 
            // employeeStatusSetupToolStripMenuItem
            // 
            this.employeeStatusSetupToolStripMenuItem.Name = "employeeStatusSetupToolStripMenuItem";
            this.employeeStatusSetupToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.employeeStatusSetupToolStripMenuItem.Text = "Employee Status Setup";
            // 
            // employeeEdToolStripMenuItem
            // 
            this.employeeEdToolStripMenuItem.Name = "employeeEdToolStripMenuItem";
            this.employeeEdToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.employeeEdToolStripMenuItem.Text = "Employee Leave Type";
            // 
            // employeeInformationToolStripMenuItem
            // 
            this.employeeInformationToolStripMenuItem.Name = "employeeInformationToolStripMenuItem";
            this.employeeInformationToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.employeeInformationToolStripMenuItem.Text = "Employee Information";
            // 
            // employeeSalaryIncrementToolStripMenuItem
            // 
            this.employeeSalaryIncrementToolStripMenuItem.Name = "employeeSalaryIncrementToolStripMenuItem";
            this.employeeSalaryIncrementToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.employeeSalaryIncrementToolStripMenuItem.Text = "Employee Salary Increment";
            // 
            // promotionToolStripMenuItem
            // 
            this.promotionToolStripMenuItem.Name = "promotionToolStripMenuItem";
            this.promotionToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.promotionToolStripMenuItem.Text = "Promotion";
            // 
            // employeeSalaryInformationToolStripMenuItem
            // 
            this.employeeSalaryInformationToolStripMenuItem.Name = "employeeSalaryInformationToolStripMenuItem";
            this.employeeSalaryInformationToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.employeeSalaryInformationToolStripMenuItem.Text = "Employee Salary Information";
            // 
            // employeeSalaryGenerateToolStripMenuItem
            // 
            this.employeeSalaryGenerateToolStripMenuItem.Name = "employeeSalaryGenerateToolStripMenuItem";
            this.employeeSalaryGenerateToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.employeeSalaryGenerateToolStripMenuItem.Text = "Employee Salary Generate";
            // 
            // employeeSalaryGenerateCancelToolStripMenuItem
            // 
            this.employeeSalaryGenerateCancelToolStripMenuItem.Name = "employeeSalaryGenerateCancelToolStripMenuItem";
            this.employeeSalaryGenerateCancelToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.employeeSalaryGenerateCancelToolStripMenuItem.Text = "Employee Salary Generate Cancel";
            // 
            // employeeActiveOrInactiveToolStripMenuItem
            // 
            this.employeeActiveOrInactiveToolStripMenuItem.Name = "employeeActiveOrInactiveToolStripMenuItem";
            this.employeeActiveOrInactiveToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.employeeActiveOrInactiveToolStripMenuItem.Text = "Employee Active Or Inactive Processes";
            // 
            // procurementToolStripMenuItem
            // 
            this.procurementToolStripMenuItem.Name = "procurementToolStripMenuItem";
            this.procurementToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.procurementToolStripMenuItem.Text = "Procurement";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setupToolStripMenuItem,
            this.taskToolStripMenuItem,
            this.inventoryToolStripMenuItem,
            this.indoorToolStripMenuItem,
            this.outdoorToolStripMenuItem,
            this.hRToolStripMenuItem,
            this.pathologyToolStripMenuItem,
            this.doctorToolStripMenuItem,
            this.reportsToolStripMenuItem,
            this.nurseToolStripMenuItem,
            this.otherServicesToolStripMenuItem,
            this.cancelToolStripMenuItem,
            this.userManagementToolStripMenuItem,
            this.othersToolStripMenuItem,
            this.logoutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1088, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // userManagementToolStripMenuItem
            // 
            this.userManagementToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newUserToolStripMenuItem,
            this.changePasswordToolStripMenuItem,
            this.securityAssignToolStripMenuItem});
            this.userManagementToolStripMenuItem.Name = "userManagementToolStripMenuItem";
            this.userManagementToolStripMenuItem.Size = new System.Drawing.Size(122, 20);
            this.userManagementToolStripMenuItem.Text = "User Management";
            // 
            // newUserToolStripMenuItem
            // 
            this.newUserToolStripMenuItem.Name = "newUserToolStripMenuItem";
            this.newUserToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.newUserToolStripMenuItem.Text = "New User";
            // 
            // changePasswordToolStripMenuItem
            // 
            this.changePasswordToolStripMenuItem.Name = "changePasswordToolStripMenuItem";
            this.changePasswordToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.changePasswordToolStripMenuItem.Text = "Change Password";
            // 
            // securityAssignToolStripMenuItem
            // 
            this.securityAssignToolStripMenuItem.Name = "securityAssignToolStripMenuItem";
            this.securityAssignToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.securityAssignToolStripMenuItem.Text = "Security Assign";
            // 
            // othersToolStripMenuItem
            // 
            this.othersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem,
            this.logoffToolStripMenuItem,
            this.databaseToolStripMenuItem,
            this.supportCenterToolStripMenuItem,
            this.licenceUpdateToolStripMenuItem,
            this.configurationSetupToolStripMenuItem});
            this.othersToolStripMenuItem.Name = "othersToolStripMenuItem";
            this.othersToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.othersToolStripMenuItem.Text = "Others";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // logoffToolStripMenuItem
            // 
            this.logoffToolStripMenuItem.Name = "logoffToolStripMenuItem";
            this.logoffToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.logoffToolStripMenuItem.Text = "Logoff";
            // 
            // databaseToolStripMenuItem
            // 
            this.databaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backupToolStripMenuItem,
            this.restoreToolStripMenuItem,
            this.resetToolStripMenuItem});
            this.databaseToolStripMenuItem.Name = "databaseToolStripMenuItem";
            this.databaseToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.databaseToolStripMenuItem.Text = "Database";
            // 
            // backupToolStripMenuItem
            // 
            this.backupToolStripMenuItem.Name = "backupToolStripMenuItem";
            this.backupToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.backupToolStripMenuItem.Text = "Backup";
            // 
            // restoreToolStripMenuItem
            // 
            this.restoreToolStripMenuItem.Name = "restoreToolStripMenuItem";
            this.restoreToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.restoreToolStripMenuItem.Text = "Restore";
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.resetToolStripMenuItem.Text = "Reset";
            // 
            // supportCenterToolStripMenuItem
            // 
            this.supportCenterToolStripMenuItem.Name = "supportCenterToolStripMenuItem";
            this.supportCenterToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.supportCenterToolStripMenuItem.Text = "Support Center";
            // 
            // licenceUpdateToolStripMenuItem
            // 
            this.licenceUpdateToolStripMenuItem.Name = "licenceUpdateToolStripMenuItem";
            this.licenceUpdateToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.licenceUpdateToolStripMenuItem.Text = "Licence Update";
            // 
            // configurationSetupToolStripMenuItem
            // 
            this.configurationSetupToolStripMenuItem.Name = "configurationSetupToolStripMenuItem";
            this.configurationSetupToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.configurationSetupToolStripMenuItem.Text = "Configuration Setup";
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.logoutToolStripMenuItem.Text = "Logout";
            // 
            // babyBornInformationToolStripMenuItem
            // 
            this.babyBornInformationToolStripMenuItem.Name = "babyBornInformationToolStripMenuItem";
            this.babyBornInformationToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.babyBornInformationToolStripMenuItem.Text = "Baby Born Information";
            // 
            // HomePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(224)))), ((int)(((byte)(248)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1088, 600);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "HomePage";
            this.Text = "HomePage";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.HomePage_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem setupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem controlAccountToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subsidiaryAccountsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountsSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indoorSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem admissionChargeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem roomAndBedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem roomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem roomCategoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem roomBedChargeCountDownHourSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem patientDepartmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem corporateClientSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outdoorSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem doctorChargeToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem pathologySetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem referenceCategoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem referenceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pathologyTestItemCategoryInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pathologyTestItemSubCategoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pathologySpecimenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pathologyItemPriceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pathologyTestCommentsSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oTSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indoorOTCategoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oTNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oTNoteTemplateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventorySetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem catageruToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem subCategoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem brandToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unitTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventorySupplierSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem doctorSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem doctorNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outdoorDoctorSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem doctorSpecializationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem doctorTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem doctorChargeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem doctorAdviceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem medicineDosesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nurseSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nurseSetupToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem otherServicesSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem serviceNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem16;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem17;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem18;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem19;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem20;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem21;
        private System.Windows.Forms.ToolStripMenuItem taskToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outdoorDoctorPaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indoorDoctorPaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refaralDoctorPaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pathologyReferelCommissionPaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem collectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indoorPatientCollectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outdoorPatientCollectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pathologyPatientCollectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oTCollectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outdoorOTCollectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherServicesCollectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem corporateClientCollectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventoryProductPurchageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventoryPStockOutUseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indoorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indoorPaiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indoorPatientDischargeCollectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indoorPatientBedChangeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indoorPatientDoctorChangeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indoorOTServicesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outdoorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outdoorPatientAddmissionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outdoorPatientToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outdoorOTServicesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pathologyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addPathologyInvestigationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pathologyTestResultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pathologyTestResultEditToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem doctorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem doctorWorkStationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem doctorPrescriptionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nurseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nurseWorkStationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherServicesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addOtherServiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indoorReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem individualPatientReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem patientListReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem patientIDCardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outdoorReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem individualPatientReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem outdoorPatientListReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outdoorIDCardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pathologyReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pathologyServiceSalesReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pathologyCatagoryListReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pathologySubCatagoryListReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pathologyTestResultReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pathologyDeliveryReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pathologyUndeliveryReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pathologyInvestigationChargeReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem individualInvoiceReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventoryProductPurchageReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventoryPStockOutUseReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventoryProductPurchageReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem inventoryStockReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outstandingReprotToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indoorPatientReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outdoorPatientReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pathologyOutstandingReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherServiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem medicineReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indoorOTReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outdoorOTReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem corporateClientOutstandingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherServicesReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherServiceSellReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem doctorReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem doctorListReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nurseReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem certificateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem birthCertificateReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deathCertificateReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dischargeCertificateReprotToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hRToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem oTSetupToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem floorSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indoorCancelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelIndoorPatientAddmissionToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cancelIndoorPatientDischargeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelIndoorOTServicesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outdoorCancelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pathologyCancelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventoryCancelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelInventoryProductPurchageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelInventoryPStockOutUseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherServiceCancelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelOutdoorOTServicesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cancelOutdoorPatientRegistrationToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cancelOutdoorPatientTicketToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cancelPathologyTestResultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelAddOtherServiceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cancelIndoorOTCollectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelIndoorPatientCollectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelOutdoorOTCollectionToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cancelOutdoorPatientCollectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelPathologyCollectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelOtherServicesCollectionToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cancelIndoorDoctorCommissionPaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelOutdoorDoctorCommissionPaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelPathologyCommissionPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelInventorySupplierPaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancelVoucherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem companyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bankToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem voucherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem voucherPostingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userManagementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem othersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoffToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changePasswordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem securityAssignToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem databaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restoreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supportCenterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem licenceUpdateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurationSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem doctorCommissionOutstandingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem doctorWisePatientListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indoorPatientDischargeListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indoorOTReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outdoorOTReportsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem diseaseSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeSalaryIncrementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem promotionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeSalaryInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hRSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeDepartmentSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeGradeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeDesignationSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeReligionSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employCategorySetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeEducationSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeStatusSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeEdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeSalaryGenerateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeSalaryGenerateCancelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeActiveOrInactiveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pathologySampleCollectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indoorPatientCollectionReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indoorOTCollectionReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem individualCollectionReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem outdoorPatientCollectionReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outdoorOTCollectionReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem individualCollectionReportToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem pathologyCollectionReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem individualCollectionReportToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem otherServicesCollectionReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem advanceCollectionReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem advanceCollectionReportToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem advanceCollectionReportToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem indoorDoctorPaymentReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refaralDoctorComissionPaymentReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem individualPaymentReportToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem indoorDoctorLedgerReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem referalDoctorLedgerReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem corporateClientLedgerReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem outdoorDoctorPaymentReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refaralDoctorComissionPaymentReportsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem individualPaymentReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem outdoorDoctorLedgerReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem referalDoctorLedgerReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refaralDoctorComissionPaymentReportsToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem pathologyReferelCommissionPaymentReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem individualPaymentReportToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem referalDoctorLedgerReportToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem individualPatientLedgerReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventorySupplierLedgerReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem individualPaymentReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem individualPatientLedgerReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem individualCollectionReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem typeOfVisitSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loginWToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loginWiseAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hRReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indivisdualSalaryReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountsReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userOrDeskWiseCollectionReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hospitalTotalCollectionReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem totalExpenseReportToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem profitLossToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem patientDiscountReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem totalPaymentReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productPurchaseReturnToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem voucherCancelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem doctorRoasterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nurseRoasterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allPatientBloodGroupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allPatientBloodGroupToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem radiologyAndImagingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem electronicToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem procurementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laundryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem doctorAppointmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem followUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem referForAdmissionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem patientDemographicInformationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem patientClinicalHistoryAndVisitHistoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem automaticPathologyReportGenerationFromAnalyserMachineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem patientManagementExpensesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem babyBornInformationToolStripMenuItem;

    }
}