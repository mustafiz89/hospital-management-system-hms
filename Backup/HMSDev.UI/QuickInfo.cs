﻿using System.Windows.Forms;
using DynamicSoft.Security;

namespace POS_SYSTEM
{
    public partial class QuickInfo : Form
    {
        public QuickInfo(string user_id)
        {
            InitializeComponent();
            // CashBankList();
            // OutstandingList();
            lblUserID.Text = DASSessionInfoClass.USER_ID + "(" + DASSessionInfoClass.BRANCH_NAME + ")";// user_id;}}}
        }
    }
}
//            cashgridload();
//            outstandinggridload();
//            //label1.Text = Application.StartupPath;
//            bindstatics();
//            bindcompanyimage();
         
            
            
//            bool SHOW = OtherFetch.GetFeatureValue("1", DASSessionInfoClass.COMPANY_ID,DASSessionInfoClass.USER_ID);
//            if (SHOW == true)
//            {
//                groupBox1.Visible = true;
//                dataGridView1.Visible = true;

//                groupBox2.Visible = true;
//                dgReceivable.Visible = true;

//                groupBox3.Visible = true;
//                dgPayable.Visible = true;

//                groupBox4.Visible = true;
//                label3.Visible = true;
//                lblSales.Visible = true;
//                label4.Visible = true;
//                lblPurchase.Visible = true;
//                label5.Visible = true;
//                lblCollection.Visible = true;
//                label7.Visible = true;
//                lblPayment.Visible = true;
//                label8.Visible = true;
//                lblReceivable.Visible = true;
//                label9.Visible = true;
//                lblPayable.Visible = true;

//            }
//            else
//            {
//                groupBox1.Visible = false;
//                dataGridView1.Visible = false;

//                groupBox2.Visible = false;
//                dgReceivable.Visible = false;

//                groupBox3.Visible = false;
//                dgPayable.Visible = false;

//                groupBox4.Visible = false;
//                label3.Visible = false;
//                lblSales.Visible = false;
//                label4.Visible = false;
//                lblPurchase.Visible = false;
//                label5.Visible = false;
//                lblCollection.Visible = false;
//                label7.Visible = false;
//                lblPayment.Visible = false;
//                label8.Visible = false;
//                lblReceivable.Visible = false;
//                label9.Visible = false;
//                lblPayable.Visible = false;
//            }

//            //show or hide quick link
//            SHOW = OtherFetch.GetFeatureValue("3", DASSessionInfoClass.COMPANY_ID, DASSessionInfoClass.USER_ID);
//            if (SHOW == true)
//            {
//                groupBox5.Visible = true;
//                linkLabel5.Visible = true;
//                linkLabel6.Visible = true;
//                linkLabel7.Visible = true;
//                linkLabel8.Visible = true;
//            }
//            else
//            {
//                groupBox5.Visible = false;
//                linkLabel5.Visible = false;
//                linkLabel6.Visible = false;
//                linkLabel7.Visible = false;
//                linkLabel8.Visible = false;
//            }


//        }
//        private void bindcompanyimage()
//        {
//            try
//            {
//                COMPANY objCOMPANY = SettingsFatch.GetCompany(DASSessionInfoClass.COMPANY_ID);
//                CompanyImage.Image = HelperClass.GetImage(objCOMPANY.IMAGE_FILE);
//            }
//            catch { }
//        }

//        private void bindstatics()
//        {

//            SqlConnection conn = ConnectionClass.GetConnection();
//            SqlCommand objSqlcommand = new SqlCommand();
//            objSqlcommand.CommandText = "FSP_STATICS_GA";
//            objSqlcommand.CommandType = CommandType.StoredProcedure;
//            objSqlcommand.Parameters.Add(new SqlParameter("@COMPANY_ID", SqlDbType.NVarChar, 10));
//            objSqlcommand.Parameters["@COMPANY_ID"].Value = DASSessionInfoClass.COMPANY_ID;
//            objSqlcommand.Parameters.Add(new SqlParameter("@BRANCH_ID", SqlDbType.NVarChar, 10));
//            objSqlcommand.Parameters["@BRANCH_ID"].Value = DASSessionInfoClass.BRANCH_ID;
//            objSqlcommand.Connection = conn;
//            conn.Open();
//            using (objSqlcommand)
//            {
//                try
//                {

//                    SqlDataReader dr = objSqlcommand.ExecuteReader();
//                    while (dr.Read())
//                    {
//                        lblSales.Text = dr["SALES"].ToString();
//                        lblPurchase.Text = dr["PURCHASE"].ToString();
//                        lblCollection.Text = dr["COLLECTED_AMOUNT"].ToString();
//                        lblPayment.Text = dr["PAID_AMOUNT"].ToString();
//                        //lblProfit.Text = dr["PROFIT"].ToString();
//                        lblReceivable.Text = dr["RECEIVABLE"].ToString();
//                        lblPayable.Text = dr["PAYABLE"].ToString();
//                        //lblStock.Text = dr["STOCK"].ToString();
//                        // InvoiceNo = Convert.ToDecimal(1 + Convert.ToInt32(dr["PURCHASE_INVOICE_NO"]));
//                    }
//                }
//                catch { }
//            }
//        }

//        private void cashgridload()
//        {
//            List<ACCOUNTS> objListACCOUNTS = AccountsFetch.GetCashAccount("90190120002", "90190120003", DASSessionInfoClass.COMPANY_ID, DASSessionInfoClass.BRANCH_ID);
//            dataGridView1.AutoGenerateColumns = false;
//            dataGridView1.DataSource = objListACCOUNTS;
//            // dataGridView2.DataSource = objListACCOUNTS;
//            dataGridView1.Rows[0].Selected = false;
//        }


//        private void outstandinggridload()
//        {

//            try
//            {
//                List<BUYER_SUPPLIER_INFO> objBUYER_SUPPLIER_PAYABLE = OtherFetch.GetAllBUYER_SUPPLIER_PAYABLE(DASSessionInfoClass.COMPANY_ID, DASSessionInfoClass.BRANCH_ID);
//                dgPayable.AutoGenerateColumns = false;
//                dgPayable.DataSource = objBUYER_SUPPLIER_PAYABLE;
//                try
//                {
//                    dgPayable.Rows[0].Selected = false;
//                }
//                catch { }
//                List<BUYER_SUPPLIER_INFO> objBUYER_SUPPLIER_RECEIVABLE = OtherFetch.GetAllBUYER_SUPPLIER_RECEIVABLE(DASSessionInfoClass.COMPANY_ID, DASSessionInfoClass.BRANCH_ID);
//                dgReceivable.AutoGenerateColumns = false;
//                dgReceivable.DataSource = objBUYER_SUPPLIER_RECEIVABLE;
//                try
//                {
//                    dgReceivable.Rows[0].Selected = false;
//                }
//                catch { }

//            }
//            catch { }
//        }
//        //private void OutstandingList()
//        //{


//        //    List<BUYER_SUPPLIER_INFO> objListBUYER_SUPPLIER_INFO = OtherFetch.GetAllBUYER_SUPPLIER_INFO();
//        //    int i = 0;
//        //    foreach (BUYER_SUPPLIER_INFO obj in objListBUYER_SUPPLIER_INFO)
//        //    {


//        //        if (obj.BS_ACCOUNT_BALANCE < 0)
//        //        {
//        //            Label label = new Label();
//        //            label.Text = String.Format("   {0}", obj.BUYER_SUPPLIER_NAME);
//        //            //Position label on screen
//        //            label.Left = 10;
//        //            label.Top = (i + 1) * 21;
//        //            //Create textbox
//        //            Label label1 = new Label();
//        //            //Position textbox on screen
//        //            label1.Text = String.Format("   {0}", obj.BS_ACCOUNT_BALANCE);
//        //            label1.Left = 140;
//        //            label1.Top = (i + 1) * 21;
//        //            //Add controls to form
//        //            pnlReceivale.Controls.Add(label);
//        //            pnlReceivale.Controls.Add(label1);
//        //            i++;



//        //        }





//        //    }



//        //}



//        //private void CashBankList()
//        //{
//        //    List<ACCOUNTS> objListACCOUNTS = AccountsFetch.GetCashAccount("90190120002", "90190120003");
//        //    dataGridView1.DataSource = objListACCOUNTS;
//        //    dataGridView2.DataSource = objListACCOUNTS;
//        //    dataGridView1.Rows[0].Selected = false;
//        //    int i = 0;
//        //    foreach (ACCOUNTS obj in objListACCOUNTS)
//        //    {
//        //        Label label = new Label();
//        //        label.Text = String.Format("   {0}", obj.DESCRIPTION);
//        //        //Position label on screen
//        //        label.Left = 10;
//        //        label.Top = (i + 1) * 21;
//        //        //Create textbox
//        //        Label label1 = new Label();
//        //        //Position textbox on screen
//        //        label1.Text = String.Format("   {0}", obj.ACCOUNTS_BALANCE);
//        //        label1.Left = 125;
//        //        label1.Top = (i + 1) * 21;
//        //        //Add controls to form
//        //        panel1.Controls.Add(label);
//        //        panel1.Controls.Add(label1);
//        //        i++;
//        //    }

//        //}

//        private void button1_Click(object sender, EventArgs e)
//        {
//            gbCashBank.Visible = false;
//        }

//        private void button2_Click(object sender, EventArgs e)
//        {

//        }

//        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
//        {
//            POS_SYSTEM.Task.Sales.ProductSalesPOS obj = new POS_SYSTEM.Task.Sales.ProductSalesPOS(lblUserID.Text);

//            obj.MdiParent = this;
//            obj.Show();
//            //this.Hide();
//        }

//        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
//        {
//            IA.UI.Task.Purchase.ProductPurchase obj = new IA.UI.Task.Purchase.ProductPurchase(lblUserID.Text,0);

//            obj.Show();
//            this.Hide();
//        }

//        private void button1_Click_1(object sender, EventArgs e)
//        {
//            //IA.UI.HomePage objM = new IA.UI.HomePage("");
//            IA.UI.Task.Purchase.ProductPurchase obj = new IA.UI.Task.Purchase.ProductPurchase(lblUserID.Text,0);
//            // obj.MdiParent = objM;
//            this.Hide();
//            obj.Show();

//        }

//        #region gridview
//        /// <summary>
//        /// /add by rahimin
//        /// </summary>
//        /// <param name="sender"></param>
//        /// <param name="e"></param>
//        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
//        {
//            for (int i = 1; i <= 100; i++)
//            {
//                // Wait 100 milliseconds.
//                Thread.Sleep(1);
//                // Report progress.
//                backgroundWorker1.ReportProgress(i);
//            }
//        }

//        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
//        {
//            progressBar1.Value = e.ProgressPercentage;
//            // Set the text.
//            this.Text = e.ProgressPercentage.ToString();
//        }

//        private void button2_Click_1(object sender, EventArgs e)
//        {
//            try
//            {
//                progressBar1.Visible = true;
//                List<ACCOUNTS> objListACCOUNTS = AccountsFetch.GetCashAccount("90190120002", "90190120003", DASSessionInfoClass.COMPANY_ID,DASSessionInfoClass.BRANCH_ID);
//                dataGridView1.AutoGenerateColumns = false;
//                dataGridView1.DataSource = objListACCOUNTS;
//                dataGridView1.FirstDisplayedCell = null;
//                dataGridView1.ClearSelection();
//                //dataGridView1.Font.Size = Font.Bold;
//                backgroundWorker1.RunWorkerAsync();
//                bindstatics();
//                bindcompanyimage();
//                outstandinggridload();
//            }
//            catch { }
//        }


//        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
//        {
//            DataGridViewRow row = dataGridView1.Rows[e.RowIndex];// get you required index
//            // check the cell value under your specific column and then you can toggle your colors
//            row.DefaultCellStyle.BackColor = Color.FromArgb(192, 255, 255);
//        }

//        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
//        {
//            this.dataGridView1.Rows[e.RowIndex].Selected = false;
//        }

//        private void linkLabel5_MouseHover(object sender, EventArgs e)
//        {
//            //          gbcashbankk.Visible = true;
//            //          this.gbcashbankk.Location = new Point(
//            //Cursor.Position.X + 10,
//            //   Cursor.Position.Y - 10);
//        }

//        private void linkLabel5_MouseLeave(object sender, EventArgs e)
//        {
//            // gbcashbankk.Visible = false;
//        }

//        private void dgPayable1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
//        {
//            DataGridViewRow row = dgReceivable.Rows[e.RowIndex];// get you required index
//            // check the cell value under your specific column and then you can toggle your colors
//            row.DefaultCellStyle.BackColor = Color.FromArgb(192, 255, 255);
//        }

//        private void dgPayable1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
//        {
//            this.dgReceivable.Rows[e.RowIndex].Selected = false;
//        }

//        private void dgPayable2_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
//        {
//            DataGridViewRow row = dgPayable.Rows[e.RowIndex];// get you required index
//            // check the cell value under your specific column and then you can toggle your colors
//            row.DefaultCellStyle.BackColor = Color.FromArgb(192, 255, 255);
//        }

//        private void dgPayable2_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
//        {
//            this.dgPayable.Rows[e.RowIndex].Selected = false;
//        }

//        private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
//        {
//            //IA.UI.Task.Sales.ProductSales obj = new IA.UI.Task.Sales.ProductSales(lblUserID.Text);          
//            //obj.Show();
//            POS_SYSTEM.Task.Sales.ProductSalesPOS obj = new POS_SYSTEM.Task.Sales.ProductSalesPOS(lblUserID.Text);
//            obj.Show();
//        }

//        private void linkLabel6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
//        {
//            IA.UI.Task.Purchase.ProductPurchase obj = new IA.UI.Task.Purchase.ProductPurchase(lblUserID.Text,0);
//            //Task.Purchase.ProductPurchase obj = new Task.Purchase.ProductPurchase();
//            //obj.MdiParent = this;
//            obj.Show();
//        }

//        private void linkLabel7_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
//        {
//            IA.UI.Task.Collection.Collection obj = new IA.UI.Task.Collection.Collection(lblUserID.Text);
//            obj.Show();
//        }

//        private void linkLabel8_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
//        {
//            IA.UI.Task.Payment.Payment obj = new IA.UI.Task.Payment.Payment(lblUserID.Text);
//            obj.Show();
//        }

//        private void button3_Click(object sender, EventArgs e)
//        {
//            Application.Exit();
//        }

//        ///////////////////////
//        #endregion

//    }
//}