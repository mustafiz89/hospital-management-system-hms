﻿namespace HMSDev.UI
{
    partial class Indoor_Patient_Addmission
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.txtAddmissionNo = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbDOByear = new System.Windows.Forms.ComboBox();
            this.cmbGender = new System.Windows.Forms.ComboBox();
            this.cmbmaritalStatus = new System.Windows.Forms.ComboBox();
            this.cmbOccupation = new System.Windows.Forms.ComboBox();
            this.cmbCity = new System.Windows.Forms.ComboBox();
            this.cmbNationality = new System.Windows.Forms.ComboBox();
            this.cmbReligion = new System.Windows.Forms.ComboBox();
            this.cmbBloodGroup = new System.Windows.Forms.ComboBox();
            this.cmbDOBMonth = new System.Windows.Forms.ComboBox();
            this.cmbDOBdate = new System.Windows.Forms.ComboBox();
            this.cmbSalution = new System.Windows.Forms.ComboBox();
            this.txtPatientname = new System.Windows.Forms.TextBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.txtMotherinfo = new System.Windows.Forms.TextBox();
            this.txtFatherinfo = new System.Windows.Forms.TextBox();
            this.txtGurdianMobile = new System.Windows.Forms.TextBox();
            this.txtGurdianName = new System.Windows.Forms.TextBox();
            this.txtRelation = new System.Windows.Forms.TextBox();
            this.txtAge = new System.Windows.Forms.TextBox();
            this.txtMobile = new System.Windows.Forms.TextBox();
            this.oldPatientradioButton = new System.Windows.Forms.RadioButton();
            this.newPatientRadioButton = new System.Windows.Forms.RadioButton();
            this.txtCollectedAmount = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnReport = new System.Windows.Forms.Button();
            this.btnAReport = new System.Windows.Forms.Button();
            this.btnIDCard = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtReferalAddress = new System.Windows.Forms.TextBox();
            this.cmbReferal = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.cmbDiseaseName = new System.Windows.Forms.ComboBox();
            this.cmbAdmittedDept = new System.Windows.Forms.ComboBox();
            this.cmbAdmittedUnder = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dgvRoomBedInformation1 = new System.Windows.Forms.DataGridView();
            this.ACCOUNT_GROUP_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CONTROL_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label19 = new System.Windows.Forms.Label();
            this.cmbRoomBedCategory = new System.Windows.Forms.ComboBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dgvRoomBedInformation2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.cmbCollectionType = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRoomBedInformation1)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRoomBedInformation2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.txtAddmissionNo);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cmbDOByear);
            this.groupBox1.Controls.Add(this.cmbGender);
            this.groupBox1.Controls.Add(this.cmbmaritalStatus);
            this.groupBox1.Controls.Add(this.cmbOccupation);
            this.groupBox1.Controls.Add(this.cmbCity);
            this.groupBox1.Controls.Add(this.cmbNationality);
            this.groupBox1.Controls.Add(this.cmbReligion);
            this.groupBox1.Controls.Add(this.cmbBloodGroup);
            this.groupBox1.Controls.Add(this.cmbDOBMonth);
            this.groupBox1.Controls.Add(this.cmbDOBdate);
            this.groupBox1.Controls.Add(this.cmbSalution);
            this.groupBox1.Controls.Add(this.txtPatientname);
            this.groupBox1.Controls.Add(this.txtAddress);
            this.groupBox1.Controls.Add(this.txtMotherinfo);
            this.groupBox1.Controls.Add(this.txtFatherinfo);
            this.groupBox1.Controls.Add(this.txtGurdianMobile);
            this.groupBox1.Controls.Add(this.txtGurdianName);
            this.groupBox1.Controls.Add(this.txtRelation);
            this.groupBox1.Controls.Add(this.txtAge);
            this.groupBox1.Controls.Add(this.txtMobile);
            this.groupBox1.Controls.Add(this.oldPatientradioButton);
            this.groupBox1.Controls.Add(this.newPatientRadioButton);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(6, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(705, 393);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Indoor Patient Admission";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(366, 45);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(126, 25);
            this.dateTimePicker1.TabIndex = 5;
            // 
            // txtAddmissionNo
            // 
            this.txtAddmissionNo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtAddmissionNo.Location = new System.Drawing.Point(116, 46);
            this.txtAddmissionNo.Name = "txtAddmissionNo";
            this.txtAddmissionNo.Size = new System.Drawing.Size(196, 22);
            this.txtAddmissionNo.TabIndex = 5;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.checkBox1.Location = new System.Drawing.Point(34, 299);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(78, 20);
            this.checkBox1.TabIndex = 4;
            this.checkBox1.Text = "Gardian";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(68, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 16);
            this.label5.TabIndex = 3;
            this.label5.Text = "DOB :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label17.Location = new System.Drawing.Point(503, 280);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(114, 16);
            this.label17.TabIndex = 3;
            this.label17.Text = "Gurdian Mobile :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(259, 280);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(108, 16);
            this.label16.TabIndex = 3;
            this.label16.Text = "Gurdian Name :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(119, 280);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(69, 16);
            this.label15.TabIndex = 3;
            this.label15.Text = "Relation :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(27, 257);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(85, 16);
            this.label12.TabIndex = 3;
            this.label12.Text = "Father Info :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(28, 232);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 16);
            this.label10.TabIndex = 3;
            this.label10.Text = "Nationality :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(43, 206);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 16);
            this.label9.TabIndex = 3;
            this.label9.Text = "Religion :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(46, 183);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 16);
            this.label8.TabIndex = 3;
            this.label8.Text = "Address :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(9, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "Marital Status :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(363, 257);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(88, 16);
            this.label13.TabIndex = 3;
            this.label13.Text = "Mother Info :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(363, 233);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 16);
            this.label11.TabIndex = 3;
            this.label11.Text = "Occupation :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(411, 207);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 16);
            this.label7.TabIndex = 3;
            this.label7.Text = "City :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(394, 157);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Mobile :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(217, 155);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 16);
            this.label6.TabIndex = 3;
            this.label6.Text = "Blood Group :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(497, 131);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 16);
            this.label14.TabIndex = 3;
            this.label14.Text = "Gender :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(363, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Age :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(199, 106);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(102, 16);
            this.label18.TabIndex = 3;
            this.label18.Text = "Patient Name :";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(2, 49);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(110, 16);
            this.label27.TabIndex = 3;
            this.label27.Text = "Addmission No :";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(318, 49);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(45, 16);
            this.label28.TabIndex = 3;
            this.label28.Text = "Date :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(43, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "Salution :";
            // 
            // cmbDOByear
            // 
            this.cmbDOByear.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbDOByear.FormattingEnabled = true;
            this.cmbDOByear.Items.AddRange(new object[] {
            "2020",
            "2019 ",
            "2018  ",
            "2017  ",
            "2016 ",
            "2015 ",
            "2014 ",
            "2013 ",
            "2012 ",
            "2011",
            "2010 ",
            "2009 ",
            "2008 ",
            "2007  ",
            "2006 ",
            "2005 ",
            "2004 ",
            "2003 ",
            "2002 ",
            "2001",
            "2000",
            "1999 ",
            "1998 ",
            "1997 ",
            "1996  ",
            "1995 ",
            "1994 ",
            "1993 ",
            "1992 ",
            "1991",
            "1990 ",
            "1989 ",
            "1988  ",
            "1987 ",
            "1986  ",
            "1985 ",
            "1984 ",
            "1983 ",
            "1982 ",
            "1981",
            "1980 ",
            "1979 ",
            "1978 ",
            "1977 ",
            "1976 ",
            "1975 ",
            "1974 ",
            "1973 ",
            "1972 ",
            "1971",
            "1970 ",
            "1969 ",
            "1968 ",
            "1967 ",
            "1966 ",
            "1965 ",
            "1964 ",
            "1963 ",
            "1962 ",
            "1961",
            "1960  ",
            "1959  ",
            "1958 ",
            "1957  ",
            "1956  ",
            "1955 ",
            "1954 ",
            "1953  ",
            "1952  ",
            "1951",
            "1950",
            "1949  ",
            "1948 ",
            "1947 ",
            "1946 ",
            "1945 ",
            "1944 ",
            "1943",
            "1942",
            "1941",
            "1940  ",
            "1939 ",
            "1938 ",
            "1937 ",
            "1936  ",
            "1935 ",
            "1934 ",
            "1933 ",
            "1932 ",
            "1931",
            "1930 ",
            "1929 ",
            "1928 ",
            "1927 ",
            "1926  ",
            "1925 ",
            "1924 ",
            "1923 ",
            "1922  ",
            "1921",
            "1920  "});
            this.cmbDOByear.Location = new System.Drawing.Point(289, 127);
            this.cmbDOByear.Name = "cmbDOByear";
            this.cmbDOByear.Size = new System.Drawing.Size(68, 23);
            this.cmbDOByear.TabIndex = 2;
            // 
            // cmbGender
            // 
            this.cmbGender.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbGender.FormattingEnabled = true;
            this.cmbGender.Location = new System.Drawing.Point(566, 128);
            this.cmbGender.Name = "cmbGender";
            this.cmbGender.Size = new System.Drawing.Size(127, 23);
            this.cmbGender.TabIndex = 2;
            // 
            // cmbmaritalStatus
            // 
            this.cmbmaritalStatus.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbmaritalStatus.FormattingEnabled = true;
            this.cmbmaritalStatus.Location = new System.Drawing.Point(116, 152);
            this.cmbmaritalStatus.Name = "cmbmaritalStatus";
            this.cmbmaritalStatus.Size = new System.Drawing.Size(97, 23);
            this.cmbmaritalStatus.TabIndex = 2;
            // 
            // cmbOccupation
            // 
            this.cmbOccupation.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbOccupation.FormattingEnabled = true;
            this.cmbOccupation.Location = new System.Drawing.Point(460, 228);
            this.cmbOccupation.Name = "cmbOccupation";
            this.cmbOccupation.Size = new System.Drawing.Size(233, 23);
            this.cmbOccupation.TabIndex = 2;
            // 
            // cmbCity
            // 
            this.cmbCity.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbCity.FormattingEnabled = true;
            this.cmbCity.Location = new System.Drawing.Point(460, 203);
            this.cmbCity.Name = "cmbCity";
            this.cmbCity.Size = new System.Drawing.Size(233, 23);
            this.cmbCity.TabIndex = 2;
            // 
            // cmbNationality
            // 
            this.cmbNationality.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbNationality.FormattingEnabled = true;
            this.cmbNationality.Location = new System.Drawing.Point(116, 228);
            this.cmbNationality.Name = "cmbNationality";
            this.cmbNationality.Size = new System.Drawing.Size(241, 23);
            this.cmbNationality.TabIndex = 2;
            // 
            // cmbReligion
            // 
            this.cmbReligion.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbReligion.FormattingEnabled = true;
            this.cmbReligion.Location = new System.Drawing.Point(116, 203);
            this.cmbReligion.Name = "cmbReligion";
            this.cmbReligion.Size = new System.Drawing.Size(241, 23);
            this.cmbReligion.TabIndex = 2;
            // 
            // cmbBloodGroup
            // 
            this.cmbBloodGroup.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbBloodGroup.FormattingEnabled = true;
            this.cmbBloodGroup.Location = new System.Drawing.Point(319, 153);
            this.cmbBloodGroup.Name = "cmbBloodGroup";
            this.cmbBloodGroup.Size = new System.Drawing.Size(65, 23);
            this.cmbBloodGroup.TabIndex = 2;
            // 
            // cmbDOBMonth
            // 
            this.cmbDOBMonth.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbDOBMonth.FormattingEnabled = true;
            this.cmbDOBMonth.Items.AddRange(new object[] {
            "Jan",
            "Feb",
            "March",
            "April",
            "May",
            "june",
            "July",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"});
            this.cmbDOBMonth.Location = new System.Drawing.Point(186, 127);
            this.cmbDOBMonth.Name = "cmbDOBMonth";
            this.cmbDOBMonth.Size = new System.Drawing.Size(97, 23);
            this.cmbDOBMonth.TabIndex = 2;
            // 
            // cmbDOBdate
            // 
            this.cmbDOBdate.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbDOBdate.FormattingEnabled = true;
            this.cmbDOBdate.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.cmbDOBdate.Location = new System.Drawing.Point(116, 127);
            this.cmbDOBdate.Name = "cmbDOBdate";
            this.cmbDOBdate.Size = new System.Drawing.Size(64, 23);
            this.cmbDOBdate.TabIndex = 2;
            // 
            // cmbSalution
            // 
            this.cmbSalution.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbSalution.FormattingEnabled = true;
            this.cmbSalution.Location = new System.Drawing.Point(116, 102);
            this.cmbSalution.Name = "cmbSalution";
            this.cmbSalution.Size = new System.Drawing.Size(77, 23);
            this.cmbSalution.TabIndex = 2;
            // 
            // txtPatientname
            // 
            this.txtPatientname.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtPatientname.Location = new System.Drawing.Point(307, 103);
            this.txtPatientname.Name = "txtPatientname";
            this.txtPatientname.Size = new System.Drawing.Size(386, 22);
            this.txtPatientname.TabIndex = 1;
            // 
            // txtAddress
            // 
            this.txtAddress.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtAddress.Location = new System.Drawing.Point(116, 179);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(577, 22);
            this.txtAddress.TabIndex = 1;
            // 
            // txtMotherinfo
            // 
            this.txtMotherinfo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtMotherinfo.Location = new System.Drawing.Point(460, 254);
            this.txtMotherinfo.Name = "txtMotherinfo";
            this.txtMotherinfo.Size = new System.Drawing.Size(233, 22);
            this.txtMotherinfo.TabIndex = 1;
            // 
            // txtFatherinfo
            // 
            this.txtFatherinfo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtFatherinfo.Location = new System.Drawing.Point(116, 254);
            this.txtFatherinfo.Name = "txtFatherinfo";
            this.txtFatherinfo.Size = new System.Drawing.Size(241, 22);
            this.txtFatherinfo.TabIndex = 1;
            // 
            // txtGurdianMobile
            // 
            this.txtGurdianMobile.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtGurdianMobile.Location = new System.Drawing.Point(503, 297);
            this.txtGurdianMobile.Name = "txtGurdianMobile";
            this.txtGurdianMobile.Size = new System.Drawing.Size(190, 22);
            this.txtGurdianMobile.TabIndex = 1;
            // 
            // txtGurdianName
            // 
            this.txtGurdianName.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtGurdianName.Location = new System.Drawing.Point(262, 297);
            this.txtGurdianName.Name = "txtGurdianName";
            this.txtGurdianName.Size = new System.Drawing.Size(235, 22);
            this.txtGurdianName.TabIndex = 1;
            // 
            // txtRelation
            // 
            this.txtRelation.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtRelation.Location = new System.Drawing.Point(116, 297);
            this.txtRelation.Name = "txtRelation";
            this.txtRelation.Size = new System.Drawing.Size(140, 22);
            this.txtRelation.TabIndex = 1;
            // 
            // txtAge
            // 
            this.txtAge.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtAge.Location = new System.Drawing.Point(409, 128);
            this.txtAge.Name = "txtAge";
            this.txtAge.Size = new System.Drawing.Size(83, 22);
            this.txtAge.TabIndex = 1;
            // 
            // txtMobile
            // 
            this.txtMobile.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtMobile.Location = new System.Drawing.Point(460, 154);
            this.txtMobile.Name = "txtMobile";
            this.txtMobile.Size = new System.Drawing.Size(233, 22);
            this.txtMobile.TabIndex = 1;
            // 
            // oldPatientradioButton
            // 
            this.oldPatientradioButton.AutoSize = true;
            this.oldPatientradioButton.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.oldPatientradioButton.Location = new System.Drawing.Point(119, 25);
            this.oldPatientradioButton.Name = "oldPatientradioButton";
            this.oldPatientradioButton.Size = new System.Drawing.Size(97, 20);
            this.oldPatientradioButton.TabIndex = 0;
            this.oldPatientradioButton.TabStop = true;
            this.oldPatientradioButton.Text = "Old Patient";
            this.oldPatientradioButton.UseVisualStyleBackColor = true;
            // 
            // newPatientRadioButton
            // 
            this.newPatientRadioButton.AutoSize = true;
            this.newPatientRadioButton.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.newPatientRadioButton.Location = new System.Drawing.Point(9, 25);
            this.newPatientRadioButton.Name = "newPatientRadioButton";
            this.newPatientRadioButton.Size = new System.Drawing.Size(103, 20);
            this.newPatientRadioButton.TabIndex = 0;
            this.newPatientRadioButton.TabStop = true;
            this.newPatientRadioButton.Text = "New Patient";
            this.newPatientRadioButton.UseVisualStyleBackColor = true;
            // 
            // txtCollectedAmount
            // 
            this.txtCollectedAmount.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtCollectedAmount.Location = new System.Drawing.Point(186, 59);
            this.txtCollectedAmount.Name = "txtCollectedAmount";
            this.txtCollectedAmount.Size = new System.Drawing.Size(116, 22);
            this.txtCollectedAmount.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnEdit);
            this.groupBox3.Controls.Add(this.btnClose);
            this.groupBox3.Controls.Add(this.btnSave);
            this.groupBox3.Controls.Add(this.btnReport);
            this.groupBox3.Controls.Add(this.btnAReport);
            this.groupBox3.Controls.Add(this.btnIDCard);
            this.groupBox3.Location = new System.Drawing.Point(909, 18);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(209, 121);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnEdit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnEdit.Location = new System.Drawing.Point(112, 52);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(92, 29);
            this.btnEdit.TabIndex = 6;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = false;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnClose.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnClose.Location = new System.Drawing.Point(112, 88);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(92, 29);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnSave.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnSave.Location = new System.Drawing.Point(13, 16);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(97, 29);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnReport
            // 
            this.btnReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnReport.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnReport.Location = new System.Drawing.Point(113, 16);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(92, 29);
            this.btnReport.TabIndex = 4;
            this.btnReport.Text = "Report";
            this.btnReport.UseVisualStyleBackColor = false;
            // 
            // btnAReport
            // 
            this.btnAReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnAReport.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnAReport.Location = new System.Drawing.Point(13, 88);
            this.btnAReport.Name = "btnAReport";
            this.btnAReport.Size = new System.Drawing.Size(97, 29);
            this.btnAReport.TabIndex = 5;
            this.btnAReport.Text = "A.Report";
            this.btnAReport.UseVisualStyleBackColor = false;
            // 
            // btnIDCard
            // 
            this.btnIDCard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnIDCard.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnIDCard.Location = new System.Drawing.Point(13, 52);
            this.btnIDCard.Name = "btnIDCard";
            this.btnIDCard.Size = new System.Drawing.Size(97, 29);
            this.btnIDCard.TabIndex = 5;
            this.btnIDCard.Text = "ID Card";
            this.btnIDCard.UseVisualStyleBackColor = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label31);
            this.groupBox2.Controls.Add(this.txtReferalAddress);
            this.groupBox2.Controls.Add(this.cmbReferal);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.cmbDiseaseName);
            this.groupBox2.Controls.Add(this.cmbAdmittedDept);
            this.groupBox2.Controls.Add(this.cmbAdmittedUnder);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold);
            this.groupBox2.Location = new System.Drawing.Point(717, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(419, 146);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Referal Information";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label31.Location = new System.Drawing.Point(10, 116);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(116, 16);
            this.label31.TabIndex = 11;
            this.label31.Text = "Referal Address :";
            // 
            // txtReferalAddress
            // 
            this.txtReferalAddress.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtReferalAddress.Location = new System.Drawing.Point(135, 115);
            this.txtReferalAddress.Name = "txtReferalAddress";
            this.txtReferalAddress.Size = new System.Drawing.Size(271, 22);
            this.txtReferalAddress.TabIndex = 10;
            // 
            // cmbReferal
            // 
            this.cmbReferal.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbReferal.FormattingEnabled = true;
            this.cmbReferal.Location = new System.Drawing.Point(134, 89);
            this.cmbReferal.Name = "cmbReferal";
            this.cmbReferal.Size = new System.Drawing.Size(272, 23);
            this.cmbReferal.TabIndex = 9;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(65, 91);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(62, 16);
            this.label21.TabIndex = 8;
            this.label21.Text = "Referal :";
            // 
            // cmbDiseaseName
            // 
            this.cmbDiseaseName.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbDiseaseName.FormattingEnabled = true;
            this.cmbDiseaseName.Location = new System.Drawing.Point(135, 66);
            this.cmbDiseaseName.Name = "cmbDiseaseName";
            this.cmbDiseaseName.Size = new System.Drawing.Size(272, 23);
            this.cmbDiseaseName.TabIndex = 4;
            // 
            // cmbAdmittedDept
            // 
            this.cmbAdmittedDept.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbAdmittedDept.FormattingEnabled = true;
            this.cmbAdmittedDept.Location = new System.Drawing.Point(135, 42);
            this.cmbAdmittedDept.Name = "cmbAdmittedDept";
            this.cmbAdmittedDept.Size = new System.Drawing.Size(272, 23);
            this.cmbAdmittedDept.TabIndex = 4;
            // 
            // cmbAdmittedUnder
            // 
            this.cmbAdmittedUnder.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbAdmittedUnder.FormattingEnabled = true;
            this.cmbAdmittedUnder.Location = new System.Drawing.Point(135, 18);
            this.cmbAdmittedUnder.Name = "cmbAdmittedUnder";
            this.cmbAdmittedUnder.Size = new System.Drawing.Size(272, 23);
            this.cmbAdmittedUnder.TabIndex = 4;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label30.Location = new System.Drawing.Point(21, 68);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(106, 16);
            this.label30.TabIndex = 3;
            this.label30.Text = "Disease Name :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(21, 46);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(106, 16);
            this.label22.TabIndex = 3;
            this.label22.Text = "Admitted Dept :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(12, 21);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(115, 16);
            this.label20.TabIndex = 3;
            this.label20.Text = "Admitted Under :";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.dgvRoomBedInformation1);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.cmbRoomBedCategory);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold);
            this.groupBox4.Location = new System.Drawing.Point(717, 157);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(444, 243);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Room/Bed Information";
            // 
            // dgvRoomBedInformation1
            // 
            this.dgvRoomBedInformation1.AllowUserToAddRows = false;
            this.dgvRoomBedInformation1.AllowUserToDeleteRows = false;
            this.dgvRoomBedInformation1.AllowUserToResizeColumns = false;
            this.dgvRoomBedInformation1.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgvRoomBedInformation1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvRoomBedInformation1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRoomBedInformation1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(224)))), ((int)(((byte)(248)))));
            this.dgvRoomBedInformation1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ACCOUNT_GROUP_NAME,
            this.Column2,
            this.CONTROL_NAME,
            this.Column1});
            this.dgvRoomBedInformation1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvRoomBedInformation1.Location = new System.Drawing.Point(3, 45);
            this.dgvRoomBedInformation1.Name = "dgvRoomBedInformation1";
            this.dgvRoomBedInformation1.RowHeadersVisible = false;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvRoomBedInformation1.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvRoomBedInformation1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRoomBedInformation1.Size = new System.Drawing.Size(416, 195);
            this.dgvRoomBedInformation1.TabIndex = 4;
            // 
            // ACCOUNT_GROUP_NAME
            // 
            this.ACCOUNT_GROUP_NAME.DataPropertyName = "BED_NUMBER";
            this.ACCOUNT_GROUP_NAME.HeaderText = "Bed ";
            this.ACCOUNT_GROUP_NAME.Name = "ACCOUNT_GROUP_NAME";
            this.ACCOUNT_GROUP_NAME.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "ROOM_NUMBER";
            this.Column2.HeaderText = "Room";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // CONTROL_NAME
            // 
            this.CONTROL_NAME.DataPropertyName = "CHARGE_PER_DAY";
            this.CONTROL_NAME.HeaderText = "Charge ";
            this.CONTROL_NAME.Name = "CONTROL_NAME";
            this.CONTROL_NAME.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "STATUS_FLAG";
            this.Column1.HeaderText = "Status";
            this.Column1.Name = "Column1";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(31, 21);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(147, 16);
            this.label19.TabIndex = 3;
            this.label19.Text = "Room/Bed Category : ";
            // 
            // cmbRoomBedCategory
            // 
            this.cmbRoomBedCategory.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbRoomBedCategory.FormattingEnabled = true;
            this.cmbRoomBedCategory.Location = new System.Drawing.Point(198, 17);
            this.cmbRoomBedCategory.Name = "cmbRoomBedCategory";
            this.cmbRoomBedCategory.Size = new System.Drawing.Size(209, 23);
            this.cmbRoomBedCategory.TabIndex = 2;
            this.cmbRoomBedCategory.SelectedIndexChanged += new System.EventHandler(this.cmbRoomBedCategory_SelectedIndexChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.textBox2);
            this.groupBox5.Controls.Add(this.textBox1);
            this.groupBox5.Controls.Add(this.dgvRoomBedInformation2);
            this.groupBox5.Controls.Add(this.groupBox3);
            this.groupBox5.Controls.Add(this.label24);
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Controls.Add(this.cmbCollectionType);
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.txtCollectedAmount);
            this.groupBox5.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold);
            this.groupBox5.Location = new System.Drawing.Point(6, 406);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(1125, 150);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Collection  Information";
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.textBox2.Location = new System.Drawing.Point(184, 116);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(116, 22);
            this.textBox2.TabIndex = 6;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.textBox1.Location = new System.Drawing.Point(184, 89);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(116, 22);
            this.textBox1.TabIndex = 5;
            // 
            // dgvRoomBedInformation2
            // 
            this.dgvRoomBedInformation2.AllowUserToAddRows = false;
            this.dgvRoomBedInformation2.AllowUserToDeleteRows = false;
            this.dgvRoomBedInformation2.AllowUserToResizeColumns = false;
            this.dgvRoomBedInformation2.AllowUserToResizeRows = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgvRoomBedInformation2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvRoomBedInformation2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRoomBedInformation2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(224)))), ((int)(((byte)(248)))));
            this.dgvRoomBedInformation2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.dgvRoomBedInformation2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvRoomBedInformation2.Location = new System.Drawing.Point(460, 18);
            this.dgvRoomBedInformation2.Name = "dgvRoomBedInformation2";
            this.dgvRoomBedInformation2.RowHeadersVisible = false;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvRoomBedInformation2.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvRoomBedInformation2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRoomBedInformation2.Size = new System.Drawing.Size(443, 121);
            this.dgvRoomBedInformation2.TabIndex = 4;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "CONTROL_CODE";
            this.dataGridViewTextBoxColumn1.HeaderText = "Control Code";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "ACCOUNT_GROUP_NAME";
            this.dataGridViewTextBoxColumn2.HeaderText = "Room/Bed ";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "CONTROL_NAME";
            this.dataGridViewTextBoxColumn3.HeaderText = "Charge ";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 4;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Status";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(44, 62);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(129, 16);
            this.label24.TabIndex = 3;
            this.label24.Text = "Collected Amount :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(59, 37);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(114, 16);
            this.label23.TabIndex = 3;
            this.label23.Text = "Collection Type :";
            // 
            // cmbCollectionType
            // 
            this.cmbCollectionType.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbCollectionType.FormattingEnabled = true;
            this.cmbCollectionType.Items.AddRange(new object[] {
            "CASH",
            "TT"});
            this.cmbCollectionType.Location = new System.Drawing.Point(186, 34);
            this.cmbCollectionType.Name = "cmbCollectionType";
            this.cmbCollectionType.Size = new System.Drawing.Size(70, 23);
            this.cmbCollectionType.TabIndex = 2;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(50, 116);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(122, 16);
            this.label26.TabIndex = 3;
            this.label26.Text = "Advance Deposit :";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label25.Location = new System.Drawing.Point(64, 89);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(109, 16);
            this.label25.TabIndex = 3;
            this.label25.Text = "Admission Fee :";
            // 
            // Indoor_Patient_Addmission
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(224)))), ((int)(((byte)(248)))));
            this.ClientSize = new System.Drawing.Size(1143, 557);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Indoor_Patient_Addmission";
            this.Text = "Indoor Patient Addmission";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRoomBedInformation1)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRoomBedInformation2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton oldPatientradioButton;
        private System.Windows.Forms.RadioButton newPatientRadioButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbSalution;
        private System.Windows.Forms.TextBox txtMobile;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbDOByear;
        private System.Windows.Forms.ComboBox cmbmaritalStatus;
        private System.Windows.Forms.ComboBox cmbBloodGroup;
        private System.Windows.Forms.ComboBox cmbDOBMonth;
        private System.Windows.Forms.ComboBox cmbDOBdate;
        private System.Windows.Forms.TextBox txtPatientname;
        private System.Windows.Forms.TextBox txtRelation;
        private System.Windows.Forms.TextBox txtAge;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbReligion;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbCity;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbNationality;
        private System.Windows.Forms.ComboBox cmbOccupation;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtMotherinfo;
        private System.Windows.Forms.TextBox txtFatherinfo;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtGurdianMobile;
        private System.Windows.Forms.TextBox txtCollectedAmount;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnReport;
        private System.Windows.Forms.Button btnAReport;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cmbGender;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cmbRoomBedCategory;
        private System.Windows.Forms.DataGridView dgvRoomBedInformation1;
        private System.Windows.Forms.ComboBox cmbAdmittedUnder;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cmbAdmittedDept;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cmbCollectionType;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtGurdianName;
        private System.Windows.Forms.DataGridView dgvRoomBedInformation2;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnIDCard;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.TextBox txtAddmissionNo;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox cmbDiseaseName;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox cmbReferal;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtReferalAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACCOUNT_GROUP_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CONTROL_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
    }
}