﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.BIZ.Setup.Common_Setup;
using IA.BIZ.Setup.Doctor;
using IA.BIZ.Setup.Indoor_Setup;
using IA.BIZ.Setup.Pathology_Setup;

namespace HMSDev.UI
{
    public partial class Indoor_Patient_Addmission : Form
    {
        public Indoor_Patient_Addmission()
        {
            InitializeComponent();
            cmbDOBdate.SelectedIndex = 0;
            cmbDOBMonth.SelectedIndex = 0;
            cmbDOByear.SelectedIndex = 0;
            cmbCollectionType.SelectedIndex = 0;
            BindComboSalution();
            BindComboGender();
            BindComboMaritalStatus();
            BindComboBloodGroup();
            BindComboReligion();
            BindComboNationality();
            BindComboOccupation();
            BindComboIndoorDocotr();
            BindComboPatientDepartment();
            BindComboDisease();
            BindComboRoomCategory();
            BindComboBed();
            BindComboReferencePerson();

        }

        private void BindComboSalution()
        {
            cmbSalution.DataSource = CommonSetupFetch.getALLSALUTION();
            cmbSalution.ValueMember = "SALUTION_ID";
            cmbSalution.DisplayMember = "SALUTION_NAME";
            cmbSalution.SelectedIndex = 0;
        }

        private void BindComboGender()
        {
            cmbGender.DataSource = CommonSetupFetch.getAllGENDER();
            cmbGender.ValueMember = "GENDER_ID";
            cmbGender.DisplayMember = "GENDER_NAME";
            cmbGender.SelectedIndex = 0;
        }

        private void BindComboMaritalStatus()
        {
            cmbmaritalStatus.DataSource = CommonSetupFetch.getAllMARITALSTATUS();
            cmbmaritalStatus.ValueMember = "MARITAL_STATUS_ID";
            cmbmaritalStatus.DisplayMember = "MARITAL_STATUS_NAME";
            cmbmaritalStatus.SelectedIndex = 0;
        }

        private void BindComboBloodGroup()
        {
            cmbBloodGroup.DataSource = CommonSetupFetch.getAllBLOOD_GROUP();
            cmbBloodGroup.ValueMember = "BLOOD_GROUP_ID";
            cmbBloodGroup.DisplayMember = "BLOOD_GROUP_NAME";
            cmbBloodGroup.SelectedIndex = 0;
        }

        private void BindComboReligion()
        {
            cmbReligion.DataSource = CommonSetupFetch.getRELIGION();
            cmbReligion.ValueMember = "RELIGION_ID";
            cmbReligion.DisplayMember = "RELIGION_NAME";
            cmbReligion.SelectedIndex = 0;
        }


        private void BindComboNationality()
        {
            cmbNationality.DataSource = CommonSetupFetch.getAllNATIONALITY();
            cmbNationality.ValueMember = "NATIONALITY_ID";
            cmbNationality.DisplayMember = "NATIONALITY_NAME";
            cmbNationality.SelectedIndex = 0;
        }


        private void BindComboOccupation()
        {
            cmbOccupation.DataSource = CommonSetupFetch.getALLOCCUPATION();
            cmbOccupation.ValueMember = "OCCUPATION_ID";
            cmbOccupation.DisplayMember = "OCCUPATION_NAME";
            cmbOccupation.SelectedIndex = 0;
        }

        private void BindComboIndoorDocotr()
        {
            cmbAdmittedUnder.DataSource = DoctorFetch.getALLIndoorDoctor();
            cmbAdmittedUnder.ValueMember = "DOCTOR_ID";
            cmbAdmittedUnder.DisplayMember = "DOCTOR_NAME";
            cmbAdmittedUnder.SelectedIndex = 0;
        }

        private void BindComboPatientDepartment()
        {
            cmbAdmittedDept.DataSource = IndoorFetch.getALLPatientDepartment();
            cmbAdmittedDept.ValueMember = "PATIENT_DEPARTMENT_ID";
            cmbAdmittedDept.DisplayMember = "PATIENT_DEPARTMENT_NAME";
            cmbAdmittedDept.SelectedIndex = 0;
        }

        private void BindComboDisease()
        {
            cmbDiseaseName.DataSource = IndoorFetch.getALLDisease();
            cmbDiseaseName.ValueMember = "DISEASE_ID";
            cmbDiseaseName.DisplayMember = "DISEASE_NAME";
            cmbDiseaseName.SelectedIndex = 0;
        }

        private void BindComboRoomCategory()
        {
            cmbRoomBedCategory.DataSource = IndoorFetch.getALLRoomCategory();
            cmbRoomBedCategory.ValueMember = "ROOM_CATEGORY_ID";
            cmbRoomBedCategory.DisplayMember = "ROOM_CATEGORY_NAME";
            cmbRoomBedCategory.SelectedIndex = 0;
        }

        private void BindComboBed()
        {
            dgvRoomBedInformation1.AutoGenerateColumns = false;
            dgvRoomBedInformation1.DataSource = IndoorFetch.getALLBed(new Guid( cmbRoomBedCategory.SelectedValue.ToString()));
        }

        private void cmbRoomBedCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindComboBed();
            }
            catch { }
        }

        private void BindComboReferencePerson()
        {
            cmbReferal.DataSource = PathologyFetch.getALLReferencePerson();
            cmbReferal.ValueMember = "PATHOLOGY_REFERENCE_PERSON_ID";
            cmbReferal.DisplayMember = "PATHOLOGY_REFERENCE_PERSON_NAME";
            cmbReferal.SelectedIndex = 0;
        }



        private void btnSave_Click(object sender, EventArgs e)
        {

        }
       
    }
}
