﻿///*************************************************************************************
///	|| Creation History ||
///-------------------------------------------------------------------------------------
/// Copyright	  : Copyright© Dynamic Software Ltd. All rights reserved.
///	NameSpace	  :	IA.UI.Setup.Accounts
/// Class Name    : AccountControl
/// Inherits      : None
///	Author	      :	MD Billal 
///	Purpose	      :	This is a Class to save,update and delete AccountControl
///                 
///	Creation Date :	1/11/2013
/// ====================================================================================
/// || Modification History ||
///  -----------------------------------------------------------------------------------
/// Sl No.	Date:		Author:			Ver:	Area of Change:
/// 1        11/24/2013   Partha          1.01    save,update and delete Button
///	 -----------------------------------------------------------------------------------
///	************************************************************************************

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.BIZ.Setup.Accounts;
using IA.Model.Setup.Accounts;
using POS_SYSTEM;
using DynamicSoft.Security;

namespace IA.UI.Setup.Accounts
{
    public partial class AccountControl : Form
    {
        string user_id = "";
        public AccountControl(string _user_id)
        {
            InitializeComponent();
            dgAccountControl.AutoGenerateColumns = false;
            dgAccountControl.DataSource = AccountsFetch.GetAllControlAccount("", 0, DASSessionInfoClass.COMPANY_ID);
            BindCombo();
            user_id = _user_id;
            lblUserID.Text = user_id;
            cmbAcccountGroup.Focus();
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        private void BindCombo()
        {
            try
            {
                //cmbAcccountGroup.Items.Clear();
                cmbAcccountGroup.DataSource = AccountsFetch.GetAllAccountGroup();
                //cmbAcccountGroup.DataTextField = "ACCOUNT_GROUP_NAME";
                //cmbAcccountGroup.DataValueField = "ACCOUNT_GROUP_CODE";
                //cmbAcccountGroup.DataBind();

                cmbAcccountGroup.ValueMember = "ACCOUNT_GROUP_CODE";
                cmbAcccountGroup.DisplayMember = "ACCOUNT_GROUP_NAME";
            }
            catch { }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int txtLength = txtControlName.Text.Length;
                if (txtLength > 25)
                {
                    DSMessageBox objDSMessageBox = new DSMessageBox("CONTROL NAME NAME is Too Big", 3);
                    objDSMessageBox.Show();
                    return;
                }
                if (txtControlName.Text.Length == 0)
                {
                    DSMessageBox objDSMessageBox = new DSMessageBox("Insert CONTROL NAME", 3);
                    objDSMessageBox.Show();
                    return;
                }
                CONTROL_ACCOUNT objCONTROL_ACCOUNT = new CONTROL_ACCOUNT();
                objCONTROL_ACCOUNT.CONTROL_CODE = "";
                objCONTROL_ACCOUNT.ACCOUNT_GROUP_CODE = cmbAcccountGroup.SelectedValue.ToString();
                objCONTROL_ACCOUNT.CONTROL_NAME = txtControlName.Text;
                objCONTROL_ACCOUNT.COMPANY_ID = DASSessionInfoClass.COMPANY_ID;
                objCONTROL_ACCOUNT.NAME_VALUE_LIST = "I";
                int a = AccountSave.SaveControlGroup(objCONTROL_ACCOUNT);
                if (a > 0)
                {
                    DSMessageBox objDSMessageBox = new DSMessageBox("Save Successfully", 1);
                    objDSMessageBox.Show();
                    txtControlName.Text = "";
                    dgAccountControl.AutoGenerateColumns = false;
                    dgAccountControl.DataSource = AccountsFetch.GetAllControlAccount("", 0, DASSessionInfoClass.COMPANY_ID);
                }
                else
                {
                    DSMessageBox objDSMessageBox = new DSMessageBox("Save Not Successfully", 2);
                    objDSMessageBox.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void dgAccountControl_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                CONTROL_ACCOUNT objCONTROL_ACCOUNT = new CONTROL_ACCOUNT();
                objCONTROL_ACCOUNT = AccountsFetch.GetControlAccount(dgAccountControl.Rows[e.RowIndex].Cells[0].Value.ToString(), DASSessionInfoClass.COMPANY_ID);
                txtControlceode.Text = dgAccountControl.Rows[e.RowIndex].Cells[0].Value.ToString();
                cmbAcccountGroup.SelectedValue = objCONTROL_ACCOUNT.ACCOUNT_GROUP_CODE;
                txtControlName.Text = objCONTROL_ACCOUNT.CONTROL_NAME;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                int txtLength = txtControlName.Text.Length;
                if (txtLength > 25)
                {
                    DSMessageBox objDSMessageBox = new DSMessageBox("CONTROL NAME NAME is Too Big", 3);
                    objDSMessageBox.Show();
                    return;
                }
                if (txtControlName.Text.Length == 0)
                {
                    DSMessageBox objDSMessageBox = new DSMessageBox("Insert CONTROL NAME", 3);
                    objDSMessageBox.Show();
                    return;
                }

                string CONTROL_CODE = txtControlceode.Text;
                CONTROL_ACCOUNT objCONTROL_ACCOUNT = new CONTROL_ACCOUNT();
                objCONTROL_ACCOUNT.CONTROL_CODE = CONTROL_CODE;
                objCONTROL_ACCOUNT.CONTROL_NAME = txtControlName.Text;
                objCONTROL_ACCOUNT.COMPANY_ID = DASSessionInfoClass.COMPANY_ID;
                int i = AccountSave.UpdateCONTROL_ACCOUNT(objCONTROL_ACCOUNT);
                if (i > 0)
                {
                    dgAccountControl.DataSource = AccountsFetch.GetAllControlAccount("", 0, DASSessionInfoClass.COMPANY_ID);
                    DSMessageBox objDSMessageBox = new DSMessageBox("Update Successfully", 1);
                    objDSMessageBox.Show();

                    clearfield();
                }
                else
                {
                    DSMessageBox objDSMessageBox = new DSMessageBox("Update Not Successfully", 2);
                    objDSMessageBox.Show();
                    clearfield();
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
        private void clearfield()
        {

            txtControlName.Text = "";
            cmbAcccountGroup.SelectedIndex = 0;
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete this feature?",

        "Confirm Delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    int i = 0;
                    CONTROL_ACCOUNT objCONTROL_ACCOUNT = new CONTROL_ACCOUNT();
                    objCONTROL_ACCOUNT.CONTROL_CODE = txtControlceode.Text;
                    objCONTROL_ACCOUNT.COMPANY_ID = DASSessionInfoClass.COMPANY_ID;
                    //string CONTROL_CODE = txtControlceode.Text;
                    i = AccountSave.DeleteCONTROL_ACCOUNT(objCONTROL_ACCOUNT);
                    if (i > 0)
                    {
                        dgAccountControl.DataSource = AccountsFetch.GetAllControlAccount("", 0, DASSessionInfoClass.COMPANY_ID);
                        DSMessageBox objDSMessageBox = new DSMessageBox("Delete Successfully", 1);
                        objDSMessageBox.Show();
                        clearfield();
                    }
                    else
                    {
                        DSMessageBox objDSMessageBox = new DSMessageBox("Delete Not Successfully", 2);
                        objDSMessageBox.Show();
                        clearfield();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void txtControlName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }
        private void btnAdd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }
        private void cmbAcccountGroup_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
        }
    }
}
