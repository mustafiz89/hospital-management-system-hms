﻿namespace IA.UI.Setup.Accounts
{
    partial class AccountControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dgAccountControl = new System.Windows.Forms.DataGridView();
            this.CONTROL_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACCOUNT_GROUP_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CONTROL_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblUserID = new System.Windows.Forms.Label();
            this.txtControlceode = new System.Windows.Forms.TextBox();
            this.cmbAcccountGroup = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtControlName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAccountControl)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(7, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(856, 314);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Control Accounts";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnClose);
            this.groupBox3.Controls.Add(this.btnAdd);
            this.groupBox3.Controls.Add(this.btnEdit);
            this.groupBox3.Controls.Add(this.btnDelete);
            this.groupBox3.Location = new System.Drawing.Point(9, 253);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(407, 51);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnClose.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnClose.Location = new System.Drawing.Point(303, 16);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(92, 29);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnAdd.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnAdd.Location = new System.Drawing.Point(10, 16);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(100, 29);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            this.btnAdd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.btnAdd_KeyPress);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnEdit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnEdit.Location = new System.Drawing.Point(113, 16);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(92, 29);
            this.btnEdit.TabIndex = 4;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnDelete.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnDelete.Location = new System.Drawing.Point(208, 16);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(92, 29);
            this.btnDelete.TabIndex = 5;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.dgAccountControl);
            this.groupBox4.Location = new System.Drawing.Point(424, 16);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(421, 288);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            // 
            // dgAccountControl
            // 
            this.dgAccountControl.AllowUserToAddRows = false;
            this.dgAccountControl.AllowUserToDeleteRows = false;
            this.dgAccountControl.AllowUserToResizeColumns = false;
            this.dgAccountControl.AllowUserToResizeRows = false;
            this.dgAccountControl.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(224)))), ((int)(((byte)(248)))));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgAccountControl.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgAccountControl.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CONTROL_CODE,
            this.ACCOUNT_GROUP_NAME,
            this.CONTROL_NAME});
            this.dgAccountControl.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgAccountControl.Location = new System.Drawing.Point(8, 17);
            this.dgAccountControl.Name = "dgAccountControl";
            this.dgAccountControl.RowHeadersVisible = false;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgAccountControl.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgAccountControl.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgAccountControl.Size = new System.Drawing.Size(404, 265);
            this.dgAccountControl.TabIndex = 0;
            this.dgAccountControl.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgAccountControl_CellClick);
            // 
            // CONTROL_CODE
            // 
            this.CONTROL_CODE.DataPropertyName = "CONTROL_CODE";
            this.CONTROL_CODE.HeaderText = "Control Code";
            this.CONTROL_CODE.Name = "CONTROL_CODE";
            this.CONTROL_CODE.Visible = false;
            // 
            // ACCOUNT_GROUP_NAME
            // 
            this.ACCOUNT_GROUP_NAME.DataPropertyName = "ACCOUNT_GROUP_NAME";
            this.ACCOUNT_GROUP_NAME.HeaderText = "Account Group";
            this.ACCOUNT_GROUP_NAME.Name = "ACCOUNT_GROUP_NAME";
            this.ACCOUNT_GROUP_NAME.ReadOnly = true;
            this.ACCOUNT_GROUP_NAME.Width = 200;
            // 
            // CONTROL_NAME
            // 
            this.CONTROL_NAME.DataPropertyName = "CONTROL_NAME";
            this.CONTROL_NAME.HeaderText = "Control Name";
            this.CONTROL_NAME.Name = "CONTROL_NAME";
            this.CONTROL_NAME.ReadOnly = true;
            this.CONTROL_NAME.Width = 200;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblUserID);
            this.groupBox2.Controls.Add(this.txtControlceode);
            this.groupBox2.Controls.Add(this.cmbAcccountGroup);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtControlName);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(10, 16);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(407, 236);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // lblUserID
            // 
            this.lblUserID.AutoSize = true;
            this.lblUserID.Location = new System.Drawing.Point(299, 177);
            this.lblUserID.Name = "lblUserID";
            this.lblUserID.Size = new System.Drawing.Size(57, 18);
            this.lblUserID.TabIndex = 30;
            this.lblUserID.Text = "UserID";
            this.lblUserID.Visible = false;
            // 
            // txtControlceode
            // 
            this.txtControlceode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtControlceode.Location = new System.Drawing.Point(138, 192);
            this.txtControlceode.Name = "txtControlceode";
            this.txtControlceode.Size = new System.Drawing.Size(140, 26);
            this.txtControlceode.TabIndex = 14;
            this.txtControlceode.Visible = false;
            // 
            // cmbAcccountGroup
            // 
            this.cmbAcccountGroup.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbAcccountGroup.FormattingEnabled = true;
            this.cmbAcccountGroup.Location = new System.Drawing.Point(134, 21);
            this.cmbAcccountGroup.Name = "cmbAcccountGroup";
            this.cmbAcccountGroup.Size = new System.Drawing.Size(231, 23);
            this.cmbAcccountGroup.TabIndex = 1;
            this.cmbAcccountGroup.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbAcccountGroup_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 18);
            this.label4.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(11, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "Account Group :";
            // 
            // txtControlName
            // 
            this.txtControlName.Font = new System.Drawing.Font("Arial", 9F);
            this.txtControlName.Location = new System.Drawing.Point(134, 54);
            this.txtControlName.Name = "txtControlName";
            this.txtControlName.Size = new System.Drawing.Size(231, 21);
            this.txtControlName.TabIndex = 2;
            this.txtControlName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtControlName_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(18, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 16);
            this.label3.TabIndex = 8;
            this.label3.Text = "Control Name :";
            // 
            // AccountControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(224)))), ((int)(((byte)(248)))));
            this.ClientSize = new System.Drawing.Size(879, 335);
            this.Controls.Add(this.groupBox1);
            this.Name = "AccountControl";
            this.Text = "AccountControl";
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgAccountControl)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dgAccountControl;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbAcccountGroup;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtControlName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn CONTROL_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACCOUNT_GROUP_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CONTROL_NAME;
        private System.Windows.Forms.TextBox txtControlceode;
        private System.Windows.Forms.Label lblUserID;
    }
}