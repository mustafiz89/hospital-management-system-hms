﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.OT_Setup;
using IA.BIZ.Setup.OT_Setup;
using POS_SYSTEM;


namespace HMSDev.UI.Setup
{
    public partial class OT_Comments_Setup : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public OT_Comments_Setup()
        {
            InitializeComponent();
            OTName();
            dgvOTComments.AutoGenerateColumns = false;
            dgvOTComments.DataSource = OTFetch.getALLOTComments();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtOTComments.Text == "")
            {
                er++;
                ep.SetError(txtOTComments, "Name Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                OT_COMMENTS objOTComments = new OT_COMMENTS();
                objOTComments.OT_ID =new Guid (cmbOTName.SelectedValue.ToString());
                objOTComments.OT_COMMENT = txtOTComments.Text;


                int i = OTSetup.saveOTComments(objOTComments);

                if (i > 0)
                {
                    dgvOTComments.AutoGenerateColumns = false;
                    dgvOTComments.DataSource = OTFetch.getALLOTComments();
                    MessageBox.Show("Save Successfully");
                    txtOTComments.Text = "";
                }
                else
                    MessageBox.Show("Save Not Successfully");

            }
        }


        private void OTName()
        {
            cmbOTName.DataSource = OTFetch.getALLOT();
            cmbOTName.ValueMember = "OT_ID";
            cmbOTName.DisplayMember = "OT_NAME";
        }


        private void OT_Comments_Setup_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbOTName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtOIComments_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbOTCategoryName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void btnEdit_Click(object sender, EventArgs e)
        {
            OT_COMMENTS objOTComments = new OT_COMMENTS();
            objOTComments.OT_COMMENTS_ID =new Guid (txtOTCommentsID.Text);
            objOTComments.OT_ID = new Guid(cmbOTName.SelectedValue.ToString());
            objOTComments.OT_COMMENT = txtOTComments.Text;


            int i = OTSetup.UpdateOTComments(objOTComments);

            if (i > 0)
            {
                dgvOTComments.AutoGenerateColumns = false;
                dgvOTComments.DataSource = OTFetch.getALLOTComments();
                MessageBox.Show("Update Successfully");

                txtOTCommentsID.Text = "";
                txtOTComments.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvOIComments_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvOTComments.SelectedRows[0];
            txtOTCommentsID.Text = dr.Cells[0].Value.ToString();
            cmbOTName.SelectedIndex =cmbOTName.FindString (dr.Cells[1].Value.ToString());
            txtOTComments.Text = dr.Cells[2].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            OT_COMMENTS objOTComments = new OT_COMMENTS();
            objOTComments.OT_COMMENTS_ID = new Guid(txtOTCommentsID.Text);

            int i = OTSetup.DeleteOTComments(objOTComments);

            if (i > 0)
            {
                dgvOTComments.AutoGenerateColumns = false;
                dgvOTComments.DataSource = OTFetch.getALLOTComments();
                MessageBox.Show("Delete Successfully");

                txtOTCommentsID.Text = "";
                txtOTComments.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
