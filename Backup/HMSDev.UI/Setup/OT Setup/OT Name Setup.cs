﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.OT_Setup;
using IA.BIZ.Setup.OT_Setup;

namespace HMSDev.UI.Setup.OT_Setup
{
    public partial class OT_Setup : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public OT_Setup()
        {
            InitializeComponent();
            BindComboOTCategory();
            cmbActive.SelectedIndex = 0;
            dgvOT.AutoGenerateColumns = false;
            dgvOT.DataSource = OTFetch.getALLOT();
        }

        

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtOTName.Text == "")
            {
                er++;
                ep.SetError(txtOTName, "Name Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                OT objOTName = new OT();
                objOTName.OT_CATEGORY_ID =new Guid (cmbOTCategoryName.SelectedValue.ToString());
                objOTName.OT_NAME = txtOTName.Text;

                if (cmbActive.SelectedIndex == 0)
                {
                    objOTName.STATUS_FLAG = "A";
                }
                else
                    objOTName.STATUS_FLAG = "U";

                int i = OTSetup.saveOTName(objOTName);

                if (i > 0)
                {
                    dgvOT.AutoGenerateColumns = false;
                    dgvOT.DataSource = OTFetch.getALLOT();
                    MessageBox.Show("Save Successfully");

                    txtOTName.Text = "";
                }
                else
                    MessageBox.Show("Save Not Successfully");

            }
        }

        private void OT_Setup_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtOTName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void BindComboOTCategory()
        {
            cmbOTCategoryName.DataSource = OTFetch.getALLOTCategory();
            cmbOTCategoryName.ValueMember = "OT_CATEGORY_ID";
            cmbOTCategoryName.DisplayMember = "OT_CATEGORY_NAME";
            cmbOTCategoryName.SelectedIndex = 0;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            OT objOTName = new OT();
            objOTName.OT_ID = new Guid(txtOTID.Text.ToString());
            objOTName.OT_CATEGORY_ID = new Guid(cmbOTCategoryName.SelectedValue.ToString());
            objOTName.OT_NAME = txtOTName.Text;

            if (cmbActive.SelectedIndex == 0)
            {
                objOTName.STATUS_FLAG = "A";
            }
            else
                objOTName.STATUS_FLAG = "U";

            int i = OTSetup.UpdateOTName(objOTName);

            if (i > 0)
            {
                dgvOT.AutoGenerateColumns = false;
                dgvOT.DataSource = OTFetch.getALLOT();
                MessageBox.Show("Update Successfully");

                txtOTID.Text = "";
                txtOTName.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvOICategory_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvOT.SelectedRows[0];
            txtOTID.Text = dr.Cells[0].Value.ToString();
            txtOTName.Text = dr.Cells[1].Value.ToString();
            cmbOTCategoryName.SelectedIndex = cmbOTCategoryName.FindString(dr.Cells[2].Value.ToString());

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            OT objOTName = new OT();
            objOTName.OT_ID = new Guid(txtOTID.Text.ToString());

            int i = OTSetup.DeleteOTName(objOTName);

            if (i > 0)
            {
                dgvOT.AutoGenerateColumns = false;
                dgvOT.DataSource = OTFetch.getALLOT();
                MessageBox.Show("Delete Successfully");

                txtOTID.Text = "";
                txtOTName.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
