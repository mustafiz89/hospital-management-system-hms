﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.OT_Setup;
using IA.BIZ.Setup.OT_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.OT_Setup
{
    public partial class OT__Category : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public OT__Category()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvOTCategory.AutoGenerateColumns = false;
            dgvOTCategory.DataSource = OTFetch.getALLOTCategory();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtOTCategoryName.Text == "")
            {
                er++;
                ep.SetError(txtOTCategoryName, "OT Category Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                OT_CATEGORY objOT_CATEGORY = new OT_CATEGORY();
                objOT_CATEGORY.OT_CATEGORY_NAME = txtOTCategoryName.Text;
                if (cmbActive.SelectedIndex == 0)
                    objOT_CATEGORY.STATUS_FLAG = "A";
                else
                    objOT_CATEGORY.STATUS_FLAG = "U";

                int i = OTSetup.saveOT_CATEGORY(objOT_CATEGORY);

                if (i > 0)
                {
                    dgvOTCategory.AutoGenerateColumns = false;
                    dgvOTCategory.DataSource = OTFetch.getALLOTCategory();
                    MessageBox.Show("Save Successfully");

                    txtOTCategoryName.Text = "";
                }
                else
                    MessageBox.Show("Save Not Successfully");
            }
        }

        private void txtOTCategoryName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            OT_CATEGORY objOT_CATEGORY = new OT_CATEGORY();
            objOT_CATEGORY.OT_CATEGORY_ID =new Guid (txtOTCategoryID.Text);
            objOT_CATEGORY.OT_CATEGORY_NAME = txtOTCategoryName.Text;
            if (cmbActive.SelectedIndex == 0)
                objOT_CATEGORY.STATUS_FLAG = "A";
            else
                objOT_CATEGORY.STATUS_FLAG = "U";

            int i = OTSetup.UpdateOT_Category(objOT_CATEGORY);

            if (i > 0)
            {
                dgvOTCategory.AutoGenerateColumns = false;
                dgvOTCategory.DataSource = OTFetch.getALLOTCategory();
                MessageBox.Show("Update Successfully");

                txtOTCategoryID.Text = "";
                txtOTCategoryName.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvOTCategory_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvOTCategory.SelectedRows[0];
            txtOTCategoryID.Text = dr.Cells[0].Value.ToString();
            txtOTCategoryName.Text = dr.Cells[1].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            OT_CATEGORY objOT_CATEGORY = new OT_CATEGORY();
            objOT_CATEGORY.OT_CATEGORY_ID = new Guid(txtOTCategoryID.Text);

            int i = OTSetup.DeleteOT_Category(objOT_CATEGORY);

            if (i > 0)
            {
                dgvOTCategory.AutoGenerateColumns = false;
                dgvOTCategory.DataSource = OTFetch.getALLOTCategory();
                MessageBox.Show("Delete Successfully");

                txtOTCategoryID.Text = "";
                txtOTCategoryName.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
