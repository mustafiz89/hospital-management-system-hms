﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Outdoor_Setup;
using IA.BIZ.Setup.Outdoor_Setup;
using IA.BIZ.Setup.Doctor;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Outdoor
{
    public partial class Doctor_Charge_Setup : Form
    {
        public Doctor_Charge_Setup()
        {
            InitializeComponent();
            BindComboDoctorCharge();
            cmbActive.SelectedIndex = 0;
            try
            {
                dgvOutdoorDoctorCharge.AutoGenerateColumns = false;
                dgvOutdoorDoctorCharge.DataSource = OutdoorFetch.getALLDoctorCharge();
            }
            catch { }
        }

        

        private void btnAdd_Click(object sender, EventArgs e)
        {
            OUTDORR_DOCTOR_CHARGE objDoctorCharge = new OUTDORR_DOCTOR_CHARGE();
            objDoctorCharge.DOCTOR_ID =new Guid (cmbOutdoorDoctorName.SelectedValue.ToString());
            objDoctorCharge.OUTDORR_DOCTOR_NEW_VISIT_CHARGE = Convert.ToDecimal(txtNewVisitCharge.Text);
            objDoctorCharge.OUTDORR_DOCTOR_FOLLOW_UP_CHARGE = Convert.ToDecimal(txtFollowUpCharge.Text);
            objDoctorCharge.OUTDORR_DOCTOR_CALL_ON_CHARGE = Convert.ToDecimal(txtCallOnCharge.Text);

            if (cmbActive.SelectedIndex == 0)
            {
                objDoctorCharge.STATUS_FLAG = "A";
            }
            else
                objDoctorCharge.STATUS_FLAG = "U";


            int i = OutdoorSetup.saveDoctorCharge(objDoctorCharge);

            if (i > 0)
            {
                dgvOutdoorDoctorCharge.AutoGenerateColumns = false;
                dgvOutdoorDoctorCharge.DataSource = OutdoorFetch.getALLDoctorCharge();
                MessageBox.Show("Save Successfully");

                txtNewVisitCharge.Text = "";
                txtFollowUpCharge.Text = "";
                txtCallOnCharge.Text = "";
            }
            else
                MessageBox.Show("Save Not Successfully");
        }

        private void BindComboDoctorCharge()
        {
            cmbOutdoorDoctorName.DataSource = DoctorFetch.getALLOutdoorDoctor();
            cmbOutdoorDoctorName.ValueMember = "DOCTOR_ID";
            cmbOutdoorDoctorName.DisplayMember = "DOCTOR_NAME";
            //cmbOutdoorDoctorName.SelectedIndex = 0;
        }

        private void Doctor_Charge_Setup_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtNewVisitCharge_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtFollowUpCharge_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtCallOnCharge_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            OUTDORR_DOCTOR_CHARGE objDoctorCharge = new OUTDORR_DOCTOR_CHARGE();
            objDoctorCharge.OUTDORR_DOCTOR_CHARGE_ID =new Guid (txtChargeID.Text);
            objDoctorCharge.DOCTOR_ID = new Guid(cmbOutdoorDoctorName.SelectedValue.ToString());
            objDoctorCharge.OUTDORR_DOCTOR_NEW_VISIT_CHARGE = Convert.ToDecimal(txtNewVisitCharge.Text);
            objDoctorCharge.OUTDORR_DOCTOR_FOLLOW_UP_CHARGE = Convert.ToDecimal(txtFollowUpCharge.Text);
            objDoctorCharge.OUTDORR_DOCTOR_CALL_ON_CHARGE = Convert.ToDecimal(txtCallOnCharge.Text);

            if (cmbActive.SelectedIndex == 0)
            {
                objDoctorCharge.STATUS_FLAG = "A";
            }
            else
                objDoctorCharge.STATUS_FLAG = "U";


            int i = OutdoorSetup.UpdateDoctorCharge(objDoctorCharge);

            if (i > 0)
            {
                dgvOutdoorDoctorCharge.AutoGenerateColumns = false;
                dgvOutdoorDoctorCharge.DataSource = OutdoorFetch.getALLDoctorCharge();
                MessageBox.Show("Update Successfully");

                txtChargeID.Text = "";
                txtNewVisitCharge.Text = "";
                txtFollowUpCharge.Text = "";
                txtCallOnCharge.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvOutdoorDoctorCharge_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvOutdoorDoctorCharge.SelectedRows[0];
            txtChargeID.Text = dr.Cells[0].Value.ToString();
            cmbOutdoorDoctorName.SelectedIndex = cmbOutdoorDoctorName.FindString(dr.Cells[1].Value.ToString());
            txtNewVisitCharge.Text = dr.Cells[2].Value.ToString();
            txtFollowUpCharge.Text = dr.Cells[3].Value.ToString();
            txtCallOnCharge.Text = dr.Cells[4].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            OUTDORR_DOCTOR_CHARGE objDoctorCharge = new OUTDORR_DOCTOR_CHARGE();
            objDoctorCharge.OUTDORR_DOCTOR_CHARGE_ID = new Guid(txtChargeID.Text);

            int i = OutdoorSetup.DeleteDoctorCharge(objDoctorCharge);

            if (i > 0)
            {
                dgvOutdoorDoctorCharge.AutoGenerateColumns = false;
                dgvOutdoorDoctorCharge.DataSource = OutdoorFetch.getALLDoctorCharge();
                MessageBox.Show("Delete Successfully");

                txtChargeID.Text = "";
                txtNewVisitCharge.Text = "";
                txtFollowUpCharge.Text = "";
                txtCallOnCharge.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }

        private void cmbOutdoorDoctorName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }
    }
}
