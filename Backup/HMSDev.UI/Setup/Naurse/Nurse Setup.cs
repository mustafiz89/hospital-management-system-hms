﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Nurse_Setup;
using IA.BIZ.Setup.Nurse_Setup;
using IA.BIZ.Setup.Indoor_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Naurse
{
    public partial class Nurse_Setup : Form
    {
        public Nurse_Setup()
        {
            InitializeComponent();
            BindComboFloorNo();
            dgvNurseInfo.AutoGenerateColumns = false;
            dgvNurseInfo.DataSource = NurseFetch.getALLNurse();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            NURSE objNurse = new NURSE();
            objNurse.NURSE_NAME = txtNurseName.Text;
            objNurse.FLOOR_ID =new Guid (cmbFloorNo.SelectedValue.ToString());
            objNurse.NURSE_ADDRESS = txtNurseAddress.Text;
            objNurse.NURSE_CONTACT = txtContact.Text;

            int i = NurseSave.saveNurse(objNurse);

            if (i > 0)
            {
                dgvNurseInfo.AutoGenerateColumns = false;
                dgvNurseInfo.DataSource = NurseFetch.getALLNurse();
                MessageBox.Show("Save Successfully");

                txtNurseName.Text = "";
                txtNurseAddress.Text = "";
                txtContact.Text = "";
            }
            else
                MessageBox.Show("Save Not Successfully");
        }

        private void Nurse_Setup_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbFloorNo_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtNurseAddress_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtContact_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtNurseName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void BindComboFloorNo()
        {
            cmbFloorNo.DataSource = IndoorFetch.getALLFLOOR();
            cmbFloorNo.ValueMember = "FLOOR_ID";
            cmbFloorNo.DisplayMember = "FLOOR_NAME";
            cmbFloorNo.SelectedIndex = 0;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            NURSE objNurse = new NURSE();
            objNurse.NURSE_ID =new Guid (txtNurseID.Text);
            objNurse.NURSE_NAME = txtNurseName.Text;
            objNurse.FLOOR_ID = new Guid(cmbFloorNo.SelectedValue.ToString());
            objNurse.NURSE_ADDRESS = txtNurseAddress.Text;
            objNurse.NURSE_CONTACT = txtContact.Text;

            int i = NurseSave.UpdateNurse(objNurse);

            if (i > 0)
            {
                dgvNurseInfo.AutoGenerateColumns = false;
                dgvNurseInfo.DataSource = NurseFetch.getALLNurse();
                MessageBox.Show("Update Successfully");

                txtNurseID.Text = "";
                txtNurseName.Text = "";
                txtNurseAddress.Text = "";
                txtContact.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvNurseInfo_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvNurseInfo.SelectedRows[0];
            txtNurseID.Text = dr.Cells[0].Value.ToString();
            txtNurseName.Text = dr.Cells[1].Value.ToString();
            cmbFloorNo.SelectedIndex=cmbFloorNo.FindString(dr.Cells[2].Value.ToString());
            txtNurseAddress.Text = dr.Cells[3].Value.ToString();
            txtContact.Text = dr.Cells[4].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            NURSE objNurse = new NURSE();
            objNurse.NURSE_ID = new Guid(txtNurseID.Text);

            int i = NurseSave.DeleteNurse(objNurse);

            if (i > 0)
            {
                dgvNurseInfo.AutoGenerateColumns = false;
                dgvNurseInfo.DataSource = NurseFetch.getALLNurse();
                MessageBox.Show("Delete Successfully");

                txtNurseID.Text = "";
                txtNurseName.Text = "";
                txtNurseAddress.Text = "";
                txtContact.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
