﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Pathology_Setup;
using IA.BIZ.Setup.Pathology_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Pathology
{
    public partial class Pathology_Test_Item_Category_Setup : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Pathology_Test_Item_Category_Setup()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvPathologyCategory.AutoGenerateColumns = false;
            dgvPathologyCategory.DataSource = PathologyFetch.getALLPathologyCategory();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtPathologyCategoryName.Text == "")
            {
                er++;
                ep.SetError(txtPathologyCategoryName, "Name Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                PATHOLOGY_TEST_ITEM_CATEGORY objPathologyTestItemCategory = new PATHOLOGY_TEST_ITEM_CATEGORY();
                objPathologyTestItemCategory.PATHOLOGY_TEST_ITEM_CATEGORY_NAME = txtPathologyCategoryName.Text;

                if (cmbActive.SelectedIndex == 0)
                    objPathologyTestItemCategory.STATUS_FLAG = "A";
                else
                    objPathologyTestItemCategory.STATUS_FLAG = "U";


                int i = PathologySetup.savePathologyTestItemCategory(objPathologyTestItemCategory);

                if (i > 0)
                {
                    dgvPathologyCategory.AutoGenerateColumns = false;
                    dgvPathologyCategory.DataSource = PathologyFetch.getALLPathologyCategory();
                    MessageBox.Show("Save Successfully");
                }
                else
                    MessageBox.Show("Save Not Successfully");

                txtPathologyCategoryName.Text = "";
            }
        }

        private void Pathology_Test_Item_Category_Setup_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            PATHOLOGY_TEST_ITEM_CATEGORY objPathologyTestItemCategory = new PATHOLOGY_TEST_ITEM_CATEGORY();
            objPathologyTestItemCategory.PATHOLOGY_TEST_ITEM_CATEGORY_ID =new Guid (txtCategoryID.Text);
            objPathologyTestItemCategory.PATHOLOGY_TEST_ITEM_CATEGORY_NAME = txtPathologyCategoryName.Text;

            if (cmbActive.SelectedIndex == 0)
                objPathologyTestItemCategory.STATUS_FLAG = "A";
            else
                objPathologyTestItemCategory.STATUS_FLAG = "U";


            int i = PathologySetup.UpdatePathologyTestItemCategory(objPathologyTestItemCategory);

            if (i > 0)
            {
                dgvPathologyCategory.AutoGenerateColumns = false;
                dgvPathologyCategory.DataSource = PathologyFetch.getALLPathologyCategory();
                MessageBox.Show("Update Successfully");

                txtCategoryID.Text = "";
                txtPathologyCategoryName.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvPathologyCategory_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvPathologyCategory.SelectedRows[0];
            txtCategoryID.Text = dr.Cells[0].Value.ToString();
            txtPathologyCategoryName.Text = dr.Cells[1].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            PATHOLOGY_TEST_ITEM_CATEGORY objPathologyTestItemCategory = new PATHOLOGY_TEST_ITEM_CATEGORY();
            objPathologyTestItemCategory.PATHOLOGY_TEST_ITEM_CATEGORY_ID = new Guid(txtCategoryID.Text);

            int i = PathologySetup.DeletePathologyTestItemCategory(objPathologyTestItemCategory);

            if (i > 0)
            {
                dgvPathologyCategory.AutoGenerateColumns = false;
                dgvPathologyCategory.DataSource = PathologyFetch.getALLPathologyCategory();
                MessageBox.Show("Delete Successfully");

                txtCategoryID.Text = "";
                txtPathologyCategoryName.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }

    }
}
