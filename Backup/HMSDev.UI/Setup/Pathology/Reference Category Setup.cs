﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Pathology_Setup;
using IA.BIZ.Setup.Pathology_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Pathology
{
    public partial class Reference_Category_Setup : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Reference_Category_Setup()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvReferenceCategoryInfo.AutoGenerateColumns = false;
            dgvReferenceCategoryInfo.DataSource = PathologyFetch.getALLReferenceCategory();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtReferenceCategoryName.Text == "")
            {
                er++;
                ep.SetError(txtReferenceCategoryName, "Name Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                PATHOLOGY_REFERENCE_CATEGORY objPathologyReferenceCategory = new PATHOLOGY_REFERENCE_CATEGORY();
                objPathologyReferenceCategory.PATHOLOGY_REFERENCE_CATEGORY_NAME = txtReferenceCategoryName.Text;

                if (cmbActive.SelectedIndex == 0)
                    objPathologyReferenceCategory.STATUS_FLAG = "A";
                else
                    objPathologyReferenceCategory.STATUS_FLAG = "U";


                int i = PathologySetup.savePathologyReferenceCategory(objPathologyReferenceCategory);

                if (i > 0)
                {
                    dgvReferenceCategoryInfo.AutoGenerateColumns = false;
                    dgvReferenceCategoryInfo.DataSource = PathologyFetch.getALLReferenceCategory();
                    MessageBox.Show("Save Successfully");

                    txtReferenceCategoryName.Text = "";
                }
                else
                    MessageBox.Show("Save Not Successfully");
            }
        }

        private void txtReferenceCategoryName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            PATHOLOGY_REFERENCE_CATEGORY objPathologyReferenceCategory = new PATHOLOGY_REFERENCE_CATEGORY();
            objPathologyReferenceCategory.PATHOLOGY_REFERENCE_CATEGORY_ID =new Guid (txtReferenceCtegoryID.Text);
            objPathologyReferenceCategory.PATHOLOGY_REFERENCE_CATEGORY_NAME = txtReferenceCategoryName.Text;

            if (cmbActive.SelectedIndex == 0)
                objPathologyReferenceCategory.STATUS_FLAG = "A";
            else
                objPathologyReferenceCategory.STATUS_FLAG = "U";


            int i = PathologySetup.UpdatePathologyReferenceCategory(objPathologyReferenceCategory);

            if (i > 0)
            {
                dgvReferenceCategoryInfo.AutoGenerateColumns = false;
                dgvReferenceCategoryInfo.DataSource = PathologyFetch.getALLReferenceCategory();
                MessageBox.Show("Update Successfully");

                txtReferenceCtegoryID.Text = "";
                txtReferenceCategoryName.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvReferenceCategoryInfo_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvReferenceCategoryInfo.SelectedRows[0];
            txtReferenceCtegoryID.Text = dr.Cells[0].Value.ToString();
            txtReferenceCategoryName.Text = dr.Cells[1].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            PATHOLOGY_REFERENCE_CATEGORY objPathologyReferenceCategory = new PATHOLOGY_REFERENCE_CATEGORY();
            objPathologyReferenceCategory.PATHOLOGY_REFERENCE_CATEGORY_ID = new Guid(txtReferenceCtegoryID.Text);

            int i = PathologySetup.DeletePathologyReferenceCategory(objPathologyReferenceCategory);

            if (i > 0)
            {
                dgvReferenceCategoryInfo.AutoGenerateColumns = false;
                dgvReferenceCategoryInfo.DataSource = PathologyFetch.getALLReferenceCategory();
                MessageBox.Show("Delete Successfully");

                txtReferenceCtegoryID.Text = "";
                txtReferenceCategoryName.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
