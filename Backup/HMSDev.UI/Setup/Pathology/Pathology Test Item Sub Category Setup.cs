﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Pathology_Setup;
using IA.BIZ.Setup.Pathology_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Pathology
{
    public partial class Pathology_Test_Item_Sub_Category_Setup : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Pathology_Test_Item_Sub_Category_Setup()
        {
            InitializeComponent();
            BindComboPathologyCategoryName();
            cmbActive.SelectedIndex = 0;
            dgvPathologySubCategory.AutoGenerateColumns = false;
            dgvPathologySubCategory.DataSource = PathologyFetch.getALLPathologySubCategory();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtSubCategoryName.Text == "")
            {
                er++;
                ep.SetError(txtSubCategoryName, "Name Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                PATHOLOGY_TEST_ITEM_SUB_CATEGORY objPathologyTestItemSubCategory = new PATHOLOGY_TEST_ITEM_SUB_CATEGORY();
                objPathologyTestItemSubCategory.PATHOLOGY_TEST_ITEM_CATEGORY_ID =new Guid (cmbPathologyCategoryName.SelectedValue.ToString());
                objPathologyTestItemSubCategory.PATHOLOGY_TEST_ITEM_SUB_CATEGORY_NAME = txtSubCategoryName.Text;

                if (cmbActive.SelectedIndex == 0)
                    objPathologyTestItemSubCategory.STATUS_FLAG = "A";
                else
                    objPathologyTestItemSubCategory.STATUS_FLAG = "U";


                int i = PathologySetup.savePathologyTestItemSubCategory(objPathologyTestItemSubCategory);

                if (i > 0)
                {
                    dgvPathologySubCategory.AutoGenerateColumns = false;
                    dgvPathologySubCategory.DataSource = PathologyFetch.getALLPathologySubCategory();
                    MessageBox.Show("Save Successfully");

                    txtSubCategoryName.Text = "";
                }
                else
                    MessageBox.Show("Save Not Successfully");

            }
        }

        private void BindComboPathologyCategoryName()
        {
            cmbPathologyCategoryName.DataSource = PathologyFetch.getALLPathologyCategory();
            cmbPathologyCategoryName.ValueMember = "PATHOLOGY_TEST_ITEM_CATEGORY_ID";
            cmbPathologyCategoryName.DisplayMember = "PATHOLOGY_TEST_ITEM_CATEGORY_NAME";
            cmbPathologyCategoryName.SelectedIndex = 0;
        }

        private void Pathology_Test_Item_Sub_Category_Setup_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtSubCategoryName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            PATHOLOGY_TEST_ITEM_SUB_CATEGORY objPathologyTestItemSubCategory = new PATHOLOGY_TEST_ITEM_SUB_CATEGORY();
            objPathologyTestItemSubCategory.PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID =new Guid (txtSubCategoryID.Text);
            objPathologyTestItemSubCategory.PATHOLOGY_TEST_ITEM_CATEGORY_ID = new Guid(cmbPathologyCategoryName.SelectedValue.ToString());
            objPathologyTestItemSubCategory.PATHOLOGY_TEST_ITEM_SUB_CATEGORY_NAME = txtSubCategoryName.Text;

            if (cmbActive.SelectedIndex == 0)
                objPathologyTestItemSubCategory.STATUS_FLAG = "A";
            else
                objPathologyTestItemSubCategory.STATUS_FLAG = "U";


            int i = PathologySetup.UpdatePathologyTestItemSubCategory(objPathologyTestItemSubCategory);

            if (i > 0)
            {
                dgvPathologySubCategory.AutoGenerateColumns = false;
                dgvPathologySubCategory.DataSource = PathologyFetch.getALLPathologySubCategory();
                MessageBox.Show("Update Successfully");

                txtSubCategoryID.Text = "";
                txtSubCategoryName.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvPathologySubCategory_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvPathologySubCategory.SelectedRows[0];
            txtSubCategoryID.Text = dr.Cells[0].Value.ToString();
            txtSubCategoryName.Text = dr.Cells[1].Value.ToString();
            cmbPathologyCategoryName.SelectedIndex = cmbPathologyCategoryName.FindString(dr.Cells[2].Value.ToString());
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            PATHOLOGY_TEST_ITEM_SUB_CATEGORY objPathologyTestItemSubCategory = new PATHOLOGY_TEST_ITEM_SUB_CATEGORY();
            objPathologyTestItemSubCategory.PATHOLOGY_TEST_ITEM_SUB_CATEGORY_ID = new Guid(txtSubCategoryID.Text);


            int i = PathologySetup.DeletePathologyTestItemSubCategory(objPathologyTestItemSubCategory);

            if (i > 0)
            {
                dgvPathologySubCategory.AutoGenerateColumns = false;
                dgvPathologySubCategory.DataSource = PathologyFetch.getALLPathologySubCategory();
                MessageBox.Show("Delete Successfully");

                txtSubCategoryID.Text = "";
                txtSubCategoryName.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
