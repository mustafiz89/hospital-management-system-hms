﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Pathology_Setup;
using IA.BIZ.Setup.Pathology_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Pathology
{
    public partial class Pathology_Test_Comments_Setup : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Pathology_Test_Comments_Setup()
        {
            InitializeComponent();
            dgvComments.AutoGenerateColumns = false;
            dgvComments.DataSource = PathologyFetch.getALLTestComments();
        }


        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtPathologyComments.Text == "")
            {
                er++;
                ep.SetError(txtPathologyComments, "Pathology Comments Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                PATHOLOGY_TEST_COMMENTS objPathologyTestComments = new PATHOLOGY_TEST_COMMENTS();
                objPathologyTestComments.PATHOLOGY_TEST_COMMENTS_COMMENTS = txtPathologyComments.Text;

                int i = PathologySetup.savePathologyTestComments(objPathologyTestComments);

                if (i > 0)
                {
                    dgvComments.AutoGenerateColumns = false;
                    dgvComments.DataSource = PathologyFetch.getALLTestComments();
                    MessageBox.Show("Save Successfully");

                    txtPathologyComments.Text = "";
                }
                else
                    MessageBox.Show("Save Not Successfully");

            }
        }

        private void txtPathologyComments_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            PATHOLOGY_TEST_COMMENTS objPathologyTestComments = new PATHOLOGY_TEST_COMMENTS();
            objPathologyTestComments.PATHOLOGY_TEST_COMMENTS_ID =new Guid (txtCommentsID.Text);
            objPathologyTestComments.PATHOLOGY_TEST_COMMENTS_COMMENTS = txtPathologyComments.Text;

            int i = PathologySetup.UpdatePathologyTestComments(objPathologyTestComments);

            if (i > 0)
            {
                dgvComments.AutoGenerateColumns = false;
                dgvComments.DataSource = PathologyFetch.getALLTestComments();
                MessageBox.Show("Update Successfully");

                txtCommentsID.Text = "";
                txtPathologyComments.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvComments_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvComments.SelectedRows[0];
            txtCommentsID.Text = dr.Cells[0].Value.ToString();
            txtPathologyComments.Text = dr.Cells[1].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            PATHOLOGY_TEST_COMMENTS objPathologyTestComments = new PATHOLOGY_TEST_COMMENTS();
            objPathologyTestComments.PATHOLOGY_TEST_COMMENTS_ID = new Guid(txtCommentsID.Text);

            int i = PathologySetup.DeletePathologyTestComments(objPathologyTestComments);

            if (i > 0)
            {
                dgvComments.AutoGenerateColumns = false;
                dgvComments.DataSource = PathologyFetch.getALLTestComments();
                MessageBox.Show("Delete Successfully");

                txtCommentsID.Text = "";
                txtPathologyComments.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
