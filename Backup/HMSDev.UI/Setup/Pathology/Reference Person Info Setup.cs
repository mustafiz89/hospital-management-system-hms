﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Pathology_Setup;
using IA.BIZ.Setup.Pathology_Setup;
using POS_SYSTEM;


namespace HMSDev.UI.Setup.Pathology
{
    public partial class Reference_Person_Info_Setup : Form
    {
        public Reference_Person_Info_Setup()
        {
            InitializeComponent();
            BindComboReferenceCategory();
            dgvReferencepersonInfo.AutoGenerateColumns = false;
            dgvReferencepersonInfo.DataSource = PathologyFetch.getALLReferencePerson();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            PATHOLOGY_REFERENCE_PERSON objPathologyReferencePerson = new PATHOLOGY_REFERENCE_PERSON();
            objPathologyReferencePerson.PATHOLOGY_REFERENCE_CATEGORY_ID =new Guid (cmbReferenceCategoryName.SelectedValue.ToString());
            objPathologyReferencePerson.PATHOLOGY_REFERENCE_PERSON_NAME = txtReferencepersonName.Text;
            objPathologyReferencePerson.PATHOLOGY_REFERENCE_PERSON_ADDRESS = txtAddress.Text;
            objPathologyReferencePerson.PATHOLOGY_REFERENCE_PERSON_CONTACT = txtConactNo.Text;
            objPathologyReferencePerson.PATHOLOGY_TESTWISE_COMMISSION = Convert.ToDecimal(txtTestWiseCommission.Text);

            int i = PathologySetup.savePathologyReferencePerson(objPathologyReferencePerson);

            if (i > 0)
            {
                dgvReferencepersonInfo.AutoGenerateColumns = false;
                dgvReferencepersonInfo.DataSource = PathologyFetch.getALLReferencePerson();
                MessageBox.Show("Save Successfully");

                txtReferencepersonName.Text = "";
                txtAddress.Text = "";
                txtConactNo.Text = "";
                txtTestWiseCommission.Text = "";
             
            }
            else
                MessageBox.Show("Save Not Successfully");
        }

        private void BindComboReferenceCategory()
        {
            cmbReferenceCategoryName.DataSource = PathologyFetch.getALLReferenceCategory();
            cmbReferenceCategoryName.ValueMember = "PATHOLOGY_REFERENCE_CATEGORY_ID";
            cmbReferenceCategoryName.DisplayMember = "PATHOLOGY_REFERENCE_CATEGORY_NAME";
            cmbReferenceCategoryName.SelectedIndex = 0;
        }

        private void Reference_Person_Info_Setup_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtReferencepersonName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtAddress_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtConactNo_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtTestWiseCommission_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {            
            PATHOLOGY_REFERENCE_PERSON objPathologyReferencePerson = new PATHOLOGY_REFERENCE_PERSON();
            objPathologyReferencePerson.PATHOLOGY_REFERENCE_PERSON_ID =new Guid (txtboxReferencePersonID.Text);
            objPathologyReferencePerson.PATHOLOGY_REFERENCE_CATEGORY_ID =new Guid (cmbReferenceCategoryName.SelectedValue.ToString());
            objPathologyReferencePerson.PATHOLOGY_REFERENCE_PERSON_NAME = txtReferencepersonName.Text;
            objPathologyReferencePerson.PATHOLOGY_REFERENCE_PERSON_ADDRESS = txtAddress.Text;
            objPathologyReferencePerson.PATHOLOGY_REFERENCE_PERSON_CONTACT = txtConactNo.Text;
            objPathologyReferencePerson.PATHOLOGY_TESTWISE_COMMISSION = Convert.ToDecimal(txtTestWiseCommission.Text);

            int i = PathologySetup.UpdatePathologyReferencePerson(objPathologyReferencePerson);

            if (i > 0)
            {
                dgvReferencepersonInfo.AutoGenerateColumns = false;
                dgvReferencepersonInfo.DataSource = PathologyFetch.getALLReferencePerson();
                MessageBox.Show("Update Successfully");

                txtboxReferencePersonID.Text="";
                txtReferencepersonName.Text = "";
                txtAddress.Text = "";
                txtConactNo.Text = "";
                txtTestWiseCommission.Text = "";
             
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvReferencepersonInfo_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvReferencepersonInfo.SelectedRows[0];
            txtboxReferencePersonID.Text = dr.Cells[0].Value.ToString();
            txtReferencepersonName.Text = dr.Cells[1].Value.ToString();
            cmbReferenceCategoryName.SelectedIndex = cmbReferenceCategoryName.FindString(dr.Cells[2].Value.ToString());
            txtAddress.Text = dr.Cells[3].Value.ToString();
            txtConactNo.Text = dr.Cells[4].Value.ToString();
            txtTestWiseCommission.Text = dr.Cells[5].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            PATHOLOGY_REFERENCE_PERSON objPathologyReferencePerson = new PATHOLOGY_REFERENCE_PERSON();
            objPathologyReferencePerson.PATHOLOGY_REFERENCE_PERSON_ID = new Guid(txtboxReferencePersonID.Text);

            int i = PathologySetup.DeletePathologyReferencePerson(objPathologyReferencePerson);

            if (i > 0)
            {
                dgvReferencepersonInfo.AutoGenerateColumns = false;
                dgvReferencepersonInfo.DataSource = PathologyFetch.getALLReferencePerson();
                MessageBox.Show("Delete Successfully");

                txtboxReferencePersonID.Text = "";
                txtReferencepersonName.Text = "";
                txtAddress.Text = "";
                txtConactNo.Text = "";
                txtTestWiseCommission.Text = "";

            }
            else
                MessageBox.Show("Delete Not Successfully");
        }

        private void cmbReferenceCategoryName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

       
    }
}
