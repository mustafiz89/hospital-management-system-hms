﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Indoor_Setup;
using IA.BIZ.Setup.Indoor_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Indoor
{
    public partial class Room_Category_Setup : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Room_Category_Setup()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvRoomCategorySetup.AutoGenerateColumns = false;
            dgvRoomCategorySetup.DataSource = IndoorFetch.getALLRoomCategory();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtRoomCategoryName.Text == "")
            {
                er++;
                ep.SetError(txtRoomCategoryName, "Name Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                ROOM_CATEGORY objRoomCategory = new ROOM_CATEGORY();
                objRoomCategory.ROOM_CATEGORY_NAME = txtRoomCategoryName.Text;
                if (cmbActive.SelectedIndex == 0)
                    objRoomCategory.STATUS_FLAG = "A";
                else
                    objRoomCategory.STATUS_FLAG = "U";

                int i = IndoorSave.saveRoomCategory(objRoomCategory);

                if (i > 0)
                {
                    dgvRoomCategorySetup.AutoGenerateColumns = false;
                    dgvRoomCategorySetup.DataSource = IndoorFetch.getALLRoomCategory();
                    MessageBox.Show("Save Successfully");
                }
                else
                    MessageBox.Show("Save Not Successfully");

                txtRoomCategoryName.Text = "";
            }
        }

        private void Room_Category_Setup_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            ROOM_CATEGORY objRoomCategory = new ROOM_CATEGORY();
            objRoomCategory.ROOM_CATEGORY_ID =new Guid (txtRoomCategoeyID.Text);
            objRoomCategory.ROOM_CATEGORY_NAME = txtRoomCategoryName.Text;
            if (cmbActive.SelectedIndex == 0)
                objRoomCategory.STATUS_FLAG = "A";
            else
                objRoomCategory.STATUS_FLAG = "U";

            int i = IndoorSave.UpdateRoomCategory(objRoomCategory);

            if (i > 0)
            {
                dgvRoomCategorySetup.AutoGenerateColumns = false;
                dgvRoomCategorySetup.DataSource = IndoorFetch.getALLRoomCategory();
                MessageBox.Show("Save Successfully");

                txtRoomCategoeyID.Text = "";
                txtRoomCategoryName.Text = "";
            }
            else
                MessageBox.Show("Save Not Successfully");
        }

        private void dgvRoomCategorySetup_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvRoomCategorySetup.SelectedRows[0];
            txtRoomCategoeyID.Text = dr.Cells[0].Value.ToString();
            txtRoomCategoryName.Text = dr.Cells[1].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            ROOM_CATEGORY objRoomCategory = new ROOM_CATEGORY();
            objRoomCategory.ROOM_CATEGORY_ID = new Guid(txtRoomCategoeyID.Text);

            int i = IndoorSave.DeleteRoomCategory(objRoomCategory);

            if (i > 0)
            {
                dgvRoomCategorySetup.AutoGenerateColumns = false;
                dgvRoomCategorySetup.DataSource = IndoorFetch.getALLRoomCategory();
                MessageBox.Show("Delete Successfully");

                txtRoomCategoeyID.Text = "";
                txtRoomCategoryName.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }

        private void txtRoomCategoryName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }
    }
}
