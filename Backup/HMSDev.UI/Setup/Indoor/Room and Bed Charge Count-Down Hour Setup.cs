﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Indoor_Setup;
using IA.BIZ.Setup.Indoor_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Indoor
{
    public partial class Room_and_Bed_Charge_Count_Down_Hour_Setup : Form
    {
        public Room_and_Bed_Charge_Count_Down_Hour_Setup()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ROOM_BED_CHARGE_COUNT_DOWN objRoomBedChargeCountDown = new ROOM_BED_CHARGE_COUNT_DOWN();
            objRoomBedChargeCountDown.TIME_START = dateTimePicker1.Value;
            objRoomBedChargeCountDown.TIME_END = dateTimePicker2.Value;

            int i = IndoorSave.saveRoomBedChargeCountDown(objRoomBedChargeCountDown);
            if (i > 0)
                MessageBox.Show("Save Successfully");
            else
                MessageBox.Show("Save Not Successfully");
        }

        private void dateTimePicker1_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void dateTimePicker2_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
