﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Indoor_Setup;
using IA.BIZ.Setup.Indoor_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Indoor
{
    public partial class Corporate_Client_Setup : Form
    {
        public Corporate_Client_Setup()
        {
            InitializeComponent();
            dgvCorporateClient.AutoGenerateColumns = false;
            dgvCorporateClient.DataSource = IndoorFetch.getALLCorporateClient();
        }

       
        private void btnAdd_Click(object sender, EventArgs e)
        {
            CORPORATE_CLIENT objCorporateClient = new CORPORATE_CLIENT();
            objCorporateClient.CORPORATE_CLIENT_NAME = txtClientName.Text;
            objCorporateClient.PERSON_NAME = txtPersonName.Text;
            objCorporateClient.CORPORATE_CLIENT_DESIGNATION = txtDesignation.Text;
            objCorporateClient.CORPORATE_CLIENT_ADDRESS = txtClientAddress.Text;
            objCorporateClient.PERSON_ADDRESS = txtPersonAddress.Text;
            objCorporateClient.CORPORATE_CLIENT_PHONE = txtClientPhone.Text;
            objCorporateClient.PERSON_PHONE = txtPersonPhone.Text;
            objCorporateClient.CORPORATE_CLIENT_STARTING_DATE = dateTimePicker1.Value;
            objCorporateClient.CORPORATE_CLIENT_EXPIRE_DATE = dateTimePicker2.Value;
            objCorporateClient.CORPORATE_CLIENT_DISCOUNT= Convert.ToDecimal(txtDiscount.Text);
            objCorporateClient.CORPORATE_CLIENT_BALANCE = Convert.ToDecimal(txtBalance.Text);

            int i = IndoorSave.saveCorporateClient(objCorporateClient);

            if (i > 0)
            {
                dgvCorporateClient.AutoGenerateColumns = false;
                dgvCorporateClient.DataSource = IndoorFetch.getALLCorporateClient();
                MessageBox.Show("Save Successfully");

                txtClientName.Text = "";
                txtPersonName.Text = "";
                txtDesignation.Text = "";
                txtClientAddress.Text = "";
                txtPersonAddress.Text = "";
                txtClientPhone.Text = "";
                txtPersonPhone.Text = "";
                txtDiscount.Text = "";
                txtBalance.Text = "";
            }
            else
                MessageBox.Show("Save Not Successfully");
        }

        private void txtClientCode_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtClientName_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtPersonName_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtDesignation_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtClientAddress_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtPersonAddress_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtClientPhone_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtPersonPhone_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void dateTimePicker1_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void dateTimePicker2_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtDiscount_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtBalance_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            CORPORATE_CLIENT objCorporateClient = new CORPORATE_CLIENT();
            objCorporateClient.CORPORATE_CLIENT_CODE =new Guid (txtID.Text);
            objCorporateClient.CORPORATE_CLIENT_NAME = txtClientName.Text;
            objCorporateClient.PERSON_NAME = txtPersonName.Text;
            objCorporateClient.CORPORATE_CLIENT_DESIGNATION = txtDesignation.Text;
            objCorporateClient.CORPORATE_CLIENT_ADDRESS = txtClientAddress.Text;
            objCorporateClient.PERSON_ADDRESS = txtPersonAddress.Text;
            objCorporateClient.CORPORATE_CLIENT_PHONE = txtClientPhone.Text;
            objCorporateClient.PERSON_PHONE = txtPersonPhone.Text;
            objCorporateClient.CORPORATE_CLIENT_STARTING_DATE = dateTimePicker1.Value;
            objCorporateClient.CORPORATE_CLIENT_EXPIRE_DATE = dateTimePicker2.Value;
            objCorporateClient.CORPORATE_CLIENT_DISCOUNT = Convert.ToDecimal(txtDiscount.Text);
            objCorporateClient.CORPORATE_CLIENT_BALANCE = Convert.ToDecimal(txtBalance.Text);

            int i = IndoorSave.UpdateCorporateClient(objCorporateClient);

            if (i > 0)
            {
                dgvCorporateClient.AutoGenerateColumns = false;
                dgvCorporateClient.DataSource = IndoorFetch.getALLCorporateClient();
                MessageBox.Show("Update Successfully");

                txtID.Text = "";
                txtClientName.Text = "";
                txtPersonName.Text = "";
                txtDesignation.Text = "";
                txtClientAddress.Text = "";
                txtPersonAddress.Text = "";
                txtClientPhone.Text = "";
                txtPersonPhone.Text = "";
                txtDiscount.Text = "";
                txtBalance.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvCorporateClient_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvCorporateClient.SelectedRows[0];
            txtID.Text = dr.Cells[0].Value.ToString();
            txtClientName.Text = dr.Cells[1].Value.ToString();
            txtPersonName.Text = dr.Cells[2].Value.ToString();
            txtDesignation.Text = dr.Cells[3].Value.ToString();
            txtClientAddress.Text = dr.Cells[4].Value.ToString();
            txtPersonAddress.Text = dr.Cells[5].Value.ToString();
            txtClientPhone.Text = dr.Cells[6].Value.ToString();
            txtPersonPhone.Text = dr.Cells[7].Value.ToString();
            dateTimePicker1.Text = dr.Cells[8].Value.ToString();
            dateTimePicker2.Text = dr.Cells[9].Value.ToString();
            txtDiscount.Text = dr.Cells[10].Value.ToString();
            txtBalance.Text = dr.Cells[11].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            CORPORATE_CLIENT objCorporateClient = new CORPORATE_CLIENT();
            objCorporateClient.CORPORATE_CLIENT_CODE = new Guid(txtID.Text);

            int i = IndoorSave.DeleteCorporateClient(objCorporateClient);

            if (i > 0)
            {
                dgvCorporateClient.AutoGenerateColumns = false;
                dgvCorporateClient.DataSource = IndoorFetch.getALLCorporateClient();
                MessageBox.Show("Delete Successfully");

                txtID.Text = "";
                txtClientName.Text = "";
                txtPersonName.Text = "";
                txtDesignation.Text = "";
                txtClientAddress.Text = "";
                txtPersonAddress.Text = "";
                txtClientPhone.Text = "";
                txtPersonPhone.Text = "";
                txtDiscount.Text = "";
                txtBalance.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
