﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Common_Setup;
using IA.BIZ.Setup.Common_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Common_Setup
{
    public partial class Occupation_Setup : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Occupation_Setup()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvOccupationInfo.AutoGenerateColumns = false;
            dgvOccupationInfo.DataSource = CommonSetupFetch.getALLOCCUPATION();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtOccupation.Text == "")
            {
                er++;
                ep.SetError(txtOccupation, "Name Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                OCCUPATION objOccupation = new OCCUPATION();
                objOccupation.OCCUPATION_NAME = txtOccupation.Text;
                if (cmbActive.SelectedIndex == 0)
                    objOccupation.STATUS_FLAG = "A";
                else
                    objOccupation.STATUS_FLAG = "U";
                int i = CommonSetupSave.saveOccupation(objOccupation);
                if (i > 0)
                {
                    dgvOccupationInfo.AutoGenerateColumns = false;
                    dgvOccupationInfo.DataSource = CommonSetupFetch.getALLOCCUPATION();
                    MessageBox.Show("Save Successfully");

                    txtOccupation.Text = "";
                }
                else
                    MessageBox.Show("Save Not Successfully");

            }
        }

        private void txtOccupation_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            OCCUPATION objOccupation = new OCCUPATION();
            objOccupation.OCCUPATION_ID =new Guid (txtOccupationID.Text);
            objOccupation.OCCUPATION_NAME = txtOccupation.Text;
            if (cmbActive.SelectedIndex == 0)
                objOccupation.STATUS_FLAG = "A";
            else
                objOccupation.STATUS_FLAG = "U";
            int i = CommonSetupSave.UpdateOccupation(objOccupation);
            if (i > 0)
            {
                dgvOccupationInfo.AutoGenerateColumns = false;
                dgvOccupationInfo.DataSource = CommonSetupFetch.getALLOCCUPATION();
                MessageBox.Show("Update Successfully");

                txtOccupation.Text = "";
                txtOccupationID.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");

        }

        private void dgvOccupationInfo_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvOccupationInfo.SelectedRows[0];
            txtOccupationID.Text = dr.Cells[0].Value.ToString();
            txtOccupation.Text = dr.Cells[1].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            OCCUPATION objOccupation = new OCCUPATION();
            objOccupation.OCCUPATION_ID = new Guid(txtOccupationID.Text);
          
            int i = CommonSetupSave.DeleteOccupation(objOccupation);
            if (i > 0)
            {
                dgvOccupationInfo.AutoGenerateColumns = false;
                dgvOccupationInfo.DataSource = CommonSetupFetch.getALLOCCUPATION();
                MessageBox.Show("Delete Successfully");

                txtOccupation.Text = "";
                txtOccupationID.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
