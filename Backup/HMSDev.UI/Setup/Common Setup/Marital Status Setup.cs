﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Common_Setup;
using IA.BIZ.Setup.Common_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Common_Setup
{
    public partial class Marital_Status_Setup : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Marital_Status_Setup()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvMaritalStatusUpdate.AutoGenerateColumns = false;
            dgvMaritalStatusUpdate.DataSource = CommonSetupFetch.getAllMARITALSTATUS();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtMaritalStatus.Text == "")
            {
                er++;
                ep.SetError(txtMaritalStatus, "Name Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                MARITAL_STATUS objMARITAL_STATUS = new MARITAL_STATUS();
                objMARITAL_STATUS.MARITAL_STATUS_NAME = txtMaritalStatus.Text;
                if (cmbActive.SelectedIndex == 0)
                    objMARITAL_STATUS.STATUS_FLAG = "A";
                else
                    objMARITAL_STATUS.STATUS_FLAG = "U";
                int i = CommonSetupSave.saveMARITAL_STATUS(objMARITAL_STATUS);
                if (i > 0)
                {
                    dgvMaritalStatusUpdate.AutoGenerateColumns = false;
                    dgvMaritalStatusUpdate.DataSource = CommonSetupFetch.getAllMARITALSTATUS();
                    MessageBox.Show("Save Successfully");

                    txtMaritalStatus.Text = "";
                }
                else
                    MessageBox.Show("Save Not Succesfully");

            }
        }

        private void txtMaritalStatus_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dgvMaritalStatusUpdate_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvMaritalStatusUpdate.SelectedRows[0];
            txtMaritalStatusID.Text = dr.Cells[0].Value.ToString();
            txtMaritalStatus.Text = dr.Cells[1].Value.ToString();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            MARITAL_STATUS objMARITAL_STATUS = new MARITAL_STATUS();
            objMARITAL_STATUS.MARITAL_STATUS_ID =new Guid (txtMaritalStatusID.Text);
            objMARITAL_STATUS.MARITAL_STATUS_NAME = txtMaritalStatus.Text;
            if (cmbActive.SelectedIndex == 0)
                objMARITAL_STATUS.STATUS_FLAG = "A";
            else
                objMARITAL_STATUS.STATUS_FLAG = "U";
            int i = CommonSetupSave.UpdateMaritalStatus(objMARITAL_STATUS);
            if (i > 0)
            {
                dgvMaritalStatusUpdate.AutoGenerateColumns = false;
                dgvMaritalStatusUpdate.DataSource = CommonSetupFetch.getAllMARITALSTATUS();
                MessageBox.Show("Update Successfully");

                txtMaritalStatus.Text = "";
                txtMaritalStatusID.Text = "";
            }
            else
                MessageBox.Show("Update Not Succesfully");

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            MARITAL_STATUS objMARITAL_STATUS = new MARITAL_STATUS();
            objMARITAL_STATUS.MARITAL_STATUS_ID = new Guid(txtMaritalStatusID.Text);
            
            int i = CommonSetupSave.DeleteMaritalStatus(objMARITAL_STATUS);
            if (i > 0)
            {
                dgvMaritalStatusUpdate.AutoGenerateColumns = false;
                dgvMaritalStatusUpdate.DataSource = CommonSetupFetch.getAllMARITALSTATUS();
                MessageBox.Show("Delete Successfully");

                txtMaritalStatus.Text = "";
                txtMaritalStatusID.Text = "";
            }
            else
                MessageBox.Show("Delete Not Succesfully");
        }
    }
}
