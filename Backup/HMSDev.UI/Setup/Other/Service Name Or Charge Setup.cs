﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Other_Service_Setup;
using IA.BIZ.Setup.Other_Service_Setup;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Other
{
    public partial class Service_Name_Or_Charge_Setup : Form
    {
        public Service_Name_Or_Charge_Setup()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvOtherServiceInfo.AutoGenerateColumns = false;
            dgvOtherServiceInfo.DataSource = OtherServiceFetch.getALLOtherService();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            OTHER_SERVICE objOtherService = new OTHER_SERVICE();
            objOtherService.OTHER_SERVICE_NAME = txtServiceName.Text;
            objOtherService.OTHER_SERVICE_CHARGE = Convert.ToDecimal(txtServiceChange.Text);

            if (cmbActive.SelectedIndex == 0)
            {
                objOtherService.STATUS_FLAG = "A";
            }
            else
            {
                objOtherService.STATUS_FLAG = "U";
            }
                

            int i = OtherServiceSetup.saveOtherService(objOtherService);

            if (i > 0)
            {
                dgvOtherServiceInfo.AutoGenerateColumns = false;
                dgvOtherServiceInfo.DataSource = OtherServiceFetch.getALLOtherService();
                MessageBox.Show("Save Successfully");

                txtServiceName.Text = "";
                txtServiceChange.Text = "";
            }
            else
                MessageBox.Show("Save Not Successfully");
        }

        private void Service_Name_Or_Charge_Setup_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtServiceChange_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtServiceName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            OTHER_SERVICE objOtherService = new OTHER_SERVICE();
            objOtherService.OTHER_SERVICE_ID =new Guid (txtServiceChargeID.Text);
            objOtherService.OTHER_SERVICE_NAME = txtServiceName.Text;
            objOtherService.OTHER_SERVICE_CHARGE = Convert.ToDecimal(txtServiceChange.Text);

            if (cmbActive.SelectedIndex == 0)
            {
                objOtherService.STATUS_FLAG = "A";
            }
            else
            {
                objOtherService.STATUS_FLAG = "U";
            }


            int i = OtherServiceSetup.UpdateOtherService(objOtherService);

            if (i > 0)
            {
                dgvOtherServiceInfo.AutoGenerateColumns = false;
                dgvOtherServiceInfo.DataSource = OtherServiceFetch.getALLOtherService();
                MessageBox.Show("Update Successfully");

                txtServiceChargeID.Text = "";
                txtServiceName.Text = "";
                txtServiceChange.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvOtherServiceInfo_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvOtherServiceInfo.SelectedRows[0];
            txtServiceChargeID.Text = dr.Cells[0].Value.ToString();
            txtServiceName.Text = dr.Cells[1].Value.ToString();
            txtServiceChange.Text = dr.Cells[2].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            OTHER_SERVICE objOtherService = new OTHER_SERVICE();
            objOtherService.OTHER_SERVICE_ID = new Guid(txtServiceChargeID.Text);

            int i = OtherServiceSetup.DeleteOtherService(objOtherService);

            if (i > 0)
            {
                dgvOtherServiceInfo.AutoGenerateColumns = false;
                dgvOtherServiceInfo.DataSource = OtherServiceFetch.getALLOtherService();
                MessageBox.Show("Delete Successfully");

                txtServiceChargeID.Text = "";
                txtServiceName.Text = "";
                txtServiceChange.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
