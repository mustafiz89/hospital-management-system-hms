﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Doctor;
using IA.BIZ.Setup.Doctor;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Doctor
{
    public partial class Outdoor_Doctor_Setup : Form
    {
        public Outdoor_Doctor_Setup()
        {
            InitializeComponent();
            BindComboDoctorSpecialization();
            BindComboDoctorType();
            dgvOutdoorDoctor.AutoGenerateColumns = false;
            dgvOutdoorDoctor.DataSource = DoctorFetch.getALLOutdoorDoctor();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            OUTDOOR_DOCTOR ObjOutdoorDoctor = new OUTDOOR_DOCTOR();
            ObjOutdoorDoctor.DOCTOR_SPECIALIZATION_ID =new Guid(cmbDoctorSpecialization.SelectedValue.ToString());
            ObjOutdoorDoctor.DOCTOR_TYPE_ID =new Guid(cmbDoctorType.SelectedValue.ToString());
            ObjOutdoorDoctor.DOCTOR_NAME = txtDoctorName.Text;
            ObjOutdoorDoctor.DOCTOR_CONTACT_PERSON = txtContactPerson.Text;
            ObjOutdoorDoctor.DOCTOR_PERSONAL_ADDRESS = txtPersonalAddress.Text;
            ObjOutdoorDoctor.DOCTOR_CHAMBER_ADDRESS = txtChamberAddress.Text;
            ObjOutdoorDoctor.DOCTOR_PERSONAL_CONTACT = txtDoctorContact.Text;
            ObjOutdoorDoctor.DOCTOR_PS_CONTACT = txtDoctorPSContact.Text;
            ObjOutdoorDoctor.DOCTOR_PATHOLOGY_COMMISSION = Convert.ToDecimal(txtPathologyCommission.Text);
            ObjOutdoorDoctor.DOCTOR_PATIENT_COMMISSION = Convert.ToDecimal(txtPatientCommission.Text);

            int i = DoctorSave.saveOutdoorDoctor(ObjOutdoorDoctor);
            if (i > 0)
            {
                dgvOutdoorDoctor.AutoGenerateColumns = false;
                dgvOutdoorDoctor.DataSource = DoctorFetch.getALLOutdoorDoctor();
                MessageBox.Show("Save Successfully");
            }
            else
                MessageBox.Show("Save Not Successfully");

            txtDoctorName.Text = "";
            txtContactPerson.Text = "";
            txtPersonalAddress.Text = "";
            txtChamberAddress.Text = "";
            txtDoctorContact.Text = "";
            txtDoctorPSContact.Text = "";
            txtPathologyCommission.Text = "";
            txtPatientCommission.Text = "";
        }

        private void cmbDoctorSpecialization_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbDoctorType_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtDoctorName_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtContactPerson_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtPersonalAddress_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtChamberAddress_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtDoctorContact_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtDoctorPSContact_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtPathologyCommission_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtPatientCommission_KeyUp(object sender, KeyEventArgs e)
        {

            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void BindComboDoctorSpecialization()
        {
            cmbDoctorSpecialization.DataSource = DoctorFetch.getALLSpecialization();
            cmbDoctorSpecialization.ValueMember = "DOCTOR_SPECIALIZATION_ID";
            cmbDoctorSpecialization.DisplayMember = "DOCTOR_SPECIALIZATION_NAME";
            cmbDoctorSpecialization.SelectedIndex = 0;
        }

        private void BindComboDoctorType()
        {
            cmbDoctorType.DataSource = DoctorFetch.getALLDoctorType();
            cmbDoctorType.ValueMember = "DOCTOR_TYPE_ID";
            cmbDoctorType.DisplayMember = "DOCTOR_TYPE_NAME";
            cmbDoctorType.SelectedIndex = 0;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
