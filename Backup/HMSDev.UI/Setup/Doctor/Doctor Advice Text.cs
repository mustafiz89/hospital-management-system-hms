﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Doctor;
using IA.BIZ.Setup.Doctor;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Doctor
{
    public partial class Doctor_Advice_Text : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Doctor_Advice_Text()
        {
            InitializeComponent();
            dgvDoctorAdviceText.AutoGenerateColumns = false;
            dgvDoctorAdviceText.DataSource = DoctorFetch.getALLAdviceComments();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtDoctorAdviceComments.Text == "")
            {
                er++;
                ep.SetError(txtDoctorAdviceComments, "Advice Comments Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                DOCTOR_ADVICE_COMMENTS objDoctorAdviceComments = new DOCTOR_ADVICE_COMMENTS();
                objDoctorAdviceComments.DOCTOR_ADVICE_COMMENTS_TEXT = txtDoctorAdviceComments.Text;

                int i = DoctorSave.saveDoctorAdviceComments(objDoctorAdviceComments);
                if (i > 0)
                {
                    dgvDoctorAdviceText.AutoGenerateColumns = false;
                    dgvDoctorAdviceText.DataSource = DoctorFetch.getALLAdviceComments();
                    MessageBox.Show("Save Successfully");

                    txtDoctorAdviceComments.Text = "";
                }
                else
                    MessageBox.Show("Save Not Successfully");

            }
        }

        private void txtDoctorAdviceComments_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DOCTOR_ADVICE_COMMENTS objDoctorAdviceComments = new DOCTOR_ADVICE_COMMENTS();
            objDoctorAdviceComments.DOCTOR_ADVICE_COMMENTS_ID =new Guid (txtCoomentsID.Text);
            objDoctorAdviceComments.DOCTOR_ADVICE_COMMENTS_TEXT = txtDoctorAdviceComments.Text;

            int i = DoctorSave.UpdateDoctorAdviceComments(objDoctorAdviceComments);
            if (i > 0)
            {
                dgvDoctorAdviceText.AutoGenerateColumns = false;
                dgvDoctorAdviceText.DataSource = DoctorFetch.getALLAdviceComments();
                MessageBox.Show("Update Successfully");

                txtCoomentsID.Text = "";
                txtDoctorAdviceComments.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvDoctorAdviceText_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvDoctorAdviceText.SelectedRows[0];
            txtCoomentsID.Text = dr.Cells[0].Value.ToString();
            txtDoctorAdviceComments.Text = dr.Cells[1].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DOCTOR_ADVICE_COMMENTS objDoctorAdviceComments = new DOCTOR_ADVICE_COMMENTS();
            objDoctorAdviceComments.DOCTOR_ADVICE_COMMENTS_ID = new Guid(txtCoomentsID.Text);

            int i = DoctorSave.DeleteDoctorAdviceComments(objDoctorAdviceComments);
            if (i > 0)
            {
                dgvDoctorAdviceText.AutoGenerateColumns = false;
                dgvDoctorAdviceText.DataSource = DoctorFetch.getALLAdviceComments();
                MessageBox.Show("Delete Successfully");

                txtCoomentsID.Text = "";
                txtDoctorAdviceComments.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
