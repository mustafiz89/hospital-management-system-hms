﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Doctor;
using IA.BIZ.Setup.Doctor;
using POS_SYSTEM;

namespace HMSDev.UI.Setup.Doctor
{
    public partial class Doctor_Type_of_Visit : Form
    {
        ErrorProvider ep = new ErrorProvider();
        public Doctor_Type_of_Visit()
        {
            InitializeComponent();
            cmbActive.SelectedIndex = 0;
            dgvTypOfVisit.AutoGenerateColumns = false;
            dgvTypOfVisit.DataSource = DoctorFetch.getALLTypeOfVisit();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            int er = 0;
            ep.Clear();
            if (txtTypeOfVisitName.Text == "")
            {
                er++;
                ep.SetError(txtTypeOfVisitName, "Type Of Visit Required");
                //MessageBox.Show("required field needed");
            }
            else
            {
                DOCTOR_TYPE_OF_VISIT objDoctorTypeOfVisit = new DOCTOR_TYPE_OF_VISIT();
                objDoctorTypeOfVisit.DOCTOR_TYPE_OF_VISIT_NAME = txtTypeOfVisitName.Text;
                if (cmbActive.SelectedIndex == 0)
                    objDoctorTypeOfVisit.STATUS_FLAG = "A";
                else
                    objDoctorTypeOfVisit.STATUS_FLAG = "U";
                int i = DoctorSave.saveDoctorTypeOfVisit(objDoctorTypeOfVisit);
                if (i > 0)
                {
                    dgvTypOfVisit.AutoGenerateColumns = false;
                    dgvTypOfVisit.DataSource = DoctorFetch.getALLTypeOfVisit();
                    MessageBox.Show("Save Successfully");
                }
                else
                    MessageBox.Show("Save Not Successfully");

                txtTypeOfVisitName.Text = "";
            }
        }

        private void txtTypeOfVisitName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DOCTOR_TYPE_OF_VISIT objDoctorTypeOfVisit = new DOCTOR_TYPE_OF_VISIT();
            objDoctorTypeOfVisit.DOCTOR_TYPE_OF_VISIT_ID =new Guid (txtVisitNameID.Text);
            objDoctorTypeOfVisit.DOCTOR_TYPE_OF_VISIT_NAME = txtTypeOfVisitName.Text;
            if (cmbActive.SelectedIndex == 0)
                objDoctorTypeOfVisit.STATUS_FLAG = "A";
            else
                objDoctorTypeOfVisit.STATUS_FLAG = "U";
            int i = DoctorSave.UpdateDoctorTypeOfVisit(objDoctorTypeOfVisit);
            if (i > 0)
            {
                dgvTypOfVisit.AutoGenerateColumns = false;
                dgvTypOfVisit.DataSource = DoctorFetch.getALLTypeOfVisit();
                MessageBox.Show("Update Successfully");

                txtVisitNameID.Text = "";
                txtTypeOfVisitName.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvTypOfVisit_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvTypOfVisit.SelectedRows[0];
            txtVisitNameID.Text = dr.Cells[0].Value.ToString();
            txtTypeOfVisitName.Text = dr.Cells[1].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DOCTOR_TYPE_OF_VISIT objDoctorTypeOfVisit = new DOCTOR_TYPE_OF_VISIT();
            objDoctorTypeOfVisit.DOCTOR_TYPE_OF_VISIT_ID = new Guid(txtVisitNameID.Text);
           
            int i = DoctorSave.DeleteDoctorTypeOfVisit(objDoctorTypeOfVisit);
            if (i > 0)
            {
                dgvTypOfVisit.AutoGenerateColumns = false;
                dgvTypOfVisit.DataSource = DoctorFetch.getALLTypeOfVisit();
                MessageBox.Show("Delete Successfully");

                txtVisitNameID.Text = "";
                txtTypeOfVisitName.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
