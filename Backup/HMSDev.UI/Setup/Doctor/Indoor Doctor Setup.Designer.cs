﻿namespace HMSDev.UI.Setup.Doctor
{
    partial class Indoor_Doctor_Setup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnReport = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dgvDoctorInfo = new System.Windows.Forms.DataGridView();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbIndoorOutdoor = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDocotrID = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbDoctorType = new System.Windows.Forms.ComboBox();
            this.cmbDoctorSpecialization = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtPatientCommission = new System.Windows.Forms.TextBox();
            this.txtDoctorPSContact = new System.Windows.Forms.TextBox();
            this.txtPathologyCommission = new System.Windows.Forms.TextBox();
            this.txtDoctorContact = new System.Windows.Forms.TextBox();
            this.txtChamberAddress = new System.Windows.Forms.TextBox();
            this.txtPersonalAddress = new System.Windows.Forms.TextBox();
            this.txtContactPerson = new System.Windows.Forms.TextBox();
            this.txtDoctorName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDoctorInfo)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold);
            this.groupBox2.Location = new System.Drawing.Point(12, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(982, 474);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Doctor Info";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnReport);
            this.groupBox4.Controls.Add(this.btnClose);
            this.groupBox4.Controls.Add(this.btnAdd);
            this.groupBox4.Controls.Add(this.btnEdit);
            this.groupBox4.Controls.Add(this.btnDelete);
            this.groupBox4.Location = new System.Drawing.Point(12, 395);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(957, 63);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            // 
            // btnReport
            // 
            this.btnReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnReport.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnReport.Location = new System.Drawing.Point(426, 20);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(92, 29);
            this.btnReport.TabIndex = 15;
            this.btnReport.Text = "Report";
            this.btnReport.UseVisualStyleBackColor = false;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnClose.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnClose.Location = new System.Drawing.Point(219, 20);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(92, 29);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnAdd.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnAdd.Location = new System.Drawing.Point(11, 20);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(100, 29);
            this.btnAdd.TabIndex = 11;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnEdit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnEdit.Location = new System.Drawing.Point(121, 20);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(92, 29);
            this.btnEdit.TabIndex = 12;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnDelete.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.btnDelete.Location = new System.Drawing.Point(316, 20);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 29);
            this.btnDelete.TabIndex = 14;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dgvDoctorInfo);
            this.groupBox3.Location = new System.Drawing.Point(432, 15);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(543, 339);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            // 
            // dgvDoctorInfo
            // 
            this.dgvDoctorInfo.AllowUserToAddRows = false;
            this.dgvDoctorInfo.AllowUserToDeleteRows = false;
            this.dgvDoctorInfo.AllowUserToResizeColumns = false;
            this.dgvDoctorInfo.AllowUserToResizeRows = false;
            this.dgvDoctorInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDoctorInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDoctorInfo.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(224)))), ((int)(((byte)(248)))));
            this.dgvDoctorInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column5,
            this.Column1,
            this.Column11,
            this.Column6,
            this.Column7,
            this.Column2,
            this.Column4,
            this.Column3,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column12});
            this.dgvDoctorInfo.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvDoctorInfo.Location = new System.Drawing.Point(6, 17);
            this.dgvDoctorInfo.Name = "dgvDoctorInfo";
            this.dgvDoctorInfo.RowHeadersVisible = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvDoctorInfo.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDoctorInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDoctorInfo.Size = new System.Drawing.Size(531, 315);
            this.dgvDoctorInfo.TabIndex = 7;
            this.dgvDoctorInfo.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgvDoctorInfo_MouseClick);
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "DOCTOR_ID";
            this.Column5.HeaderText = "ID";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Visible = false;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "DOCTOR_NAME";
            this.Column1.HeaderText = "Name";
            this.Column1.Name = "Column1";
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "DOCTOR_SPECIALIZATION_NAME";
            this.Column11.HeaderText = "Specialization";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "DOCTOR_TYPE_NAME";
            this.Column6.HeaderText = "Type";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "DOCTOR_CONTACT_PERSON";
            this.Column7.HeaderText = "PS";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Visible = false;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "DOCTOR_PERSONAL_ADDRESS";
            this.Column2.HeaderText = "Personal Address";
            this.Column2.Name = "Column2";
            this.Column2.Visible = false;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "DOCTOR_CHAMBER_ADDRESS";
            this.Column4.HeaderText = "Chamber Address";
            this.Column4.Name = "Column4";
            this.Column4.Visible = false;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "DOCTOR_PERSONAL_CONTACT";
            this.Column3.HeaderText = "Dr.Contact";
            this.Column3.Name = "Column3";
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "DOCTOR_PS_CONTACT";
            this.Column8.HeaderText = "PS Contact";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Visible = false;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "DOCTOR_PATHOLOGY_COMMISSION";
            this.Column9.HeaderText = "Pathology Commission";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Visible = false;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "DOCTOR_PATIENT_COMMISSION";
            this.Column10.HeaderText = "Patient Commission";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            this.Column10.Visible = false;
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "INDOOR_OUTDOOR_FLAG";
            this.Column12.HeaderText = "Status";
            this.Column12.Name = "Column12";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmbIndoorOutdoor);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.txtDocotrID);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.cmbDoctorType);
            this.groupBox1.Controls.Add(this.cmbDoctorSpecialization);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.txtPatientCommission);
            this.groupBox1.Controls.Add(this.txtDoctorPSContact);
            this.groupBox1.Controls.Add(this.txtPathologyCommission);
            this.groupBox1.Controls.Add(this.txtDoctorContact);
            this.groupBox1.Controls.Add(this.txtChamberAddress);
            this.groupBox1.Controls.Add(this.txtPersonalAddress);
            this.groupBox1.Controls.Add(this.txtContactPerson);
            this.groupBox1.Controls.Add(this.txtDoctorName);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(13, 20);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(415, 379);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // cmbIndoorOutdoor
            // 
            this.cmbIndoorOutdoor.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbIndoorOutdoor.FormattingEnabled = true;
            this.cmbIndoorOutdoor.Items.AddRange(new object[] {
            "Indoor",
            "Outdoor"});
            this.cmbIndoorOutdoor.Location = new System.Drawing.Point(183, 306);
            this.cmbIndoorOutdoor.Name = "cmbIndoorOutdoor";
            this.cmbIndoorOutdoor.Size = new System.Drawing.Size(83, 23);
            this.cmbIndoorOutdoor.TabIndex = 13;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(37, 308);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(139, 16);
            this.label13.TabIndex = 14;
            this.label13.Text = "INDOOR_OUTDOOR :";
            // 
            // txtDocotrID
            // 
            this.txtDocotrID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtDocotrID.Location = new System.Drawing.Point(182, 346);
            this.txtDocotrID.Name = "txtDocotrID";
            this.txtDocotrID.Size = new System.Drawing.Size(196, 22);
            this.txtDocotrID.TabIndex = 11;
            this.txtDocotrID.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(101, 349);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 16);
            this.label12.TabIndex = 12;
            this.label12.Text = "Doctor ID :";
            this.label12.Visible = false;
            // 
            // cmbDoctorType
            // 
            this.cmbDoctorType.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbDoctorType.FormattingEnabled = true;
            this.cmbDoctorType.Location = new System.Drawing.Point(184, 45);
            this.cmbDoctorType.Name = "cmbDoctorType";
            this.cmbDoctorType.Size = new System.Drawing.Size(196, 23);
            this.cmbDoctorType.TabIndex = 2;
            this.cmbDoctorType.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cmbDoctorType_KeyUp);
            // 
            // cmbDoctorSpecialization
            // 
            this.cmbDoctorSpecialization.Font = new System.Drawing.Font("Arial", 9F);
            this.cmbDoctorSpecialization.FormattingEnabled = true;
            this.cmbDoctorSpecialization.Location = new System.Drawing.Point(184, 16);
            this.cmbDoctorSpecialization.Name = "cmbDoctorSpecialization";
            this.cmbDoctorSpecialization.Size = new System.Drawing.Size(196, 23);
            this.cmbDoctorSpecialization.TabIndex = 1;
            this.cmbDoctorSpecialization.SelectedIndexChanged += new System.EventHandler(this.cmbDoctorSpecialization_SelectedIndexChanged);
            this.cmbDoctorSpecialization.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cmbDoctorSpecialization_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(86, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "Doctor Type :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(25, 18);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(152, 16);
            this.label20.TabIndex = 7;
            this.label20.Text = "Doctor Specialization :";
            // 
            // txtPatientCommission
            // 
            this.txtPatientCommission.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtPatientCommission.Location = new System.Drawing.Point(183, 273);
            this.txtPatientCommission.Name = "txtPatientCommission";
            this.txtPatientCommission.Size = new System.Drawing.Size(86, 22);
            this.txtPatientCommission.TabIndex = 10;
            this.txtPatientCommission.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPatientComission_KeyUp);
            // 
            // txtDoctorPSContact
            // 
            this.txtDoctorPSContact.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtDoctorPSContact.Location = new System.Drawing.Point(183, 214);
            this.txtDoctorPSContact.Name = "txtDoctorPSContact";
            this.txtDoctorPSContact.Size = new System.Drawing.Size(196, 22);
            this.txtDoctorPSContact.TabIndex = 8;
            this.txtDoctorPSContact.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtDoctorPSContact_KeyUp);
            // 
            // txtPathologyCommission
            // 
            this.txtPathologyCommission.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtPathologyCommission.Location = new System.Drawing.Point(183, 245);
            this.txtPathologyCommission.Name = "txtPathologyCommission";
            this.txtPathologyCommission.Size = new System.Drawing.Size(87, 22);
            this.txtPathologyCommission.TabIndex = 9;
            this.txtPathologyCommission.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPathologyCommission_KeyUp);
            // 
            // txtDoctorContact
            // 
            this.txtDoctorContact.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtDoctorContact.Location = new System.Drawing.Point(183, 186);
            this.txtDoctorContact.Name = "txtDoctorContact";
            this.txtDoctorContact.Size = new System.Drawing.Size(197, 22);
            this.txtDoctorContact.TabIndex = 7;
            this.txtDoctorContact.Tag = "7";
            this.txtDoctorContact.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtDoctorContact_KeyUp);
            // 
            // txtChamberAddress
            // 
            this.txtChamberAddress.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtChamberAddress.Location = new System.Drawing.Point(184, 158);
            this.txtChamberAddress.Name = "txtChamberAddress";
            this.txtChamberAddress.Size = new System.Drawing.Size(196, 22);
            this.txtChamberAddress.TabIndex = 6;
            this.txtChamberAddress.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtChamberAddress_KeyUp);
            // 
            // txtPersonalAddress
            // 
            this.txtPersonalAddress.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtPersonalAddress.Location = new System.Drawing.Point(184, 130);
            this.txtPersonalAddress.Name = "txtPersonalAddress";
            this.txtPersonalAddress.Size = new System.Drawing.Size(196, 22);
            this.txtPersonalAddress.TabIndex = 5;
            this.txtPersonalAddress.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPersonalAddress_KeyUp);
            // 
            // txtContactPerson
            // 
            this.txtContactPerson.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtContactPerson.Location = new System.Drawing.Point(184, 102);
            this.txtContactPerson.Name = "txtContactPerson";
            this.txtContactPerson.Size = new System.Drawing.Size(196, 22);
            this.txtContactPerson.TabIndex = 4;
            this.txtContactPerson.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtContactPerson_KeyUp);
            // 
            // txtDoctorName
            // 
            this.txtDoctorName.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtDoctorName.Location = new System.Drawing.Point(184, 74);
            this.txtDoctorName.Name = "txtDoctorName";
            this.txtDoctorName.Size = new System.Drawing.Size(196, 22);
            this.txtDoctorName.TabIndex = 3;
            this.txtDoctorName.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtDoctorName_KeyUp);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(35, 276);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 16);
            this.label4.TabIndex = 7;
            this.label4.Text = "Patient Commission :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(38, 217);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(139, 16);
            this.label9.TabIndex = 7;
            this.label9.Text = "Doctor (PS) Contact :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(276, 276);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 16);
            this.label11.TabIndex = 7;
            this.label11.Text = "(%)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(276, 248);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 16);
            this.label10.TabIndex = 7;
            this.label10.Text = "(%)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(16, 248);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(161, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "Pathology Commission :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(68, 189);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 16);
            this.label8.TabIndex = 7;
            this.label8.Text = "Doctor Contact :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(49, 161);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(128, 16);
            this.label7.TabIndex = 7;
            this.label7.Text = "Chamber Address :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(51, 133);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 16);
            this.label6.TabIndex = 7;
            this.label6.Text = "Personal Address :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(35, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(142, 16);
            this.label5.TabIndex = 7;
            this.label5.Text = "Contact Person (PS) :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(79, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "Doctor Name :";
            // 
            // Indoor_Doctor_Setup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(224)))), ((int)(((byte)(248)))));
            this.ClientSize = new System.Drawing.Size(1003, 471);
            this.Controls.Add(this.groupBox2);
            this.Name = "Indoor_Doctor_Setup";
            this.Text = "Indoor Doctor Setup";
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDoctorInfo)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnReport;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dgvDoctorInfo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtDoctorName;
        private System.Windows.Forms.ComboBox cmbDoctorSpecialization;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cmbDoctorType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDoctorPSContact;
        private System.Windows.Forms.TextBox txtDoctorContact;
        private System.Windows.Forms.TextBox txtChamberAddress;
        private System.Windows.Forms.TextBox txtPersonalAddress;
        private System.Windows.Forms.TextBox txtContactPerson;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPatientCommission;
        private System.Windows.Forms.TextBox txtPathologyCommission;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtDocotrID;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmbIndoorOutdoor;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
    }
}