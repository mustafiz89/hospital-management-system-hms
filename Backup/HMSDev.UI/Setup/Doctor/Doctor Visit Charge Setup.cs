﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using IA.Model.Setup.Other.Doctor;
using IA.BIZ.Setup.Doctor;
using POS_SYSTEM;
namespace HMSDev.UI.Setup.Doctor
{
    public partial class Doctor_Visit_Charge_Setup : Form
    {
        public Doctor_Visit_Charge_Setup()
        {
            InitializeComponent();
            BindComboDoctorName();
            cmbActive.SelectedIndex = 0;
            dgvDoctorCharge.AutoGenerateColumns = false;
            dgvDoctorCharge.DataSource = DoctorFetch.getALLDoctorCharge();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            DOCTOR_CHARGE objDoctorVisitCharge = new DOCTOR_CHARGE();
            objDoctorVisitCharge.DOCTOR_ID =new Guid( cmbDoctorName.SelectedValue.ToString());
            objDoctorVisitCharge.DOCTOR_NEW_VISIT_CHARGE = Convert.ToDecimal(txtNewVisitCharge.Text);
            objDoctorVisitCharge.DOCTOR_FOLLOW_UP_CHARGE = Convert.ToDecimal(txtFollowUpCharge.Text);
            objDoctorVisitCharge.DOCTOR_CALL_ON_CHARGE = Convert.ToDecimal(txtCallOnCharge.Text);
            if (cmbActive.SelectedIndex == 0)
                objDoctorVisitCharge.STATUS_FLAG = "A";
            else
                objDoctorVisitCharge.STATUS_FLAG = "U";
            int i = DoctorSave.saveDoctorVisitCharge(objDoctorVisitCharge);
            if (i > 0)
            {
                dgvDoctorCharge.AutoGenerateColumns = false;
                dgvDoctorCharge.DataSource = DoctorFetch.getALLDoctorCharge();
                MessageBox.Show("Save Successfully");

                txtNewVisitCharge.Text = "";
                txtFollowUpCharge.Text = "";
                txtCallOnCharge.Text = "";
            }
            else
                MessageBox.Show("Save Not Successfully");
        }

        private void BindComboDoctorName()
        {
            cmbDoctorName.DataSource = DoctorFetch.getALLDoctor();
            cmbDoctorName.ValueMember = "DOCTOR_ID";
            cmbDoctorName.DisplayMember = "DOCTOR_NAME";
            cmbDoctorName.SelectedIndex = 0;
        }

        private void cmbDoctorName_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtNewVisitCharge_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtFollowUpCharge_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void txtCallOnCharge_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void cmbActive_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if ((e.KeyCode == Keys.Enter) || (e.KeyCode == Keys.Return))
                {
                    this.SelectNextControl((Control)sender, true, true, true, true);
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DOCTOR_CHARGE objDoctorVisitCharge = new DOCTOR_CHARGE();
            objDoctorVisitCharge.DOCTOR_ID =new Guid (txtID.Text);
            objDoctorVisitCharge.DOCTOR_NAME =cmbDoctorName.SelectedValue.ToString();
            objDoctorVisitCharge.DOCTOR_NEW_VISIT_CHARGE = Convert.ToDecimal(txtNewVisitCharge.Text);
            objDoctorVisitCharge.DOCTOR_FOLLOW_UP_CHARGE = Convert.ToDecimal(txtFollowUpCharge.Text);
            objDoctorVisitCharge.DOCTOR_CALL_ON_CHARGE = Convert.ToDecimal(txtCallOnCharge.Text);
            if (cmbActive.SelectedIndex == 0)
                objDoctorVisitCharge.STATUS_FLAG = "A";
            else
                objDoctorVisitCharge.STATUS_FLAG = "U";
            int i = DoctorSave.UpdateDoctorVisitCharge(objDoctorVisitCharge);
            if (i > 0)
            {
                dgvDoctorCharge.AutoGenerateColumns = false;
                dgvDoctorCharge.DataSource = DoctorFetch.getALLDoctorCharge();
                MessageBox.Show("Update Successfully");

                txtID.Text = "";
                txtNewVisitCharge.Text = "";
                txtFollowUpCharge.Text = "";
                txtCallOnCharge.Text = "";
            }
            else
                MessageBox.Show("Update Not Successfully");
        }

        private void dgvDoctorCharge_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridViewRow dr = dgvDoctorCharge.SelectedRows[0];
            cmbDoctorName.SelectedIndex = cmbDoctorName.FindString(dr.Cells["DOCTOR_NAME"].Value.ToString());
            txtID.Text = dr.Cells["Column1"].Value.ToString();
            txtNewVisitCharge.Text = dr.Cells["Column3"].Value.ToString();
            txtFollowUpCharge.Text = dr.Cells["Column4"].Value.ToString();
            txtCallOnCharge.Text = dr.Cells["Column5"].Value.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DOCTOR_CHARGE objDoctorVisitCharge = new DOCTOR_CHARGE();
            objDoctorVisitCharge.DOCTOR_ID = new Guid(txtID.Text);
          
            int i = DoctorSave.DeleteDoctorVisitCharge(objDoctorVisitCharge);
            if (i > 0)
            {
                dgvDoctorCharge.AutoGenerateColumns = false;
                dgvDoctorCharge.DataSource = DoctorFetch.getALLDoctorCharge();
                MessageBox.Show("Delete Successfully");

                txtID.Text = "";
                txtNewVisitCharge.Text = "";
                txtFollowUpCharge.Text = "";
                txtCallOnCharge.Text = "";
            }
            else
                MessageBox.Show("Delete Not Successfully");
        }
    }
}
