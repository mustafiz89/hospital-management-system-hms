﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IA.Model.Setup.Other.Other_Service_Setup;
using System.Data.SqlClient;
using DynamicSoft.DAL;
using System.Data;

namespace IA.BIZ.Setup.Other_Service_Setup
{
    public class OtherServiceFetch
    {
        public static List<OTHER_SERVICE> getALLOtherService()
        {
            List<OTHER_SERVICE> objOtherService = new List<OTHER_SERVICE>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_OTHER_SERVICE_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        OTHER_SERVICE obj = new OTHER_SERVICE();
                        obj.OTHER_SERVICE_ID =new Guid( dr["OTHER_SERVICE_ID"].ToString());
                        obj.OTHER_SERVICE_NAME = dr["OTHER_SERVICE_NAME"].ToString();
                        obj.OTHER_SERVICE_CHARGE =decimal.Parse(dr["OTHER_SERVICE_CHARGE"].ToString());
                        obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                        objOtherService.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objOtherService;
        }
    }
}
