﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DynamicSoft.DAL;
using IA.Model.Setup.Other.Other_Service_Setup;
using System.Data.SqlClient;
using System.Data;
namespace IA.BIZ.Setup.Other_Service_Setup
{
    public class OtherServiceSetup
    {
        public static int saveOtherService(OTHER_SERVICE objOtherService)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_OTHER_SERVICE_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OTHER_SERVICE_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@OTHER_SERVICE_NAME"].Value = objOtherService.OTHER_SERVICE_NAME;
                cmd.Parameters.Add(new SqlParameter("@OTHER_SERVICE_CHARGE", SqlDbType.Decimal));
                cmd.Parameters["@OTHER_SERVICE_CHARGE"].Value = objOtherService.OTHER_SERVICE_CHARGE;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar,30));
                cmd.Parameters["@STATUS_FLAG"].Value = objOtherService.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdateOtherService(OTHER_SERVICE objOtherService)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_OTHER_SERVICE_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OTHER_SERVICE_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@OTHER_SERVICE_ID"].Value = objOtherService.OTHER_SERVICE_ID;
                cmd.Parameters.Add(new SqlParameter("@OTHER_SERVICE_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@OTHER_SERVICE_NAME"].Value = objOtherService.OTHER_SERVICE_NAME;
                cmd.Parameters.Add(new SqlParameter("@OTHER_SERVICE_CHARGE", SqlDbType.Decimal));
                cmd.Parameters["@OTHER_SERVICE_CHARGE"].Value = objOtherService.OTHER_SERVICE_CHARGE;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objOtherService.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteOtherService(OTHER_SERVICE objOtherService)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_OTHER_SERVICE_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OTHER_SERVICE_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@OTHER_SERVICE_ID"].Value = objOtherService.OTHER_SERVICE_ID;
               
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }
    }
}
