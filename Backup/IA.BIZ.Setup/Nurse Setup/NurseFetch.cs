﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IA.Model.Setup.Other.Nurse_Setup;
using System.Data.SqlClient;
using DynamicSoft.DAL;
using System.Data;

namespace IA.BIZ.Setup.Nurse_Setup
{
    public class NurseFetch
    {
        public static List<NURSE> getALLNurse()
        {
            List<NURSE> objNurse = new List<NURSE>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_NURSE_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        NURSE obj = new NURSE();
                        obj.NURSE_ID =new Guid (dr["NURSE_ID"].ToString());
                        obj.NURSE_NAME = dr["NURSE_NAME"].ToString();
                        obj.FLOOR_ID =new Guid (dr["FLOOR_ID"].ToString());
                        obj.NURSE_ADDRESS = dr["NURSE_ADDRESS"].ToString();
                        obj.NURSE_CONTACT = dr["NURSE_CONTACT"].ToString();
                        obj.FLOOR_NAME = dr["FLOOR_NAME"].ToString();
                        objNurse.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objNurse;
        }
    }
}
