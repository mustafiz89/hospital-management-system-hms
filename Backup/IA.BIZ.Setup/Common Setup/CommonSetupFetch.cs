﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IA.Model.Setup.Other.Common_Setup;
using System.Data.SqlClient;
using DynamicSoft.DAL;
using System.Data;

namespace IA.BIZ.Setup.Common_Setup
{
   public class CommonSetupFetch
    {
       public static List<BLOOD_GROUP> getAllBLOOD_GROUP()
       {
           List< BLOOD_GROUP> objBloodGroup = new List< BLOOD_GROUP>();
           SqlConnection conn = ConnectionClass.GetConnection();
           SqlCommand objSqlCommand = new SqlCommand();
           objSqlCommand.CommandText = "FSP_BLOOD_GROUP_GA";
           objSqlCommand.CommandType = CommandType.StoredProcedure;
           objSqlCommand.Connection = conn;
           conn.Open();
           try
           {
               using (objSqlCommand)
               {
                   SqlDataReader dr = objSqlCommand.ExecuteReader();
                   while (dr.Read())
                   {
                        BLOOD_GROUP obj = new  BLOOD_GROUP();
                        obj.BLOOD_GROUP_ID =new Guid (dr["BLOOD_GROUP_ID"].ToString());
                        obj.BLOOD_GROUP_NAME = dr["BLOOD_GROUP_NAME"].ToString();
                        obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                       objBloodGroup.Add(obj);
                   }
               }
           }
           catch { }
           finally 
           {
               conn.Close();
           }
           return objBloodGroup;
       }

       public static List<NATIONALITY> getAllNATIONALITY()
       {
           List<NATIONALITY> objNationality = new List<NATIONALITY>();
           SqlConnection conn = ConnectionClass.GetConnection();
           SqlCommand objSqlCommand = new SqlCommand();
           objSqlCommand.CommandText = "FSP_NATIONALITY_GA";
           objSqlCommand.CommandType = CommandType.StoredProcedure;
           objSqlCommand.Connection = conn;
           conn.Open();
           try
           {
               using (objSqlCommand)
               {
                   SqlDataReader dr = objSqlCommand.ExecuteReader();
                   while (dr.Read())
                   {
                       NATIONALITY obj = new NATIONALITY();
                       obj.NATIONALITY_ID =new Guid (dr["NATIONALITY_ID"].ToString());
                       obj.NATIONALITY_NAME = dr["NATIONALITY_NAME"].ToString();
                       obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                       objNationality.Add(obj);
                   }
               }
           }
           catch { }
           finally
           {
               conn.Close();
           }
           return objNationality;
       }

       public static List<GENDER> getAllGENDER()
       {
           List<GENDER> objGender= new List<GENDER>();
           SqlConnection conn = ConnectionClass.GetConnection();
           SqlCommand objSqlCommand = new SqlCommand();
           objSqlCommand.CommandText = "FSP_GENDER_GA";
           objSqlCommand.CommandType = CommandType.StoredProcedure;
           objSqlCommand.Connection = conn;
           conn.Open();
           try
           {
               using (objSqlCommand)
               {
                   SqlDataReader dr = objSqlCommand.ExecuteReader();
                   while (dr.Read())
                   {
                       GENDER obj = new GENDER();
                       obj.GENDER_ID =new Guid (dr["GENDER_ID"].ToString());
                       obj.GENDER_NAME = dr["GENDER_NAME"].ToString();
                       obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                       objGender.Add(obj);
                   }
               }
           }
           catch { }
           finally
           {
               conn.Close();
           }
           return objGender;
       }

       public static List<MARITAL_STATUS> getAllMARITALSTATUS()
       {
           List<MARITAL_STATUS> objMARITAL_STATUS = new List<MARITAL_STATUS>();
           SqlConnection conn = ConnectionClass.GetConnection();
           SqlCommand objSqlCommand = new SqlCommand();
           objSqlCommand.CommandText = "FSP_MARITAL_STATUS_GA";
           objSqlCommand.CommandType = CommandType.StoredProcedure;
           objSqlCommand.Connection = conn;
           conn.Open();
           try
           {
               using (objSqlCommand)
               {
                   SqlDataReader dr = objSqlCommand.ExecuteReader();
                   while (dr.Read())
                   {
                       MARITAL_STATUS obj = new MARITAL_STATUS();
                       obj.MARITAL_STATUS_ID =new Guid (dr["MARITAL_STATUS_ID"].ToString());
                       obj.MARITAL_STATUS_NAME = dr["MARITAL_STATUS_NAME"].ToString();
                       obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                       objMARITAL_STATUS.Add(obj);
                   }
               }
           }
           catch { }
           finally
           {
               conn.Close();
           }
           return objMARITAL_STATUS;
       }

       public static List<OCCUPATION> getALLOCCUPATION()
       {
           List<OCCUPATION> objOccupation = new List<OCCUPATION>();
           SqlConnection conn = ConnectionClass.GetConnection();
           SqlCommand objSqlCommand = new SqlCommand();
           objSqlCommand.CommandText = "FSP_OCCUPATION_GA";
           objSqlCommand.CommandType = CommandType.StoredProcedure;
           objSqlCommand.Connection = conn;
           conn.Open();
           try
           {
               using (objSqlCommand)
               {
                   SqlDataReader dr = objSqlCommand.ExecuteReader();
                   while (dr.Read())
                   {
                       OCCUPATION obj = new OCCUPATION();
                       obj.OCCUPATION_ID =new Guid (dr["OCCUPATION_ID"].ToString());
                       obj.OCCUPATION_NAME = dr["OCCUPATION_NAME"].ToString();
                       obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                       objOccupation.Add(obj);
                   }
               }
           }
           catch { }
           finally
           {
               conn.Close();
           }
           return objOccupation;
       }

       public static List<RELIGION> getRELIGION()
       {
           List<RELIGION> objReligion = new List<RELIGION>();
           SqlConnection conn = ConnectionClass.GetConnection();
           SqlCommand objSqlCommand = new SqlCommand();
           objSqlCommand.CommandText = "FSP_RELIGION_GA";
           objSqlCommand.CommandType = CommandType.StoredProcedure;
           objSqlCommand.Connection = conn;
           conn.Open();
           try
           {
               using (objSqlCommand)
               {
                   SqlDataReader dr = objSqlCommand.ExecuteReader();
                   while (dr.Read())
                   {
                       RELIGION obj = new RELIGION();
                       obj.RELIGION_ID =new Guid (dr["RELIGION_ID"].ToString());
                       obj.RELIGION_NAME = dr["RELIGION_NAME"].ToString();
                       obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                       objReligion.Add(obj);
                   }
               }
           }
           catch { }
           finally
           {
               conn.Close();
           }
           return objReligion;
       }

       public static List<SALUTION> getALLSALUTION()
       {
           List<SALUTION> objSalution = new List<SALUTION>();
           SqlConnection conn = ConnectionClass.GetConnection();
           SqlCommand objSqlCommand = new SqlCommand();
           objSqlCommand.CommandText = "FSP_SALUTION_GA";
           objSqlCommand.CommandType = CommandType.StoredProcedure;
           objSqlCommand.Connection = conn;
           conn.Open();
           try
           {
               using (objSqlCommand)
               {
                   SqlDataReader dr = objSqlCommand.ExecuteReader();
                   while (dr.Read())
                   {
                       SALUTION obj = new SALUTION();
                       obj.SALUTION_ID =new Guid (dr["SALUTION_ID"].ToString());
                       obj.SALUTION_NAME = dr["SALUTION_NAME"].ToString();
                       obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                       objSalution.Add(obj);
                   }
               }
           }
           catch { }
           finally
           {
               conn.Close();
           }
           return objSalution;
       }
   
    }
}
