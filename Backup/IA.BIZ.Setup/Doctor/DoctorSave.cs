﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DynamicSoft.DAL;
using IA.Model.Setup.Other.Doctor;
using System.Data.SqlClient;
using System.Data;

namespace IA.BIZ.Setup.Doctor
{
    public class DoctorSave
    {
        public static int saveIndoorDoctor(INDOOR_DOCTOR ObjIndoorDoctor)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_DOCTOR_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_SPECIALIZATION_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@DOCTOR_SPECIALIZATION_ID"].Value = ObjIndoorDoctor.DOCTOR_SPECIALIZATION_ID;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_TYPE_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@DOCTOR_TYPE_ID"].Value = ObjIndoorDoctor.DOCTOR_TYPE_ID;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_NAME",SqlDbType.NVarChar,30));
                cmd.Parameters["@DOCTOR_NAME"].Value = ObjIndoorDoctor.DOCTOR_NAME;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_CONTACT_PERSON", SqlDbType.NVarChar, 30));
                cmd.Parameters["@DOCTOR_CONTACT_PERSON"].Value = ObjIndoorDoctor.DOCTOR_CONTACT_PERSON;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_PERSONAL_ADDRESS", SqlDbType.NVarChar, 30));
                cmd.Parameters["@DOCTOR_PERSONAL_ADDRESS"].Value = ObjIndoorDoctor.DOCTOR_PERSONAL_ADDRESS;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_CHAMBER_ADDRESS", SqlDbType.NVarChar, 30));
                cmd.Parameters["@DOCTOR_CHAMBER_ADDRESS"].Value = ObjIndoorDoctor.DOCTOR_CHAMBER_ADDRESS;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_PERSONAL_CONTACT", SqlDbType.NVarChar, 30));
                cmd.Parameters["@DOCTOR_PERSONAL_CONTACT"].Value = ObjIndoorDoctor.DOCTOR_PERSONAL_CONTACT;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_PS_CONTACT", SqlDbType.NVarChar, 30));
                cmd.Parameters["@DOCTOR_PS_CONTACT"].Value = ObjIndoorDoctor.DOCTOR_PS_CONTACT;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_PATHOLOGY_COMMISSION", SqlDbType.Decimal));
                cmd.Parameters["@DOCTOR_PATHOLOGY_COMMISSION"].Value = ObjIndoorDoctor.DOCTOR_PATHOLOGY_COMMISSION;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_PATIENT_COMMISSION", SqlDbType.Decimal));
                cmd.Parameters["@DOCTOR_PATIENT_COMMISSION"].Value = ObjIndoorDoctor.DOCTOR_PATIENT_COMMISSION;
                cmd.Parameters.Add(new SqlParameter("@INDOOR_OUTDOOR_FLAG", SqlDbType.Char,1));
                cmd.Parameters["@INDOOR_OUTDOOR_FLAG"].Value = ObjIndoorDoctor.INDOOR_OUTDOOR_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally { conn.Close(); }
            }
            return i;
        }

        public static int UpdateIndoorDoctor(INDOOR_DOCTOR ObjIndoorDoctor)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_DOCTOR_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@DOCTOR_ID"].Value = ObjIndoorDoctor.DOCTOR_ID;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_SPECIALIZATION_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@DOCTOR_SPECIALIZATION_ID"].Value = ObjIndoorDoctor.DOCTOR_SPECIALIZATION_ID;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_TYPE_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@DOCTOR_TYPE_ID"].Value = ObjIndoorDoctor.DOCTOR_TYPE_ID;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@DOCTOR_NAME"].Value = ObjIndoorDoctor.DOCTOR_NAME;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_CONTACT_PERSON", SqlDbType.NVarChar, 30));
                cmd.Parameters["@DOCTOR_CONTACT_PERSON"].Value = ObjIndoorDoctor.DOCTOR_CONTACT_PERSON;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_PERSONAL_ADDRESS", SqlDbType.NVarChar, 30));
                cmd.Parameters["@DOCTOR_PERSONAL_ADDRESS"].Value = ObjIndoorDoctor.DOCTOR_PERSONAL_ADDRESS;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_CHAMBER_ADDRESS", SqlDbType.NVarChar, 30));
                cmd.Parameters["@DOCTOR_CHAMBER_ADDRESS"].Value = ObjIndoorDoctor.DOCTOR_CHAMBER_ADDRESS;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_PERSONAL_CONTACT", SqlDbType.NVarChar, 30));
                cmd.Parameters["@DOCTOR_PERSONAL_CONTACT"].Value = ObjIndoorDoctor.DOCTOR_PERSONAL_CONTACT;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_PS_CONTACT", SqlDbType.NVarChar, 30));
                cmd.Parameters["@DOCTOR_PS_CONTACT"].Value = ObjIndoorDoctor.DOCTOR_PS_CONTACT;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_PATHOLOGY_COMMISSION", SqlDbType.Decimal));
                cmd.Parameters["@DOCTOR_PATHOLOGY_COMMISSION"].Value = ObjIndoorDoctor.DOCTOR_PATHOLOGY_COMMISSION;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_PATIENT_COMMISSION", SqlDbType.Decimal));
                cmd.Parameters["@DOCTOR_PATIENT_COMMISSION"].Value = ObjIndoorDoctor.DOCTOR_PATIENT_COMMISSION;
                cmd.Parameters.Add(new SqlParameter("@INDOOR_OUTDOOR_FLAG", SqlDbType.Char,1));
                cmd.Parameters["@INDOOR_OUTDOOR_FLAG"].Value = ObjIndoorDoctor.INDOOR_OUTDOOR_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally { conn.Close(); }
            }
            return i;
        }

        public static int DeleteDoctor(INDOOR_DOCTOR ObjIndoorDoctor)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_DOCTOR_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@DOCTOR_ID"].Value = ObjIndoorDoctor.DOCTOR_ID;
               
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally { conn.Close(); }
            }
            return i;
        }

        public static int saveDoctorSpecialization(DOCTOR_SPECIALIZATION objDoctorSpecialization)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_DOCTOR_SPECIALIZATION_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_SPECIALIZATION_NAME",SqlDbType.NVarChar,30));
                cmd.Parameters["@DOCTOR_SPECIALIZATION_NAME"].Value=objDoctorSpecialization.DOCTOR_SPECIALIZATION_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objDoctorSpecialization.STATUS_FLAG;
                conn.Open();
                try
                {
                    i=cmd.ExecuteNonQuery();
                }
                catch{}
                    finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdateDoctorSpecialization(DOCTOR_SPECIALIZATION objDoctorSpecialization)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_DOCTOR_SPECIALIZATION_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_SPECIALIZATION_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@DOCTOR_SPECIALIZATION_ID"].Value = objDoctorSpecialization.DOCTOR_SPECIALIZATION_ID;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_SPECIALIZATION_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@DOCTOR_SPECIALIZATION_NAME"].Value = objDoctorSpecialization.DOCTOR_SPECIALIZATION_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objDoctorSpecialization.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteDoctorSpecialization(DOCTOR_SPECIALIZATION objDoctorSpecialization)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_DOCTOR_SPECIALIZATION_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_SPECIALIZATION_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@DOCTOR_SPECIALIZATION_ID"].Value = objDoctorSpecialization.DOCTOR_SPECIALIZATION_ID;
            
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int saveDoctorType(DOCTOR_TYPE objDoctorType)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_DOCTOR_TYPE_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_TYPE_NAME",SqlDbType.NVarChar,30));
                cmd.Parameters["@DOCTOR_TYPE_NAME"].Value=objDoctorType.DOCTOR_TYPE_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objDoctorType.STATUS_FLAG;
                conn.Open();
                try
                {
                    i=cmd.ExecuteNonQuery();
                }
                catch{}
                    finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdateDoctorType(DOCTOR_TYPE objDoctorType)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_DOCTOR_TYPE_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_TYPE_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@DOCTOR_TYPE_ID"].Value = objDoctorType.DOCTOR_TYPE_ID;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_TYPE_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@DOCTOR_TYPE_NAME"].Value = objDoctorType.DOCTOR_TYPE_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objDoctorType.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteDoctorType(DOCTOR_TYPE objDoctorType)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_DOCTOR_TYPE_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_TYPE_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@DOCTOR_TYPE_ID"].Value = objDoctorType.DOCTOR_TYPE_ID;
            
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int saveDoctorTypeOfVisit(DOCTOR_TYPE_OF_VISIT objDoctorTypeOfVisit)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_DOCTOR_TYPE_OF_VISIT_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_TYPE_OF_VISIT_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@DOCTOR_TYPE_OF_VISIT_NAME"].Value = objDoctorTypeOfVisit.DOCTOR_TYPE_OF_VISIT_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objDoctorTypeOfVisit.STATUS_FLAG;
                conn.Open();

                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdateDoctorTypeOfVisit(DOCTOR_TYPE_OF_VISIT objDoctorTypeOfVisit)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_DOCTOR_TYPE_OF_VISIT_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_TYPE_OF_VISIT_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@DOCTOR_TYPE_OF_VISIT_ID"].Value = objDoctorTypeOfVisit.DOCTOR_TYPE_OF_VISIT_ID;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_TYPE_OF_VISIT_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@DOCTOR_TYPE_OF_VISIT_NAME"].Value = objDoctorTypeOfVisit.DOCTOR_TYPE_OF_VISIT_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objDoctorTypeOfVisit.STATUS_FLAG;
                conn.Open();

                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteDoctorTypeOfVisit(DOCTOR_TYPE_OF_VISIT objDoctorTypeOfVisit)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_DOCTOR_TYPE_OF_VISIT_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_TYPE_OF_VISIT_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@DOCTOR_TYPE_OF_VISIT_ID"].Value = objDoctorTypeOfVisit.DOCTOR_TYPE_OF_VISIT_ID;
            
                conn.Open();

                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }


        public static int saveDoctorVisitCharge(DOCTOR_CHARGE objDoctorVisitCharge)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_DOCTOR_CHARGE_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@DOCTOR_ID"].Value = objDoctorVisitCharge.DOCTOR_ID;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_NEW_VISIT_CHARGE", SqlDbType.Decimal));
                cmd.Parameters["@DOCTOR_NEW_VISIT_CHARGE"].Value = objDoctorVisitCharge.DOCTOR_NEW_VISIT_CHARGE;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_FOLLOW_UP_CHARGE", SqlDbType.Decimal));
                cmd.Parameters["@DOCTOR_FOLLOW_UP_CHARGE"].Value = objDoctorVisitCharge.DOCTOR_FOLLOW_UP_CHARGE;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_CALL_ON_CHARGE", SqlDbType.Decimal));
                cmd.Parameters["@DOCTOR_CALL_ON_CHARGE"].Value = objDoctorVisitCharge.DOCTOR_CALL_ON_CHARGE;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objDoctorVisitCharge.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }

        }

        public static int UpdateDoctorVisitCharge(DOCTOR_CHARGE objDoctorVisitCharge)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_DOCTOR_CHARGE_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@DOCTOR_ID"].Value = objDoctorVisitCharge.DOCTOR_ID;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_NEW_VISIT_CHARGE", SqlDbType.Decimal));
                cmd.Parameters["@DOCTOR_NEW_VISIT_CHARGE"].Value = objDoctorVisitCharge.DOCTOR_NEW_VISIT_CHARGE;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_FOLLOW_UP_CHARGE", SqlDbType.Decimal));
                cmd.Parameters["@DOCTOR_FOLLOW_UP_CHARGE"].Value = objDoctorVisitCharge.DOCTOR_FOLLOW_UP_CHARGE;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_CALL_ON_CHARGE", SqlDbType.Decimal));
                cmd.Parameters["@DOCTOR_CALL_ON_CHARGE"].Value = objDoctorVisitCharge.DOCTOR_CALL_ON_CHARGE;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objDoctorVisitCharge.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteDoctorVisitCharge(DOCTOR_CHARGE objDoctorVisitCharge)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_DOCTOR_CHARGE_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DOCTOR_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@DOCTOR_ID"].Value = objDoctorVisitCharge.DOCTOR_ID;
              
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int saveMedicineDose(MEDICINE_DOSE objMedicineDose)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_MEDICINE_DOSE_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@MEDICINE_DOSE_NAME", SqlDbType.NVarChar, 50));
                cmd.Parameters["@MEDICINE_DOSE_NAME"].Value = objMedicineDose.MEDICINE_DOSE_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objMedicineDose.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdateMedicineDose(MEDICINE_DOSE objMedicineDose)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_MEDICINE_DOSE_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@MEDICINE_DOSE_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@MEDICINE_DOSE_ID"].Value = objMedicineDose.MEDICINE_DOSE_ID;
                cmd.Parameters.Add(new SqlParameter("@MEDICINE_DOSE_NAME", SqlDbType.NVarChar, 50));
                cmd.Parameters["@MEDICINE_DOSE_NAME"].Value = objMedicineDose.MEDICINE_DOSE_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objMedicineDose.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteMedicineDose(MEDICINE_DOSE objMedicineDose)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_MEDICINE_DOSE_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@MEDICINE_DOSE_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@MEDICINE_DOSE_ID"].Value = objMedicineDose.MEDICINE_DOSE_ID;
         
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

       
            public static int saveOutdoorDoctor(OUTDOOR_DOCTOR ObjOutdoorDoctor)
            {
                int i = 0;
                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlConnection conn = ConnectionClass.GetConnection();
                    cmd.Connection = conn;
                    cmd.CommandText = "FSP_OUTDOOR_DOCTOR_I";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_SPECIALIZATION_ID", SqlDbType.UniqueIdentifier));
                    cmd.Parameters["@DOCTOR_SPECIALIZATION_ID"].Value = ObjOutdoorDoctor.DOCTOR_SPECIALIZATION_ID;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_TYPE_ID", SqlDbType.UniqueIdentifier));
                    cmd.Parameters["@DOCTOR_TYPE_ID"].Value = ObjOutdoorDoctor.DOCTOR_TYPE_ID;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_NAME", SqlDbType.NVarChar, 30));
                    cmd.Parameters["@DOCTOR_NAME"].Value = ObjOutdoorDoctor.DOCTOR_NAME;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_CONTACT_PERSON", SqlDbType.NVarChar, 30));
                    cmd.Parameters["@DOCTOR_CONTACT_PERSON"].Value = ObjOutdoorDoctor.DOCTOR_CONTACT_PERSON;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_PERSONAL_ADDRESS", SqlDbType.NVarChar, 30));
                    cmd.Parameters["@DOCTOR_PERSONAL_ADDRESS"].Value = ObjOutdoorDoctor.DOCTOR_PERSONAL_ADDRESS;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_CHAMBER_ADDRESS", SqlDbType.NVarChar, 30));
                    cmd.Parameters["@DOCTOR_CHAMBER_ADDRESS"].Value = ObjOutdoorDoctor.DOCTOR_CHAMBER_ADDRESS;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_PERSONAL_CONTACT", SqlDbType.NVarChar, 30));
                    cmd.Parameters["@DOCTOR_PERSONAL_CONTACT"].Value = ObjOutdoorDoctor.DOCTOR_PERSONAL_CONTACT;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_PS_CONTACT", SqlDbType.NVarChar, 30));
                    cmd.Parameters["@DOCTOR_PS_CONTACT"].Value = ObjOutdoorDoctor.DOCTOR_PS_CONTACT;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_PATHOLOGY_COMMISSION", SqlDbType.Decimal));
                    cmd.Parameters["@DOCTOR_PATHOLOGY_COMMISSION"].Value = ObjOutdoorDoctor.DOCTOR_PATHOLOGY_COMMISSION;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_PATIENT_COMMISSION", SqlDbType.Decimal));
                    cmd.Parameters["@DOCTOR_PATIENT_COMMISSION"].Value = ObjOutdoorDoctor.DOCTOR_PATIENT_COMMISSION;
                    

                    conn.Open();
                    try
                    {
                        i = cmd.ExecuteNonQuery();
                    }
                    catch { }
                    finally { conn.Close(); }
                }
                return i;
            }

            public static int UpdateOutdoorDoctor(OUTDOOR_DOCTOR ObjOutdoorDoctor)
            {
                int i = 0;
                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlConnection conn = ConnectionClass.GetConnection();
                    cmd.Connection = conn;
                    cmd.CommandText = "FSP_OUTDOOR_DOCTOR_U";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_ID", SqlDbType.UniqueIdentifier));
                    cmd.Parameters["@DOCTOR_ID"].Value = ObjOutdoorDoctor.DOCTOR_ID;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_SPECIALIZATION_ID", SqlDbType.UniqueIdentifier));
                    cmd.Parameters["@DOCTOR_SPECIALIZATION_ID"].Value = ObjOutdoorDoctor.DOCTOR_SPECIALIZATION_ID;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_TYPE_ID", SqlDbType.UniqueIdentifier));
                    cmd.Parameters["@DOCTOR_TYPE_ID"].Value = ObjOutdoorDoctor.DOCTOR_TYPE_ID;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_NAME", SqlDbType.NVarChar, 30));
                    cmd.Parameters["@DOCTOR_NAME"].Value = ObjOutdoorDoctor.DOCTOR_NAME;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_CONTACT_PERSON", SqlDbType.NVarChar, 30));
                    cmd.Parameters["@DOCTOR_CONTACT_PERSON"].Value = ObjOutdoorDoctor.DOCTOR_CONTACT_PERSON;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_PERSONAL_ADDRESS", SqlDbType.NVarChar, 30));
                    cmd.Parameters["@DOCTOR_PERSONAL_ADDRESS"].Value = ObjOutdoorDoctor.DOCTOR_PERSONAL_ADDRESS;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_CHAMBER_ADDRESS", SqlDbType.NVarChar, 30));
                    cmd.Parameters["@DOCTOR_CHAMBER_ADDRESS"].Value = ObjOutdoorDoctor.DOCTOR_CHAMBER_ADDRESS;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_PERSONAL_CONTACT", SqlDbType.NVarChar, 30));
                    cmd.Parameters["@DOCTOR_PERSONAL_CONTACT"].Value = ObjOutdoorDoctor.DOCTOR_PERSONAL_CONTACT;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_PS_CONTACT", SqlDbType.NVarChar, 30));
                    cmd.Parameters["@DOCTOR_PS_CONTACT"].Value = ObjOutdoorDoctor.DOCTOR_PS_CONTACT;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_PATHOLOGY_COMMISSION", SqlDbType.Decimal));
                    cmd.Parameters["@DOCTOR_PATHOLOGY_COMMISSION"].Value = ObjOutdoorDoctor.DOCTOR_PATHOLOGY_COMMISSION;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_PATIENT_COMMISSION", SqlDbType.Decimal));
                    cmd.Parameters["@DOCTOR_PATIENT_COMMISSION"].Value = ObjOutdoorDoctor.DOCTOR_PATIENT_COMMISSION;


                    conn.Open();
                    try
                    {
                        i = cmd.ExecuteNonQuery();
                    }
                    catch { }
                    finally { conn.Close(); }
                }
                return i;
            }

            public static int saveDoctorAdviceComments(DOCTOR_ADVICE_COMMENTS objDoctorAdviceComments)
            {
                int i = 0;
                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlConnection conn = ConnectionClass.GetConnection();
                    cmd.Connection = conn;
                    cmd.CommandText = "FSP_DOCTOR_ADVICE_COMMENTS_I";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_ADVICE_COMMENTS_TEXT", SqlDbType.NVarChar, 50));
                    cmd.Parameters["@DOCTOR_ADVICE_COMMENTS_TEXT"].Value = objDoctorAdviceComments.DOCTOR_ADVICE_COMMENTS_TEXT;
                    conn.Open();
                    try
                    {
                        i = cmd.ExecuteNonQuery();
                    }
                    catch { }
                    finally
                    {
                        conn.Close();
                    }
                    return i;

                }
            }

            public static int UpdateDoctorAdviceComments(DOCTOR_ADVICE_COMMENTS objDoctorAdviceComments)
            {
                int i = 0;
                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlConnection conn = ConnectionClass.GetConnection();
                    cmd.Connection = conn;
                    cmd.CommandText = "FSP_DOCTOR_ADVICE_COMMENTS_U";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_ADVICE_COMMENTS_ID", SqlDbType.UniqueIdentifier));
                    cmd.Parameters["@DOCTOR_ADVICE_COMMENTS_ID"].Value = objDoctorAdviceComments.DOCTOR_ADVICE_COMMENTS_ID;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_ADVICE_COMMENTS_TEXT", SqlDbType.NVarChar, 50));
                    cmd.Parameters["@DOCTOR_ADVICE_COMMENTS_TEXT"].Value = objDoctorAdviceComments.DOCTOR_ADVICE_COMMENTS_TEXT;
                    conn.Open();
                    try
                    {
                        i = cmd.ExecuteNonQuery();
                    }
                    catch { }
                    finally
                    {
                        conn.Close();
                    }
                    return i;
                }
            }

            public static int DeleteDoctorAdviceComments(DOCTOR_ADVICE_COMMENTS objDoctorAdviceComments)
            {
                int i = 0;
                using (SqlCommand cmd = new SqlCommand())
                {
                    SqlConnection conn = ConnectionClass.GetConnection();
                    cmd.Connection = conn;
                    cmd.CommandText = "FSP_DOCTOR_ADVICE_COMMENTS_D";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@DOCTOR_ADVICE_COMMENTS_ID", SqlDbType.UniqueIdentifier));
                    cmd.Parameters["@DOCTOR_ADVICE_COMMENTS_ID"].Value = objDoctorAdviceComments.DOCTOR_ADVICE_COMMENTS_ID;
                  
                    conn.Open();
                    try
                    {
                        i = cmd.ExecuteNonQuery();
                    }
                    catch { }
                    finally
                    {
                        conn.Close();
                    }
                    return i;
                }
            }

    }
}
