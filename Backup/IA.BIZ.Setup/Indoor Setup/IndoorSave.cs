﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DynamicSoft.DAL;
using IA.Model.Setup.Other.Indoor_Setup;
using System.Data.SqlClient;
using System.Data;
namespace IA.BIZ.Setup.Indoor_Setup
{
    public class IndoorSave
    {
        public static int saveAddmissionCharge(ADDMISSION_CHARGE objAddmissionCharge)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_ADDMISSION_CHARGE_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ADMISSION_CHARGE_AMOUNT", SqlDbType.Decimal));
                cmd.Parameters["@ADMISSION_CHARGE_AMOUNT"].Value = objAddmissionCharge.ADMISSION_CHARGE_AMOUNT;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objAddmissionCharge.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdateAddmissionCharge(ADDMISSION_CHARGE objAddmissionCharge)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_ADDMISSION_CHARGE_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ADMISSION_CHARGE_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@ADMISSION_CHARGE_ID"].Value = objAddmissionCharge.ADMISSION_CHARGE_ID;
                cmd.Parameters.Add(new SqlParameter("@ADMISSION_CHARGE_AMOUNT", SqlDbType.Decimal));
                cmd.Parameters["@ADMISSION_CHARGE_AMOUNT"].Value = objAddmissionCharge.ADMISSION_CHARGE_AMOUNT;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objAddmissionCharge.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }


        public static int saveBed(BED objBed)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_BED_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@BED_NUMBER", SqlDbType.NVarChar, 20));
                cmd.Parameters["@BED_NUMBER"].Value = objBed.BED_NUMBER;
                cmd.Parameters.Add(new SqlParameter("@ROOM_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@ROOM_ID"].Value = objBed.ROOM_ID;
                cmd.Parameters.Add(new SqlParameter("@CHARGE_PER_DAY", SqlDbType.Decimal));
                cmd.Parameters["@CHARGE_PER_DAY"].Value = objBed.CHARGE_PER_DAY;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objBed.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdateBed(BED objBed)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_BED_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@BED_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@BED_ID"].Value = objBed.BED_ID;
                cmd.Parameters.Add(new SqlParameter("@BED_NUMBER", SqlDbType.NVarChar, 20));
                cmd.Parameters["@BED_NUMBER"].Value = objBed.BED_NUMBER;
                cmd.Parameters.Add(new SqlParameter("@ROOM_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@ROOM_ID"].Value = objBed.ROOM_ID;
                cmd.Parameters.Add(new SqlParameter("@CHARGE_PER_DAY", SqlDbType.Decimal));
                cmd.Parameters["@CHARGE_PER_DAY"].Value = objBed.CHARGE_PER_DAY;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objBed.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteBed(BED objBed)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_BED_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@BED_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@BED_ID"].Value = objBed.BED_ID;
                
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }


        public static int saveDisease(DISEASE objDisease)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_DISEASE_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DISEASE_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@DISEASE_NAME"].Value = objDisease.DISEASE_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objDisease.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdateDisease(DISEASE objDisease)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_DISEASE_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DISEASE_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@DISEASE_ID"].Value = objDisease.DISEASE_ID;
                cmd.Parameters.Add(new SqlParameter("@DISEASE_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@DISEASE_NAME"].Value = objDisease.DISEASE_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objDisease.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteDisease(DISEASE objDisease)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_DISEASE_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DISEASE_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@DISEASE_ID"].Value = objDisease.DISEASE_ID;
                
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int saveFloor(FLOOR objFloor)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_FLOOR_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@FLOOR_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@FLOOR_NAME"].Value = objFloor.FLOOR_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objFloor.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdateFloor(FLOOR objFloor)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_FLOOR_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@FLOOR_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@FLOOR_ID"].Value = objFloor.FLOOR_ID;
                cmd.Parameters.Add(new SqlParameter("@FLOOR_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@FLOOR_NAME"].Value = objFloor.FLOOR_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objFloor.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteFloor(FLOOR objFloor)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_FLOOR_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@FLOOR_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@FLOOR_ID"].Value = objFloor.FLOOR_ID;
             
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }


        public static int savePatientDepartment(PATIENT_DEPARTMENT objPatientDepartment)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATIENT_DEPARTMENT_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATIENT_DEPARTMENT_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@PATIENT_DEPARTMENT_NAME"].Value = objPatientDepartment.PATIENT_DEPARTMENT_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objPatientDepartment.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdatePatientDepartment(PATIENT_DEPARTMENT objPatientDepartment)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATIENT_DEPARTMENT_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATIENT_DEPARTMENT_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATIENT_DEPARTMENT_ID"].Value = objPatientDepartment.PATIENT_DEPARTMENT_ID;
                cmd.Parameters.Add(new SqlParameter("@PATIENT_DEPARTMENT_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@PATIENT_DEPARTMENT_NAME"].Value = objPatientDepartment.PATIENT_DEPARTMENT_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objPatientDepartment.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeletePatientDepartment(PATIENT_DEPARTMENT objPatientDepartment)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_PATIENT_DEPARTMENT_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PATIENT_DEPARTMENT_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@PATIENT_DEPARTMENT_ID"].Value = objPatientDepartment.PATIENT_DEPARTMENT_ID;
               
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int saveRoomCategory(ROOM_CATEGORY objRoomCategory)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_ROOM_CATEGORY_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ROOM_CATEGORY_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@ROOM_CATEGORY_NAME"].Value = objRoomCategory.ROOM_CATEGORY_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objRoomCategory.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdateRoomCategory(ROOM_CATEGORY objRoomCategory)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_ROOM_CATEGORY_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ROOM_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@ROOM_CATEGORY_ID"].Value = objRoomCategory.ROOM_CATEGORY_ID;
                cmd.Parameters.Add(new SqlParameter("@ROOM_CATEGORY_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@ROOM_CATEGORY_NAME"].Value = objRoomCategory.ROOM_CATEGORY_NAME;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objRoomCategory.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteRoomCategory(ROOM_CATEGORY objRoomCategory)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_ROOM_CATEGORY_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ROOM_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@ROOM_CATEGORY_ID"].Value = objRoomCategory.ROOM_CATEGORY_ID;
             
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int saveRoom(ROOM objRoom)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_ROOM_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ROOM_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@ROOM_CATEGORY_ID"].Value =objRoom.ROOM_CATEGORY_ID;
                cmd.Parameters.Add(new SqlParameter("@FLOOR_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@FLOOR_ID"].Value =objRoom.FLOOR_ID;
                cmd.Parameters.Add(new SqlParameter("@ROOM_NUMBER", SqlDbType.NVarChar,30));
                cmd.Parameters["@ROOM_NUMBER"].Value = objRoom.ROOM_NUMBER;
                cmd.Parameters.Add(new SqlParameter("@ROOM_PHONE", SqlDbType.NVarChar,30));
                cmd.Parameters["@ROOM_PHONE"].Value = objRoom.ROOM_PHONE;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objRoom.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdateRoom(ROOM objRoom)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_ROOM_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ROOM_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@ROOM_ID"].Value = objRoom.ROOM_ID;
                cmd.Parameters.Add(new SqlParameter("@ROOM_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@ROOM_CATEGORY_ID"].Value = objRoom.ROOM_CATEGORY_ID;
                cmd.Parameters.Add(new SqlParameter("@FLOOR_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@FLOOR_ID"].Value = objRoom.FLOOR_ID;
                cmd.Parameters.Add(new SqlParameter("@ROOM_NUMBER", SqlDbType.NVarChar, 30));
                cmd.Parameters["@ROOM_NUMBER"].Value = objRoom.ROOM_NUMBER;
                cmd.Parameters.Add(new SqlParameter("@ROOM_PHONE", SqlDbType.NVarChar, 30));
                cmd.Parameters["@ROOM_PHONE"].Value = objRoom.ROOM_PHONE;
                cmd.Parameters.Add(new SqlParameter("@STATUS_FLAG", SqlDbType.NVarChar, 30));
                cmd.Parameters["@STATUS_FLAG"].Value = objRoom.STATUS_FLAG;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteRoom(ROOM objRoom)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_ROOM_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ROOM_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@ROOM_ID"].Value = objRoom.ROOM_ID;
                conn.Open();
              
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }


        public static int saveCorporateClient(CORPORATE_CLIENT objCorporateClient)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_CORPORATE_CLIENT_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CORPORATE_CLIENT_NAME", SqlDbType.NVarChar,30));
                cmd.Parameters["@CORPORATE_CLIENT_NAME"].Value = objCorporateClient.CORPORATE_CLIENT_NAME;
                cmd.Parameters.Add(new SqlParameter("@PERSON_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@PERSON_NAME"].Value = objCorporateClient.PERSON_NAME;
                cmd.Parameters.Add(new SqlParameter("@CORPORATE_CLIENT_DESIGNATION", SqlDbType.NVarChar, 30));
                cmd.Parameters["@CORPORATE_CLIENT_DESIGNATION"].Value = objCorporateClient.CORPORATE_CLIENT_DESIGNATION;
                cmd.Parameters.Add(new SqlParameter("@CORPORATE_CLIENT_ADDRESS", SqlDbType.NVarChar, 30));
                cmd.Parameters["@CORPORATE_CLIENT_ADDRESS"].Value = objCorporateClient.CORPORATE_CLIENT_ADDRESS;
                cmd.Parameters.Add(new SqlParameter("@PERSON_ADDRESS", SqlDbType.NVarChar, 30));
                cmd.Parameters["@PERSON_ADDRESS"].Value = objCorporateClient.PERSON_ADDRESS;
                cmd.Parameters.Add(new SqlParameter("@CORPORATE_CLIENT_PHONE", SqlDbType.NVarChar, 30));
                cmd.Parameters["@CORPORATE_CLIENT_PHONE"].Value = objCorporateClient.CORPORATE_CLIENT_PHONE;
                cmd.Parameters.Add(new SqlParameter("@PERSON_PHONE", SqlDbType.NVarChar, 30));
                cmd.Parameters["@PERSON_PHONE"].Value = objCorporateClient.PERSON_PHONE;
                cmd.Parameters.Add(new SqlParameter("@CORPORATE_CLIENT_STARTING_DATE", SqlDbType.DateTime));
                cmd.Parameters["@CORPORATE_CLIENT_STARTING_DATE"].Value = objCorporateClient.CORPORATE_CLIENT_STARTING_DATE;
                cmd.Parameters.Add(new SqlParameter("@CORPORATE_CLIENT_EXPIRE_DATE", SqlDbType.DateTime));
                cmd.Parameters["@CORPORATE_CLIENT_EXPIRE_DATE"].Value = objCorporateClient.CORPORATE_CLIENT_EXPIRE_DATE;
                cmd.Parameters.Add(new SqlParameter("@CORPORATE_CLIENT_DISCOUNT", SqlDbType.Decimal));
                cmd.Parameters["@CORPORATE_CLIENT_DISCOUNT"].Value = objCorporateClient.CORPORATE_CLIENT_DISCOUNT;
                cmd.Parameters.Add(new SqlParameter("@CORPORATE_CLIENT_BALANCE", SqlDbType.Decimal));
                cmd.Parameters["@CORPORATE_CLIENT_BALANCE"].Value = objCorporateClient.CORPORATE_CLIENT_BALANCE;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdateCorporateClient(CORPORATE_CLIENT objCorporateClient)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_CORPORATE_CLIENT_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CORPORATE_CLIENT_CODE", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@CORPORATE_CLIENT_CODE"].Value = objCorporateClient.CORPORATE_CLIENT_CODE;
                cmd.Parameters.Add(new SqlParameter("@CORPORATE_CLIENT_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@CORPORATE_CLIENT_NAME"].Value = objCorporateClient.CORPORATE_CLIENT_NAME;
                cmd.Parameters.Add(new SqlParameter("@PERSON_NAME", SqlDbType.NVarChar, 30));
                cmd.Parameters["@PERSON_NAME"].Value = objCorporateClient.PERSON_NAME;
                cmd.Parameters.Add(new SqlParameter("@CORPORATE_CLIENT_DESIGNATION", SqlDbType.NVarChar, 30));
                cmd.Parameters["@CORPORATE_CLIENT_DESIGNATION"].Value = objCorporateClient.CORPORATE_CLIENT_DESIGNATION;
                cmd.Parameters.Add(new SqlParameter("@CORPORATE_CLIENT_ADDRESS", SqlDbType.NVarChar, 30));
                cmd.Parameters["@CORPORATE_CLIENT_ADDRESS"].Value = objCorporateClient.CORPORATE_CLIENT_ADDRESS;
                cmd.Parameters.Add(new SqlParameter("@PERSON_ADDRESS", SqlDbType.NVarChar, 30));
                cmd.Parameters["@PERSON_ADDRESS"].Value = objCorporateClient.PERSON_ADDRESS;
                cmd.Parameters.Add(new SqlParameter("@CORPORATE_CLIENT_PHONE", SqlDbType.NVarChar, 30));
                cmd.Parameters["@CORPORATE_CLIENT_PHONE"].Value = objCorporateClient.CORPORATE_CLIENT_PHONE;
                cmd.Parameters.Add(new SqlParameter("@PERSON_PHONE", SqlDbType.NVarChar, 30));
                cmd.Parameters["@PERSON_PHONE"].Value = objCorporateClient.PERSON_PHONE;
                cmd.Parameters.Add(new SqlParameter("@CORPORATE_CLIENT_STARTING_DATE", SqlDbType.DateTime));
                cmd.Parameters["@CORPORATE_CLIENT_STARTING_DATE"].Value = objCorporateClient.CORPORATE_CLIENT_STARTING_DATE;
                cmd.Parameters.Add(new SqlParameter("@CORPORATE_CLIENT_EXPIRE_DATE", SqlDbType.DateTime));
                cmd.Parameters["@CORPORATE_CLIENT_EXPIRE_DATE"].Value = objCorporateClient.CORPORATE_CLIENT_EXPIRE_DATE;
                cmd.Parameters.Add(new SqlParameter("@CORPORATE_CLIENT_DISCOUNT", SqlDbType.Decimal));
                cmd.Parameters["@CORPORATE_CLIENT_DISCOUNT"].Value = objCorporateClient.CORPORATE_CLIENT_DISCOUNT;
                cmd.Parameters.Add(new SqlParameter("@CORPORATE_CLIENT_BALANCE", SqlDbType.Decimal));
                cmd.Parameters["@CORPORATE_CLIENT_BALANCE"].Value = objCorporateClient.CORPORATE_CLIENT_BALANCE;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteCorporateClient(CORPORATE_CLIENT objCorporateClient)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_CORPORATE_CLIENT_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CORPORATE_CLIENT_CODE", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@CORPORATE_CLIENT_CODE"].Value = objCorporateClient.CORPORATE_CLIENT_CODE;
            
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int saveRoomBedChargeCountDown(ROOM_BED_CHARGE_COUNT_DOWN objRoomBedChargeCountDown)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_ROOM_BED_CHARGE_COUNT_DOWN_I";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@TIME_START", SqlDbType.DateTime));
                cmd.Parameters["@TIME_START"].Value = objRoomBedChargeCountDown.TIME_START;
                cmd.Parameters.Add(new SqlParameter("@TIME_END T", SqlDbType.DateTime));
                cmd.Parameters["@TIME_END "].Value = objRoomBedChargeCountDown.TIME_END;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;

            }
        }

        public static int UpdateRoomBedChargeCountDown(ROOM_BED_CHARGE_COUNT_DOWN objRoomBedChargeCountDown)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_ROOM_BED_CHARGE_COUNT_DOWN_U";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ROOM_BED_CHARGE_COUNT_DOWN_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@ROOM_BED_CHARGE_COUNT_DOWN_ID"].Value = objRoomBedChargeCountDown.ROOM_BED_CHARGE_COUNT_DOWN_ID;
                cmd.Parameters.Add(new SqlParameter("@TIME_START", SqlDbType.DateTime));
                cmd.Parameters["@TIME_START"].Value = objRoomBedChargeCountDown.TIME_START;
                cmd.Parameters.Add(new SqlParameter("@TIME_END T", SqlDbType.DateTime));
                cmd.Parameters["@TIME_END "].Value = objRoomBedChargeCountDown.TIME_END;
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }

        public static int DeleteRoomBedChargeCountDown(ROOM_BED_CHARGE_COUNT_DOWN objRoomBedChargeCountDown)
        {
            int i = 0;
            using (SqlCommand cmd = new SqlCommand())
            {
                SqlConnection conn = ConnectionClass.GetConnection();
                cmd.Connection = conn;
                cmd.CommandText = "FSP_ROOM_BED_CHARGE_COUNT_DOWN_D";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ROOM_BED_CHARGE_COUNT_DOWN_ID", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@ROOM_BED_CHARGE_COUNT_DOWN_ID"].Value = objRoomBedChargeCountDown.ROOM_BED_CHARGE_COUNT_DOWN_ID;
               
                conn.Open();
                try
                {
                    i = cmd.ExecuteNonQuery();
                }
                catch { }
                finally
                {
                    conn.Close();
                }
                return i;
            }
        }
    }
}
