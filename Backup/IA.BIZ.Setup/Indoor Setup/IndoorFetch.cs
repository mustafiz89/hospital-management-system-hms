﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IA.Model.Setup.Other.Indoor_Setup;
using System.Data.SqlClient;
using DynamicSoft.DAL;
using System.Data;

namespace IA.BIZ.Setup.Indoor_Setup
{
    public class IndoorFetch
    {
        public static List<ADDMISSION_CHARGE> getALLAdmissionCharge()
        {
            List<ADDMISSION_CHARGE> objAdmissionCharge = new List<ADDMISSION_CHARGE>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_ADDMISSION_CHARGE_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        ADDMISSION_CHARGE obj = new ADDMISSION_CHARGE();
                        obj.ADMISSION_CHARGE_ID =new Guid (dr["ADMISSION_CHARGE_ID"].ToString());
                        obj.ADMISSION_CHARGE_AMOUNT = decimal.Parse(dr["ADMISSION_CHARGE_AMOUNT"].ToString());
                        obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                        objAdmissionCharge.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objAdmissionCharge;
        }

        public static List<BED> getALLBed()
        {
            List<BED> objBed = new List<BED>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_BED_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        BED obj = new BED();
                        obj.BED_ID =new Guid(dr["BED_ID"].ToString());
                        obj.BED_NUMBER = dr["BED_NUMBER"].ToString();
                        obj.ROOM_ID =new Guid(dr["ROOM_ID"].ToString());
                        obj.CHARGE_PER_DAY = decimal.Parse(dr["CHARGE_PER_DAY"].ToString());
                        obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                        obj.ROOM_NUMBER = dr["ROOM_NUMBER"].ToString();
                        objBed.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objBed;
        }

        public static List<BED> getALLBed(Guid ROOM_CATEGORY_ID)
        {
            List<BED> objBed = new List<BED>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_BED_GK";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@ROOM_CATEGORY_ID", SqlDbType.UniqueIdentifier));
            objSqlCommand.Parameters["@ROOM_CATEGORY_ID"].Value = ROOM_CATEGORY_ID;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        BED obj = new BED();
                        obj.BED_ID = new Guid(dr["BED_ID"].ToString());
                        obj.BED_NUMBER = dr["BED_NUMBER"].ToString();
                        obj.ROOM_ID = new Guid(dr["ROOM_ID"].ToString());
                        obj.CHARGE_PER_DAY = decimal.Parse(dr["CHARGE_PER_DAY"].ToString());
                        obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                        obj.ROOM_NUMBER = dr["ROOM_NUMBER"].ToString();
                        obj.ROOM_CATEGORY_NAME = dr["ROOM_CATEGORY_NAME"].ToString();
                        objBed.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objBed;
        }

        public static List<CORPORATE_CLIENT> getALLCorporateClient()
        {
            List<CORPORATE_CLIENT> objCorporateClient = new List<CORPORATE_CLIENT>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_CORPORATE_CLIENT_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        CORPORATE_CLIENT obj = new CORPORATE_CLIENT();
                        obj.CORPORATE_CLIENT_CODE =new Guid (dr["CORPORATE_CLIENT_CODE"].ToString());
                        obj.CORPORATE_CLIENT_NAME = dr["CORPORATE_CLIENT_NAME"].ToString();
                        obj.PERSON_NAME = dr["PERSON_NAME"].ToString();
                        obj.CORPORATE_CLIENT_DESIGNATION = dr["CORPORATE_CLIENT_DESIGNATION"].ToString();
                        obj.CORPORATE_CLIENT_ADDRESS = dr["CORPORATE_CLIENT_ADDRESS"].ToString();
                        obj.PERSON_ADDRESS = dr["PERSON_ADDRESS"].ToString();
                        obj.CORPORATE_CLIENT_PHONE = dr["CORPORATE_CLIENT_PHONE"].ToString();
                        obj.PERSON_PHONE = dr["PERSON_PHONE"].ToString();
                        obj.CORPORATE_CLIENT_STARTING_DATE = DateTime.Parse(dr["CORPORATE_CLIENT_STARTING_DATE"].ToString());
                        obj.CORPORATE_CLIENT_EXPIRE_DATE = DateTime.Parse(dr["CORPORATE_CLIENT_EXPIRE_DATE"].ToString());
                        obj.CORPORATE_CLIENT_DISCOUNT = decimal.Parse(dr["CORPORATE_CLIENT_DISCOUNT"].ToString());
                        obj.CORPORATE_CLIENT_BALANCE = decimal.Parse(dr["CORPORATE_CLIENT_BALANCE"].ToString());
                        objCorporateClient.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objCorporateClient;
        }

        public static List<DISEASE> getALLDisease()
        {
            List<DISEASE> objDisease = new List<DISEASE>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_DISEASE_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        DISEASE obj = new DISEASE();
                        obj.DISEASE_ID =new Guid (dr["DISEASE_ID"].ToString());
                        obj.DISEASE_NAME = dr["DISEASE_NAME"].ToString();
                        obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                        objDisease.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objDisease;
        }

        public static List<FLOOR> getALLFLOOR()
        {
            List<FLOOR> objFloor = new List<FLOOR>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_FLOOR_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        FLOOR obj = new FLOOR();
                        obj.FLOOR_ID =new Guid (dr["FLOOR_ID"].ToString());
                        obj.FLOOR_NAME = dr["FLOOR_NAME"].ToString();
                        obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                        objFloor.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objFloor;
        }

        public static List<PATIENT_DEPARTMENT> getALLPatientDepartment()
        {
            List<PATIENT_DEPARTMENT> objPatientDepartment = new List<PATIENT_DEPARTMENT>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_PATIENT_DEPARTMENT_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        PATIENT_DEPARTMENT obj = new PATIENT_DEPARTMENT();
                        obj.PATIENT_DEPARTMENT_ID =new Guid (dr["PATIENT_DEPARTMENT_ID"].ToString());
                        obj.PATIENT_DEPARTMENT_NAME = dr["PATIENT_DEPARTMENT_NAME"].ToString();
                        obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                        objPatientDepartment.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objPatientDepartment;
        }

        public static List<ROOM_CATEGORY> getALLRoomCategory()
        {
            List<ROOM_CATEGORY> objRoomCategory = new List<ROOM_CATEGORY>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_ROOM_CATEGORY_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        ROOM_CATEGORY obj = new ROOM_CATEGORY();
                        obj.ROOM_CATEGORY_ID =new Guid(dr["ROOM_CATEGORY_ID"].ToString());
                        obj.ROOM_CATEGORY_NAME = dr["ROOM_CATEGORY_NAME"].ToString();
                        obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                        objRoomCategory.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objRoomCategory;
        }

        public static List<ROOM> getALLRoom()
        {
            List<ROOM> objRoom = new List<ROOM>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_ROOM_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        ROOM obj = new ROOM();
                        obj.ROOM_ID =new Guid (dr["ROOM_ID"].ToString());
                        obj.ROOM_CATEGORY_ID =new Guid (dr["ROOM_CATEGORY_ID"].ToString());
                        obj.FLOOR_ID =new Guid (dr["FLOOR_ID"].ToString());
                        obj.ROOM_NUMBER = dr["ROOM_NUMBER"].ToString();
                        obj.ROOM_PHONE = dr["ROOM_PHONE"].ToString();
                        obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                        obj.ROOM_CATEGORY_NAME = dr["ROOM_CATEGORY_NAME"].ToString();
                        obj.FLOOR_NAME = dr["FLOOR_NAME"].ToString();
                        objRoom.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objRoom;
        }

        public static List<ROOM> getALLRoom(string ROOM_CATEGORY_ID)
        {

            List<ROOM> objRoom = new List<ROOM>();
            SqlConnection conn = ConnectionClass.GetConnection();
            try
            {

                SqlCommand objSqlCommand = new SqlCommand();
                objSqlCommand.CommandText = "FSP_ROOM_GK";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add(new SqlParameter("@ROOM_CATEGORY_ID", SqlDbType.UniqueIdentifier));
                objSqlCommand.Parameters["@ROOM_CATEGORY_ID"].Value = new Guid(ROOM_CATEGORY_ID);
                objSqlCommand.Connection = conn;
                conn.Open();

                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        ROOM obj = new ROOM();
                        obj.ROOM_ID =new Guid (dr["ROOM_ID"].ToString());
                        obj.ROOM_CATEGORY_ID =new Guid (dr["ROOM_CATEGORY_ID"].ToString());
                        obj.FLOOR_ID =new Guid(dr["FLOOR_ID"].ToString());
                        obj.ROOM_NUMBER = dr["ROOM_NUMBER"].ToString();
                        obj.ROOM_PHONE = dr["ROOM_PHONE"].ToString();
                        obj.STATUS_FLAG = dr["STATUS_FLAG"].ToString();
                        objRoom.Add(obj);
                    }
                }
            }
            catch { }
            finally
            {
                conn.Close();
            }
            return objRoom;
        }
    }
}
