﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;


namespace IA.Provider
{
    public class ConnectionClass
    {
        public static SqlConnection GetConnection()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["application"].ConnectionString);
            return conn;
        }
        public static SqlConnection GetConnectionMaster()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["applicationMaster"].ConnectionString);
            return conn;
        }
        
    }
}

