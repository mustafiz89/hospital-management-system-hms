﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DynamicSoft.Security
{
    public static class DASSessionInfoClass
    {
        public static string USER_ID { get; set; }
        public static string COMPANY_ID { get; set; }
        public static string BRANCH_ID { get; set; }
        public static string COMPANY_NAME { get; set; }
        public static string BRANCH_NAME { get; set; }
        public static string ADDRESS { get; set; }
        public static int HO_FLAG { get; set; }
        public static int WARE_HOUSE_FLAG { get; set; }

        public static int MIDICINE_MODULE_FLAG { get; set; }
        public static int PRODUCTION_MODULE_FLAG { get; set; }
        public static int WAREHOUSE_MODULE_FLAG { get; set; }
        
    }
}
