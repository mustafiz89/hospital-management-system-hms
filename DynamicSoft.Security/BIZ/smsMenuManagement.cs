﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DynamicSoft.Security.MODEL;
using DynamicSoft.DAL;
using System.Data.SqlClient;
using System.Data;

namespace DynamicSoft.Security.BIZ
{
    public class smsMenuManagement
    {

        public static List<SM_MENU> getAllSM_MENU()
        {
            List<SM_MENU> objListDS_MENU_FUNCTION = new List<SM_MENU>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_SM_MENU_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        SM_MENU objDS_MENU_FUNCTION = new SM_MENU();
                        objDS_MENU_FUNCTION.MENU_ID = dr["MENU_ID"].ToString();
                        objDS_MENU_FUNCTION.MENU_NAME_PARENT = dr["MENU_NAME_PARENT"].ToString();
                        objDS_MENU_FUNCTION.MENU_NAME = dr["MENU_NAME"].ToString();
                        objDS_MENU_FUNCTION.MENU_BACK_NAME = dr["MENU_BACK_NAME"].ToString();
                        objDS_MENU_FUNCTION.MENU_PARENT_ID = dr["MENU_PARENT_ID"].ToString();
                        objDS_MENU_FUNCTION.TOP_FLAG = Convert.ToInt32(dr["TOP_FLAG"].ToString());                      
                        objListDS_MENU_FUNCTION.Add(objDS_MENU_FUNCTION);
                    }
                }
            }
            catch { }
            finally { conn.Close(); }
            return objListDS_MENU_FUNCTION;
        }

      
        public static List<DS_MENU_FUNCTION> getAllMenuList()
        {
            List<DS_MENU_FUNCTION> objListDS_MENU_FUNCTION = new List<DS_MENU_FUNCTION>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_DS_MENU_FUNCTION_GA";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        DS_MENU_FUNCTION objDS_MENU_FUNCTION = new DS_MENU_FUNCTION();
                        objDS_MENU_FUNCTION.SERVICE_NM = dr["SERVICE_NM"].ToString();
                        objDS_MENU_FUNCTION.MODULE_NM = dr["MODULE_NM"].ToString();
                        objDS_MENU_FUNCTION.FUNCTION_NM = dr["FUNCTION_NM"].ToString();
                        objDS_MENU_FUNCTION.TARGET_PATH = dr["TARGET_PATH"].ToString();
                        objDS_MENU_FUNCTION.MENU_NM = dr["MENU_NM"].ToString();
                        objDS_MENU_FUNCTION.MENU_ID = Convert.ToInt32(dr["MENU_ID"].ToString());
                        if (!string.IsNullOrEmpty(dr["PARENT_MENU_ID"].ToString()))
                            objDS_MENU_FUNCTION.PARENT_MENU_ID = Convert.ToInt32(dr["PARENT_MENU_ID"]);
                        objDS_MENU_FUNCTION.MENU_LEVEL = Convert.ToInt32(dr["MENU_LEVEL"].ToString());
                        objDS_MENU_FUNCTION.ITEM_TYPE = dr["ITEM_TYPE"].ToString();
                        objDS_MENU_FUNCTION.FUNCTION_ID = dr["FUNCTION_ID"].ToString();
                        objDS_MENU_FUNCTION.FAST_PATH_NO = dr["FAST_PATH_NO"].ToString();
                        objDS_MENU_FUNCTION.FUNCTION_ASSIGN_FLAG = Convert.ToInt32( dr["FUNCTION_ASSIGN_FLAG"]);
                        objListDS_MENU_FUNCTION.Add(objDS_MENU_FUNCTION);
                    }
                }
            }
            catch { }
            finally { conn.Close(); }
            return objListDS_MENU_FUNCTION;
        }

        public static List<DS_MENU_FUNCTION> getHmsMenuList(string user_id, string NAME_VALUE_LIST)
        {
            List<DS_MENU_FUNCTION> objListDS_MENU_FUNCTION = new List<DS_MENU_FUNCTION>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_DS_MENU_FUNCTION_LIST";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@USER_ID", SqlDbType.NVarChar, 20));
            objSqlCommand.Parameters["@USER_ID"].Value = user_id;
            objSqlCommand.Parameters.Add(new SqlParameter("@NAME_VALUE_LIST", SqlDbType.Char));
            objSqlCommand.Parameters["@NAME_VALUE_LIST"].Value = NAME_VALUE_LIST; //1 FOR GET DATA AND 0 FOR DELETE TEMP
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        DS_MENU_FUNCTION objDS_MENU_FUNCTION = new DS_MENU_FUNCTION();
                        objDS_MENU_FUNCTION.SERVICE_NM = dr["SERVICE_NM"].ToString();
                        objDS_MENU_FUNCTION.MODULE_NM = dr["MODULE_NM"].ToString();
                        objDS_MENU_FUNCTION.FUNCTION_NM = dr["FUNCTION_NM"].ToString();
                        objDS_MENU_FUNCTION.TARGET_PATH = dr["TARGET_PATH"].ToString();
                        objDS_MENU_FUNCTION.MENU_NM = dr["MENU_NM"].ToString();
                        objDS_MENU_FUNCTION.MENU_ID = Convert.ToInt32(dr["MENU_ID"].ToString());
                        if (!string.IsNullOrEmpty(dr["PARENT_MENU_ID"].ToString()))
                            objDS_MENU_FUNCTION.PARENT_MENU_ID = Convert.ToInt32(dr["PARENT_MENU_ID"]); 
                        //getParentMenuList(dr["PARENT_MENU_ID"].ToString(), objListDS_MENU_FUNCTION, objSqlCommand, conn);
                        objDS_MENU_FUNCTION.MENU_LEVEL = Convert.ToInt32(dr["MENU_LEVEL"].ToString());
                        objDS_MENU_FUNCTION.ITEM_TYPE = dr["ITEM_TYPE"].ToString();
                        objDS_MENU_FUNCTION.FUNCTION_ID = dr["FUNCTION_ID"].ToString();
                        objDS_MENU_FUNCTION.FAST_PATH_NO = dr["FAST_PATH_NO"].ToString();
                        objDS_MENU_FUNCTION.FUNCTION_ASSIGN_FLAG = Convert.ToInt32(dr["FUNCTION_ASSIGN_FLAG"]);
                        objListDS_MENU_FUNCTION.Add(objDS_MENU_FUNCTION);
                    }
                }
            }
            catch { }
            finally { conn.Close(); }
            return objListDS_MENU_FUNCTION;
        }

        //public static List<string> getHmsMenuListParentCount(string user_id, List<DS_MENU_FUNCTION> objListDS_MENU_FUNCTION)
        //{
        //    List<string> objListPARENT_MENU_ID = new List<string>();
        //    SqlConnection conn = ConnectionClass.GetConnection();
        //    SqlCommand objSqlCommand = new SqlCommand();
        //    objSqlCommand.CommandText = "FSP_GET_USER_ASSIGN_FUNCTION";
        //    objSqlCommand.CommandType = CommandType.StoredProcedure;
        //    objSqlCommand.Parameters.Add(new SqlParameter("@USER_ID", SqlDbType.NVarChar, 20));
        //    objSqlCommand.Parameters["@USER_ID"].Value = user_id;
        //    objSqlCommand.Parameters.Add(new SqlParameter("@NAME_VALUE_LIST", SqlDbType.Char));
        //    objSqlCommand.Parameters["@NAME_VALUE_LIST"].Value = "2";
        //    objSqlCommand.Connection = conn;
        //    conn.Open();
        //    try
        //    {
        //        using (objSqlCommand)
        //        {
        //            SqlDataReader dr = objSqlCommand.ExecuteReader();
        //            while (dr.Read())
        //            {
        //                string objDS_MENU_FUNCTION = dr["PARENT_MENU_ID"].ToString();
        //                objListPARENT_MENU_ID.Add(objDS_MENU_FUNCTION);
        //            }
        //        }
        //    }
        //    catch { }
        //    finally { conn.Close(); }
        //    foreach (string a in objListPARENT_MENU_ID)
        //    {
        //        getParentMenuList(a, objListDS_MENU_FUNCTION);
        //    }
        //    return objListPARENT_MENU_ID;
        //}

        //public static List<DS_MENU_FUNCTION> getParentMenuList(string parent_id, List<DS_MENU_FUNCTION> objListDS_MENU_FUNCTION)//, 
        //    //List<DS_MENU_FUNCTION> objListDS_MENU_FUNCTION, SqlCommand objSqlCommand, SqlConnection conn)
        //{
        //    //List<DS_MENU_FUNCTION> objListDS_MENU_FUNCTION = new List<DS_MENU_FUNCTION>();
        //    SqlConnection conn = ConnectionClass.GetConnection();
        //    SqlCommand objSqlCommand = new SqlCommand();
        //    objSqlCommand.CommandText = "FSP_GET_PARENT_MENU";
        //    objSqlCommand.CommandType = CommandType.StoredProcedure;
        //    objSqlCommand.Parameters.Add(new SqlParameter("@PARENT_MENU_ID", SqlDbType.NVarChar, 20));
        //    objSqlCommand.Parameters["@PARENT_MENU_ID"].Value = parent_id;
        //    objSqlCommand.Connection = conn;
        //    objSqlCommand.Connection = conn;
        //    try
        //    {
        //        using (objSqlCommand)
        //        {
        //            SqlDataReader dr = objSqlCommand.ExecuteReader();
        //            while (dr.Read())
        //            {
        //                DS_MENU_FUNCTION objDS_MENU_FUNCTION = new DS_MENU_FUNCTION();
        //                objDS_MENU_FUNCTION.SERVICE_NM = dr["SERVICE_NM"].ToString();
        //                objDS_MENU_FUNCTION.MODULE_NM = dr["MODULE_NM"].ToString();
        //                objDS_MENU_FUNCTION.FUNCTION_NM = dr["FUNCTION_NM"].ToString();
        //                objDS_MENU_FUNCTION.TARGET_PATH = dr["TARGET_PATH"].ToString();
        //                objDS_MENU_FUNCTION.MENU_NM = dr["MENU_NM"].ToString();
        //                objDS_MENU_FUNCTION.MENU_ID = Convert.ToInt32(dr["MENU_ID"].ToString());
        //                if (!string.IsNullOrEmpty(dr["PARENT_MENU_ID"].ToString()))
        //                    objDS_MENU_FUNCTION.PARENT_MENU_ID = Convert.ToInt32(dr["PARENT_MENU_ID"]);
        //                //getParentMenuList(dr["PARENT_MENU_ID"].ToString(), objListDS_MENU_FUNCTION, objSqlCommand, conn);
        //                objDS_MENU_FUNCTION.MENU_LEVEL = Convert.ToInt32(dr["MENU_LEVEL"].ToString());
        //                objDS_MENU_FUNCTION.ITEM_TYPE = dr["ITEM_TYPE"].ToString();
        //                objDS_MENU_FUNCTION.FUNCTION_ID = dr["FUNCTION_ID"].ToString();
        //                objDS_MENU_FUNCTION.FAST_PATH_NO = dr["FAST_PATH_NO"].ToString();
        //                objDS_MENU_FUNCTION.FUNCTION_ASSIGN_FLAG = Convert.ToInt32(dr["FUNCTION_ASSIGN_FLAG"]);
        //                objListDS_MENU_FUNCTION.Add(objDS_MENU_FUNCTION);
        //            }
        //        }
        //    }
        //    catch { }
        //    finally { conn.Close(); }
        //    return objListDS_MENU_FUNCTION;
        //}


        public static List<SM_MENU_FUNCTION> get_SM_AllMenuListFunctionWise(string FUNCTION_ID, string USER_ID)
        {
            List<SM_MENU_FUNCTION> objListDS_MENU_FUNCTION = new List<SM_MENU_FUNCTION>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_SM_MENU_ASSIGN_OR_SM_MENU";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@FUNCTION_ID", SqlDbType.Int));
            objSqlCommand.Parameters["@FUNCTION_ID"].Value = FUNCTION_ID;
            objSqlCommand.Parameters.Add(new SqlParameter("@USER_ID", SqlDbType.NVarChar, 30));
            objSqlCommand.Parameters["@USER_ID"].Value = USER_ID; //1 FOR GET DATA AND 0 FOR DELETE TEMP
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        SM_MENU_FUNCTION objDS_MENU_FUNCTION = new SM_MENU_FUNCTION();
                        objDS_MENU_FUNCTION.FUNCTION_NAME = dr["FUNCTION_NAME"].ToString();
                        objDS_MENU_FUNCTION.FUNCTION_BACK_NAME = dr["FUNCTION_BACK_NAME"].ToString();
                        objDS_MENU_FUNCTION.MENU_ID = Convert.ToInt32(dr["MENU_ID"]);
                        objDS_MENU_FUNCTION.MENU_NAME = dr["MENU_NAME"].ToString();
                        objDS_MENU_FUNCTION.MENU_BACK_NAME = dr["MENU_BACK_NAME"].ToString();
                        objDS_MENU_FUNCTION.HAS_CHIELD = Convert.ToInt32(dr["HAS_CHIELD"].ToString());
                        objDS_MENU_FUNCTION.USER_ID = dr["USER_ID"].ToString();
                        objDS_MENU_FUNCTION.ASSIGN_VALUE = Convert.ToBoolean(dr["ASSIGN_VALUE"]);
                        objListDS_MENU_FUNCTION.Add(objDS_MENU_FUNCTION);
                    }
                }
            }
            catch { }
            finally { conn.Close(); }
            return objListDS_MENU_FUNCTION;
        }

        public static List<SM_MENU_FUNCTION> get_SM_AllMenuListUserWise( string USER_ID)
        {
            List<SM_MENU_FUNCTION> objListDS_MENU_FUNCTION = new List<SM_MENU_FUNCTION>();
            SqlConnection conn = ConnectionClass.GetConnection();
            SqlCommand objSqlCommand = new SqlCommand();
            objSqlCommand.CommandText = "FSP_SM_MENU_GET_USER_WISE";
            objSqlCommand.CommandType = CommandType.StoredProcedure;
            objSqlCommand.Parameters.Add(new SqlParameter("@USER_ID", SqlDbType.NVarChar, 30));
            objSqlCommand.Parameters["@USER_ID"].Value = USER_ID; //1 FOR GET DATA AND 0 FOR DELETE TEMP
            objSqlCommand.Connection = conn;
            conn.Open();
            try
            {
                using (objSqlCommand)
                {
                    SqlDataReader dr = objSqlCommand.ExecuteReader();
                    while (dr.Read())
                    {
                        SM_MENU_FUNCTION objDS_MENU_FUNCTION = new SM_MENU_FUNCTION();
                        objDS_MENU_FUNCTION.FUNCTION_NAME = dr["FUNCTION_NAME"].ToString();
                        objDS_MENU_FUNCTION.FUNCTION_BACK_NAME = dr["FUNCTION_BACK_NAME"].ToString();
                        objDS_MENU_FUNCTION.MENU_ID = Convert.ToInt32(dr["MENU_ID"]);
                        objDS_MENU_FUNCTION.MENU_NAME = dr["MENU_NAME"].ToString();
                        objDS_MENU_FUNCTION.MENU_BACK_NAME = dr["MENU_BACK_NAME"].ToString();
                        objDS_MENU_FUNCTION.ASSIGN_VALUE = Convert.ToBoolean(dr["ASSIGN_VALUE"]);
                        objListDS_MENU_FUNCTION.Add(objDS_MENU_FUNCTION);
                    }
                }
            }
            catch { }
            finally { conn.Close(); }
            return objListDS_MENU_FUNCTION;
        }

    }
}
